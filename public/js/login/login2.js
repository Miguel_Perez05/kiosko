var login = {
    login_in:function(e){
        var usuario = $("#usuario").val();
        var password = $("#contrasena").val();
        var token = $("#csrf-token").val();

        if (usuario == "") {
            alert("El Nick Name no puede ir vacio");
            $("#usuario").css("border","1px solid red");
        }else if (password == "") {
            alert("Debes ingresar la contraseña");
            $("#contrasena").css("border","1px solid red");
        }else{
            var data={
                '_token':token,
                'Usuario_NickName':usuario,
                'Usuario_Password':password
            };

            $.ajax({
                url: main_path+"/loginpost",
                type:"POST",
                data:data,
                datatype:"json",
                    success:function(json){
                        if (json.status) {
                            window.location.href=main_path+"/login";
                        }
                    },
                    error:function(json){
                    }
            });
        }
    },
}

$(document).ready(function() {
    $('.form-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).removeClass('visible');
        $('.form-panel.one').removeClass('hidden');
        $('.form').animate({
        'height': panelOne
        }, 200);
    });
});

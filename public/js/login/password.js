var login = {
    login_in:function(e){
        var Uid_Usuario = $("#Uid_Usuario").val();
        var Usuario_Password = $("#Usuario_Password").val();
        var Usuario_Password_ = $("#Usuario_Password_").val();
        var token = $("#csrf-token").val();

        if (Usuario_Password == "") {
            alert("Debes ingresar la contraseña");
        }
        else if (Usuario_Password != Usuario_Password_) {
            alert("Las contraseñas deben coincidir");
        }else{
            var data={
                '_token':token,
                'Uid_Usuario':Uid_Usuario,
                'Usuario_Password':Usuario_Password,
                'Usuario_Password_':Usuario_Password_
            };

            $.ajax({
                url: main_path+"/updatepassword",
                type:"POST",
                data:data,
                datatype:"json",
                success:function(json){
                    window.location.href=main_path+"/login";
                    if (json.status) {

                    }
                },
                error:function(json){
                }
            });
        }
    },
}

$(document).ready(function() {
    $('.form-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).removeClass('visible');
        $('.form-panel.one').removeClass('hidden');
        $('.form').animate({
        'height': panelOne
        }, 200);
    });
});

function CrearEscolaridad()
{
    var errors=validator();
    if(errors!="")
    {
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var registro =new Object();
    registro.Uid_Empleado=$("#Uid_Empleado").val();

    registro.Uid_Carrera=$("#Uid_Carrera").val();
    registro.Uid_Escuela=$("#Uid_Escuela").val();
    registro.Escolaridad_AnoIngreso=$("#Escolaridad_AnoIngreso").val();
    registro.Escolaridad_AnoEgreso=$("#Escolaridad_AnoEgreso").val();
    registro.Escolaridad_Concluida=document.getElementById("Escolaridad_Concluida").checked==true ? 1 :0;
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/escolaridades',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#task-add-carrera").modal("hide");
            LoadEstudios();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/escolaridades/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function validator()
{
    var Message="";

    if($("#Uid_Carrera").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado la Carrera</li>";
        $(`#Uid_Carrera`).css("border", "2px solid red");
    }
    if($("#Uid_Escuela").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado la Institución Educativa</li>";
        $(`#Uid_Escuela`).css("border", "2px solid red");
    }
    return Message;
}

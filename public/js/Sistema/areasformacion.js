var table;
$('document').ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": "AreaFormacion_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Edit</a>
                            </div>
                        </div>`;
                }
            }
        ],
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_AreaFormacion)
    });

    LoadAreas();
});

function LoadAreas()
{
    $.ajax({
        url:`${main_path}/areasformacion/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_AreaFormacion)
{
    $.ajax({
        url:main_path+'/areasformacion/'+Uid_AreaFormacion+'/edit',
        type:'get',
        datatype:'json',
        success:function(response){
            $("#AreaFormacion_Nombre").val(response.AreaFormacion_Nombre);
            $("#Uid_AreaFormacion").val(response.Uid_AreaFormacion);
            $("#img").attr('src', response.Logo);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Area de Formación';
            $("#modal").modal("show");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function nuevo(){
    $("#AreaFormacion_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nueva Area de Formación';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var registro =new Object();
    registro.AreaFormacion_Nombre=$("#AreaFormacion_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/areasformacion',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadAreas();
            $("#modal").modal("hide");
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    registro.AreaFormacion_Nombre=$("#AreaFormacion_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/areasformacion/${$("#Uid_AreaFormacion").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadAreas();
            $("#modal").modal("hide");
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

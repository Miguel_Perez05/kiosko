var table;
$('document').ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": 'Empleado_Nombre',name:'e.Empleado_Nombre'},
            {"data": 'Empleado_APaterno',name:'e.Empleado_APaterno'},
            {"data": 'Empleado_AMaterno',name:'e.Empleado_AMaterno'},
            {"data": "Usuario_NickName",name:'u.Usuario_NickName'},
            {"data": "Usuario_Correo",name:'u.Usuario_Correo'},
            {"data": "Usuario_Perfil",name:'p.Perfil_Nombre'}
        ],
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 0, 'asc' ]],
        "lengthMenu": [[10,25, 50, -1], [10,25, 50, "All"]],
        "processing": true,
        "serverSide": true,
        "ajax": `${main_path}/usuarios/0`,
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });
});

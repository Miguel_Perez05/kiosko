var table;
$('document').ready(function(){
    
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoIncidencia_Nombre"},
            {"data": "TipoIncidencia_DeduccionPercepcion",render:function (data) {
                    if(data==2)
                        return "Deducción";
                    else if(data==1)
                        return "Percepción";
                }
            },
            {"data": "Id_Estatus",render:function (data) {
                    if(data==0)
                        return "Inactiva";
                    else if(data==1)
                    return "Activa";
                }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_TipoIncidencia)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });

    $('#table tbody').on( 'click', '.Activate', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,1,"Activar");
    });

    LoadTiposIncidencias();
});

function LoadTiposIncidencias()
{
    $.ajax({
        url:`${main_path}/tipoincidencias/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Id_TipoIncidencia){
    $.ajax({
        url:`${main_path}/tipoincidencias/${Id_TipoIncidencia}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#TipoIncidencia_Nombre").val(response.TipoIncidencia_Nombre);
            $("#TipoIncidencia_DeduccionPercepcion").val(response.TipoIncidencia_DeduccionPercepcion);
            $("#Id_TipoIncidencia").val(response.Id_TipoIncidencia);
            if(response.TipoIncidencia_ManejaTiempo==1)
                document.getElementById("TipoIncidencia_ManejaTiempo").checked = true;
            else
                document.getElementById("TipoIncidencia_ManejaTiempo").checked = false;
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Comentarios de Incidencia';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#TipoIncidencia_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Tipo Incidencia';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var register= new Object();
    register.TipoIncidencia_Nombre=$("#TipoIncidencia_Nombre").val();
    register.TipoIncidencia_DeduccionPercepcion=$("#TipoIncidencia_DeduccionPercepcion").val();
    register.TipoIncidencia_ManejaTiempo=0;
    if(document.getElementById("TipoIncidencia_ManejaTiempo").checked == true)
    {
        register.TipoIncidencia_ManejaTiempo=1;
    }
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tipoincidencias`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadTiposIncidencias();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register= new Object();
    register.TipoIncidencia_Nombre=$("#TipoIncidencia_Nombre").val();
    register.TipoIncidencia_DeduccionPercepcion=$("#TipoIncidencia_DeduccionPercepcion").val();
    register.TipoIncidencia_ManejaTiempo=0;

    if(document.getElementById("TipoIncidencia_ManejaTiempo").checked == true)
    {
        TipoIncidencia_ManejaTiempo="1";
    }
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/tipoincidencias/${$("#Id_TipoIncidencia").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadTiposIncidencias();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Tipo Incidencia`,
        text: `Esta seguro de ${labelStatus} el Tipo Incidencia?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);            
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tipoincidencias/${data.Id_TipoIncidencia}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTiposIncidencias();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}


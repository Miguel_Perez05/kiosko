var table;
$(document).ready(function(){
    table =$('#table').DataTable({
        "columns":[            
            {"data": "Idioma_Nombre"}, 
            {"data": "Id_Estatus",render:function (data) {
                    if(data==1)
                        return "Activo";
                    else
                        return "Desactivado";
                }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;    
                }                                                
            }      
        ],            
        // dom: 'Bfrtip',
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    }); 
    
    LoadIdiomas();

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Idioma);
    });
  
    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });
  
    $('#table tbody').on( 'click', '.Activate', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,1,"Activar");
    });

});

function LoadIdiomas()
{
    $.ajax({
        url:`${main_path}/idiomas/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Idioma){
    $.ajax({
        url:`${main_path}/idiomas/${Uid_Idioma}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            console.log(response);
            $("#Idioma_Nombre").val(response.Idioma_Nombre);
            $("#Uid_Idioma").val(response.Uid_Idioma);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Idioma';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#Idioma_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Idioma';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var registro =new Object();
    registro.Idioma_Nombre=$("#Idioma_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/idiomas`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadIdiomas();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register =new Object();
    register.Idioma_Nombre=$("#Idioma_Nombre").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/idiomas/${$("#Uid_Idioma").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadIdiomas();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Idioma`,
        text: `Esta seguro de ${labelStatus} el Idioma?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);            
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/idiomas/${data.Uid_Idioma}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadIdiomas();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

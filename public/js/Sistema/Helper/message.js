function success(response, redirectUrl="")
{
    var html=response.html ? response.html : false;
    swal({
        html:html,
        title: response.Accion,
        text: response.message,
        type: response.status,
        showCancelButton: false,
        closeOnConfirm: true,
        showLoaderOnConfirm: true
    },
    function (isConfirm)
    {
        // if(i)
        if(response.status=='success')
        {
            if(response.logout=='1')
            {
                window.location.href = main_path+'/logout';
            }
            if(redirectUrl!="")
                window.location.href =redirectUrl;
        }
    });
}

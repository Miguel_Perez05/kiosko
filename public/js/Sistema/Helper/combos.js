function LoadComboAreasFormacion() //Carga el select de areas de formación
{
    $.ajax({
        url:`${main_path}/areasformacion/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_AreaFormacion").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_AreaFormacion).html(response[key].AreaFormacion_Nombre));
            });
            $("#Uid_AreaFormacion").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboAreasFormacionC() //Carga el select de areas de formación
{
    $.ajax({
        url:`${main_path}/areasformacion/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_AreaFormacion_C").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_AreaFormacion).html(response[key].AreaFormacion_Nombre));
            });
            $("#Uid_AreaFormacion_C").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboNivelEducativo() //Carga el select de niveles educativos
{
    $.ajax({
        url:`${main_path}/niveleseducativos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_NivelEducativo").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_NivelEducativo).html(response[key].NivelEducativo_Nombre));
            });
            $("#Uid_NivelEducativo").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboDepartamentos() //Carga el select de departamentos
{
    $.ajax({
        url:`${main_path}/departamentos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Departamento").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Departamento).html(response[key].Departamento_Nombre));
            });
            $("#Uid_Departamento").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboGrupos() //Carga el select de grupos
{
    $.ajax({
        url:`${main_path}/grupos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Grupo").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Grupo).html(response[key].Grupo_Nombre));
            });
            $("#Uid_Grupo").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTurnos() //Carga el select de turnos
{
    $.ajax({
        url:`${main_path}/turnos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Turno").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Turno).html(response[key].Turno_Nombre));
            });
            $("#Uid_Turno").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEmpleados() //Carga el select de empleados
{
    $.ajax({
        url:`${main_path}/empleados/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Empleado").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Empleado).html(response[key].Empleado_Nombre));
            });
            $("#Uid_Empleado").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboPaises(Id_Pais=0) //Carga el select de paises
{
    $.ajax({
        url:`${main_path}/paises/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_Pais").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_Pais).html(response[key].Pais_Nombre));
            });
            $("#Id_Pais").append(myselect.html());
            if(Id_Pais>0)
                $("#Id_Pais").val(Id_Pais);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEstados(Id_Pais, Id_Estado=0) //Carga el select de estados
{
    $.ajax({
        url:`${main_path}/estados/${Id_Pais}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_Estado").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_Estado).html(response[key].Estado_Nombre));
            });
            $("#Id_Estado").append(myselect.html());
            if(Id_Estado>0)
                $("#Id_Estado").val(Id_Estado);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboCiudades(Id_Pais, Id_Estado, Id_Ciudad=0) //Carga el select de estados
{
    $.ajax({
        url:`${main_path}/ciudades/0?Id_Pais=${Id_Pais}&Id_Estado=${Id_Estado}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_Ciudad").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_Ciudad).html(response[key].Ciudad_Nombre));
            });
            $("#Id_Ciudad").append(myselect.html());
            if(Id_Ciudad>0)
                $("#Id_Ciudad").val(Id_Ciudad);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboPaisesC() //Carga el select de paises
{
    $.ajax({
        url:`${main_path}/paises/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_Pais_C").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_Pais).html(response[key].Pais_Nombre));
            });
            $("#Id_Pais_C").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEstadosC(Id_Pais) //Carga el select de estados
{
    $.ajax({
        url:`${main_path}/estados/${Id_Pais}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_Estado_C").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_Estado).html(response[key].Estado_Nombre));
            });
            $("#Id_Estado_C").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEmpleados() //Carga el select de empleados
{
    $.ajax({
        url:`${main_path}/employeescombo`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Empleado").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Empleado).html(`${response[key].Empleado_Nombre} ${response[key].Empleado_APaterno} ${response[key].Empleado_AMaterno}`));
            });
            $("#Uid_Empleado").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEstadosCiviles() //Carga el select de estados civiles
{
    $.ajax({
        url:`${main_path}/estadosciviles/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_EstadoCivil").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_EstadoCivil).html(response[key].EstadoCivil_Nombre));
            });
            $("#Uid_EstadoCivil").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboNacionalidades() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/nacionalidades/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Nacionalidad").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Nacionalidad).html(response[key].Nacionalidad_Nombre));
            });
            $("#Uid_Nacionalidad").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboRegistrosPatronales() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/registrospatronales/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_RegistroPatronal").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_RegistroPatronal).html(response[key].RegistroPatronal_Nombre));
            });
            $("#Uid_RegistroPatronal").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEmpresas(Uid_Empresa='') //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/empresas/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Empresa").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Empresa).html(response[key].Empresa_Nombre));
            });
            $("#Uid_Empresa").append(myselect.html());
            if(Uid_Empresa!="")
                $("#Uid_Empresa").val(Uid_Empresa);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboCarreras() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/carreras/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Carrera").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Carrera).html(response[key].Carrera_Nombre));
            });
            $("#Uid_Carrera").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboEscuelas() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/escuelas/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Escuela").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Escuela).html(response[key].Escuela_Nombre));
            });
            $("#Uid_Escuela").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboPuestos(estatus=1) //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/puestos/${estatus}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Puesto").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Puesto).html(response[key].Puesto_Nombre));
            });
            $("#Uid_Puesto").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboIdiomas() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/idiomas/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Idioma").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Idioma).html(response[key].Idioma_Nombre));
            });
            $("#Uid_Idioma").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTipoEmpleado() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/tipoempleados/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_TipoEmpleado").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_TipoEmpleado).html(response[key].TipoEmpleado_Nombre));
            });
            $("#Uid_TipoEmpleado").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTiposNominas() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/tiponominas/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_TipoNomina").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_TipoNomina).html(response[key].TipoNomina_Nombre));
            });
            $("#Uid_TipoNomina").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTiposBonos() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/tiposbonos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_TipoBono").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_TipoBono).html(response[key].TipoBono_Nombre));
            });
            $("#Uid_TipoBono").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboCursos(Uid_AreaFormacion) //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/cursos/0?Uid_AreaFormacion=${Uid_AreaFormacion}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Uid_Curso").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Uid_Curso).html(response[key].Curso_Nombre));
            });
            $("#Uid_Curso").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTiposContratos() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/tipocontratos/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_TipoContrato").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_TipoContrato).html(response[key].TipoContrato_Nombre));
            });
            $("#Id_TipoContrato").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function LoadComboTiposIncidencias() //Carga el select de nacionalidades
{
    $.ajax({
        url:`${main_path}/tipoincidencias/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Id_TipoIncidencia").empty();
            var myselect = $('<select>');

            myselect.append( $('<option></option>').val(0).html('Seleccione') );
            $.each(response,function(key,value)
            {
                myselect.append( $('<option></option>').val(response[key].Id_TipoIncidencia).html(response[key].TipoIncidencia_Nombre));
            });
            $("#Id_TipoIncidencia").append(myselect.html());
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

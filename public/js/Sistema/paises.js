var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Pais_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
            }
        }
        ],
        //   dom: 'Bfrtip',
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_Pais)
    });

    LoadPaises();
});

function LoadPaises(){
    $.ajax({
        url:`${main_path}/paises/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Id_Pais){
    $.ajax({
    url:main_path+'/paises/'+Id_Pais+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        $("#Pais_Nombre").val(response.Pais_Nombre);
        $("#Id_Pais").val(response.Id_Pais);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Pais';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Pais_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Pais';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}
function envio(e)
{
    var Pais_Nombre=$("#Pais_Nombre").val();
    var token=$(".token").val();
    var usuario="{{ Auth::user()->Uid_Usuario}}"
    var registro = {
        'Pais_Nombre':Pais_Nombre,
        '_token':token
    };
    $.ajax({
        url:main_path+'/paises',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
          toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

  function edicion(e)
    {
        var id=$(e).data('id');

      var Pais_Nombre=$("#Pais_Nombre").val();
      var Id_Pais=$("#Id_Pais").val();
      var token=$(".token").val();
      var registro = {
        'Pais_Nombre':Pais_Nombre,
        '_token':token
      };
      $.ajax({
        url:main_path+'/paises/'+Id_Pais,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
          toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
      });
    }

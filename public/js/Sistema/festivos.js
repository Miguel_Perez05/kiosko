var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Festivo_Nombre"},
            {"data": "Festivo_Dia"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
            }
        }
        ],
        //   dom: 'Bfrtip',
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Festivo)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadFestivos();
});

function LoadFestivos()
{
    $.ajax({
        url:`${main_path}/festivos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            console.table(response);
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Festivo){
    $.ajax({
    url:`${main_path}/festivos/${Uid_Festivo}/edit`,
    type:'get',
    datatype:'json',
    success:function(response){
        $("#Festivo_Nombre").val(response.Festivo_Nombre);
        $("#Festivo_Dia").val(response.Festivo_Dia);
        $("#Uid_Festivo").val(response.Uid_Festivo);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Dia Festivo';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
    $("#Festivo_Nombre").val("");
    $("#Festivo_Dia").val(date);
    document.getElementById('LabelModal').innerHTML = 'Nuevo Dia Festivo';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register= new Object();
    register.Festivo_Nombre=$("#Festivo_Nombre").val();
    register.Festivo_Dia=$("#Festivo_Dia").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/festivos`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadFestivos();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register= new Object();
    register.Festivo_Nombre=$("#Festivo_Nombre").val();
    register.Festivo_Dia=$("#Festivo_Dia").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/festivos/${$("#Uid_Festivo").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadFestivos();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Dia Festivo",
        text: "Esta seguro de eliminar El Dia Festivo?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/festivos/${data.Uid_Festivo}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadFestivos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}
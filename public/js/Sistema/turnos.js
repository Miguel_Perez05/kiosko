var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Turno_Nombre"},
            {"data": "Turno_Hora_Inicio"},
            {"data": "Turno_Hora_Fin"},
            {"data": "Id_Estatus",render:function (data) {
                    if(data==0)
                        return "Inactivo";
                    else if(data==1)
                    return "Activo";
                }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Turno)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });

    $('#table tbody').on( 'click', '.Activate', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,1,"Activar");
    });

    LoadTurnos();
});

function LoadTurnos()
{
    $.ajax({
        url:`${main_path}/turnos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Turno){
    $.ajax({
    url:`${main_path}/turnos/${Uid_Turno}/edit`,
    type:'get',
    datatype:'json',
    success:function(response){
        $("#Turno_Nombre").val(response.Turno_Nombre);
        $("#Uid_Turno").val(response.Uid_Turno);
        $("#Turno_Hora_Inicio").val(response.Turno_Hora_Inicio);
        $("#Turno_Hora_Fin").val(response.Turno_Hora_Fin);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Turno';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Turno_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Turno';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register=new Object();
    register.Turno_Nombre=$("#Turno_Nombre").val();
    register.Turno_Hora_Inicio=$("#Turno_Hora_Inicio").val();
    register.Turno_Hora_Fin=$("#Turno_Hora_Fin").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/turnos`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadTurnos();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion()
{
    var register=new Object();
    register.Turno_Nombre=$("#Turno_Nombre").val();
    register.Turno_Hora_Inicio=$("#Turno_Hora_Inicio").val();
    register.Turno_Hora_Fin=$("#Turno_Hora_Fin").val();   
    register._token=$(".token").val();    
    $.ajax({
        url:`${main_path}/turnos/${$("#Uid_Turno").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadTurnos();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Turno`,
        text: `Esta seguro de ${labelStatus} la turno?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data,status);            
        }
    });
}

function Cancel(data,status)
{
    var register = new Object();
    register._token=$(".token").val();
    register.Id_Estatus=status;
    $.ajax({
        url:`${main_path}/turnos/${data.Uid_Turno}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTurnos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}
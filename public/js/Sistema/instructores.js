function editar(e){
    var id=$(e).data('id');
    console.log(e);
    $.ajax({
    url:main_path+'/instructores/'+id+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        console.log(response);
        $("#Instructor_Nombre").val(response.Instructor_Nombre);
        $("#Uid_Instructor").val(response.Uid_Instructor);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Instructor';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Instructor_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Instructor';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var registro =new Object();
    registro.Instructor_Nombre=$("#Instructor_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/instructores',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    registro.Instructor_Nombre=$("#Instructor_Nombre").val();
    registro._token=$(".token").val();
    registro.Uid_Instructor=$("#Uid_Instructor").val();
    $.ajax({
        url:main_path+'/instructores/'+Uid_Instructor,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/instructores/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/instructores/';
    });
}

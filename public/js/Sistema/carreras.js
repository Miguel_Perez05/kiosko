var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Carrera_Nombre"},
            {"data": "NivelEducativo_Nombre"},
            {"data": "AreaFormacion_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Carrera)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        eliminar(data.Uid_Carrera)
    });
    
    LoadCarreras();
    LoadComboAreasFormacion();
    LoadComboNivelEducativo();
});

function LoadCarreras()
{
    $.ajax({
        url:`${main_path}/carreras/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Carrera)
{
    $.ajax({
        url:`${main_path}/carreras/${Uid_Carrera}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Carrera_Nombre").val(response.Carrera_Nombre);
            $("#Uid_Carrera").val(response.Uid_Carrera);
            $("#Uid_AreaFormacion").val(response.Uid_AreaFormacion);
            $("#Uid_NivelEducativo").val(response.Uid_NivelEducativo);
            $("#img").attr('src', response.Logo);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Carrera';
            $("#modal").modal("show");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function nuevo(){
    $("#Carrera_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nueva Carrera';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var registro =new Object();
    registro.Carrera_Nombre=$("#Carrera_Nombre").val();
    registro.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();
    registro.Uid_NivelEducativo=$("#Uid_NivelEducativo").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/carreras`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadCarreras();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    var Uid_Carrera=$("#Uid_Carrera").val();
    registro.Carrera_Nombre=$("#Carrera_Nombre").val();
    registro.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();
    registro.Uid_NivelEducativo=$("#Uid_NivelEducativo").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/carreras/${Uid_Carrera}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadCarreras();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(Uid_Carrera)
{
    var registro =new Object();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/carreras/${Uid_Carrera}`,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadCarreras();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}
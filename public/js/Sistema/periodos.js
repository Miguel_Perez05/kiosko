function editar(e){
    var id=$(e).data('id');
    console.log(e);
    $.ajax({
    url:main_path+'/periodos/'+id+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        console.log(response);
        $("#Periodo_Nombre").val(response.Periodo_Nombre);
        $("#Periodo_Estado").val(response.Periodo_Estado);
        $("#Periodo_Inicial").val(response.Periodo_Inicial);
        $("#Periodo_Final").val(response.Periodo_Final);
        $("#Uid_Periodo").val(response.Uid_Periodo);
        $("#Uid_Ejercicio").val(response.Uid_Ejercicio);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Periodo';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Periodo_Nombre").val("");
    $("#Periodo_Inicial").val("");
    $("#Periodo_Final").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Periodo';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var Periodo_Nombre=$("#Periodo_Nombre").val();
    var Periodo_Estado=$("#Periodo_Estado").val();
    var Periodo_Inicial=$("#Periodo_Inicial").val();
    var Periodo_Final=$("#Periodo_Final").val();
    var Uid_Ejercicio=$("#Uid_Ejercicio").val();
    var token=$(".token").val();
    var registro = {
        'Periodo_Nombre':Periodo_Nombre,
        'Periodo_Estado':Periodo_Estado,
        'Periodo_Inicial':Periodo_Inicial,
        'Periodo_Final':Periodo_Final,
        'Uid_Ejercicio':Uid_Ejercicio,
        '_token':token
    };
    $.ajax({
        url:main_path+'/periodos',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var Periodo_Nombre=$("#Periodo_Nombre").val();
    var Periodo_Estado=$("#Periodo_Estado").val();
    var Periodo_Inicial=$("#Periodo_Inicial").val();
    var Periodo_Final=$("#Periodo_Final").val();
    var Uid_Ejercicio=$("#Uid_Ejercicio").val();
    var Uid_Periodo=$("#Uid_Periodo").val();
    var token=$(".token").val();
    var registro = {
        'Periodo_Nombre':Periodo_Nombre,
        'Periodo_Estado':Periodo_Estado,
        'Periodo_Inicial':Periodo_Inicial,
        'Periodo_Final':Periodo_Final,
        'Uid_Ejercicio':Uid_Ejercicio,
        '_token':token
    };
    $.ajax({
        url:main_path+'/periodos/'+Uid_Periodo,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/periodos/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/periodos/';
    });
}

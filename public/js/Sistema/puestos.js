$(document).ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": "Puesto_Nombre"},
            {"data": "Departamento_Nombre"},
            {"data": "Responsable"},
            {"data": "Id_Estatus",render:function (data) {
                    if(data==0)
                        return "Inactivo";
                    else if(data==1)
                        return "Cubierto";
                    else if(data==4)
                        return "Vacante";
                }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                        <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        // dom: 'Bfrtip',
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });
    LoadPuestos();
    LoadComboAreasFormacion();
    LoadComboNivelEducativo();
    LoadComboDepartamentos();

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Puesto)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });

    $('#table tbody').on( 'click', '.Activate', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,4,"Activar");
    });
});

function LoadPuestos()
{
    $.ajax({
        url:`${main_path}/puestos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
          table.clear().rows.add(response).draw();
        }
      });
}

function editar(Uid_Contrato)
{
    LoadComboPaises();
    $.ajax({
        url:`${main_path}/puestos/${Uid_Contrato}/edit`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Puesto_Descripcion").val(response.Puesto_Descripcion);
            $("#Uid_Departamento").val(response.Uid_Departamento);
            $("#Puesto_Descripcion").val(response.Puesto_Descripcion);
            $("#Puesto_Nombre").val(response.Puesto_Nombre);
            $("#Puesto_Sueldo_Min").val(response.Puesto_Sueldo_Min);
            $("#Puesto_Sueldo_Max").val(response.Puesto_Sueldo_Max);
            $("#Uid_NivelEducativo").val(response.Uid_NivelEducativo);
            $("#Uid_AreaFormacion").val(response.Uid_AreaFormacion);
            $("#Responsable").val(response.Responsable);
            $("#Uid_Puesto").val(response.Uid_Puesto);

            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Puesto';
            $("#modal").modal("show");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function nuevo(){
    $("#Puesto_Descripcion").val("");
    $("#Uid_Departamento").val(0);
    $("#Puesto_Descripcion").val("");
    $("#Puesto_Nombre").val("");
    $("#Puesto_Sueldo_Min").val(0);
    $("#Puesto_Sueldo_Max").val(0);
    $("#Uid_NivelEducativo").val(0);
    $("#Uid_AreaFormacion").val(0);
    $("#Responsable").val("");
    // $("#Uid_Puesto").val();
    document.getElementById('LabelModal').innerHTML = 'Nuevo Puesto';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register = new Object();
    register.Puesto_Nombre=$("#Puesto_Nombre").val();
    register.Puesto_Descripcion=$("#Puesto_Descripcion").val();
    register.Puesto_Sueldo_Min=$("#Puesto_Sueldo_Min").val();
    register.Puesto_Sueldo_Max=$("#Puesto_Sueldo_Max").val();
    register.Uid_NivelEducativo=$("#Uid_NivelEducativo").val();
    register.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();
    register.Uid_Departamento=$("#Uid_Departamento").val();
    register._token=$(".token").val();
    $.ajax({
        url:main_path+'/puestos',
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadPuestos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion()
{
    var register = new Object();
    register.Puesto_Nombre=$("#Puesto_Nombre").val();
    register.Puesto_Descripcion=$("#Puesto_Descripcion").val();
    register.Puesto_Sueldo_Min=$("#Puesto_Sueldo_Min").val();
    register.Puesto_Sueldo_Max=$("#Puesto_Sueldo_Max").val();
    register.Uid_NivelEducativo=$("#Uid_NivelEducativo").val();
    register.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();
    register.Uid_Departamento=$("#Uid_Departamento").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/puestos/${$("#Uid_Puesto").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadPuestos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Puesto`,
        text: `Esta seguro de ${labelStatus} el Puesto?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/puestos/${data.Uid_Puesto}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadPuestos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

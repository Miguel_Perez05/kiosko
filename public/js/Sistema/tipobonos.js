var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoBono_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_TipoBono)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        eliminar(data.Uid_TipoBono)
    });
    LoadTiposBonos();
});

function LoadTiposBonos()
{
    $.ajax({
        url:`${main_path}/tipobonos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_TipoBono)
{
    $.ajax({
        url:main_path+'/tipobonos/'+Uid_TipoBono+'/edit',
        type:'get',
        datatype:'json',
        success:function(response){
            console.log(response);
            $("#TipoBono_Nombre").val(response.TipoBono_Nombre);
            $("#Uid_TipoBono").val(response.Uid_TipoBono);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Tipo de Bono';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#TipoBono_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Tipo de Bono';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var registro =new Object();
    registro.TipoBono_Nombre=$("#TipoBono_Nombre").val();
    registro._token=$(".token").val();
    $.ajax({
        url:main_path+'/tipobonos',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadTiposBonos();
            toastr.success("Bono creado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    registro.TipoBono_Nombre=$("#TipoBono_Nombre").val();
    registro._token=$(".token").val();
    var Uid_TipoBono=$("#Uid_TipoBono").val();

    $.ajax({
        url:main_path+'/tipobonos/'+Uid_TipoBono,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadTiposBonos();
            toastr.success("Bono editado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function eliminar(Uid_TipoBono)
{
    var registro =new Object();
    registro._token=$(".token").val();
    $.ajax({
        url:main_path+'/tipobonos/'+Uid_TipoBono,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTiposBonos();
            toastr.success("Bono eliminado");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

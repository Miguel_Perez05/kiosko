function cargaGrafico(url,ultimo)
{
    var marksCanvas = document.getElementById("chart");
    'use strict'

    var ticksStyle = {
        fontColor: '#495057',
        fontStyle: 'bold'
    }

    var mode      = 'index'
    var intersect = true

    var Expectativas = new Array();
    var Labels = new Array();
    var TipoChart;
    $(document).ready(function(){
        $.get(main_path+'/satisfaccion/create', function(response){
            console.log(response);
            response.forEach(function(data){
                Expectativas.push(data.Satisfaccion_Valor);
                Labels.push(data.Expectativa_Nombre);
            });

            //     var salesChart  = new Chart($salesChart, {
            //     type: 'radar',
            //     data   : {
            //         labels  : [Labels],
            //         datasets: [
            //             {
            //                 backgroundColor: '#007bff',
            //                 data           : Expectativas
            //             }
            //         ]
            //     },
            //     options: {
            //         scale: {
            //             angleLines: {
            //                 display: true
            //             },
            //             ticks: {
            //                 suggestedMin: 1,
            //                 suggestedMax: 10
            //             }
            //         }
            //     }
            // });

            var marksData = {
                labels: Labels,
                datasets: [{
                    label: "Satisfacción",
                    backgroundColor: "rgba(200,0,0,0.2)",
                    data: Expectativas,

                }]
              };

              var chartOptions = {
                scale: {
                  ticks: {
                    beginAtZero: true,
                    min: 0,
                    max: 10,
                    stepSize: 1
                  },
                  pointLabels: {
                    fontSize: 10
                  }
                },
                legend: {
                  position: 'left'
                }
              };

            var radarChart = new Chart(marksCanvas, {
                type: 'radar',
                data: marksData,

                options: chartOptions
              });

        });
    });
}

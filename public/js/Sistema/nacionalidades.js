var table;
$('document').ready(function(){
  table =$('#table').DataTable({
    "columns":[
        {"data": "Nacionalidad_Nombre"},
        {"data": "Pais_Nombre"},
        {"data": "Id_Estatus",render:function (data) {
            if(data==0)
                return "Inactiva";
            else if(data==1)
              return "Activa";
          }
        },
        {"data": null,render:function (data) {
          var divButtons=`<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
          if(data.Id_Estatus==0)
              divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
          else
              divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
          divButtons+=`</div></div>`;
          return divButtons;
        }
      }
      ],
      scrollCollapse: true,
      autoWidth: false,
      responsive: true,
      order: [[ 1, 'asc' ]],
      "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
      columnDefs: [{
          targets: "datatable-nosort",
          orderable: false
      }],
      "language": {
          "url": "/json/Spanish.json"
      },
  });

  $('#table tbody').on( 'click', '.Edit', function () {
      var tr = $(this).closest('tr');
      var data = table.row( tr ).data();
      editar(data.Uid_Nacionalidad)
  });

  $('#table tbody').on( 'click', '.Delete', function () {
      var tr = $(this).closest('tr');
      var data = table.row( tr ).data();
      confirmStatus(data,0,"Desactivar");
  });

  $('#table tbody').on( 'click', '.Activate', function () {
      var tr = $(this).closest('tr');
      var data = table.row( tr ).data();
      confirmStatus(data,1,"Activar");
  });

  LoadNacionalidades(); 
  LoadComboPaises();   
});

function LoadNacionalidades()
{
    $.ajax({
        url:`${main_path}/nacionalidades/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Nacionalidad){
    $.ajax({
    url:`${main_path}/nacionalidades/${Uid_Nacionalidad}/edit`,
    type:'get',
    datatype:'json',
    success:function(response){
        
        $("#Nacionalidad_Nombre").val(response.Nacionalidad_Nombre);
        $("#Uid_Nacionalidad").val(response.Uid_Nacionalidad);
        $("#Id_Pais").val(response.Id_Pais);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Nacionalidad';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Nacionalidad_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nueva Nacionalidad';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
  var register= new Object();
  register.Nacionalidad_Nombre=$("#Nacionalidad_Nombre").val();
  register.Id_Pais=$("#Id_Pais").val();
  register._token=$(".token").val();
  $.ajax({
      url:`${main_path}/nacionalidades`,
      data:register,
      type:'post',
      datatype:'json',
      success:function(response){
          toastr.success(response.Message);
          LoadNacionalidades(); 
          $("#modal").modal("hide");
      },
      error:function(response){
          toastr.error(response.responseJSON.ResponseStatus.Message)
      },
  });
}

function edicion()
{
  var register= new Object();
  register.Nacionalidad_Nombre=$("#Nacionalidad_Nombre").val();
  register.Id_Pais=$("#Id_Pais").val();
  register._token=$(".token").val();
  
  $.ajax({
    url:`${main_path}/nacionalidades/${$("#Uid_Nacionalidad").val()}`,
    data:register,
    type:'put',
    datatype:'json',
    success:function(response){
      toastr.success(response.Message);
      LoadNacionalidades(); 
      $("#modal").modal("hide");
    },
    error:function(response){
        toastr.error(response.responseJSON.ResponseStatus.Message)
    },
  });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Nacionalidad`,
        text: `Esta seguro de ${labelStatus} la Nacionalidad?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);            
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/nacionalidades/${data.Uid_Nacionalidad}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTiposIncidencias();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}
function CrearEntrenamiento()
{
    var errors=validator();
    if(errors!="")
    {
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }
    var Uid_Usuario=$("#Uid_Usuario").val();
    var registro =new Object();
    registro.Uid_Empleado=$("#Uid_Empleado").val();

    registro.Uid_Curso=$("#Uid_Curso").val();
    registro.Entrenamiento_Fecha=$("#Entrenamiento_Fecha").val();
    registro.Entrenamiento_Calificacion=$("#Entrenamiento_Calificacion").val();
    registro.Entrenamiento_Certificacion=$("#Entrenamiento_Certificacion").val();
    registro.Entrenamiento_Porcentaje="100";
    registro.Entrenamiento_Duracion=$("#Entrenamiento_Duracion").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/entrenamientos/PU/'+Uid_Usuario,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadEntrenamientos();
            
            $("#Entrenamiento_Calificacion").val("");
            $("#Entrenamiento_Certificacion").val("");
            $("#Entrenamiento_Duracion").val("");
            toastr.success(response.Message);
            $("#task-add").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/entrenamientos/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function validator()
{
    var Message="";

    if($("#Uid_Curso").val()=="" || $("#Uid_Curso").val()==null)
    {
        Message+="<li type= circle>No ha seleccionado Curso</li>";
        $(`#Uid_Curso`).css("border", "2px solid red");
    }
    if($("#Entrenamiento_Fecha").val()=="")
    {
        Message+="<li type= circle>Es necesario especificar la fecha del curso</li>";
        $(`#Entrenamiento_Fecha`).css("border", "2px solid red");
    }
    if($("#Entrenamiento_Duracion").val()=="")
    {
        Message+="<li type= circle>Es necesario especificar la duración del curso</li>";
        $(`#Entrenamiento_Duracion`).css("border", "2px solid red");
    }
    if($("#Entrenamiento_Calificacion").val()=="")
    {
        Message+="<li type= circle>Es necesario especificar la calificación del curso</li>";
        $(`#Entrenamiento_Calificacion`).css("border", "2px solid red");
    }
    return Message;
}

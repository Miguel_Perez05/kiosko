function editar(e){
    var id=$(e).data('id');
    $.ajax({
    url:main_path+'/grupos/'+id+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        console.log(response);
        $("#Grupo_Nombre").val(response.Grupo_Nombre);
        $("#Uid_Grupo").val(response.Uid_Grupo);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Grupo';
        document.getElementById('_Estatus').style.display = 'block';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Grupo_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Grupo';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    document.getElementById('_Estatus').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var Grupo_Nombre=$("#Grupo_Nombre").val();
    var token=$(".token").val();
    var registro = {
        'Grupo_Nombre':Grupo_Nombre,
        '_token':token
    };
    $.ajax({
        url:main_path+'/grupos',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var Grupo_Nombre=$("#Grupo_Nombre").val();
    var Uid_Grupo=$("#Uid_Grupo").val();
    var token=$(".token").val();
    var registro = {
        'Grupo_Nombre':Grupo_Nombre,
        '_token':token
    };
    $.ajax({
        url:main_path+'/grupos/'+Uid_Grupo,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/grupos/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/grupos/';
    });
}

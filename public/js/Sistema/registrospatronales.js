var table;
var type;
$('document').ready(function(){    
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    type=pathArray[3];

    table =$('#table').DataTable({
        "columns":[
            {"data": "RegistroPatronal_Nombre"},
            {"data": "Empresa_Nombre"},
            {"data": "Id_Estatus",render:function (data) {
                if(data==0)
                    return "Inactiva";
                else if(data==1)
                  return "Activa";
              }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                        <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_RegistroPatronal)
    });
  
    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });
  
    $('#table tbody').on( 'click', '.Activate', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,1,"Activar");
    });

    LoadRegistrosPatronales();
    LoadComboEmpresas(); 
});

function LoadRegistrosPatronales()
{
    $.ajax({
        url:`${main_path}/registrospatronales/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_RegistroPatronal){
    $.ajax({
        url:`${main_path}/registrospatronales/${Uid_RegistroPatronal}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#RegistroPatronal_Nombre").val(response.RegistroPatronal_Nombre);
            $("#Uid_RegistroPatronal").val(response.Uid_RegistroPatronal);
            $("#Uid_Empresa").val(response.Uid_Empresa);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Registro Patronal';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#RegistroPatronal_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Registro Patronal';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register= new Object();
    register.RegistroPatronal_Nombre=$("#RegistroPatronal_Nombre").val();
    register.Uid_Empresa=$("#Uid_Empresa").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/registrospatronales`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadRegistrosPatronales();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register= new Object();
    register.RegistroPatronal_Nombre=$("#RegistroPatronal_Nombre").val();
    register.Uid_Empresa=$("#Uid_Empresa").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/registrospatronales/${$("#Uid_RegistroPatronal").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadRegistrosPatronales();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Registro Patronal`,
        text: `Esta seguro de ${labelStatus} el Registro Patronal?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);            
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/registrospatronales/${data.Uid_RegistroPatronal}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadRegistrosPatronales();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

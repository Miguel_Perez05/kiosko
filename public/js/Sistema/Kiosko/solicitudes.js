var table;
var type;
$('document').ready(function(){    
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    type=pathArray[3];

    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoSolicitud_Nombre"},
            {"data": "Solicitud_Inicio_Auscencia"},
            {"data": "Solicitud_Fin_Auscencia"},
            {"data": "Solicitud_Status"},
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Comment"><i class="fa fa-pencil"></i> Comentarios</a>`;
                    if(data.Solicitud_Status!= 'Rechazado')
                        divButtons+=`<a class="dropdown-item Rejected"><i class="fa fa-pencil"></i> Rechazar</a>`;
                    if(data.Solicitud_Status!= 'Autorizado')
                        divButtons+=`<a class="dropdown-item Authorize"><i class="fa fa-pencil"></i> Autorizar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Comment', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        comentarios(data.Uid_Solicitud)
    });

    $('#table tbody').on( 'click', '.Rejected', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        cambiaEstatus(data,'Rechazado');
    });

    $('#table tbody').on( 'click', '.Authorize', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        cambiaEstatus(data,'Autorizado');
    });

    LoadSolicitudes(type);
});

function LoadSolicitudes(type)
{
    $.ajax({
        url:`${main_path}/solicitudes/${type}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function nuevo(){
    $('#Solicitud_Motivo').val('');
    if(!$("#TipoIncidencia").val())
        $("#success-modal-tramites").modal("show");
    else
        $("#success-modal").modal("show");
}

function cambiaEstatus(data, status)
{
    if(status=='Rechazado')
        document.getElementById('BTN_Status').innerHTML = 'Rechazar';
    if(status=='Autorizado')
        document.getElementById('BTN_Status').innerHTML = 'Autorizar';
    $("#Uid_Solicitud").val(data.Uid_Solicitud);
    $("#Solicitud_StatusR").val(status);
    $("#Solicitud_Observaciones").val('');
    document.getElementById("Solicitud_MotivoC").disabled=true;
    document.getElementById("Motivo").style.display = 'none';
    document.getElementById("Solicitud_Observaciones").disabled=false;
    document.getElementById('BTN_Status').style.display = 'block';
    $("#modal_Comentarios").modal("show");
}

function estatus()
{
    var register= new Object();
    register.Uid_Solicitud=$("#Uid_Solicitud").val();
    register.Solicitud_Status=$("#Solicitud_StatusR").val();
    register.Solicitud_Observaciones=$("#Solicitud_Observaciones").val();
    register._token=$(".token").val();

    $.ajax({
        url:main_path+'/solicitudes/estatus',
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadSolicitudes(type);
            $("#modal_Comentarios").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:`${main_path}/solicitudes/${id}`,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/solicitudes';
    });
}

function comentarios(Uid_Solicitud){
    $.ajax({
        url:`${main_path}/solicitudes/${Uid_Solicitud}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Solicitud_MotivoC").val(response.Solicitud_Motivo);
            $("#Solicitud_Observaciones").val(response.Solicitud_Observaciones);
            document.getElementById("Solicitud_MotivoC").disabled=true;
            document.getElementById("Solicitud_Observaciones").disabled=true;
            document.getElementById("Motivo").style.display = 'block';
            $("#modal_Comentarios").modal("show");
            document.getElementById('BTN_Status').style.display = 'none';
        }
    });
}

var table;
$('document').ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": "Aviso_Dirijido"},
            {"data": "Aviso_Titulo"},
            {"data": "Aviso_Prioridad"},
            {"data": "Aviso_Inicio"},
            {"data": "Aviso_Vigencia"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Edit</a>
                            </div>

                        </div>`;
                         // @if(AuthUser::get_perfil()=='true')
                        //     <a class="dropdown-item" data-id="{{ $aviso->Uid_Aviso }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                        // @endif
                }
            }
        ],
        dom: 'Bfrtip',
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Aviso)
    });
    LoadAvisos();
    LoadGrupos();
    LoadEmpleados();
    LoadTurnos();
    LoadDepartamentos();
    nuevo();
});

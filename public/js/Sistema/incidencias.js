var type;
var typeDescription;
var table;
$(document).ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    type=pathArray[3];
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoIncidencia_Nombre"},
            {"data": "Incidencia_Dia"},
            {"data": "Incidencia_Minutos"},
            {"data": null,render:function (data) {
                return `${data.Empleado_Nombre} ${data.Empleado_APaterno} ${data.Empleado_AMaterno}`;
            }},
            {"data": null,render:function (data) {
                    if(data.Incidencia_FechaJustificacion)
                        return "Justificada";
                    else
                        return "";
                }
            },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                        <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">`;
                    divButtons+=`<a class="dropdown-item Comments"><i class="fa fa-pencil"></i> Comentarios</a>`;
                    if(data.Id_Estatus>0 && !data.Incidencia_FechaJustificacion)
                        divButtons+=`<a class="dropdown-item Delete"><i class="fa fa-trash"></i> Desactivar</a>`;
                    if(!data.Incidencia_FechaJustificacion)
                        divButtons+=`<a class="dropdown-item Justify"><i class="fa fa-check"></i> Justificar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        // dom: 'Bfrtip',
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    LoadComboEmpleados();
    LoadIncidencias();
    LoadTipoIncidencias();

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });

    $('#table tbody').on( 'click', '.Comments', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        comentarios(data.Uid_Incidencia);
    });

    $('#table tbody').on( 'click', '.Justify', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        justifica(data);
    });

});

function LoadIncidencias()
{
    $.ajax({
        url:`${main_path}/incidencias/${type}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function LoadTipoIncidencias()
{
    $.ajax({
        url:`${main_path}/tipoincidencias/${type}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            typeDescription=response.TipoIncidencia_Nombre;
            document.getElementById('labelH5').innerHTML = `Catalogo de ${response.TipoIncidencia_Nombre}`;
            document.getElementById('labelP').innerHTML = `Mantenimiento al catálogo de ${response.TipoIncidencia_Nombre}`;
        }
    });
}

function comentarios(Uid_Incidencia){
    $.ajax({
        url:`${main_path}/incidencias/${Uid_Incidencia}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Incidencia_Comentarios").val(response.Incidencia_Comentario);
            document.getElementById('LabelModalComentarios').innerHTML = 'Comentarios de Incidencia';
            document.getElementById('Justificado').style.display = 'none';

            if(response.Incidencia_FechaJustificacion)
            {
                var Incidencia_QuienJustifica='';
                if(response.Uid_Empleado)
                {
                    Incidencia_QuienJustifica=response.Empleado_Nombre + " " + response.Empleado_APaterno;
                }
                else{
                    Incidencia_QuienJustifica=response.Usuario_NickName;
                }
                document.getElementById('Justificado').style.display = 'block';
                $("#Incidencia_QuienJustifica").val(Incidencia_QuienJustifica);
                $("#Incidencia_FechaJustificacion").val(response.Incidencia_FechaJustificacion);
                $("#Incidencia_ComentarioJustifica").val(response.Incidencia_ComentarioJustifica);
            }
            $("#modal_Comentarios").modal("show");
            document.getElementById("Incidencia_Comentarios").disabled=true;
            document.getElementById("Incidencia_QuienJustifica").disabled=true;
            document.getElementById("Incidencia_FechaJustificacion").disabled=true;
            document.getElementById("Incidencia_ComentarioJustifica").disabled=true;
            document.getElementById('BTN_Justificar').style.display = 'none';
        }
    });
}

function nuevo(){
    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
    $('#Incidencia_Dia').val(date);
    $("#Incidencia_Comentario").val("");
    document.getElementById('LabelModal').innerHTML = `Nueva incidencia: ${typeDescription}`;
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('BTN_Justificar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var register=new Object();
    register.Incidencia_Comentario=$("#Incidencia_Comentario").val();
    register.Incidencia_Tipo=$("#Incidencia_Tipo").val();
    register.Uid_Empleado=$("#Uid_Empleado").val();
    register.Incidencia_Dia=$("#Incidencia_Dia").val();
    register.Incidencia_Minutos=$("#Incidencia_Minutos").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/incidencias`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function justifica(Uid_Incidencia){
    $("#Uid_Incidencia").val(Uid_Incidencia);
    $("#Incidencia_Comentarios").val("");
    document.getElementById('LabelModalComentarios').innerHTML = 'Justificar Incidencia';
    $("#modal_Comentarios").modal("show");
    document.getElementById("Incidencia_Comentarios").disabled=false;
    document.getElementById('Justificado').style.display = 'none';
    document.getElementById('BTN_Justificar').style.display = 'block';
}

async function justificar(e){
    var id=$("#Uid_Incidencia").val();
    var Incidencia_ComentarioJustifica=$("#Incidencia_Comentarios").val();
    var token=$(".token").val();
    var registro = {
        'Incidencia_ComentarioJustifica':Incidencia_ComentarioJustifica,
        '_token':token
    };
    $.ajax({
        url:main_path+'/incidencias/justificacion/'+id,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/incidencias/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    id: response.id,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function (id)
    {
        console.log(id);
        window.location.href = main_path+'/incidencias/nominas'+id;
    });
}

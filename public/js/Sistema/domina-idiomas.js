function editar(e){
    var id=$(e).data('id');
    console.log(e);
    $.ajax({
        url:main_path+'/dominaidiomas/'+id+'/edit',
        type:'get',
        datatype:'json',
        success:function(response){
            console.log(response);
            $("#Idioma_Nombre").val(response.Idioma_Nombre);
            $("#Uid_Dominio").val(response.Uid_Dominio);
            $("#Empleado_Nombre").val(response.Empleado_Nombre);
            $("#DominaIdioma_PorcentajeEscritura").val(response.DominaIdioma_PorcentajeEscritura);
            $("#DominaIdioma_PorcentajeConversacion").val(response.DominaIdioma_PorcentajeConversacion);

            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición del Dominio de Idioma';
            $("#modal").modal("show");
        }
    });
}

function edicion(e)
{
    var registro =new Object();
    var Uid_Dominio=$("#Uid_Dominio").val();
    registro.DominaIdioma_PorcentajeEscritura=$("#DominaIdioma_PorcentajeEscritura").val();
    registro.DominaIdioma_PorcentajeConversacion=$("#DominaIdioma_PorcentajeConversacion").val();
    registro._token=$(".token").val();

    console.log(Uid_Dominio);
    $.ajax({
        url:main_path+'/dominaidiomas/'+Uid_Dominio,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
        alerta(response);
        },
        error:function(response){
            alerta(response);
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/dominaidiomas/';
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
    url:main_path+'/dominaidiomas/'+id,
    data:registro,
    type:'delete',
    datatype:'json',
    success:function(response){
        alerta(response);
    },
    error:function(response){
        alerta(response);
        },
    });
}

function CrearDominaIdioma()
{
    errors="";
    if($("#Uid_Idioma").val()==0)
    {
        errors+="<li type= circle>Es necesario especificar el idioma</li>";
        $(`#Uid_Idioma`).css("border", "2px solid red");
    }
    if($("#DominaIdioma_PorcentajeEscritura").val()<1)
    {
        errors+="<li type= circle>Es necesario especificar el porcentaje de escritura</li>";
        $(`#DominaIdioma_PorcentajeEscritura`).css("border", "2px solid red");
    }
    if($("#DominaIdioma_PorcentajeConversacion").val()<1)
    {
        errors+="<li type= circle>Es necesario especificar el porcentaje de conversación</li>";
        $(`#DominaIdioma_PorcentajeConversacion`).css("border", "2px solid red");
    }
    if(errors!="")
    {
        toastr.options.escapeHtml = true;
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var Uid_Usuario=$("#Uid_Usuario").val();
    var registro =new Object();
    registro.Uid_Idioma=$("#Uid_Idioma").val();

    registro.DominaIdioma_Fecha=$("#DominaIdioma_Fecha").val();
    registro.Uid_Empleado=$("#Uid_Empleado").val();
    registro.DominaIdioma_PorcentajeEscritura=$("#DominaIdioma_PorcentajeEscritura").val();
    registro.DominaIdioma_PorcentajeConversacion=$("#DominaIdioma_PorcentajeConversacion").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/dominaidiomas/PU/'+Uid_Usuario,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadDominaIdiomas();
            toastr.success(response.Message);
            $("#DominaIdioma_PorcentajeEscritura").val("");
            $("#DominaIdioma_PorcentajeConversacion").val("");
            $("#task-add-idiomas").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

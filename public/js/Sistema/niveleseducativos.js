var table;
$('document').ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": "NivelEducativo_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Edit</a>
                            </div>

                        </div>`;
                         // @if(AuthUser::get_perfil()=='true')
                        //     <a class="dropdown-item" data-id="{{ $aviso->Uid_Aviso }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                        // @endif
                }
            }
        ],
        dom: 'Bfrtip',
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_NivelEducativo)
    });
    LoadNivelesEducativos();
});

function LoadNivelesEducativos()
{
    $.ajax({
        url:`${main_path}/niveleseducativos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_NivelEducativo)
{
    $.ajax({
        url:main_path+'/niveleseducativos/'+Uid_NivelEducativo+'/edit',
        type:'get',
        datatype:'json',
        success:function(response){
            $("#NivelEducativo_Nombre").val(response.NivelEducativo_Nombre);
            $("#Uid_NivelEducativo").val(response.Uid_NivelEducativo);
            $("#img").attr('src', response.Logo);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Nivel Educativo';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#NivelEducativo_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Nivel Educativo';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var registro =new Object();
    registro.NivelEducativo_Nombre=$("#NivelEducativo_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/niveleseducativos',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadNivelesEducativos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    var Uid_NivelEducativo=$("#Uid_NivelEducativo").val();
    registro.NivelEducativo_Nombre=$("#NivelEducativo_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/niveleseducativos/'+Uid_NivelEducativo,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $("#modal").modal("hide");
            LoadNivelesEducativos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var registro =new Object();
    var id=$(e).data('id');
    registro._token=$(".token").val();

    $.ajax({
        url:main_path+'/niveleseducativos/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

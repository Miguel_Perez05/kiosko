var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;

    table =$('#table').DataTable({
        "columns":[
            {"data": "Departamento_Nombre"},
            {"data": "Encargado"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Departamento)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });
    LoadDepartures();
});

function LoadDepartures()
{
    $.ajax({
        url:`${main_path}/departamentos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Departamento)
{
    LoadComboEmpleados();
    $.ajax({
        url:`${main_path}/departamentos/${Uid_Departamento}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Departamento_Nombre").val(response.Departamento_Nombre);
            $("#Departamento_Descripcion").val(response.Departamento_Descripcion);
            $("#Encargado").val(response.Encargado);
            $("#Uid_Departamento").val(response.Uid_Departamento);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Departamento';
            $("#modal").modal("show");
            console.log($("#Uid_Departamento").val());
        }
    });
}

function nuevo(){
    LoadComboEmpleados();
    $("#Departamento_Nombre").val("");
    $("#Departamento_Descripcion").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Departamento';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    if(!validator())
        return;

    var registro =new Object();
    registro.Departamento_Nombre=$("#Departamento_Nombre").val();
    registro.Departamento_Descripcion=$("#Departamento_Descripcion").val();
    // registro.Departamento_Jefe=$("#Uid_Empleado").val();
    registro._token=$(".token").val();
    $.ajax({
        url:`${main_path}/departamentos`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadDepartures();
            toastr.success("Departamento Creado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    if(!validator())
        return;

    var registro =new Object();
    var Uid_Departamento=$("#Uid_Departamento").val();
    registro.Departamento_Nombre=$("#Departamento_Nombre").val();
    registro.Departamento_Descripcion=$("#Departamento_Descripcion").val();
    // registro.Departamento_Jefe=$("#Uid_Empleado").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/departamentos/${Uid_Departamento}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadDepartures();
            toastr.success("Departamento editado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Curso",
        text: "Esta seguro de eliminar el curso?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var registro =new Object();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/departamentos/${data.Uid_Departamento}`,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadDepartures();
            toastr.success("Departamento Eliminado");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function validator()
{
    // if($("#Uid_Empleado").val()<1)
    // {
    //     toastr.error("Es necesario indicar el responsable del departamento");
    //     return false;
    // }
    // else
    if($("#Departamento_Nombre").val()=="")
    {
        toastr.error("Es necesario especificar el nombre del departamento");
        return false;
    }
    else if($("#Departamento_Descripcion").val()=="")
    {
        toastr.error("Es necesario especificar la descripción del departamento");
        return false;
    }
    return true;
}

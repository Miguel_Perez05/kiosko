function CrearReconocimiento()
{
    var mensaje='';
    var elem = document.getElementById('img-R');
    elem.getAttribute('src') == "" ? mensaje+='<h6>Imagen</h6><br>' : '';
    !$("#Reconocimiento_Titulo").val() ? (document.getElementById('Reconocimiento_Titulo').style.backgroundColor='Red', mensaje+='<h6>Titulo del reconocimiento</h6><br>') : document.getElementById('Reconocimiento_Titulo').style.backgroundColor='';

    if(elem.getAttribute('src') == "" || !$("#Reconocimiento_Titulo").val())
    {
        var alerta={
            status:'warning',
            message:'<h5>Falta informacion necesaria:</h5><br>'+mensaje,
            Accion:'Creación',
            html:true
        }
        success(alerta);
    }
    else
    {
        var registro =new Object();
        registro.Uid_Empleado=$("#Uid_Empleado").val();
        registro.Reconocimiento_Titulo=$("#Reconocimiento_Titulo").val();
        registro.Reconocimiento_Image=getBase64Image(document.getElementById("img-R"),140,140);
        registro._token=$(".token").val();
        console.log(registro);
        $.ajax({
            url:main_path+'/reconocimientos/PU/'+$("#Uid_Usuario").val(),
            data:registro,
            type:'post',
            datatype:'json',
            success:function(response){
                LoadReconocimientos();
                toastr.success(response.Message);
                $("#Reconocimiento_Titulo").val("");                
                $('label[id*=logo-id-R]').empty();
                $("#task-add-reconocimiento").modal("hide");
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message);
            },
        });
    }
}

function getBase64Image(img, wantedWidth, wantedHeight) {
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    var ratio=1;
    ratio=0;
    var width = img.width;    // Current image width
    var height = img.height;

    if(width > height)
    {
        ratio =  wantedWidth / width ;
        wantedHeight=height*ratio;
    }
    else if(height > width)
    {
        ratio = wantedHeight/height;
        wantedWidth=width*ratio;
    }

    canvas.width = wantedWidth;
    canvas.height = wantedHeight;
    ctx.drawImage(img,0, 0,canvas.width ,canvas.height);
    var dataURL = canvas.toDataURL();
    return dataURL;
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
    '_token':token
    };
    $.ajax({
        url:main_path+'/reconocimientos/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function success(response)
{
    var html=response.html ? response.html : false;
    swal({
        html:html,
        title: response.Accion,
        text: response.message,
        type: response.status,
        showCancelButton: false,
        closeOnConfirm: true,
        showLoaderOnConfirm: true
    },
    function ()
    {
        if(response.status=='success')
        {
            window.location.href = main_path+'/empleados/'+response.Uid_Empleado+'/edit';
        }
    });
}


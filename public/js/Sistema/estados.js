var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Estado_Nombre"},
            {"data": "Pais_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
            }
        }
        ],
        //   dom: 'Bfrtip',
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_Estado)
    });

    LoadComboPaisesC();

    $('#Id_Pais_C').on('change', function (e) {
        LoadEstados();
    });
});

function LoadEstados()
{
    var Id_Pais= document.getElementById("Id_Pais_C").value;
    $.ajax({
        url:`${main_path}/estados/${Id_Pais}`,
        type:'get',
        datatype:'json',
        success:function(response){
        table.clear().rows.add(response).draw();
        }
    });
}

function editar(id_Estado)
{
    LoadComboPaises();
    $.ajax({
        url:`${main_path}/estados/${id_Estado}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Estado_Nombre").val(response.Estado_Nombre);
            $("#Id_Estado").val(response.Id_Estado);
            $("#Id_Pais").val(response.Id_Pais);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Estado';
            $("#modal").modal("show");
        }
    });
}

function nuevo()
{
    LoadComboPaises($("#Id_Pais_C").val());
    $("#Estado_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Estado';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    if(!validator())
        return;

    var register = new Object();
    register.Estado_Nombre=$("#Estado_Nombre").val();
    register.Id_Pais=$("#Id_Pais").val();
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/estados`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadEstados();
            toastr.success("Estado creado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion()
{
    if(!validator())
        return;

    var Id_Estado=$("#Id_Estado").val();

    var register = new Object();
    register.Estado_Nombre=$("#Estado_Nombre").val();
    register.Id_Pais=$("#Id_Pais").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/estados/${Id_Estado}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadEstados();
            toastr.success("Estado Editado");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Estado",
        text: "Esta seguro de eliminar el estado?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/estados/${data.Id_Estado}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadEstados();
            toastr.success("Estado eliminado");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function validator()
{
    if($("#Id_Pais").val()<1)
    {
        toastr.error("Es necesario indicar el pais");
        return false;
    }
    else if($("#Estado_Nombre").val()=="")
    {
        toastr.error("Es necesario especificar el nombre del estado");
        return false;
    }
    return true;
}

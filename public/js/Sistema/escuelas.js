var table;

$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Escuela_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Escuela)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadEscuelas();
});

function LoadEscuelas()
{
    $.ajax({
        url:`${main_path}/escuelas/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Escuela){
    $.ajax({
    url:`${main_path}/escuelas/${Uid_Escuela}/edit`,
    type:'get',
    datatype:'json',
    success:function(response){
        $("#Escuela_Nombre").val(response.Escuela_Nombre);
        $("#Uid_Escuela").val(response.Uid_Escuela);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Escuela';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Escuela_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Escuela';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var registro =new Object();
    registro.Escuela_Nombre=$("#Escuela_Nombre").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/escuelas`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadEscuelas();
            toastr.success("Escuela creada");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    registro.Escuela_Nombre=$("#Escuela_Nombre").val();
    var Uid_Escuela=$("#Uid_Escuela").val();
    registro._token=$(".token").val();
    $.ajax({
        url:`${main_path}/escuelas/${Uid_Escuela}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadEscuelas();
            toastr.success("Escuela creada");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Escuela",
        text: "Esta seguro de eliminar la escuela?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/escuelas/${data.Uid_Escuela}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadEscuelas();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;

    table =$('#table').DataTable({
        "columns":[
            {"data": "Curso_Nombre"},
            {"data": "AreaFormacion_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Curso)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadComboAreasFormacionC();
});

$('#Uid_AreaFormacion_C').on('change', function (e) {
    LoadCursos();
});

function LoadCursos()
{
    var Uid_AreaFormacion= document.getElementById("Uid_AreaFormacion_C").value;
    $.ajax({
        url:`${main_path}/cursos/0?Uid_AreaFormacion=${Uid_AreaFormacion}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Curso)
{
    LoadComboAreasFormacion();
    $.ajax({
        url:`${main_path}/cursos/${Uid_Curso}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Curso_Nombre").val(response.Curso_Nombre);
            $("#Uid_Curso").val(response.Uid_Curso);
            $("#Uid_AreaFormacion").val(response.Uid_AreaFormacion);
            $("#img").attr('src', response.Logo);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Curso';
            $("#modal").modal("show");
        }
    });
}

function nuevo()
{
    LoadComboAreasFormacion();
    $("#Curso_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nueva Curso';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var errors=validator();
    if(errors!="")
    {
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var register =new Object();
    register.Curso_Nombre=$("#Curso_Nombre").val();
    $("#Curso_Nombre").css("border", "1px solid gray");
    $("#Uid_AreaFormacion").css("border", "1px solid gray");
    register._token=$(".token").val();
    register.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();

    $.ajax({
        url:`${main_path}/cursos`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadCursos();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            if(response.responseJSON.errors)
            {
                toastr.options.escapeHtml = true;
                var errors="<ul>";
                $.each(response.responseJSON.errors, function(idx, el) {
                    $(`#${idx}`).css("border", "2px solid red");
                    errors+=`<li type= circle>${el[0]}</li>`;
                });
                errors+=`</ul>`;
                toastr.error(errors);
            }
            else
                toastr.error(response.ResponseStatus.message);
        },
    });
}

function edicion()
{
    var errors=validator();
    if(errors!="")
    {
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var Uid_Curso=$("#Uid_Curso").val();
    var register =new Object();
    register.Curso_Nombre=$("#Curso_Nombre").val();
    register._token=$(".token").val();
    register.Uid_AreaFormacion=$("#Uid_AreaFormacion").val();

    $.ajax({
        url:`${main_path}/cursos/${Uid_Curso}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadCursos();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Curso",
        text: "Esta seguro de eliminar el curso?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/cursos/${data.Uid_Curso}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadCursos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function validator()
{
    var Message="";
    if($("#Uid_AreaFormacion").val()<1)
    {
        Message+="<li type= circle>Es necesario indicar el area de formación</li>";
        $(`#Uid_AreaFormacion`).css("border", "2px solid red");
    }
    else if($("#Curso_Nombre").val()=="")
    {
        Message+="<li type= circle>Es necesario especificar el nombre del curso</li>";
        $(`#Curso_Nombre`).css("border", "2px solid red");
    }
   
    return Message;
}


var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoSolicitud_Nombre"},
            {"data": "TipoIncidencia_Nombre"},
            // {"data": "Id_Estatus",render:function (data) {
            //         if(data==0)
            //             return "Inactivo";
            //         else if(data==1)
            //         return "Activo";
            //     }
            // },
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>`;
                    // if(data.Id_Estatus==0)
                    //     divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    // else
                    //     divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_TipoSolicitud)
    });

    // $('#table tbody').on( 'click', '.Delete', function () {
    //     var tr = $(this).closest('tr');
    //     var data = table.row( tr ).data();
    //     confirmStatus(data,0,"Desactivar");
    // });

    // $('#table tbody').on( 'click', '.Activate', function () {
    //     var tr = $(this).closest('tr');
    //     var data = table.row( tr ).data();
    //     confirmStatus(data,1,"Activar");
    // });

    LoadTiposSolicitudes();
    LoadComboTiposIncidencias();
});

function LoadTiposSolicitudes()
{
    $.ajax({
        url:`${main_path}/tiposolicitudes/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_TipoSolicitud){
    $.ajax({
        url:`${main_path}/tiposolicitudes/${Uid_TipoSolicitud}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#TipoSolicitud_Nombre").val(response.TipoSolicitud_Nombre);
            $("#Id_TipoIncidencia").val(response.Id_TipoIncidencia);
            $("#Uid_TipoSolicitud").val(response.Uid_TipoSolicitud);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edicion Tipo de Solicitud';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#TipoSolicitud_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Tipo de Solicitud';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var register= new Object();
    register.TipoSolicitud_Nombre=$("#TipoSolicitud_Nombre").val();
    register.Id_TipoIncidencia=$("#Id_TipoIncidencia").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tiposolicitudes`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadTiposSolicitudes();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register= new Object();
    register.TipoSolicitud_Nombre=$("#TipoSolicitud_Nombre").val();
    register.Id_TipoIncidencia=$("#Id_TipoIncidencia").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tiposolicitudes/${$("#Uid_TipoSolicitud").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadTiposSolicitudes();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

// function confirmStatus(data,status,labelStatus)
// {
//     swal({
//         title: `${labelStatus} Tipo Solicitud`,
//         text: `Esta seguro de ${labelStatus} el Tipo Solicitud?`,
//         icon: "warning",
//         buttons: ["No", "Si"],
//         dangerMode: true,
//     })
//     .then(value => {
//         if (value) {
//             Status(data,status);
//         }
//     });
// }

// function Status(data,status)
// {
//     var register = new Object();
//     register._token=$(".token").val();
//     register.Id_Estatus=status;
//     $.ajax({
//         url:`${main_path}/tiposolicitudes/${data.Uid_TipoSolicitud}`,
//         data:register,
//         type:'delete',
//         datatype:'json',
//         success:function(response){
//             LoadTiposSolicitudes();
//             toastr.success(response.Message);
//         },
//         error:function(response){
//             toastr.error(response.responseJSON.ResponseStatus.Message);
//         },
//     });
// }
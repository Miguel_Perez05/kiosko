var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;

    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoNomina_Nombre"},
            {"data": "TipoNomina_Dias"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_TipoNomina)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadTiposNominas();
});

function LoadTiposNominas()
{
    $.ajax({
        url:`${main_path}/tiponominas/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_TipoNomina){
    $.ajax({
        url:`${main_path}/tiponominas/${Uid_TipoNomina}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#TipoNomina_Nombre").val(response.TipoNomina_Nombre);
            $("#TipoNomina_Dias").val(response.TipoNomina_Dias);
            $("#Uid_TipoNomina").val(response.Uid_TipoNomina);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Tipo de Empleado';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#TipoNomina_Nombre").val("");
    $("#TipoNomina_Dias").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Tipo de Empleado';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register =new Object();
    register.TipoNomina_Nombre=$("#TipoNomina_Nombre").val();
    register.TipoNomina_Dias=$("#TipoNomina_Dias").val();
    register._token=$(".token").val();
    $.ajax({
        url:main_path+'/tiponominas',
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadTiposNominas();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.ResponseStatus.message);
        },
    });
}

function edicion()
{
    var register =new Object();
    register.TipoNomina_Nombre=$("#TipoNomina_Nombre").val();
    register.TipoNomina_Dias=$("#TipoNomina_Dias").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tiponominas/${$("#Uid_TipoNomina").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadTiposNominas();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.ResponseStatus.message);
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Curso",
        text: "Esta seguro de eliminar el curso?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tiponominas/${data.Uid_TipoNomina}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTiposNominas();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.ResponseStatus.message);
        },
    });
}

var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    
    table =$('#table').DataTable({
        "columns":[
            {"data": "Ciudad_Nombre"},
            {"data": "Estado_Nombre"},
            {"data": "Pais_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_Ciudad)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadComboPaisesC();  
});

$('#Id_Pais_C').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadComboEstadosC(valueSelected);
});

$('#Id_Estado_C').on('change', function (e) {
    LoadCiudades()
});

$('#Id_Pais').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadComboEstados(valueSelected);
});

function LoadCiudades()
{
    var Id_Estado= document.getElementById("Id_Estado_C").value;
    var Id_Pais= document.getElementById("Id_Pais_C").value;
    $.ajax({
        url:`${main_path}/ciudades/0?Id_Pais=${Id_Pais}&Id_Estado=${Id_Estado}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Id_Ciudad)
{
    LoadComboPaises();
    $.ajax({
        url:`${main_path}/ciudades/${Id_Ciudad}/edit`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            LoadComboEstados(response.Id_Pais,response.Id_Estado);
            $("#Ciudad_Nombre").val(response.Ciudad_Nombre);
            $("#Id_Pais").val(response.Id_Pais);
            $("#Id_Ciudad").val(response.Id_Ciudad);
            
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Ciudad';
            $("#modal").modal("show");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function nuevo()
{
    $("#Ciudad_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nueva Ciudad';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    LoadComboPaises($("#Id_Pais_C").val());
    LoadComboEstados($("#Id_Pais_C").val(),$("#Id_Estado_C").val());
    $("#Id_Estado").empty();
    $("#modal").modal("show");
}

function envio()
{
    if(!validator())
        return;

    var register = new Object();
    register.Ciudad_Nombre=$("#Ciudad_Nombre").val();   
    register.Id_Pais=$("#Id_Pais").val();
    register.Id_Estado=$("#Id_Estado").val();
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/ciudades`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadCiudades();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    if(!validator())
        return;

    var register = new Object();
    register.Ciudad_Nombre=$("#Ciudad_Nombre").val();   
    register.Id_Pais=$("#Id_Pais").val();
    register.Id_Estado=$("#Id_Estado").val();
    register._token=$(".token").val();
    
    $.ajax({
        url:`${main_path}/ciudades/${$("#Id_Ciudad").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadCiudades();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Ciudad",
        text: "Esta seguro de eliminar la ciudad?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);            
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/ciudades/${data.Id_Ciudad}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadCiudades();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function validator()
{
    if($("#Id_Pais").val()<1)
    {
        toastr.error("Es necesario indicar el pais");
        return false;
    }
    else if($("#Id_Estado").val()<1)
    {
        toastr.error("Es necesario indicar el estado");
        return false;
    }
    else if($("#Ciudad_Nombre").val()=="")
    {
        toastr.error("Es necesario especificar el nombre de la ciudad");
        return false;
    }
    return true;
}


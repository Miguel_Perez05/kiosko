var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoContrato_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_TipoContrato);
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });
    LoadTiposContratos();
});

function LoadTiposContratos()
{
    $.ajax({
        url:`${main_path}/tipocontratos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Id_TipoContrato)
{
    $.ajax({
        url:`${main_path}/tipocontratos/${Id_TipoContrato}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            console.log(response);
            $("#TipoContrato_Nombre").val(response.TipoContrato_Nombre);
            $("#Id_TipoContrato").val(response.Id_TipoContrato);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Tipo de Contrato';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#TipoContrato_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Tipo de Contrato';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var registro =new Object();
    registro.TipoContrato_Nombre=$("#TipoContrato_Nombre").val();
    registro._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tipocontratos`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadTiposContratos();
            toastr.success("Creado con éxito");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion(e)
{
    var registro =new Object();
    registro.TipoContrato_Nombre=$("#TipoContrato_Nombre").val();
    registro._token=$(".token").val();
    var Id_TipoContrato=$("#Id_TipoContrato").val();

    $.ajax({
        url:`${main_path}/tipocontratos/${Id_TipoContrato}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadTiposContratos();
            toastr.success("Editado con éxito");
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Tipo Contrato",
        text: "Esta seguro de eliminar el Tipo de Contrato?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/tipocontratos/${data.Id_TipoContrato}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadTiposContratos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

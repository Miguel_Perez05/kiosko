var table;

$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Empresa_Nombre"},
            {"data": "Empresa_RFC"},
            {"data": "Empresa_Razon_Social"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Uid_Empresa)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadEmpresas();
});

function LoadEmpresas()
{
    $.ajax({
        url:`${main_path}/empresas/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function editar(Uid_Empresa)
{
    $.ajax({
    url:`${main_path}/empresas/${Uid_Empresa}/edit`,
    type:'get',
    datatype:'json',
    success:function(response){
        $("#Empresa_Nombre").val(response.Empresa_Nombre);
        $("#Empresa_Razon_Social").val(response.Empresa_Razon_Social);
        $("#Empresa_RFC").val(response.Empresa_RFC);
        $("#Empresa_Domicilio").val(response.Empresa_Domicilio);
        $("#Uid_Empresa").val(response.Uid_Empresa);
        $("#img").attr('src', response.Logo);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Empresa';
        document.getElementById('LblLogo').innerHTML = 'Actualizar Logo';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Empresa_Nombre").val("");
    $("#Empresa_Razon_Social").val("");
    $("#Empresa_RFC").val("");
    $("#Empresa_Domicilio").val("");
    $("#img").attr('src', 'img/sistema/no-logo.jpg');
    document.getElementById('LabelModal').innerHTML = 'Nueva Empresa';
    document.getElementById('LblLogo').innerHTML = 'Registrar Logo de Nueva Empresa';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

async function envio()
{
    var register = new Object();
    register.Empresa_Nombre=$("#Empresa_Nombre").val();
    register.Empresa_Razon_Social=$("#Empresa_Razon_Social").val();
    register.Empresa_RFC=$("#Empresa_RFC").val().toUpperCase();
    register.Empresa_Domicilio=$("#Empresa_Domicilio").val();
    register.Empresa_Logo =getBase64Image(document.getElementById("img"), 125, 150);//var Empresa_Logo = getBase64Image(document.getElementById("img"),600,600);
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/empresas`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            LoadEmpresas();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register = new Object();
    register.Empresa_Nombre=$("#Empresa_Nombre").val();
    register.Empresa_Razon_Social=$("#Empresa_Razon_Social").val();
    register.Empresa_RFC=$("#Empresa_RFC").val().toUpperCase();
    register.Empresa_Domicilio=$("#Empresa_Domicilio").val();
    register.Empresa_Logo =getBase64Image(document.getElementById("img"), 125, 150);//var Empresa_Logo = getBase64Image(document.getElementById("img"),600,600);
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/empresas/${$("#Uid_Empresa").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadEmpresas();
            toastr.success(response.Message);
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Empresa",
        text: "Esta seguro de eliminar la empresa?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/empresas/${data.Uid_Empresa}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadEmpresas();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function getBase64Image(img, wantedWidth, wantedHeight) {
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    var ratio=1;
    ratio=0;
    var width = img.width;    // Current image width
    var height = img.height;

    if(width > height)
    {
        ratio =  wantedWidth / width ;
        wantedHeight=height*ratio;
    }
    else if(height > width)
    {
        ratio = wantedHeight/height;
        wantedWidth=width*ratio;
    }

    canvas.width = wantedWidth;
    canvas.height = wantedHeight;
    ctx.drawImage(img,0, 0,canvas.width ,canvas.height);
    var dataURL = canvas.toDataURL();
    return dataURL;
}
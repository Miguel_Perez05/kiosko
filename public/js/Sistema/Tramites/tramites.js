let tipoEmpleado=2;
$(document).ready(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    tipo = urlParams.get('tipo');
    var label="";
    switch (tipo)
    {
        case "contrato":
            label=`${tipo}`;
            tipoEmpleado=1;
        break;
        case "cartarecomendacion":
            label=`carta de recomendación`;
            tipoEmpleado=0;
        break;
        case "cartarenuncia":
            label=`carta de renuncia`;
        break;
        case "cartalaboral":
            label=`carta laboral`;
        break;
    }
    document.getElementById('labelTramite').innerHTML = `Solicitud de ${label}`;
    document.getElementById('Generar').innerHTML = `Generar ${label}`;

    $("#Empleado").select2({
    
        ajax: {
            url: `${main_path}/listado?type=${tipoEmpleado}`,
            data: function (params) {
                var query = {
                    term: params.term
                }
                
                return query;
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
        }
    });
});
$("#Tipo").select2();


function generaDocumento(e)
{
    var Uid_Empleado=$("#Empleado").val();
    if(Uid_Empleado!="Seleccione")
    {
        $.ajax({
            url:main_path+'/tramitesolicitado/'+Uid_Empleado+'/'+tipo,
            type:'get',
            datatype:'json',
            success:function(response)
            {
                if(response.status==false)
                    toastr.success(response.Message);
                else
                    abrirEnPestana(main_path+'/tramitesolicitado/'+Uid_Empleado+'/'+tipo);
            },
            error:function(response)
            {
                toastr.error(response.responseJSON.ResponseStatus.Message);
            }
          });
    }
    else
    {
        alert('Seleccione un empleado');
    }
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

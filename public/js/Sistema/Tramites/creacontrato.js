var empleado;
var empresa;
$(document).ready(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    empleado = urlParams.get('empleado');
    empresa = urlParams.get('empresa');

    LoadContratoFolio();
    LoadComboTiposContratos();
    LoadComboEmpresas(empresa);
    LoadComboDepartamentos();
    LoadComboPuestos();
    LoadComboRegistrosPatronales();
    LoadComboTurnos();
    LoadComboTiposNominas();
    LoadComboTipoEmpleado();
    document.getElementById('Generar').innerHTML = `Generar Contrato`;
});
$("#Tipo").select2();
$("#Empleado").select2({
    ajax: {
        url: `${main_path}/listado?type=all`,
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
    }
});

function LoadContratoFolio()
{
    $.ajax({
        url:`${main_path}/contratos/0/edit`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            var integerFolio =parseInt(response.Contrato_Folio);
            var folio=1;
            if(!isNaN(integerFolio))
                folio=integerFolio+1;
            $("#Contrato_Folio").val(folio);
            // $("#Empleado").val(empleado).trigger('change');
            // console.log(empleado);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

$('#Uid_Puesto').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadPuesto(valueSelected);
});

function LoadPuesto(Uid_Puesto)
{
    $.ajax({
        url:`${main_path}/puestos/${Uid_Puesto}/edit`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Contrato_Salario").val(response.Puesto_Sueldo_Min);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function generaDocumento()
{
    var errors=validator();
    if(errors!="")
    {
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var Uid_Empleado=$("#Empleado").val();

    var register = new Object();
    register.Uid_Empleado=Uid_Empleado;
    register.Uid_Empresa=$("#Uid_Empresa").val();
    register.Uid_RegistroPatronal=$("#Uid_RegistroPatronal").val();
    register.Uid_Departamento=$("#Uid_Departamento").val();
    register.Uid_Puesto=$("#Uid_Puesto").val();
    register.Uid_TipoEmpleado=$("#Uid_TipoEmpleado").val();
    register.Id_TipoContrato=$("#Id_TipoContrato").val();
    register.Contrato_Inicio=$("#Contrato_Inicio").val();
    // register.Contrato_Fin=$("#Contrato_Fin").val();
    register.Contrato_Salario=$("#Contrato_Salario").val();
    register.Contrato_Folio=$("#Contrato_Folio").val();
    register.Uid_Turno=$("#Uid_Turno").val();
    register.Uid_TipoNomina=$("#Uid_TipoNomina").val();
    register._token=$(".token").val();

    // console.log(register);
    $.ajax({
        url:`${main_path}/contratos`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response)
        {
            toastr.success(response.Message);
            abrirEnPestana(`${main_path}/tramitesolicitado/${Uid_Empleado}/contrato`);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function editEmployee() {
    if($("#Empleado").val()!=null)
    {
        $.ajax({
            url:`${main_path}/empleado/${$("#Empleado").val()}/usuario`,
            type:'get',
            datatype:'json',
            success:function(response)
            {
                console.log(response.Uid_Usuario);
                var a = document.createElement("a");
                a.target = "_blank";
                a.href = `${main_path}/empleados/${response.Uid_Usuario}/edit`;
                a.click();
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message)
            }
        });

    }
}

function validator()
{
    $(`select`).css("border", "1px solid black");
    $(`:input`).css("border", "1px solid black");
    var Message="";
    if($("#Empleado").val()==null)
    {
        Message+="<li type= circle>Es necesario especificar el Empleado</li>";
        $(`#Empleado`).css("border", "2px solid red");
    }
    if($("#Uid_TipoEmpleado").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado el Tipo de Empleado</li>";
        $(`#Uid_TipoEmpleado`).css("border", "2px solid red");
    }
    if($("#Uid_Empresa").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado la Empresa</li>";
        $(`#Uid_Empresa`).css("border", "2px solid red");
    }
    if($("#Uid_RegistroPatronal").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado el Registro Patronal</li>";
        $(`#Uid_RegistroPatronal`).css("border", "2px solid red");
    }
    if($("#Id_TipoContrato").val()==0)
    {
        Message+="<li type= circle>Es necesario especificar el Tipo Contrato</li>";
        $(`#Id_TipoContrato`).css("border", "2px solid red");
    }
    if($("#Uid_TipoNomina").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado el Tipo de Nomina</li>";
        $(`#Uid_TipoNomina`).css("border", "2px solid red");
    }
    if($("#Uid_Turno").val()==0)
    {
        Message+="<li type= circle>Es necesario especificar el Turno</li>";
        $(`#Uid_Turno`).css("border", "2px solid red");
    }
    if($("#Uid_Departamento").val()==0)
    {
        Message+="<li type= circle>Es necesario especificar el Departamento</li>";
        $(`#Uid_Departamento`).css("border", "2px solid red");
    }
    if($("#Uid_Puesto").val()==0)
    {
        Message+="<li type= circle>No ha seleccionado el Puesto</li>";
        $(`#Uid_Puesto`).css("border", "2px solid red");
    }
    if($("#Contrato_Salario").val()=="")
    {
        Message+="<li type= circle>Es necesario especificar el Sueldo</li>";
        $(`#Contrato_Salario`).css("border", "2px solid red");
    }
    return Message;
}

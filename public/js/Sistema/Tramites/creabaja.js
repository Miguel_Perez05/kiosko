
$(document).ready(function(){
    document.getElementById('Generar').innerHTML = `Crea Baja`;
    document.getElementById("Recontratable_Causa").readOnly = true;
    $("#Recontratable").prop("checked", true);
});
$("#Empleado").select2({
    ajax: {
        url: `${main_path}/listado?type=2`,
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
    }
});

// $('#Recontratable').change(function () {
//     document.getElementById("Recontratable_Causa").readOnly = false;
//     $("#Recontratable_Causa").val("");
//     if($(this).prop('checked') == true)
//     {
//         document.getElementById("Recontratable_Causa").readOnly = true;
//     }
// });

function generaDocumento()
{
    var Uid_Empleado=$("#Empleado").val();
    if(Uid_Empleado!="Seleccione")
    {
        var register = new Object();
        register.Uid_Empleado=Uid_Empleado;
        register.Baja_Causa=$("#Baja_Causa").val();
        register.AplicaBaja=Number(document.getElementById("AplicaBaja").checked);
        register.Recontratable=1;
        register.Baja_Comentario=$("#Baja_Comentario").val();
        register.Recontratable_Causa=$("#Baja_Comentario").val();
        register._token=$(".token").val();

        $.ajax({
            url:`${main_path}/empleados/baja`,
            data:register,
            type:'post',
            datatype:'json',
            success:function(response)
            {
                toastr.success(response.Message);
                if($("#Baja_Causa").val()=="Voluntario")
                    abrirEnPestana(`${main_path}/tramitesolicitado/${Uid_Empleado}/cartarenuncia`);
            },
            error:function(response)
            {
                toastr.error(response.responseJSON.ResponseStatus.Message);
            }
          });
    }
    else
    {
        alert('Seleccione un empleado');
    }
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

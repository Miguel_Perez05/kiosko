$(function() {
    $("#Ejercicio_Nombre").datepicker({ dateFormat: 'yy' });
});

function editar(e){
    var id=$(e).data('id');
    console.log(e);
    $.ajax({
    url:main_path+'/ejercicios/'+id+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        console.log(response);
        $("#Ejercicio_Nombre").val(response.Ejercicio_Nombre);
        $("#Ejercicio_Estado").val(response.Ejercicio_Estado);
        $("#Uid_Ejercicio").val(response.Uid_Ejercicio);
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Ejercicio';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#Ejercicio_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Ejercicio';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var Ejercicio_Nombre=$("#Ejercicio_Nombre").val();
    var Ejercicio_Estado=$("#Ejercicio_Estado").val();
    var token=$(".token").val();
    var registro = {
        'Ejercicio_Nombre':Ejercicio_Nombre,
        'Ejercicio_Estado':Ejercicio_Estado,
        '_token':token
    };
    $.ajax({
        url:main_path+'/ejercicios',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function edicion(e)
{
    var Ejercicio_Nombre=$("#Ejercicio_Nombre").val();
    var Ejercicio_Estado=$("#Ejercicio_Estado").val();
    var Uid_Ejercicio=$("#Uid_Ejercicio").val();
    var token=$(".token").val();
    var registro = {
        'Ejercicio_Nombre':Ejercicio_Nombre,
        'Ejercicio_Estado':Ejercicio_Estado,
        '_token':token
    };
    $.ajax({
        url:main_path+'/ejercicios/'+Uid_Ejercicio,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/ejercicios/';
    });
}

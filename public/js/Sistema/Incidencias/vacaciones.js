var type;
var typeDescription;
var table;
$(document).ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    type=pathArray[3];
    table =$('#table').DataTable({
        "columns":[
            {"data": "TipoIncidencia_Nombre"},
            {"data": null,render:function (data) {
                return `${data.Empleado_Nombre} ${data.Empleado_APaterno} ${data.Empleado_AMaterno}`;
            }},
            {"data": "Incidencia_DiaInicio"},
            {"data": "Incidencia_DiaFin"},
            {"data": null,render:function (data) {
                return getBusinessDatesCount(new Date(data.Incidencia_DiaInicio),new Date(data.Incidencia_DiaFin));
            }},
            {"data": null,render:function (data) {
                if(data.Id_Estatus>0 && !data.Incidencia_FechaJustificacion)
                    return "Pendiente de Autorizar";                
                else if(data.Id_Estatus==0)
                    return "Rechazada";
                else
                    return "Autorizada";
            }},
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                        <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">`;
                    divButtons+=`<a class="dropdown-item Comments"><i class="fa fa-pencil"></i> Comentarios</a>`;
                    if(data.Id_Estatus>0 && !data.Incidencia_FechaJustificacion)
                        divButtons+=`<a class="dropdown-item Reject"><i class="fa fa-trash"></i> Rechazar</a>`;
                    if(!data.Incidencia_FechaJustificacion)
                        divButtons+=`<a class="dropdown-item Authorize"><i class="fa fa-check"></i> Autorizar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        // dom: 'Bfrtip',
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    LoadComboEmpleados();
    LoadIncidencias();

    $('#table tbody').on( 'click', '.Reject', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,"Rechazar", "Rechazo");
    });

    $('#table tbody').on( 'click', '.Authorize', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,"Autorizar", "Autorización");
    });
    
    $('#table tbody').on( 'click', '.Comments', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        comentarios(data.Uid_Incidencia);
    });

});

function calculateDays(d1, d2){
    var bd= 0, dd, incr=d1.getDate();
    // console.log(incr);
    while(d1<d2){
        d1.setDate(++incr);
        dd= d1.getDay();
        if(dd%6)++bd;
    }
    return bd;
}

function getBusinessDatesCount(startDate, endDate) {
    var elapsed, daysBeforeFirstSaturday, daysAfterLastSunday;
    var ifThen = function (a, b, c) {
        return a == b ? c : a;
    };

    elapsed = endDate - startDate;
    elapsed /= 86400000;

    daysBeforeFirstSunday = (7 - startDate.getDay()) % 7;
    daysAfterLastSunday = endDate.getDay();

    elapsed -= (daysBeforeFirstSunday + daysAfterLastSunday);
    elapsed = (elapsed / 7) * 5;
    elapsed += ifThen(daysBeforeFirstSunday - 1, -1, 0) + ifThen(daysAfterLastSunday, 6, 5);

    return Math.ceil(elapsed);
}

function LoadIncidencias()
{
    $.ajax({
        url:`${main_path}/incidencias/${type}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function comentarios(Uid_Incidencia){
    $.ajax({
        url:`${main_path}/incidencias/${Uid_Incidencia}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Incidencia_Comentarios").val(response.Incidencia_Comentario);
            document.getElementById('LabelModalComentarios').innerHTML = 'Comentarios de las Vacaciones';
            document.getElementById('Justificado').style.display = 'none';

            if(response.Incidencia_FechaJustificacion)
            {
                var Incidencia_QuienJustifica='';
                if(response.Uid_Empleado)
                {
                    Incidencia_QuienJustifica=response.Empleado_Nombre + " " + response.Empleado_APaterno;
                }
                else{
                    Incidencia_QuienJustifica=response.Usuario_NickName;
                }
                document.getElementById('Justificado').style.display = 'block';
                $("#Incidencia_QuienJustifica").val(Incidencia_QuienJustifica);
                $("#Incidencia_FechaJustificacion").val(response.Incidencia_FechaJustificacion);
                $("#Incidencia_ComentarioJustifica").val(response.Incidencia_ComentarioJustifica);
            }
            $("#modal_Comentarios").modal("show");
            document.getElementById("Incidencia_Comentarios").disabled=true;
            document.getElementById("Incidencia_QuienJustifica").disabled=true;
            document.getElementById("Incidencia_FechaJustificacion").disabled=true;
            document.getElementById("Incidencia_ComentarioJustifica").disabled=true;
            document.getElementById('BTN_Justificar').style.display = 'none';
        }
    });
}

$('.NewVacation').click(function(){
    window.location.href = `${main_path}/incidencias/nominas/3/create`;
});

function confirmStatus(data, verb, description)
{
    swal({
        title: description,
        text: `Esta seguro de ${verb} el registro?"`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            if(description=="Autorización")
                Autorizacion(data)
            else if(description=="Rechazo")
                Status(data);            
        }
    });
}

function Status(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/incidencias/${data.Uid_Incidencia}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadIncidencias();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function Autorizacion(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/incidencia/${data.Uid_Incidencia}/autorizacion`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadIncidencias();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}
var type;
$(document).ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    type=pathArray[3];
    $(".Next").hide();
});

$("#Uid_Empleado").select2({
    ajax: {
        url: `${main_path}/listado?type=2`,
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
    }
});

$('.Save').click(function(){
    var register=new Object();
    register.Incidencia_Comentario=$("#Incidencia_Comentario").val();
    register.Incidencia_Tipo=type;
    register.Uid_Empleado=$("#Uid_Empleado").val();
    register.Incidencia_DiaInicio=$("#Incidencia_DiaInicio").val();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/incidencias`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            $(".Next").show();
            $(".Save").hide();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
});

$('.Next').click(function(){
    window.location.href = `${main_path}/incidencias/nominas/${type}`;
});
var table;
$('document').ready(function(){
    if(session)
        bajaBtn=`<a class="dropdown-item Baja" ><i class="fa fa-pencil"></i> Aplicar</a>`;
    cartaBtn=`<a class="dropdown-item Carta" ><i class="fa fa-pencil"></i> Carta renuncia</a>`;
    
    table =$('#table').DataTable({
        "columns":[
            {"data": null,render:function(data){
                return `${data.Empleado_Nombre} ${data.Empleado_APaterno} ${data.Empleado_AMaterno}`
            }},
            {"data": "Id_Estatus",render:function (data) {
                if(data==1)
                    return "Pendiente de Aplicar";
                else if(data==2)
                    return "Baja Aplicada";
                return "";
            }},
            {"data": "created_at", render:function(data){
                var date=new Date(data);
                    return date.toLocaleDateString("en-US");
            }},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Details"><i class="fa fa-pencil"></i> Detalles</a>
                                ${data.Id_Estatus==1?bajaBtn:''}
                                ${data.Id_Estatus==1?cartaBtn:''}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Baja', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmBaja(data.Uid_Baja)
    });

    $('#table tbody').on( 'click', '.Details', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        details(data);
    });

    $('#table tbody').on( 'click', '.Carta', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        generaCarta(data.Uid_Empleado);
    });

    LoadBajas();  
});

function LoadBajas()
{

    $.ajax({
        url:`${main_path}/bajas/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function details(data)
{    
    $("#Baja_Causa").val(data.Baja_Causa);
    $("#Baja_Comentario").val(data.Baja_Comentario);
    $("#modal").modal("show");
}


function confirmBaja(Uid_Baja)
{
    swal({
        title: "Aplicar Baja",
        text: "Esta seguro de aplicar la baja?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            recontratable(Uid_Baja);        
        }
    });
}
function recontratable(Uid_Baja){
    swal({
        title: "Recontratable",
        text: "Es recontratable?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        causaBaja(Uid_Baja, value==true?1:0)
    });
}

function causaBaja(Uid_Baja, recontratable)
{
    swal("Escribe la causa:", {
        content: "input",
      })
      .then((value) => {
        if (value === false || value === "") {
            errorCausa(Uid_Baja, recontratable);
          
        }
            Cancel(Uid_Baja, value, recontratable);  
      });
}

function errorCausa(Uid_Baja, recontratable)
{
    swal("Es necesario que indique la causa de la baja!")
    .then((value) => {
        causaBaja(Uid_Baja, recontratable);
    });
}

function Cancel(Uid_Baja, causa, recontratable)
{
    // console.log(data); return;
    var register = new Object();
    register._token=$(".token").val();
    register.Recontratable_Estatus=recontratable;
    register.Recontratable_Causa=causa;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/bajas/${Uid_Baja}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            LoadBajas();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function generaCarta(Uid_Empleado)
{
    $.ajax({
        url:`${main_path}/tramitesolicitado/${Uid_Empleado}/cartarenuncia`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            abrirEnPestana(`${main_path}/tramitesolicitado/${Uid_Empleado}/cartarenuncia`);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
        });
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}
var Uid;
$('document').ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    Uid=pathArray[2];

    LoadPercepciones();
});

function LoadPercepciones()
{
    $.ajax({
        url:`${main_path}/percepcionesdeducciones/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var date  = new Date(element.PercepcionDeduccion_Fecha);
                register+=`<div class="profile-timeline-list">
                                <ul>
                                    <li>
                                        <br>
                                        <div class="date">${date.toLocaleDateString("en-US")} </div>
                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> ${element.TipoBono_Nombre}</div>
                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> ${element.PercepcionDeduccion_Comentario??""}</div>
                                        <p>
                                            $ ${element.PercepcionDeduccion_Monto}
                                        </p>
                                    </li>
                                </ul>
                            </div>`;
            });
            document.getElementById('percepciones').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

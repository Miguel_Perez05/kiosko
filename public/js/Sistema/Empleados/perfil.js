var Uid;
$('document').ready(function(){

    LoadComboEstadosCiviles();
    LoadComboNacionalidades();
    LoadComboRegistrosPatronales();
    LoadComboEmpresas();
    LoadComboCarreras();
    LoadComboEscuelas();
    LoadComboPuestos();
    LoadComboIdiomas();
    LoadComboTipoEmpleado();
    LoadComboTiposNominas();
    LoadComboTiposBonos();
    LoadComboAreasFormacion();
    LoadComboTurnos();
    LoadComboDepartamentos();
    LoadComboTiposNominas();

    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    Uid=pathArray[2];
    $("#Uid_Usuario").val(Uid);
    LoadEmpleados();
    bloqueaCampos();
});

function LoadEmpleados()
{
    $.ajax({
        url:`${main_path}/empleados/show/${Uid}/perfil`,
        type:'get',
        datatype:'json',
        success:function(response){

            document.getElementById("image").src=response.Avatar;
            document.getElementById("img").src=response.Avatar;

            // $("#Uid_Empleado").val(response.Uid_Empleado);


            document.getElementById('label_Nombre').innerHTML = response.Usuario_Nombre;
            document.getElementById('label_e-mail').innerHTML = response.Correo;
            document.getElementById('label_telefono').innerHTML = response.Telefono;
            document.getElementById('label_departamento').innerHTML = response.Departamento;
            document.getElementById('label_puesto').innerHTML = response.Puesto;
            // if(response.Id_Estatus==1)
            //     document.getElementById("Id_Estatus").checked = true;

            //------------------------   Personal   --------------------------//
            $("#Empleado_Nombre").val(response.Empleado_Nombre);
            $("#Empleado_AMaterno").val(response.Empleado_AMaterno);
            $("#Empleado_APaterno").val(response.Empleado_APaterno);
            $("#Empleado_Genero").val(response.Empleado_Genero);//Empleado_Correo
            $("#Empleado_RFC").val(response.Empleado_RFC);
            $("#Empleado_CURP").val(response.Empleado_CURP);
            $("#Empleado_Telefono").val(response.Empleado_Telefono);
            $("#Empleado_NSS").val(response.Empleado_NSS);
            $("#Uid_Nacionalidad").val(response.Uid_Nacionalidad);
            $("#Uid_EstadoCivil").val(response.Uid_EstadoCivil);
            $("#Empleado_Fecha_Nacimiento").val(response.Empleado_Fecha_Nacimiento);

            //-----------------------   Contactos   ---------------------------//
            $("#Empleado_Contacto1_Emergencia").val(response.Empleado_Contacto1_Emergencia);
            $("#Empleado_Telefono1_Emergencia").val(response.Empleado_Telefono1_Emergencia);
            $("#Empleado_Domicilio1_Emergencia").val(response.Empleado_Domicilio1_Emergencia);
            $("#Empleado_Contacto2_Emergencia").val(response.Empleado_Contacto2_Emergencia);
            $("#Empleado_Telefono2_Emergencia").val(response.Empleado_Telefono2_Emergencia);
            $("#Empleado_Domicilio2_Emergencia").val(response.Empleado_Domicilio2_Emergencia);
            // $("#Empleado_Contacto3_Emergencia").val(response.Empleado_Contacto3_Emergencia);
            // $("#Empleado_Telefono3_Emergencia").val(response.Empleado_Telefono3_Emergencia);
            // $("#Empleado_Domicilio3_Emergencia").val(response.Empleado_Domicilio3_Emergencia);

            //-----------------------   Usuario   ---------------------------//
            $("#Usuario_Correo_").val(response.Usuario_Correo);
            $("#Usuario_Correo_E").val(response.Usuario_Correo);
            $("#Usuario_Apodo_").val(response.Usuario_NickName);
            $("#Usuario_Apodo_E").val(response.Usuario_NickName);

            //------------------------   Domicilio   --------------------------//
            $("#Empleado_Calle").val(response.Empleado_Calle);
            $("#Empleado_Numero").val(response.Empleado_Numero);
            $("#Empleado_Colonia").val(response.Empleado_Colonia);
            $("#Empleado_CP").val(response.Empleado_CP);

            //------------------------   Identidad   --------------------------//
            $("#Uid_Empresa").val(response.Uid_Empresa);
            $("#Uid_RegistroPatronal").val(response.Uid_RegistroPatronal);

            //------------------------   Puesto   --------------------------//
            $("#Uid_Puesto").val(response.Uid_Puesto);
            $("#Uid_Turno").val(response.Uid_Turno);
            $("#Uid_Departamento").val(response.Uid_Departamento);

            $("#Uid_Jefe_Inmediato").val(response.Uid_Jefe_Inmediato);

            $("#Empleado_Fecha_Ingreso").val(response.Fecha_Ingreso);

            $("#Empleado_Fecha_ReIngreso").val(response.Fecha_Reingreso);

            //------------------------   Compensación   --------------------------//

            $("#Empleado_SalarioMensual").val(currencyFormat(response.Sueldo_Monto));
            $("#Sueldo_FechaC").val(response.Sueldo_Fecha);

            $("#Uid_TipoEmpleado").val(response.Uid_TipoEmpleado);
            $("#Uid_TipoNomina").val(response.Uid_TipoNomina);

            LoadComboPaises(response.Id_Pais);
            LoadComboEstados(response.Id_Pais, response.Id_Estado);
            LoadComboCiudades(response.Id_Pais, response.Id_Estado, response.Id_Ciudad);

        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function currencyFormat(num) {
    return '$' + parseFloat(num).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function bloqueaCampos()
{
    document.getElementById('divCorreo').style.display = 'none';
    document.getElementById('encabezado').style.display = 'block';
    document.getElementById("datos").className = "col-xl-9 col-lg-8 col-md-8 col-sm-12 mb-30";

    document.getElementById('tab_Usuario').style.display = 'none';

    //------------------------   Personal   --------------------------//
    document.getElementById('Empleado_Nombre').readOnly = true;
    document.getElementById('Empleado_AMaterno').readOnly = true;
    document.getElementById('Empleado_APaterno').readOnly = true;
    document.getElementById('Empleado_Genero').disabled = true;
    document.getElementById('Empleado_RFC').readOnly = true;
    document.getElementById('Empleado_CURP').readOnly = true;
    document.getElementById('Empleado_Telefono').readOnly = true;
    document.getElementById('Empleado_NSS').readOnly = true;
    document.getElementById('Uid_Nacionalidad').disabled = true;
    document.getElementById('Uid_EstadoCivil').disabled = true;
    document.getElementById('Empleado_Fecha_Nacimiento').readOnly = true;
    //-----------------------   Botones   ---------------------------//
    document.getElementById('BTN_Personal_Edicion').style.display = 'none';
    document.getElementById('BTN_Personal_Guardar').style.display = 'none';
    document.getElementById('BTN_Personal_Cancelar').style.display = 'none';

    //-----------------------   Contactos   ---------------------------//
    document.getElementById('Empleado_Contacto1_Emergencia').readOnly = true;
    document.getElementById('Empleado_Telefono1_Emergencia').readOnly = true;
    document.getElementById('Empleado_Domicilio1_Emergencia').readOnly = true;
    document.getElementById('Empleado_Contacto2_Emergencia').readOnly = true;
    document.getElementById('Empleado_Telefono2_Emergencia').readOnly = true;
    document.getElementById('Empleado_Domicilio2_Emergencia').readOnly = true;
    //-----------------------   Botones   ---------------------------//
    document.getElementById('BTN_Contactos_Guardar').style.display = 'none';
    document.getElementById('BTN_Contactos_Cancelar').style.display = 'none';

    //------------------------   Domicilio   --------------------------//
    document.getElementById('Empleado_Calle').readOnly = true;
    document.getElementById('Empleado_Numero').readOnly = true;
    document.getElementById('Empleado_Colonia').readOnly = true;
    document.getElementById('Empleado_CP').disabled = true;
    document.getElementById('Id_Pais').disabled = true;
    document.getElementById('Id_Estado').disabled = true;
    document.getElementById('Id_Ciudad').disabled = true;
    //-----------------------   Botones   ---------------------------//
    document.getElementById('BTN_Domicilio_Edicion').style.display = 'none';
    document.getElementById('BTN_Domicilio_Guardar').style.display = 'none';
    document.getElementById('BTN_Domicilio_Cancelar').style.display = 'none';

    //-----------------------   Usuario   ---------------------------//
    document.getElementById('Usuario_Correo_E').readOnly = true;
    document.getElementById('Usuario_Apodo_E').readOnly = true;
    //-----------------------   Botones   ---------------------------//
    document.getElementById('BTN_Usuario_Guardar').style.display = 'none';
    document.getElementById('BTN_Usuario_Cancelar').style.display = 'none';

    //------------------------   Identidad   --------------------------//
    document.getElementById('Uid_RegistroPatronal').disabled = true;
    document.getElementById('Uid_Empresa').disabled = true;

    document.getElementById('BTN_Identidad_Edicion').style.display = 'none';
    document.getElementById('BTN_Identidad_Guardar').style.display = 'none';
    document.getElementById('BTN_Identidad_Cancelar').style.display = 'none';

    //------------------------   Puesto   --------------------------//
    document.getElementById('Uid_Puesto').disabled = true;
    document.getElementById('Uid_Turno').disabled = true;
    document.getElementById('Uid_Departamento').disabled = true;
    document.getElementById('Uid_TipoEmpleado').disabled = true;
    document.getElementById('Uid_TipoNomina').disabled = true;
    document.getElementById('Sueldo_Fecha').disabled = true;
    //-----------------------   Botones   ---------------------------//
    document.getElementById('BTN_Puesto_Edicion').style.display = 'none';
    document.getElementById('BTN_Puesto_Guardar').style.display = 'none';
    document.getElementById('BTN_Puesto_Cancelar').style.display = 'none';

    //document.getElementById('Uid_Jefe_Inmediato').disabled = true;
    document.getElementById('Empleado_Fecha_Ingreso').readOnly = true;
    document.getElementById('Empleado_Fecha_ReIngreso').readOnly = true;
    // document.getElementById('Id_Estatus').disabled = true;//BTN_Contactos_Guardar
    document.getElementById('Empleado_SalarioMensual').readOnly = true;//BTN_Contactos_Guardar
    document.getElementById('Sueldo_FechaC').readOnly = true;//BTN_Contactos_Guardar

    //------------------------   Botones   --------------------------//

    document.getElementById('BTN_Personal_Crear').style.display = 'none';
    document.getElementById('Btn_ActualizaSueldo').style.display = 'none';

    document.getElementById('BTN_Compensacion_Edicion').style.display = 'none';
    document.getElementById('BTN_Compensacion_Guardar').style.display = 'none';
    document.getElementById('BTN_Compensacion_Cancelar').style.display = 'none';

    document.getElementById('Btn_Idiomas').style.display = 'none';
    document.getElementById('Btn_Entrenamiento').style.display = 'none';
    document.getElementById('Btn_Reconocimientos').style.display = 'none';
    // document.getElementById('BTN_Baja').style.display = 'none';

    // document.getElementById("Id_Estatus").checked = false;


    if(perfilUsuario)
    {
        document.getElementById('BTN_Personal_Edicion').style.display = 'block';
        document.getElementById('BTN_Domicilio_Edicion').style.display = 'block';
        document.getElementById('BTN_Identidad_Edicion').style.display = 'block';
        document.getElementById('BTN_Compensacion_Edicion').style.display = 'block';
        document.getElementById('BTN_Puesto_Edicion').style.display = 'block';
        document.getElementById('Btn_Idiomas').style.display = 'block';
        document.getElementById('Btn_Entrenamiento').style.display = 'block';
        document.getElementById('Btn_Reconocimientos').style.display = 'block';
        // document.getElementById('BTN_Baja').style.display = 'block';
        document.getElementById('Btn_ActualizaSueldo').style.display = 'block';
    }
    if(jefe)
    {
        document.getElementById('Btn_Idiomas').style.display = 'block';
        document.getElementById('Btn_Entrenamiento').style.display = 'block';
        document.getElementById('Btn_Reconocimientos').style.display = 'block';
    }

    if($("#Uid_Usuario").val()==usuarioUid)
    {
        document.getElementById('tab_Usuario').style.display = 'block';
        document.getElementById('BTN_Contactos_Edicion').style.display = 'block';
        document.getElementById('BTN_Usuario_Edicion').style.display = 'block';
        // document.getElementById('BTN_Baja').style.display = 'none';
    }
}

$('#Id_Pais').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadComboEstados(valueSelected);
});

$('#Id_Estado').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadComboCiudades($('#Id_Pais').val(),valueSelected)
});

function LoadCiudades()
{
    var Id_Estado= document.getElementById("Id_Estado").value;
    var Id_Pais= document.getElementById("Id_Pais").value;
    $.ajax({
        url:`${main_path}/ciudades/0?Id_Pais=${Id_Pais}&Id_Estado=${Id_Estado}`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function perfil(e){
    var Uid_Usuario=$(e).data('id');
    window.location.href = `${main_path}/empleados/${Uid_Usuario}/edit`;
}

function EdicionPersonal(){
    document.getElementById('Empleado_Nombre').readOnly = false;
    document.getElementById('Empleado_AMaterno').readOnly = false;
    document.getElementById('Empleado_APaterno').readOnly = false;
    document.getElementById('Empleado_Genero').disabled = false;
    document.getElementById('Empleado_RFC').readOnly = false;
    document.getElementById('Empleado_CURP').readOnly = false;
    document.getElementById('Empleado_Telefono').readOnly = false;
    document.getElementById('Empleado_NSS').readOnly = false;
    document.getElementById('Uid_Nacionalidad').disabled = false;
    document.getElementById('Uid_EstadoCivil').disabled = false;
    document.getElementById('Empleado_Fecha_Nacimiento').readOnly = false;

    document.getElementById('BTN_Personal_Edicion').style.display = 'none';
    document.getElementById('BTN_Personal_Guardar').style.display = 'block';
    document.getElementById('BTN_Personal_Cancelar').style.display = 'block';
}

function EdicionContactos(){
    document.getElementById('Empleado_Contacto1_Emergencia').readOnly = false;
    document.getElementById('Empleado_Telefono1_Emergencia').readOnly = false;
    document.getElementById('Empleado_Domicilio1_Emergencia').readOnly = false;
    document.getElementById('Empleado_Contacto2_Emergencia').readOnly = false;
    document.getElementById('Empleado_Telefono2_Emergencia').readOnly = false;
    document.getElementById('Empleado_Domicilio2_Emergencia').readOnly = false;
    // document.getElementById('Empleado_Contacto3_Emergencia').readOnly = false;
    // document.getElementById('Empleado_Telefono3_Emergencia').readOnly = false;
    // document.getElementById('Empleado_Domicilio3_Emergencia').readOnly = false;

    document.getElementById('BTN_Contactos_Edicion').style.display = 'none';
    document.getElementById('BTN_Contactos_Guardar').style.display = 'block';
    document.getElementById('BTN_Contactos_Cancelar').style.display = 'block';
}

function EdicionUsuario(){
    document.getElementById('Usuario_Correo_').readOnly = false;
    document.getElementById('Usuario_Correo_E').readOnly = false;
    document.getElementById('Usuario_Apodo_').readOnly = false;
    document.getElementById('Usuario_Apodo_E').readOnly = false;

    document.getElementById('BTN_Usuario_Edicion').style.display = 'none';
    document.getElementById('BTN_Usuario_Guardar').style.display = 'block';
    document.getElementById('BTN_Usuario_Cancelar').style.display = 'block';
}

function EdicionDomicilio(){
    document.getElementById('Empleado_Calle').readOnly = false;
    document.getElementById('Empleado_Numero').readOnly = false;
    document.getElementById('Empleado_Colonia').readOnly = false;
    document.getElementById('Empleado_CP').disabled = false;
    document.getElementById('Id_Pais').disabled = false;
    document.getElementById('Id_Estado').disabled = false;
    document.getElementById('Id_Ciudad').disabled = false;

    document.getElementById('BTN_Domicilio_Edicion').style.display = 'none';
    document.getElementById('BTN_Domicilio_Guardar').style.display = 'block';
    document.getElementById('BTN_Domicilio_Cancelar').style.display = 'block';
}

function EdicionIdentidad(){
    document.getElementById('Empleado_Fecha_Ingreso').readOnly = false;
    document.getElementById('Empleado_Fecha_ReIngreso').readOnly = false;
    document.getElementById('Uid_RegistroPatronal').disabled = false;
    document.getElementById('Uid_Empresa').disabled = false;

    document.getElementById('BTN_Identidad_Edicion').style.display = 'none';
    document.getElementById('BTN_Identidad_Guardar').style.display = 'block';
    document.getElementById('BTN_Identidad_Cancelar').style.display = 'block';
}

function EdicionPuesto(){
    //document.getElementById('Uid_Puesto').disabled = false;
    document.getElementById('Uid_Turno').disabled = false;
    document.getElementById('Uid_Departamento').disabled = false;
    document.getElementById('Empleado_Fecha_Ingreso').readOnly = false;
    document.getElementById('Empleado_Fecha_ReIngreso').readOnly = false;
    document.getElementById('Uid_TipoEmpleado').disabled = false;
    document.getElementById('Uid_TipoNomina').disabled = false;

    document.getElementById('BTN_Puesto_Edicion').style.display = 'none';
    document.getElementById('BTN_Puesto_Guardar').style.display = 'block';
    document.getElementById('BTN_Puesto_Cancelar').style.display = 'block';
}

function EdicionCompensacion(){
    document.getElementById('Uid_TipoEmpleado').disabled = false;
    document.getElementById('Uid_TipoNomina').disabled = false;

    document.getElementById('BTN_Compensacion_Edicion').style.display = 'none';
    document.getElementById('BTN_Compensacion_Guardar').style.display = 'block';
    document.getElementById('BTN_Compensacion_Cancelar').style.display = 'block';
}

function CancelarEdicion(e){
    bloqueaCampos()
}

async function ActualizarAvatar()
{
    var registro =new Object();

    registro.Usuario_Avatar=getBase64Image(document.getElementById("img"),140,140);
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/updateAvatar/${$("#Uid_Usuario").val()}/${$("#Uid_Empleado").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            $("#modal").modal("hide");
            toastr.success("El avatar ha sido modificado");
            LoadEmpleados();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

async function ActualizarCorreo()
{
    var registro =new Object();

    registro.Usuario_Correo=$("#newEmail").val();
    registro._token=$(".token").val();

    $.ajax({
        url:`${main_path}/updateemail/${$("#Uid_Usuario").val()}/${$("#Uid_Empleado").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            $("#modal-mail").modal("hide");
            toastr.success("El correo ha sido modificado");
            LoadEmpleados();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

async function Baja()
{
    var registro =new Object();
    registro._token=$(".token").val();
    registro.empresa= $("#Uid_Empresa").val();
    if(registro.empresa== null)
    {
        toastr.options.escapeHtml = true;
        toastr.error(`<h5>El empleado no esta dado de alta en ninguna empresa</h5><br>`);
        return;
    }
    $.ajax({
        url:`${main_path}/empleados/baja/${$("#Uid_Usuario").val()}?Uid_Empleado=${$("#Uid_Empleado").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success("El usuario ha sido dado de baja");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

async function GuardarPersonal()
{
    var errors=validatorEmpleado();
    if(errors!="")
    {
        toastr.options.escapeHtml = true;
        toastr.error(`<ul>${errors}</ul>`);
        return;
    }

    var registro =new Object();

    registro.Uid_Usuario=$("#Uid_Usuario").val();
    registro.Uid_Empleado=$("#Uid_Empleado").val();
    registro.Empleado_Nombre=$("#Empleado_Nombre").val();
    registro.Empleado_APaterno=$("#Empleado_APaterno").val();
    registro.Empleado_AMaterno=$("#Empleado_AMaterno").val();
    registro.Empleado_Telefono=$("#Empleado_Telefono").val();
    registro.Empleado_Genero=$("#Empleado_Genero").val();
    registro.Uid_EstadoCivil=$("#Uid_EstadoCivil").val();
    registro.Empleado_NSS=$("#Empleado_NSS").val();
    registro.Empleado_CURP=$("#Empleado_CURP").val().toUpperCase();
    registro.Empleado_RFC=$("#Empleado_RFC").val().toUpperCase();
    registro.Empleado_Fecha_Nacimiento=$("#Empleado_Fecha_Nacimiento").val();
    registro.Uid_Nacionalidad=$("#Uid_Nacionalidad").val();

    put(registro);

}

async function GuardarContactos()
{
    var registro =new Object();

    registro.Empleado_Contacto1_Emergencia=$("#Empleado_Contacto1_Emergencia").val();
    registro.Empleado_Telefono1_Emergencia=$("#Empleado_Telefono1_Emergencia").val();
    registro.Empleado_Domicilio1_Emergencia=$("#Empleado_Domicilio1_Emergencia").val();
    registro.Empleado_Contacto2_Emergencia=$("#Empleado_Contacto2_Emergencia").val();
    registro.Empleado_Telefono2_Emergencia=$("#Empleado_Telefono2_Emergencia").val();
    registro.Empleado_Domicilio2_Emergencia=$("#Empleado_Domicilio2_Emergencia").val();
    // registro.Empleado_Contacto3_Emergencia=$("#Empleado_Contacto3_Emergencia").val();
    // registro.Empleado_Telefono3_Emergencia=$("#Empleado_Telefono3_Emergencia").val();
    // registro.Empleado_Domicilio3_Emergencia=$("#Empleado_Domicilio3_Emergencia").val();
    put(registro);
}

function GuardarUsuario()
{
    var registro =new Object();

    if($("#Usuario_Correo_").val() != $("#Usuario_Correo_E").val())
        registro.Usuario_Correo=$("#Usuario_Correo_E").val();
    if($("#Usuario_Apodo_").val() != $("#Usuario_Apodo_E").val())
        registro.Usuario_Apodo=$("#Usuario_Apodo_E").val();
    if(registro!=null)
    {
        registro._token=$(".token").val();
        $.ajax({
            url:`${main_path}/usuarios/${$("#Uid_Usuario").val()}`,
            data:registro,
            type:'put',
            datatype:'json',
            success:function(response){
                toastr.success("Los datos de usuario han sido modificados");
                bloqueaCampos();
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message);
            },
        });
    }
}

function PasswordUsuario()
{
    var registro =new Object();
    if($("#Password").val()!=$("#Password1").val() )
    {
        toastr.error("Las contraseñas deben coincidir");
        return;
    }
    registro.Usuario_Password_=$("#Password").val();
    registro.Usuario_Password=$("#Password1").val();
    registro.Uid_Usuario=$("#Uid_Usuario").val();
    registro._token=$(".token").val();
    $.ajax({
        url:`${main_path}/updatepassword`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success("Password modificado");
            bloqueaCampos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

async function GuardarDomicilio()
{
    var registro =new Object();

    registro.Empleado_Calle=$("#Empleado_Calle").val();
    registro.Empleado_Numero=$("#Empleado_Numero").val();
    registro.Empleado_Colonia=$("#Empleado_Colonia").val();
    registro.Empleado_CP=$("#Empleado_CP").val();
    registro.Id_Pais=$("#Id_Pais").val();
    registro.Id_Estado=$("#Id_Estado").val();
    registro.Id_Ciudad=$("#Id_Ciudad").val();

    put(registro);
}

async function GuardarIdentidad()
{
    var registro =new Object();

    registro.Uid_Empleado=$("#Uid_Empleado").val();
    registro.Uid_Empresa=$("#Uid_Empresa").val();
    registro.Uid_RegistroPatronal=$("#Uid_RegistroPatronal").val();

    put(registro);
}

async function GuardarPuesto()
{
    var registro =new Object();

    registro.Uid_Turno=$("#Uid_Turno").val();
    registro.Uid_Departamento=$("#Uid_Departamento").val();

    put(registro);
}

async function GuardarCompensacion()
{
    var registro =new Object();
    registro.Uid_TipoEmpleado=$("#Uid_TipoEmpleado").val();
    registro.Uid_TipoNomina=$("#Uid_TipoNomina").val();

    put(registro);
}

function put(registro)
{
    registro._token=$(".token").val();
    $.ajax({
        url:`${main_path}/empleados/actualiza/${$("#Uid_Empleado").val()}?Uid_Usuario=${$("#Uid_Usuario").val()}`,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success("Empleado editado");
            bloqueaCampos();
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function success(response)
{
    var html=response.html ? response.html : false;
    swal({
        html:html,
        title: response.Accion,
        text: response.message,
        type: response.status,
        showCancelButton: false,
        closeOnConfirm: true,
        showLoaderOnConfirm: true
    },
    function ()
    {
        if(response.status=='success')
        {
            if(response.logout=='1')
            {
                window.location.href = `${main_path}/logout`;
            }
            // else
            //     window.location.href = main_path+'/empleados/'+response.Uid_Empleado+'/edit';
        }
    });
}

function getBase64Image(img, wantedWidth, wantedHeight) {
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    var ratio=1;
    ratio=0;
    var width = img.width;    // Current image width
    var height = img.height;

    if(width > height)
    {
        ratio =  wantedWidth / width ;
        wantedHeight=height*ratio;
    }
    else if(height > width)
    {
        ratio = wantedHeight/height;
        wantedWidth=width*ratio;
    }

    canvas.width = wantedWidth;
    canvas.height = wantedHeight;
    ctx.drawImage(img,0, 0,canvas.width ,canvas.height);
    var dataURL = canvas.toDataURL();
    return dataURL;
}

function validatorEmpleado()
{
    var message="";
    if($("#Empleado_Nombre").val()<1)
    {
        message+="<li type= circle>Es necesario indicar el nombre del Empleado</li>";
        $(`#Empleado_Nombre`).css("border", "2px solid red");
    }
    else if($("#Empleado_APaterno").val()=="")
    {
        message+="<li type= circle>Es necesario especificar el apellido paterno</li>";
        $(`#Empleado_APaterno`).css("border", "2px solid red");
    }
    else if($("#Empleado_AMaterno").val()=="")
    {
        message+="<li type= circle>Es necesario especificar el apellido materno</li>";
        $(`#Empleado_AMaterno`).css("border", "2px solid red");
    }
    else if($("#Empleado_Fecha_Nacimiento").val()=="")
    {
        message+="<li type= circle>Es necesario especificar la fecha de nacimiento/li>";
        $(`#Empleado_Fecha_Nacimiento`).css("border", "2px solid red");
    }
    return message;
}

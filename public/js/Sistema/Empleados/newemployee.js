$('document').ready(function(){
    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
    document.getElementById("datos").className = "col-xl-12 col-lg-8 col-md-8 col-sm-12 mb-30";
    document.getElementById('BTN_Personal_Cancelar').style.display = 'none';
    document.getElementById('BTN_Personal_Edicion').style.display = 'none';
    document.getElementById('BTN_Personal_Guardar').style.display = 'none';
    document.getElementById('BTN_Personal_Cancelar').style.display = 'none';
    LoadComboNacionalidades();
    LoadComboEstadosCiviles();
});

function CrearEmpleado()
{
    var mensaje='';
    !$("#Empleado_Nombre").val() ? (document.getElementById('Empleado_Nombre').style.backgroundColor='Red', mensaje='<h6>Nombre de Empleado</h6><br>') : document.getElementById('Empleado_Nombre').style.backgroundColor='' ;
    !$("#Empleado_APaterno").val() ? (document.getElementById('Empleado_APaterno').style.backgroundColor='Red', mensaje+='<h6>Apellido Paterno</h6><br>') : document.getElementById('Empleado_APaterno').style.backgroundColor='';
    !$("#Empleado_AMaterno").val() ? (document.getElementById('Empleado_AMaterno').style.backgroundColor='Red', mensaje+='<h6>Apellido Materno</h6><br>') : document.getElementById('Empleado_AMaterno').style.backgroundColor='';
    !$("#Empleado_RFC").val() ? (document.getElementById('Empleado_RFC').style.backgroundColor='Red', mensaje+='<h6>RFC</h6><br>') : document.getElementById('Empleado_RFC').style.backgroundColor='';
    !$("#Usuario_Correo").val() ? (document.getElementById('Usuario_Correo').style.backgroundColor='Red', mensaje+='<h6>Correo Electronico</h6><br>') : document.getElementById('Usuario_Correo').style.backgroundColor='';
    !$("#Empleado_Fecha_Nacimiento").val() ? (document.getElementById('Empleado_Fecha_Nacimiento').style.backgroundColor='Red', mensaje+='<h6>Fecha de Nacimiento</h6><br>') : document.getElementById('Empleado_Fecha_Nacimiento').style.backgroundColor='';

    if(!$("#Empleado_Nombre").val() || !$("#Empleado_APaterno").val() || !$("#Empleado_AMaterno").val() || !$("#Empleado_RFC").val() || !$("#Empleado_Fecha_Nacimiento").val() || !$("#Usuario_Correo").val())
    {
        var alerta={
            status:'warning',
            message:'<h5>Falta informacion necesaria:</h5><br>'+mensaje,
            Accion:'Creación',
            html:true
        }
        toastr.error('<h5>Falta informacion necesaria:</h5><br>'+mensaje);
    }
    else
    {
        var registro =new Object();
        registro.Empleado_Nombre=$("#Empleado_Nombre").val();
        registro.Empleado_APaterno=$("#Empleado_APaterno").val();
        registro.Empleado_AMaterno=$("#Empleado_AMaterno").val();
        registro.Empleado_Telefono=$("#Empleado_Telefono").val();
        registro.Empleado_Genero=$("#Empleado_Genero").val();
        registro.Uid_EstadoCivil=$("#Uid_EstadoCivil").val();
        registro.Empleado_NSS=$("#Empleado_NSS").val();
        registro.Empleado_CURP=$("#Empleado_CURP").val();
        registro.Empleado_RFC=$("#Empleado_RFC").val();
        registro.Empleado_Fecha_Nacimiento=$("#Empleado_Fecha_Nacimiento").val();
        registro.Uid_Nacionalidad=$("#Uid_Nacionalidad").val();
        registro.Usuario_Correo=$("#Usuario_Correo").val();
        registro._token=$(".token").val();

        $.ajax({
            url:main_path+'/empleados',
            data:registro,
            type:'post',
            datatype:'json',
            success:function(response){
                window.location.href =`${main_path}/empleados/${response.Uid_Usuario}/edit`;
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message)
            },
        });
    }
}

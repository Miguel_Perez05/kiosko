var Uid;
$('document').ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    Uid=pathArray[2];

    LoadEntrenamientos();
    LoadDominaIdiomas();
    LoadReconocimientos();
});

function LoadEntrenamientos()
{
    $.ajax({
        url:`${main_path}/entrenamientos/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var date  = new Date(element.Entrenamiento_Fecha);
                register+=`<ul>
                                <li>
                                    <br>
                                    <div class="date">${date.toLocaleDateString("en-US")} </div>
                                    <div class="task-name"><i class="ion-android-alarm-clock"></i>Curso: ${element.Curso_Nombre}</div>
                                    <p>
                                        Calificación %${element.Entrenamiento_Calificacion}
                                    </p>
                                </li>
                            </ul>`;
            });
            document.getElementById('entrenamientos').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function LoadDominaIdiomas()
{
    $.ajax({
        url:`${main_path}/dominaidiomas/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var date  = new Date(element.DominaIdioma_Fecha);
                register+=`<ul>
                                <li>
                                    <br>
                                    <div class="date">${date.toLocaleDateString("en-US")} </div>
                                    <div class="task-name"><i class="ion-android-alarm-clock"></i>Idioma: ${element.Idioma_Nombre}</div>
                                    <p>
                                        Escritura ${element.DominaIdioma_PorcentajeEscritura}%
                                    </p>
                                    <p>
                                        Conversación ${element.DominaIdioma_PorcentajeConversacion}%
                                    </p>
                                </li>
                            </ul>`;
            });
            document.getElementById('idiomas').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function LoadReconocimientos()
{
    $.ajax({
        url:`${main_path}/reconocimientos/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var date  = new Date(element.created_at);
                register+=`<ul>
                                <li>
                                    <br>
                                    <div class="date">${date.toLocaleDateString("en-US")} </div>
                                    <div class="task-name"><i class="ion-android-alarm-clock"></i>Reconocimiento: ${element.Reconocimiento_Titulo}</div>
                                    <p>
                                        <img id="image"src="${element.Reconocimiento_Image}" alt="" class="avatar-photo" style="width: 20%;">
                                    </p>
                                </li>
                            </ul>`;
            });
            document.getElementById('reconocimientos').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

$('#Uid_AreaFormacion').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    LoadComboCursos(valueSelected);
});

var Uid;
$('document').ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    Uid=pathArray[2];
});

function ActualizarSueldo()
{
    var mensaje='';
    !$("#Sueldo_Fecha").val() ? (mensaje+='<h6>Fecha no asignada</h6><br>') : document.getElementById('Sueldo_Fecha').style.backgroundColor='';
    (!$("#Sueldo_Monto").val() || $("#Sueldo_Monto").val() <=0 ) ? (mensaje+='<h6>No ha asignado un monto</h6><br>') : document.getElementById('Sueldo_Monto').style.backgroundColor='';

    if(!$("#Sueldo_Monto").val() || $("#Sueldo_Monto").val() <=0 || !$("#Sueldo_Fecha").val())
    {
        var alerta={
            status:'warning',
            message:'<h5>Falta informacion necesaria:</h5><br>'+mensaje,
            Accion:'Creación',
            html:true
        }
        success(alerta);
    }
    else
    {
        var registro =new Object();

        registro.Uid_Empleado=$("#Uid_Empleado").val();
        registro.Sueldo_Monto=$("#Sueldo_Monto").val();
        registro.Sueldo_Fecha=moment($("#Sueldo_Fecha").val()).format('YYYY-M-DD');
        registro._token=$(".token").val();

        $.ajax({
            url:`${main_path}/sueldos/PU/${Uid}`,
            data:registro,
            type:'post',
            datatype:'json',
            success:function(response){
                toastr.success("El sueldo ha sido actualizado");
                $("#task-add-sueldo").modal("hide");
                $("#Sueldo_Monto").val("");
                LoadEmpleados();
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message);
            },
        });        
    }
}

function success(response)
{
    var html=response.html ? response.html : false;
    swal({
        html:html,
        title: response.Accion,
        text: response.message,
        type: response.status,
        showCancelButton: false,
        closeOnConfirm: true,
        showLoaderOnConfirm: true
    },
    function ()
    {
        
    });
}

var Uid;
$('document').ready(function(){
    var pathname = window.location.pathname;
    var pathArray=pathname.split('/');
    Uid=pathArray[2];

    LoadEstudios();
    LoadPromociones();

    let startYear = 1980;
    let endYear = new Date().getFullYear();
    for (i = endYear; i > startYear; i--)
    {
      $('#Escolaridad_AnoIngreso').append($('<option />').val(i).html(i));
      $('#Escolaridad_AnoEgreso').append($('<option />').val(i).html(i));
    }

});

function LoadEstudios()
{
    $.ajax({
        url:`${main_path}/escolaridades/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var concluida  = "";
                if(element.Escolaridad_Concluida<1)
                {
                    concluida="Sin Concluir";
                }
                register+=`<ul>
                                <li>
                                    <br>
                                    <div class="date">${element.Escolaridad_AnoEgreso} </div>

                                    Del año ${element.Escolaridad_AnoIngreso} al año ${element.Escolaridad_AnoEgreso} ${concluida}
                                    <div class="task-name"><i class="ion-ios-book-outline"></i> Titulo: ${element.Carrera_Nombre}</div>
                                </li>
                            </ul>`;
            });
            document.getElementById('escolaridades').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function LoadPromociones()
{
    $.ajax({
        url:`${main_path}/promociones/${Uid}`,
        type:'get',
        datatype:'json',
        success:function(response){
            var register="";
            response.forEach(element => {
                var date  = new Date(element.PromocionInterna_Fecha);
                register+=`<ul>
                                <li>
                                    <br>
                                    <div class="date">${date.toLocaleDateString("en-US")} </div>
                                    <div class="task-name"><i class="ion-android-alarm-clock"></i>Promocion a puesto:</div>
                                    <p>
                                        ${element.Puesto_Nombre}
                                    </p>
                                </li>
                            </ul>`;
            });
            document.getElementById('promociones').innerHTML =register;
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

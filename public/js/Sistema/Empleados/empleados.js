var table;
var type;
$('document').ready(function(){
    var sPageURL = new URLSearchParams(window.location.search);
    type=sPageURL.get('type');

    table =$('#table').DataTable({
        "columns":[
            {"data": 'Empleado_Nombre',name:'empleados.Empleado_Nombre'},
            {"data": 'Empleado_APaterno',name:'empleados.Empleado_APaterno'},
            {"data": 'Empleado_AMaterno',name:'empleados.Empleado_AMaterno'},
            {"data": "Empleado_RFC",name:'empleados.Empleado_RFC', render:function (data){
                return data??"";
            }},
            {"data": "Puesto_Nombre",name:'puestos.Puesto_Nombre'},
            {"data": "Departamento_Nombre",name:'departamentos.Departamento_Nombre'},
            {"data": "Id_Estatus",name:'empleados.Id_Estatus',render:function (data) {
                if(data==1)
                    return "Sin Contrato"
                else if(data==2)
                    return "Con Contrato"
                else if(data==0)
                    return "De Baja"
            }},
            {"data": 'Uid_Usuario',name:'usuarios.Uid_Usuario',render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item " href="${main_path}/empleados/${data}/edit"><i class="fa fa-pencil"></i> Editar</a>
                            </div>
                        </div>`;
                }
            }
        ],
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": `${main_path}/empleados/${type}`,
        order: [[ 0, 'asc' ]],
        "lengthMenu": [[10,25, 50, -1], [10,25, 50, "All"]],
        "language": {
            "url": '/json/Spanish.json'
        },        
        responsive: true
    });
});

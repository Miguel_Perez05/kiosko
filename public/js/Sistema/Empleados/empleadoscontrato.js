var table;
var type;
$('document').ready(function(){
    var sPageURL = new URLSearchParams(window.location.search);
    type=sPageURL.get('type');

    table =$('#table').DataTable({
        "columns":[
            {"data": 'Empleado_Nombres'},
            {"data": "Empresa_Nombre"},
            {"data": "Empleado_RFC"},
            {"data": "Puesto_Nombre"},
            {"data": "Departamento_Nombre"},
            {"data": "Id_Estatus",render:function (data) {
                if(data==1)
                    return "Sin Contrato"
                else if(data==2)
                    return "Con Contrato"
                else if(data==0)
                    return "De Baja"
            }},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item " href="${main_path}/empleados/${data.Uid_Usuario}/edit"><i class="fa fa-pencil"></i> Editar</a>
                            </div>
                        </div>`;
                }
            }
        ],
        // dom: 'Bfrtip',
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[10,25, 50, -1], [10,25, 50, "All"]],
        "processing": true,
        "serverSide": true,
        "ajax": `${main_path}/empleados/${type}`,
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });
    LoadComboEmpresasEmpleados();
});

function LoadComboEmpresasEmpleados() //Carga el select de nacionalidades
{
    var Empresas = $("#Empresas");
    $.ajax({
        url:`${main_path}/empresas/0`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            var arr=[];
            Object.values(response).forEach(element => {
                arr.push(element);
            });

            response=arr.sort(fieldSorter(['Empresa_Nombre']));
            Empresas.find('option').remove();

            Empresas.append( $('<option></option>').val('').html('...') );
            $.each(response,function(key,value)
            {
                Empresas.append( $('<option></option>').val(response[key].Empresa_Nombre).html(response[key].Empresa_Nombre));
            });
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

$('#Empresas').on('change', function(){
    table.search(this.value).draw();
 });

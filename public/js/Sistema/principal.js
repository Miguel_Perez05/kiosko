$('document').ready(function(){

    $.ajax({
        url:`${main_path}/lista_estatus`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            if(response)
            {
                if([].concat.apply([], response).length>0)
                {
                    var myselect = $('<select>');
                    $("#Uid_Empleado_T").empty();
                    myselect.append( $('<option></option>').val('Seleccione').html('Seleccione') );
                    $.each(response,function(key,value)
                    {
                        myselect.append( $('<option></option>').val(response[key].Uid_Empleado).html(response[key].Empleado_Nombres) );
                    });
                    $("#Uid_Empleado_T").append(myselect.html());
                }
                else{
                    $("#Uid_Empleado_T").empty();
                }
            }else{
                $("#Uid_Empleado_T").empty();
            }
        }
    });

    $('.data-table').DataTable({
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false,
        }],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

        "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
    });

    $(function() {
        $("#Periodo_Inicial").datepicker({ dateFormat: 'yyyy-mm-dd',});
        $("#Periodo_Final").datepicker({ dateFormat: 'yyyy-mm-dd',});
        $("#Ejercicio_Nombre").datepicker({ dateFormat: 'yyyy',});
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_R(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-R').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#logo-id").change(function() {
        readURL(this);
    });
    $("#logo-id-R").change(function() {
        readURL_R(this);
    });
});

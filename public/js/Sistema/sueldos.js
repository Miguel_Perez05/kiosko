var table;
$('document').ready(function(){
    
    table =$('#table').DataTable({
        "columns":[
            {"data": "Empleado_Nombre"},
            {"data": "Sueldo_Fecha"},
            {"data": "Sueldo_Monto"},            
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    LoadSueldos();
});

function LoadSueldos()
{
    $.ajax({
        url:`${main_path}/sueldos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

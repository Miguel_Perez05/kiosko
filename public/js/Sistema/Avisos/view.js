
$('document').ready(function(){
    LoadAvisos();
});

function LoadAvisos()
{
    $.ajax({
        url:`${main_path}/avisosview`,
        type:'get',
        datatype:'json',
        success:function(response){
            avisosGenerate(response)
        }
    });
}

function avisosGenerate(data)
{
    console.log(data);
    var avisosGrid="";
    $.each(data,function(idx, aviso)
    {
        var vigencia=new Date(aviso.Aviso_Vigencia).toISOString().slice(0,10);
        var collapsed="";
        if(idx>0)
            collapsed="collapsed";
        avisosGrid+=`<div class="card">
                        <div class="card-header ${collapsed}">
                            <button class="btn btn-block " data-toggle="collapse" data-target="#faq${idx}">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">${aviso.Aviso_Titulo}</div>
                                        <div class="col-md-6" align='right'>${aviso.receptor}</div>
                                    </div>
                                </div>
                            </button>
                        </div>
                        <div id="faq${idx}" class="collapse show" data-parent="#accordion">
                            <h6 align="right" class="card-subtitle mb-2 text-muted">Fecha Vigencia: ${vigencia}</h6>
                            <div class="card-body">
                                    ${aviso.Aviso_Asunto??""}
                            </div>
                        </div>
                    </div>`;
    });
    $('#accordion').html(avisosGrid);
    
}
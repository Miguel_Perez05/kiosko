var table;
$('document').ready(function(){
    var sPageURL = new URLSearchParams(window.location.search);
    var uid=sPageURL.get('Uid');
    if(uid)
        editar(uid);
    LoadComboGrupos();
    // LoadComboEmpleados();
    LoadComboTurnos();
    LoadComboDepartamentos();
    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
    $('#Aviso_Inicio').val(date);
    $('#Aviso_Vigencia').val(date);
    
    document.getElementById('LabelModal').innerHTML = 'Nuevo Aviso';
    general();
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';

    document.getElementById("Aviso_General").checked = true;
    document.getElementById("Aviso_Departamento").checked = false;
    document.getElementById("Aviso_Grupo").checked = false;
    document.getElementById("Aviso_Turno").checked = false;
    document.getElementById("Aviso_Empleado").checked = false;

    document.getElementById('Departamento').style.display='none';
    document.getElementById("Grupo").style.display='none';
    document.getElementById("Turno").style.display='none';
    document.getElementById("Empleado").style.display='none';
});

function editar(Uid_Aviso){
    $.ajax({
        url:`${main_path}/avisos/${Uid_Aviso}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            $("#Aviso_Titulo").val(response.Aviso_Titulo);
            $("#Aviso_Asunto").val(response.Aviso_Asunto);
            var aviso_Inicio=new Date(response.Aviso_Inicio).toISOString().slice(0,10);
            $("#Aviso_Inicio").val(aviso_Inicio);
            var aviso_Vigencia=new Date(response.Aviso_Vigencia).toISOString().slice(0,10);
            $("#Aviso_Vigencia").val(aviso_Vigencia);
            
            $("#Aviso_Prioridad").val(response.Aviso_Prioridad);

            $("#Uid_Departamento").val(response.Uid_Departamento);
            $("#Uid_Grupo").val(response.Uid_Grupo);
            $("#Uid_Turno").val(response.Uid_Turno);
            $("#Uid_Empleado").val(response.Uid_Empleado);

            $("#Uid_Aviso").val(response.Uid_Aviso);
            if(response.Aviso_Tipo=="General")
                document.getElementById("Aviso_General").checked = true;
            else if(response.Aviso_Tipo=="Departamento")
            {
                document.getElementById("Aviso_Departamento").checked = true;
                document.getElementById('Departamento').style.display='block';
            }
            else if(response.Aviso_Tipo=="Grupo")
            {
                document.getElementById("Aviso_Grupo").checked = false;
                document.getElementById('Departamento').style.display='block';
                document.getElementById("Grupo").style.display='block';
            }
            else if(response.Aviso_Tipo=="Turno")
            {
                document.getElementById("Aviso_Turno").checked = false;
                document.getElementById('Departamento').style.display='block';
                document.getElementById("Grupo").style.display='block';
            }   
            else if(response.Aviso_Tipo=="Empleado")
            {
                document.getElementById("Aviso_Empleado").checked = false;
                document.getElementById('Departamento').style.display='block';
                document.getElementById("Grupo").style.display='block';
                document.getElementById("Turno").style.display='block';
                document.getElementById("Empleado").style.display='block';
            }
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Aviso';
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function envio()
{
    var register=new Object();
    register.Aviso_Titulo=$("#Aviso_Titulo").val();
    register.Aviso_Asunto=$("#Aviso_Asunto").val();
    register.Aviso_Inicio=$("#Aviso_Inicio").val();
    register.Aviso_Vigencia=$("#Aviso_Vigencia").val();
    register.Aviso_Prioridad=$("#Aviso_Prioridad").val();
    register.Id_Pais=$("#Id_Pais").val();
    register.Id_Estado=$("#Id_Estado").val();
    register.Uid_Empleado_Receptor=$("#Uid_Empleado").val();
    register.Uid_Grupo=$("#Uid_Grupo").val();
    register.Uid_Departamento=$("#Uid_Departamento").val();
    register.Uid_Turno=$("#Uid_Turno").val();
    register.Aviso_Tipo='';
    if(document.getElementById("Aviso_General").checked == true)
        register.Aviso_Tipo='General';
    else if(document.getElementById("Aviso_Departamento").checked == true)
        register.Aviso_Tipo='Departamento';
    else if(document.getElementById("Aviso_Grupo").checked == true)
        register.Aviso_Tipo='Grupo';
    else if(document.getElementById("Aviso_Turno").checked == true)
        register.Aviso_Tipo='Turno';
    else if(document.getElementById("Aviso_Empleado").checked == true)
        register.Aviso_Tipo='Empleado';
    register._token=$(".token").val();    
    $.ajax({
        url:`${main_path}/avisos`,
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);            
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function nuevo(){
    window.location.href = `${main_path}/avisos/create`;
}

function general()
{
    if(document.getElementById("Aviso_General").checked == false)
    {
        document.getElementById("Departamento").style.display='block';
        document.getElementById("Aviso_Departamento").checked = true;
        departamento();
    }
    else
    {
        document.getElementById('Departamento').style.display='none';
        document.getElementById("Grupo").style.display='none';
        document.getElementById("Turno").style.display='none';
        document.getElementById("Empleado").style.display='none';
    }
}

function departamento()
{

    if(document.getElementById("Aviso_Departamento").checked == false)
    {
        document.getElementById("Uid_Departamento").disabled=true;
        document.getElementById("Grupo").style.display='block';
        document.getElementById("Aviso_Grupo").checked = true;
        grupo();
    }
    else
    {
        document.getElementById("Uid_Departamento").disabled=false;
        document.getElementById("Grupo").style.display='none';
        document.getElementById("Turno").style.display='none';
        document.getElementById("Empleado").style.display='none';
    }
}

function grupo()
{
    if(document.getElementById("Aviso_Grupo").checked == false)
    {
        document.getElementById("Uid_Grupo").disabled=true;
        document.getElementById("Turno").style.display='block';
        document.getElementById("Aviso_Turno").checked = true;
        turno();
    }
    else
    {
        document.getElementById("Uid_Grupo").disabled=false;
        document.getElementById("Turno").style.display='none';
        document.getElementById("Empleado").style.display='none';
    }
}

function turno()
{

    if(document.getElementById("Aviso_Turno").checked == false)
    {
        document.getElementById("Uid_Turno").disabled=true;
        document.getElementById("Empleado").style.display='block';
        document.getElementById("Aviso_Empleado").checked = true;
        empleado();
    }
    else
    {
        document.getElementById("Uid_Turno").disabled=false;
        document.getElementById("Empleado").style.display='none';
    }
}

function empleado()
{

    if(document.getElementById("Aviso_Empleado").checked == false)
    {
        document.getElementById("Uid_Empleado").disabled=true;
    }
    else
    {
        document.getElementById("Uid_Empleado").disabled=false;
    }
}

$("#Uid_Empleado").select2({
    ajax: {
        url: `${main_path}/listado?type=all`,
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
    }
});
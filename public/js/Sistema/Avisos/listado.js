var table;
$('document').ready(function(){
    var deleteBtn="";
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            // {"data": "Aviso_Dirijido"},
            {"data": "Aviso_Titulo"},
            {"data": "Aviso_Prioridad"},
            {"data": "Aviso_Inicio"},
            {"data": "Aviso_Vigencia"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="${main_path}/avisos/form?Uid=${data.Uid_Aviso}"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });
    LoadAvisos();
});

function LoadAvisos()
{
    $.ajax({
        url:`${main_path}/avisos/0`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function nuevo(){
    window.location.href = `${main_path}/avisos/form`;
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Aviso",
        text: "Esta seguro de eliminar el aviso?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);            
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/avisos/${data.Id_Aviso}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadAvisos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}
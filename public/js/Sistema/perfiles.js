var table;
$('document').ready(function(){
    if(session)
        deleteBtn=`<a class="dropdown-item Delete" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    
    table =$('#table').DataTable({
        "columns":[
            {"data": "Perfil_Nombre"},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Edit"><i class="fa fa-pencil"></i> Editar</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        editar(data.Id_Perfil)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadPerfiles();  
});

function LoadPerfiles()
{
    $.ajax({
        url:`${main_path}/perfiles/0`,
        type:'get',
        datatype:'json',
        success:function(response){
        table.clear().rows.add(response).draw();
        }
    });
}

function editar(Id_Perfil){
    $.ajax({
        url:`${main_path}/perfiles/${Id_Perfil}/edit`,
        type:'get',
        datatype:'json',
        success:function(response){
            console.log(response);
            $("#Perfil_Nombre").val(response.Perfil_Nombre);
            $("#Id_Perfil").val(response.Id_Perfil);
            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Perfil';
            $("#modal").modal("show");
        }
    });
}

function nuevo(){
    $("#Perfil_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Perfil';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var register = new Object();
    register.Perfil_Nombre=$("#Perfil_Nombre").val();
    register._token=$(".token").val();
    $.ajax({
        url:main_path+'/perfiles',
        data:register,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadPerfiles();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion()
{
    var register = new Object();
    register.Perfil_Nombre=$("#Perfil_Nombre").val();
    register._token=$(".token").val();

    $.ajax({
        url:`${main_path}/perfiles/${$("#Id_Perfil").val()}`,
        data:register,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
            LoadPerfiles();
            $("#modal").modal("hide");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function confirmDelete(data)
{
    swal({
        title: "Eliminar Perfil",
        text: "Esta seguro de eliminar el perfil?",
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Cancel(data);
        }
    });
}

function Cancel(data)
{
    var register = new Object();
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/perfiles/${data.Id_Perfil}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadPerfiles();
            toastr.success("Perfil eliminado");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}
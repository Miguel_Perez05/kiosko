$('document').ready(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var Uid_Puesto = urlParams.get('Puesto');
    getPuesto(Uid_Puesto);


});
// $("#Asignacion_Sueldo").mask('000,000,000.00', { reverse: true });

function getPuesto(Uid_Puesto){
    $.ajax({
        url:`${main_path}/puestos/${Uid_Puesto}/edit`,

        type:'get',
        datatype:'json',
        success:function(response){
            $("#Departamento_Nombre").val(response.Departamento_Nombre);
            $("#Puesto_Nombre").val(response.Puesto_Nombre);
            $("#Asignacion_Sueldo").val(response.Puesto_Sueldo_Min);
            $("#Uid_Puesto").val(response.Uid_Puesto);
            console.log(response);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

$("#Uid_Empleado").select2({
    ajax: {
        url: `${main_path}/listado?type=2`,
        data: function (params) {
            var query = {
                term: params.term
            }
            return query;
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
    }
});

function asignarPuesto()
{
    var registro =new Object();
    var Uid_Puesto=$("#Uid_Puesto").val();
    registro.Uid_Responsable=$("#Uid_Empleado").val();
    registro.Id_Estatus=1;
    registro._token=$(".token").val();
    console.log(registro);
    // return;
    $.ajax({
        url:main_path+'/asignacion/'+Uid_Puesto,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);

            setTimeout(function(){
                window.location.href =`${main_path}/vacantes`;
            },3000);

        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

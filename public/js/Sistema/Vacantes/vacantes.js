var table;
$(document).ready(function(){
    table =$('#table').DataTable({
        "columns":[
            {"data": "Puesto_Nombre"},
            {"data": "Departamento_Nombre"},
            {"data": null,render:function (data) {
                    var divButtons=`<div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item Assign"><i class="fa fa-pencil"></i> Asignar</a>`;
                    if(data.Id_Estatus==0)
                        divButtons+=`<a class="dropdown-item Activate" ><i class="fa fa-check"></i> Activar</a>`;
                    else
                        divButtons+=`<a class="dropdown-item Delete" ><i class="fa fa-trash"></i> Desactivar</a>`;
                    divButtons+=`</div></div>`;
                    return divButtons;
                }
            }
        ],
        buttons: [],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
    });

    $('#table tbody').on( 'click', '.Assign', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        window.location.href =`${main_path}/assign?Puesto=${data.Uid_Puesto}`;
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmStatus(data,0,"Desactivar");
    });

    LoadVacantes();
});


function LoadVacantes()
{
    $.ajax({
        url:`${main_path}/vacantes/4`,
        type:'get',
        datatype:'json',
        success:function(response){
            table.clear().rows.add(response).draw();
        }
    });
}

function confirmStatus(data,status,labelStatus)
{
    swal({
        title: `${labelStatus} Idioma`,
        text: `Esta seguro de ${labelStatus} el Puesto?`,
        icon: "warning",
        buttons: ["No", "Si"],
        dangerMode: true,
    })
    .then(value => {
        if (value) {
            Status(data,status);
        }
    });
}

function Status(data, status)
{
    var register = new Object();
    register.Id_Estatus=status;
    register._token=$(".token").val();
    $.ajax({
        url:`${main_path}/puesto/${data.Uid_Puesto}`,
        data:register,
        type:'delete',
        datatype:'json',
        success:function(response){
            LoadPuestos();
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

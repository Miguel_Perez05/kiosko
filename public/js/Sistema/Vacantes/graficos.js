$(document).ready(function(){
    LoadComboPuestos(4);
});

function asignarPuesto()
{
    var registro =new Object();
    var Uid_Puesto=$("#Uid_Puesto_").val();
    registro.Uid_Responsable=$("#Uid_Empleado").val();
    registro.Id_Estatus=1;
    registro._token=$(".token").val();
    console.log(Uid_Puesto);
    $.ajax({
        url:main_path+'/asignacion/'+Uid_Puesto,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function cargaGrafico()
{
    document.getElementById('postulacion').style.display = 'none';
    $("#candidato").text("");
    var marksCanvas = document.getElementById("chart");
    'use strict'

    var Estudio = new Array();
    var Estudio2 = new Array();
    var Estudio3 = new Array();
    var Labels = new Array();
    var Uid='';
    var label='';
    var label2='';
    var label3='';
    var count=1;
    $("#Uid_Puesto_").val($("#Uid_Puesto").val());
    $(document).ready(function(){
        $.get(
            main_path+'/estudio/'+$("#Uid_Puesto").val(), 
            function(response){
                console.log(response.length);
                if(response.length==0)
                {
                    toastr.error('No hay candidatos para esta vacante');
                    return;
                }
                Labels.push('Nivel Estudios');
                Labels.push('Idioma Escritura');
                Labels.push('Idioma Conversacional');
                Labels.push('Promociones');
                Labels.push('Reconocimientos');
                Labels.push('Cursos');
                response.forEach(function(data){
                    if(Uid=='' && count ==1)
                    {
                        $("#Uid_Empleado").val(data.Uid_Empleado);
                        $("#candidato").text(data.Empleado_Nombre);
                        Uid=data.Uid_Empleado;
                        Estudio.push(data.Nivel);
                        Estudio.push(data.idioma_Escritura/10);
                        Estudio.push(data.idioma_Conversacional/10);
                        Estudio.push(data.promociones);
                        Estudio.push(data.reconocimientos);
                        Estudio.push(data.cursos);
                        console.log(Uid);
                        label=data.Empleado_Nombre;
                        count +=1;
                    }
                    else if(Uid != data.Uid_Empleado&& count ==2)
                    {
                        Uid=data.Uid_Empleado;
                        Estudio2.push(data.Nivel);
                        Estudio2.push(data.idioma_Escritura/10);
                        Estudio2.push(data.idioma_Conversacional/10);
                        Estudio2.push(data.promociones);
                        Estudio2.push(data.reconocimientos);
                        Estudio2.push(data.cursos);
                        label2=data.Empleado_Nombre;
                        console.log(Estudio2);
                        count +=1;
                    }
                    else if(Uid != data.Uid_Empleado&& count ==3)
                    {
                        Uid=data.Uid_Empleado;
                        Estudio3.push(data.Nivel);
                        Estudio3.push(data.idioma_Escritura/10);
                        Estudio3.push(data.idioma_Conversacional/10);
                        Estudio3.push(data.promociones);
                        Estudio3.push(data.reconocimientos);
                        Estudio3.push(data.cursos);
                        label3=data.Empleado_Nombre;
                    }
                });

                var marksData = {
                    labels: Labels,
                    datasets: [{
                        label: label,
                        backgroundColor: "rgba(200,0,0,0.2)",
                        data: Estudio,

                    },{
                        label: label2,
                        backgroundColor: "rgba(0,0,200,0.2)",
                        data: Estudio2,

                    },{
                        label: label3,
                        backgroundColor: "rgba(0,0,0,200)",
                        data: Estudio3,

                    }]
                };

                var chartOptions = {
                    scale: {
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 10,
                        stepSize: 1
                    },
                    pointLabels: {
                        fontSize: 10
                    }
                    },
                    legend: {
                    position: 'left'
                    }
                };

                var radarChart = new Chart(marksCanvas, {
                    type: 'radar',
                    data: marksData,

                    options: chartOptions
                });

                if(count!=1)
                    document.getElementById('postulacion').style.display = 'block';
            }
        );
    });

}

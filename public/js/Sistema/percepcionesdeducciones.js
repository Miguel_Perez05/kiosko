function CrearPercepcionDeduccion(e)
{
    var mensaje='';
    !$("#PercepcionDeduccion_Fecha").val() ? (mensaje+='<h6>Fecha no asignada</h6><br>') : document.getElementById('PercepcionDeduccion_Fecha').style.backgroundColor='';
    (!$("#PercepcionDeduccion_Monto").val() || $("#PercepcionDeduccion_Monto").val() <=0 ) ? (mensaje+='<h6>No ha asignado un monto</h6><br>') : document.getElementById('PercepcionDeduccion_Monto').style.backgroundColor='';

    if(!$("#PercepcionDeduccion_Monto").val() || $("#PercepcionDeduccion_Monto").val() <=0 || !$("#PercepcionDeduccion_Fecha").val())
    {
        var alerta={
            status:'warning',
            message:'<h5>Falta informacion necesaria:</h5><br>'+mensaje,
            Accion:'Creación',
            html:true
        }
        success(alerta);
    }
    else
    {
        var registro =new Object();

        registro.Uid_Empleado=$("#Uid_Empleado").val();
        registro.Id_TipoIncidencia=$(e).data('tipo');
        registro.PercepcionDeduccion_Tipo=$(e).data('tipo');
        registro.PercepcionDeduccion_Fecha=$("#PercepcionDeduccion_Fecha").val();
        registro.PercepcionDeduccion_Monto=$("#PercepcionDeduccion_Monto").val();
        registro.PercepcionDeduccion_Comentario=$("#PercepcionDeduccion_Comentario").val();
        registro.Uid_TipoBono=$("#Uid_TipoBono").val();
        registro._token=$(".token").val();

        $.ajax({
            url:main_path+'/percepcionesdeducciones/PU/'+ $("#Uid_Usuario").val(),
            data:registro,
            type:'post',
            datatype:'json',
            success:function(response){
                toastr.success(response.Message);
                $("#task-add-compensaciones").modal("hide");
                LoadPercepciones()
            },
            error:function(response){
                toastr.error(response.responseJSON.ResponseStatus.Message);
            },
        });
    }
}

function eliminar(e)
{
    var id=$(e).data('id');
    var registro =new Object();
    registro._token=$(".token").val();
    registro.Id_TipoIncidencia=$(e).data('tipo');

    $.ajax({
        url:main_path+'/percepcionesdeducciones/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function success(response)
{
    var html=response.html ? response.html : false;
    swal({
        html:html,
        title: response.Accion,
        text: response.message,
        type: response.status,
        showCancelButton: false,
        closeOnConfirm: true,
        showLoaderOnConfirm: true
    },
    function ()
    {
        if(response.status=='success')
        {
            window.location.href = main_path+'/percepcionesdeducciones';
        }
    });
}

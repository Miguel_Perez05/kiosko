function editar(e){
    var id=$(e).data('id');
    $.ajax({
    url:main_path+'/estadosciviles/'+id+'/edit',
    type:'get',
    datatype:'json',
    success:function(response){
        $("#EstadoCivil_Nombre").val(response.EstadoCivil_Nombre);
        $("#Uid_EstadoCivil").val(response.Uid_EstadoCivil);
        if(response.Id_Estatus==1)
            document.getElementById("Id_Estatus").checked = true;
          else
            document.getElementById("Id_Estatus").checked = false;
        document.getElementById('Crear').style.display = 'none';
        document.getElementById('Editar').style.display = 'block';
        document.getElementById('LabelModal').innerHTML = 'Edición de Estado Civil';
        document.getElementById('_Estatus').style.display = 'block';
        $("#modal").modal("show");
    }
    });
}

function nuevo(){
    $("#EstadoCivil_Nombre").val("");
    document.getElementById('LabelModal').innerHTML = 'Nuevo Estado Civil';
    document.getElementById('Crear').style.display = 'block';
    document.getElementById('Editar').style.display = 'none';
    document.getElementById('_Estatus').style.display = 'none';
    $("#modal").modal("show");
}

function envio()
{
    var EstadoCivil_Nombre=$("#EstadoCivil_Nombre").val();
    var Id_Estatus=1;
    if(document.getElementById("Id_Estatus").checked == false)
        Id_Estatus=0;
    var token=$(".token").val();
    var registro = {
        'EstadoCivil_Nombre':EstadoCivil_Nombre,
        'Id_Estatus':Id_Estatus,
        '_token':token
    };
    $.ajax({
        url:main_path+'/estadosciviles',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function edicion(e)
{
    var EstadoCivil_Nombre=$("#EstadoCivil_Nombre").val();
    var Uid_EstadoCivil=$("#Uid_EstadoCivil").val();
    var Id_Estatus=1;
    if(document.getElementById("Id_Estatus").checked == false)
        Id_Estatus=0;
    var token=$(".token").val();
    var registro = {
        'EstadoCivil_Nombre':EstadoCivil_Nombre,
        'Id_Estatus':Id_Estatus,
        '_token':token
    };
    $.ajax({
        url:main_path+'/estadosciviles/'+Uid_EstadoCivil,
        data:registro,
        type:'put',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function eliminar(e)
{
    var id=$(e).data('id');
    var token=$(".token").val();
    var registro = {
        '_token':token
    };
    $.ajax({
        url:main_path+'/estadosciviles/'+id,
        data:registro,
        type:'delete',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/estadosciviles/';
    });
}

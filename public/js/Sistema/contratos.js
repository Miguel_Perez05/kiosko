var table;
$('document').ready(function(){
    deleteBtn="";
    // if(session)
    //     deleteBtn=`<a class="dropdown-item Revoque" ><i class="fa fa-pencil"></i> Eliminar</a>`;
    table =$('#table').DataTable({
        "columns":[
            {"data": "Contrato_Folio"},
            {"data": "Contrato_Inicio"},
            {"data": null,render:function (data) {
                return `${data.Empleado_Nombre} ${data.Empleado_APaterno} ${data.Empleado_AMaterno}`;
            }},
            {"data": null,render:function (data) {
                return `<div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item Print"><i class="fa fa-print"></i> Imprimir</a>
                                ${deleteBtn}
                            </div>
                        </div>`;
                }
            }
        ],
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        order: [[ 1, 'asc' ]],
        "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false
        }],
        "language": {
            "url": "/json/Spanish.json"
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: `${main_path}/contratos/0`,
            "data": function ( d ) {
                return `Uid_Empresa=${document.getElementById("Uid_Empresa").value}`;
            },
            type: 'get',
            dataType: "json"
        }
    });

    $('#table tbody').on( 'click', '.Print', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        impresionContrato(data.Uid_Contrato)
    });

    $('#table tbody').on( 'click', '.Delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row( tr ).data();
        confirmDelete(data);
    });

    LoadComboEmpresas();
    
    $('#Uid_Empresa').on('change', function (e) {
        table.ajax.reload();
    });
});

function editar(Uid_Contrato)
{
    LoadComboPaises();
    $.ajax({
        url:`${main_path}/contratos/${Uid_Contrato}/edit`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            $("#Ciudad_Nombre").val(response.Ciudad_Nombre);
            $("#Id_Pais").val(response.Id_Pais);
            $("#Uid_Contrato").val(response.Uid_Contrato);

            document.getElementById('Crear').style.display = 'none';
            document.getElementById('Editar').style.display = 'block';
            document.getElementById('LabelModal').innerHTML = 'Edición de Ciudad';
            $("#modal").modal("show");
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        }
    });
}

function impresionContrato(Uid_Contrato)
{
    $.ajax({
        url:`${main_path}/ContratoShow/${Uid_Contrato}`,
        type:'get',
        datatype:'json',
        success:function(response)
        {
            abrirEnPestana(`${main_path}/ContratoShow/${Uid_Contrato}`);
        },
        error:function(response)
        {
            toastr.error(response.responseJSON.ResponseStatus.Message);
        }
    });
}

function abrirEnPestana(url) {
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function nuevo()
{
    window.location.href=`${main_path}/tramite?tipo=contrato&empresa=${$('#Uid_Empresa').val()}`;
}
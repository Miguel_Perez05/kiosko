function nuevo(){
    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
    $('#Empleado_Fecha_Nacimiento').val(date);
    $('#Empleado_Fecha_Ingreso').val(date);
    $("#success-modal").modal("show");
}

$(".tab-wizard").steps({
    headerTag: "h5",
    bodyTag: "section",
    transitionEffect: "fade",
    titleTemplate: '<span class="step">#index#</span> #title#',
    labels: {
        finish: "Submit"
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        $('.steps .current').prevAll().addClass('disabled');
    },
    onFinished: function (event, currentIndex) {
        envio();
    }
});

async function envio()
{
    var Empleado_Nombre=$("#Empleado_Nombre").val();
    var Empleado_APaterno=$("#Empleado_APaterno").val();
    var Empleado_AMaterno=$("#Empleado_AMaterno").val();
    var Usuario_Correo=$("#Usuario_Correo").val();
    var Uid_Empresa=$("#Uid_Empresa").val();
    var Uid_RegistroPatronal=$("#Uid_RegistroPatronal").val();
    var Empleado_RFC=$("#Empleado_RFC").val();
    var Empleado_Fecha_Nacimiento=$("#Empleado_Fecha_Nacimiento").val();
    var Empleado_Fecha_Ingreso=$("#Empleado_Fecha_Ingreso").val();
    var token=$(".token").val();
    var registro = {
        'Empleado_Fecha_Nacimiento':Empleado_Fecha_Nacimiento,
        'Usuario_Correo':Usuario_Correo,
        'Empleado_Nombre':Empleado_Nombre,
        'Empleado_APaterno':Empleado_APaterno,
        'Empleado_AMaterno':Empleado_AMaterno,
        'Uid_Empresa':Uid_Empresa,
        'Uid_RegistroPatronal':Uid_RegistroPatronal,
        'Empleado_RFC':Empleado_RFC,
        'Empleado_Fecha_Ingreso':Empleado_Fecha_Ingreso,
        '_token':token
    };
    $.ajax({
        url:main_path+'/altasbajas',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message);
        },
    });
}

function altabaja()
{
    var Uid_Empleado=$("#Uid_Empleado").val();
    var Uid_Empresa=$("#Uid_Empresa").val();
    var Id_Estado=$("#Id_Estado").val();
    var dateIngreso = Date();
    var dateBaja = Date();
    var dateReingreso = Date();
    if(Id_Estado == 'Baja')
        dateBaja=$("#Empleado_Fecha_Ingreso").val();
    if(Id_Estado == 'Ingreso')
        dateIngreso=$("#Empleado_Fecha_Ingreso").val();
    if(Id_Estado == 'Reingreso')
        dateReingreso=$("#Empleado_Fecha_Ingreso").val();
    var token=$(".token").val();
    var registro = {
        'Uid_Empleado':Uid_Empleado,
        'Uid_Empresa':Uid_Empresa,
        'dateIngreso':dateIngreso,
        'dateBaja':dateBaja,
        'dateReingreso':dateReingreso,
        '_token':token
    };
    $.ajax({
        url:`${main_path}/empleados/baja`,
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function aceptar(e)
{
    var Uid_Historial=$(e).data('id');
    var Uid_Empleado=$(e).data('uid_empleado');
    var Uid_Empresa=$(e).data('uid_empresa');

    var dateBaja = null;
    var dateReingreso = null;

    var Id_Estado=1;

    var date = Date();
    date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));

    if($(e).data('status')=='Baja')
    {
        dateBaja=date;
        Id_Estado=0;
    }
    if($(e).data('status')=='Reingreso')
        dateReingreso=date;

    var token=$(".token").val();
    var registro = {
        'Uid_Historial':Uid_Historial,
        'Uid_Empleado':Uid_Empleado,
        'Uid_Empresa':Uid_Empresa,
        'Id_Estado':Id_Estado,
        'dateBaja':dateBaja,
        'dateReingreso':dateReingreso,
        '_token':token
    };
    $.ajax({
        url:main_path+'/altasbajas/cambio',
        data:registro,
        type:'post',
        datatype:'json',
        success:function(response){
            toastr.success(response.Message);
        },
        error:function(response){
            toastr.error(response.responseJSON.ResponseStatus.Message)
        },
    });
}

function cambiaEstatus(e)
{
    if($(e).data('status')=='Rechazado')
        document.getElementById('BTN_Status').innerHTML = 'Rechazar';
    if($(e).data('status')=='Autorizado')
        document.getElementById('BTN_Status').innerHTML = 'Autorizar';
    $("#Uid_Solicitud").val($(e).data('id'));
    $("#Solicitud_StatusR").val($(e).data('status'));
    $("#Solicitud_Observaciones").val('');
    document.getElementById("Solicitud_MotivoC").disabled=true;
    document.getElementById("Motivo").style.display = 'none';
    document.getElementById("Solicitud_Observaciones").disabled=false;
    document.getElementById('BTN_Status').style.display = 'block';
    $("#modal_Comentarios").modal("show");
}

function alerta(response)
{
    swal({
    title: response.Accion,
    text: response.message,
    type: response.status,
    showCancelButton: false,
    closeOnConfirm: true,
    showLoaderOnConfirm: true
    },
    function ()
    {
        window.location.href = main_path+'/altasbajas/';
    });
}

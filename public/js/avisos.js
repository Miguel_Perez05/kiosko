function envia_mensaje(){
  var token=$(".token").val();
  var titulo=$("#titulo").val();
  var asunto=$("#asunto").val();
  var empleados=document.getElementById("listaempleados").value;
  if(empleados=='admin' || empleados=='general')
  {
    if(empleados=='admin')
    {
      var tipo = 1;
    }
    else
    {
      var tipo = 0;
    }
    var t_empleado = {
      't_empleado':tipo,
      'asunto':asunto,
      'titulo':titulo,
      '_token':token
    }
    $.ajax({
      url:main_path+'/avisos/tipo_usuario',
      data:t_empleado,
      type:'get',
      datatype:'json',
      success:function(response){
        if (response.status == true) {
          alert(response.message);
          window.location.href = main_path+'/';
        }else{
          var error = "";
          $.each(response.message,function(i,val){
            error += val+",";
          });
          alert("Error "+error);
        }
			},
    });
  }
  else
  {
    var aviso = {
      'Id_remite':empleados,
      'asunto':asunto,
      'titulo':titulo,
      '_token':token
    }
    $.ajax({
      url:main_path+'/avisos/envia',
      data:aviso,
      type:'post',
      datatype:'json',

      success:function(xml){
        if (xml.status == true) {
          alert(xml.message);
          window.location.href = main_path+'/';
        }else{
          var error = "";
          $.each(xml.message,function(i,val){
            error += val+",";
          });
          alert("Error "+error);
        }
      },

    });
  }
}

function responde_mensaje(){
  var token=$(".token").val();
  var titulo=$("#titulo").val();
  var asunto=$("#asunto").val();
  var destinatario=$("#destinatario").val(); //$("#destinatario").value;//
  var aviso = {
    'Id_remite':destinatario,
    'asunto':asunto,
    'titulo':titulo,
    '_token':token
  }
  console.log(aviso);
  $.ajax({
    url:main_path+'/avisos/envia',
    data:aviso,
    type:'post',
    datatype:'json',

    success:function(xml){
      if (xml.status == true) {
        alert(xml.message);
        window.location.href = main_path+'/';
      }else{
        var error = "";
        $.each(xml.message,function(i,val){
          error += val+",";
        });
        alert("Error "+error);
      }
    },

  });
}

function mensaje(e){
  var id=$(e).data('id');
  $.get(main_path+"/avisos/detalle/"+id, function(htmlexterno){
    window.location.href=main_path+'/avisos/detalle/'+id;
  });
}

function responder(e){
  var id=$(e).data('id');
  console.log("hola "+id);
  $.get(main_path+"/avisos/respuesta/"+id, function(htmlexterno){
    window.location.href=main_path+'/avisos/respuesta/'+id;
  });
}

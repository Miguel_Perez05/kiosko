function envia_solicitud(){
  var token=$(".token").val();
  var inicioauscencia=$("#inicioauscencia").val();
  var finauscencia=$("#finauscencia").val();
  var motivo=$("#motivo").val();
  var observaciones=$("#observaciones").val();
  var tipo=document.getElementById("selecttipo").value;

  var solicitud = {
    'inicioauscencia':inicioauscencia,
    'finauscencia':finauscencia,
    'motivo':motivo,
    'observaciones':observaciones,
    'tipo':tipo,
    '_token':token
  }
  $.ajax({
    url:main_path+'/solicitudes/registra',
    data:solicitud,
    type:'post',
    datatype:'json',

    success:function(xml){
      if (xml.status == true) {
        alert(xml.message);
        window.location.href = main_path+'/solicitudes';
      }else{
        var error = "";
        $.each(xml.message,function(i,val){
          error += val+",";
        });
        alert("Error "+error);
      }
    },
  });
}

function mensaje(e){
  var id=$(e).data('id');
  console.log(id);
  $.get(main_path+"/solicitudes/detalle/"+id, function(htmlexterno){
    window.location.href=main_path+'/solicitudes/detalle/'+id;
  });
}

function modal(){
  $("#modal_autorizacion").modal("show");
}

function regresar(){
  window.location.href = main_path+'/solicitudes';
}

function autorizado(e){
  var token=$(".token").val();
  var id=$(e).data('id');
  var inicioauscencia=$("#inicioauscencia").val();
  var finauscencia=$("#finauscencia").val();
  var solicitud = {
    'Id_solicitudes':id,
    'estatus':'Autorizado',
    'inicioauscencia':inicioauscencia,
    'finauscencia':finauscencia,
    '_token':token
  }
  $("#Comentario_solicitud").attr("data-id",id);
  $("#estatus").val("Autorizado");
  console.log(solicitud);
  $.ajax({
    url:main_path+'/solicitudes/estatus',
    data:solicitud,
    type:'post',
    datatype:'json',

    success:function(response){
      if (response.status == true) {

        modal();
      }else{
      }
    },
  });
}

function rechazado(e){
  var token=$(".token").val();
  var id=$(e).data('id');
  var solicitud = {
    'Id_solicitudes':id,
    'estatus':'Rechazado',
    '_token':token
  }
  $("#Comentario_solicitud").attr("data-id",id);
  $("#estatus").val("Rechazado");
  $.ajax({
    url:main_path+'/solicitudes/estatus',
    data:solicitud,
    type:'post',
    datatype:'json',

    success:function(response){
      if (response.status == true) {
        modal();
      }else{
      }
    },
  });
}

function envia_comentario(e){
  var token=$(".token").val();
  var id=$(e).data('id');
  var comentario=$("#comentario").val();
  var estatus=$("#estatus").val();
  var solicitud = {
    'Id_solicitudes':id,
    'comentario':comentario,
    'estatus':estatus,
    '_token':token
  }
  console.log(solicitud);
  $.ajax({
    url:main_path+'/solicitudes/comentarios',
    data:solicitud,
    type:'post',
    datatype:'json',

    success:function(response){
      if (response.status == true) {
        alert(response.message);
        window.location.href = main_path+'/solicitudes';
      }else{
        var error = "";
        $.each(response.message,function(i,val){
          error += val+",";
        });
        alert("Error "+error);
      }
    },
  });
}

@extends('paneles.ConfiguracionGeneral.master')
@section('content')



<div class="faq-wrap">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6"><h4 class="mb-20">Avisos de la Empresa</h4></div>
            <div class="col-md-6" align='right'><h5 class="mb-20">Dirigido a:</h5></div>
        </div>
    </div>
    <div id="accordion"></div>

</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Avisos/view.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<div class="pd-20">
    <div class="profile-timeline">
        <div class="timeline-month">
            <h5>@if($Titulo){{$Titulo->TipoIncidencia_Nombre}} @else Incidencias @endif de los ultimos 30 Dias</h5>
        </div>
        @if($Titulo)<div align="right">Total: {{$suma}}@if($Titulo->TipoIncidencia_ManejaTiempo==1) Minutos de @endif {{$Titulo->TipoIncidencia_Nombre}} </div>@endif
        @foreach($incidencias as $incidencia)

        <div class="profile-timeline-list">
            <ul>
                <li>
                    <div class="date">{{$incidencia->Incidencia_Dia}} </div>
                    <div class="task-name"><i class="ion-android-alarm-clock"></i> {{$incidencia->TipoIncidencia_Nombre}} @if($incidencia->TipoIncidencia_ManejaTiempo==1){{$incidencia->Incidencia_Minutos}} Minutos @endif</div>
                    <p>
                        {{$incidencia->Incidencia_Comentario}}
                    </p>
                    <p>
                        @if($incidencia->Incidencia_FechaJustificacion)
                            Justificada
                        @else

                        @endif
                    </p>
                </li>
            </ul>
        </div>
        @endforeach
    </div>
</div>

@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            @if($tipoSolicitudes->Id_TipoIncidencia)
            <h5 class="text-blue">Solicitudes {{$tipoSolicitudes->TipoSolicitud_Nombre}}</h5>
            @else
            <h5 class="text-blue">Trámites {{$tipoSolicitudes->TipoSolicitud_Nombre}}</h5>
            @endif
            <p class="font-14"> </p>
        </div>
    </div>
    <a>
        <input type="hidden" id="TipoIncidencia" value="{{$tipoSolicitudes->Id_TipoIncidencia}}">
        <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
            <span class="oi oi-plus" style="color: white;"></span>
        </button>
    </a>
    <div class="row">
        <table class="data-table stripe hover nowrap">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Tipo</th>
                    <th class="table-plus datatable-nosort">Fecha Inicio</th>
                    <th class="table-plus datatable-nosort">Fecha Fin</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
            <tbody>
                {{-- @foreach($solicitudes as $solicitud)
                @if($solicitud->Solicitud_Status== 'Rechazado')
                    <tr class="hidden-sm-down bg-danger">
                @elseif($solicitud->Solicitud_Status== 'Autorizado')
                    <tr class="hidden-sm-down bg-success">
                @else
                    <tr>
                @endif
                        <td>{{ $solicitud->TipoSolicitud_Nombre}}</td>
                        <td>{{ $solicitud->Solicitud_Inicio_Auscencia}}</td>
                        <td>{{ $solicitud->Solicitud_Fin_Auscencia}}</td>
                        <td>{{ $solicitud->Solicitud_Status}}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    @if($solicitud->Solicitud_Status== 'Pendiente')
                                        <a class="dropdown-item" data-id="{{ $solicitud->Uid_Solicitud }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a>
                                    @else
                                    <a class="dropdown-item" data-id="{{ $solicitud->Uid_Solicitud }}" onclick="comentarios(this)"><i class="fa fa-pencil"></i> Comentarios</a>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach --}}
            </tbody>
        </table>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{{ URL::To('/').'/src/plugins/jquery-steps/build/jquery.steps.css'}}">
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center font-18">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    <div class="clearfix">
                    <h4 class="text-blue" >Crea Solicitud {{$tipoSolicitudes->TipoSolicitud_Nombre}}</h4>
                        <p class="mb-30 font-14">Llena los campos correspondientes</p>
                    </div>
                    <div class="wizard-content">
                        <form class="tab-wizard wizard-circle wizard vertical">
                            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                            <input type="hidden" name="Uid_TipoEmpleado" id="Uid_TipoEmpleado" value="">
                            <input type="hidden" name="Uid_TipoSolicitud" id="Uid_TipoSolicitud" value="{{$tipoSolicitudes->Uid_TipoSolicitud}}">
                            <h5>General</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Solicitud_Motivo" class="control-label">Motivo:</label>
                                            <input type="text" id="Solicitud_Motivo" name="Solicitud_Motivo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h5>Fechas</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Solicitud_Inicio_Auscencia" class="control-label">Fecha Inicio</label>
                                            <input type="date" id="Solicitud_Inicio_Auscencia" name="Solicitud_Inicio_Auscencia" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Solicitud_Fin_Auscencia" class="control-label">Fecha Regreso</label>
                                            <input type="date" id="Solicitud_Fin_Auscencia" name="Solicitud_Fin_Auscencia" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_Comentarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModalComentarios"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                                <div class="form-group"  id="Motivo">
                                    <label for="Solicitud_MotivoC" class="control-label">Motivo</label>
                                    <textarea type="text" id="Solicitud_MotivoC" name="Solicitud_MotivoC" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="Solicitud_Observaciones" class="control-label">Observaciones de la respuesta</label>
                                    <textarea type="text" id="Solicitud_Observaciones" name="Solicitud_Observaciones" class="form-control"></textarea>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To('/').'/src/plugins/jquery-steps/build/jquery.steps.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/Kiosko/solicitudes.js'}}"></script>

<div class="modal fade" id="success-modal-tramites" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center font-18">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    <div class="clearfix">
                    <h4 class="text-blue" >Trámite {{$tipoSolicitudes->TipoSolicitud_Nombre}}</h4>
                        <p class="mb-30 font-14">Llena los campos correspondientes</p>
                    </div>
                    <div class="wizard-content">
                        <form class="tab-wizard wizard-circle wizard vertical">
                            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                            <input type="hidden" name="Uid_TipoEmpleado" id="Uid_TipoEmpleado" value="">
                            <input type="hidden" name="Uid_TipoSolicitud" id="Uid_TipoSolicitud" value="{{$tipoSolicitudes->Uid_TipoSolicitud}}">
                            <h5>Motivo</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <input type="text" id="Solicitud_Motivo" name="Solicitud_Motivo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h5>Fecha requerida</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="date" id="Solicitud_Inicio_Auscencia" name="Solicitud_Inicio_Auscencia" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="row">
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 mb-30">
        <div class="pd-20 bg-white border-radius-4 box-shadow">
            <div class="profile-photo">
                <a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
                <img src="{{ $usuario->Avatar}}" alt="" class="avatar-photo">
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body pd-5">
                                <div class="img-container">
                                    <img id="image" src="{{ $usuario->Avatar}}" alt="Picture">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" value="Update" class="btn btn-primary">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="text-center">{{ $usuario->Usuario_Nombre}}</h5>
            <p class="text-center text-muted"></p>
            <div class="profile-info">
                <h5 class="mb-20 weight-500">Información</h5>
                <ul>
                    <li>
                        <span>Email:</span>
                        {{ $usuario->Correo}}
                    </li>
                    <li>
                        <span>Teléfono:</span>
                        {{ $usuario->Telefono}}
                    </li>
                    <li>
                        <span>Departamento:</span>
                        {{ $usuario->Departamento}}
                    </li>

                    <li>
                        <span>Puesto:</span>
                        {{ $usuario->Puesto}}
                    </li>
                </ul>
            </div>
            {{-- <div class="profile-skills">
                <h5 class="mb-20 weight-500">Key Skills</h5>
                <h6 class="mb-5">HTML</h6>
                <div class="progress mb-20" style="height: 6px;">
                    <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>

            </div> --}}
        </div>
    </div>
    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mb-30">
        <div class="bg-white border-radius-4 box-shadow height-100-p">
            <div class="profile-tab height-100-p">
                <div class="tab height-100-p">
                    <ul class="nav nav-tabs customtab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#setting" role="tab">Personalización</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#password" role="tab">Contraseña</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">Tasks</a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <!-- Timeline Tab End -->
                        <!-- Tasks Tab start -->
                        <div class="tab-pane fade " id="tasks" role="tabpanel">
                            <div class="pd-20 profile-task-wrap">
                                <div class="container pd-0">
                                    <!-- Open Task start -->
                                    <div class="task-title row align-items-center">
                                        <div class="col-md-8 col-sm-12">
                                            <h5>Open Tasks (4 Left)</h5>
                                        </div>
                                        <div class="col-md-4 col-sm-12 text-right">
                                            <a href="task-add" data-toggle="modal" data-target="#task-add" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i> Add</a>
                                        </div>
                                    </div>
                                    <div class="profile-task-list pb-30">
                                        <ul>
                                            <li>
                                                <div class="custom-control custom-checkbox mb-5">
                                                    <input type="checkbox" class="custom-control-input" id="task-1">
                                                    <label class="custom-control-label" for="task-1"></label>
                                                </div>
                                                <div class="task-type">Email</div>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ea earum.
                                                <div class="task-assign">Assigned to Ferdinand M. <div class="due-date">due date <span>22 February 2019</span></div></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Open Task End -->
                                    <!-- Close Task start -->
                                    <div class="task-title row align-items-center">
                                        <div class="col-md-12 col-sm-12">
                                            <h5>Closed Tasks</h5>
                                        </div>
                                    </div>
                                    <div class="profile-task-list close-tasks">
                                        <ul>
                                            <li>
                                                <div class="custom-control custom-checkbox mb-5">
                                                    <input type="checkbox" class="custom-control-input" id="task-close-1" checked="" disabled="">
                                                    <label class="custom-control-label" for="task-close-1"></label>
                                                </div>
                                                <div class="task-type">Email</div>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ea earum.
                                                <div class="task-assign">Assigned to Ferdinand M. <div class="due-date">due date <span>22 February 2018</span></div></div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Tasks Tab End -->
                        <!-- Setting Tab start -->
                        <div class="tab-pane fade height-100-p show active" id="setting" role="tabpanel">
                            <div class="profile-setting">
                                    <ul class="profile-edit-list row">
                                        <li class="weight-500 col-md-12">
                                            <h4 class="text-blue mb-20">Personalice su información</h4>
                                            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                            <div class="form-group">
                                                <label>Correo</label>
                                                <input id="Empleado_Correo" name="Empleado_Correo" class="form-control form-control-lg" type="text" placeholder="Introduce su correo" value={{ $usuario->Correo}}>
                                            </div>
                                            <div class="form-group">
                                                <label>Apodo</label>
                                                <input id="Empleado_Apodo" name="Empleado_Apodo" class="form-control form-control-lg" type="text" placeholder="Introduce tu apodo" value={{ $usuario->Usuario_NickName}}>
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" class="btn btn-primary" value="Actualizar">
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade height-100-p show active" id="password" role="tabpanel">
                            <div class="profile-setting">
                                    <ul class="profile-edit-list row">
                                        <li class="weight-500 col-md-12">
                                            <h4 class="text-blue mb-20">Cambia contraseña</h4>
                                            <div class="form-group">
                                                <label>Contraseña:</label>
                                                <input class="form-control form-control-lg" type="text" placeholder="Introduce tu contraseña">
                                            </div>
                                            <div class="form-group">
                                                <label>Repite contraseña:</label>
                                                <input class="form-control form-control-lg" type="text" placeholder="Repite la contraseña">
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" class="btn btn-primary" value="Cambiar">
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                        <!-- Setting Tab End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
		window.addEventListener('DOMContentLoaded', function () {
			var image = document.getElementById('image');
			var cropBoxData;
			var canvasData;
			var cropper;

			$('#modal').on('shown.bs.modal', function () {
				cropper = new Cropper(image, {
					autoCropArea: 0.5,
					dragMode: 'move',
					aspectRatio: 3 / 3,
					restore: false,
					guides: false,
					center: false,
					highlight: false,
					cropBoxMovable: false,
					cropBoxResizable: false,
					toggleDragModeOnDblclick: false,
					ready: function () {
						cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
					}
				});
			}).on('hidden.bs.modal', function () {
				cropBoxData = cropper.getCropBoxData();
				canvasData = cropper.getCanvasData();
				cropper.destroy();
			});
		});
	</script>
@endsection

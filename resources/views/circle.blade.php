<nav class="c-circle-menu js-menu">
    <button class="c-circle-menu__toggle js-menu-toggle" title='Creación rapida'>
        <span>Toggle</span>
    </button>
    <ul class="c-circle-menu__items">
        <li class="c-circle-menu__item">
            <a href="{{ URL::To('/'). ('/tramite?tipo=baja') }}" class="c-circle-menu__link" >
                <img src="{{ URL::To('/'). ('/img/sistema/UserRemove.svg') }}" alt="" title="Baja de Empleado">
            </a>
        </li>
        <li class="c-circle-menu__item">
            <a href="{{ URL::To('/'). ('/empleados/create') }}" class="c-circle-menu__link" >
                <img src="{{ URL::To('/'). ('/img/sistema/UserAdd.svg') }}" alt="" title="Crear Empleado">
            </a>
        </li>
    </ul>
    <div class="c-circle-menu__mask js-menu-mask"></div>
</nav>

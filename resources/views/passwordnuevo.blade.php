@extends('masterlogin')
@section('content')
    <div class="form" style="height: 470px;">

        <div class="form-toggle">
        </div>
        <div class="form-panel one">
            @if(session()->has('warningmsj'))
                <div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>{{ session('warningmsj') }}</div>
            @endif
            <div class="form-header">
                <h1>Actualiza tu Contraseña</h1>
            </div>

            <div class="form-content">
                <form action="{{ URL::To('/'). '/updatepassword' }}" method="post">
                    <input type="hidden" id="csrf-token" name="_token" class="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="Uid_Usuario" id="Uid_Usuario" value="{{Auth::user()->Uid_Usuario}}" />
                    <div class="form-group">
                        <label for="usuario">Contraseña Nueva</label>
                        <input type="password" id="Usuario_Password" name="Usuario_Password" required="required">
                    </div>
                    <div class="form-group">
                        <label for="contrasena">Repite la contraseña:	</label>
                        <input type="password" id="Usuario_Password_" name="Usuario_Password_" required="required">
                    </div>
                    <div class="form-group">
                        <button type="submit">Ingresar</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="form-panel two">
        </div>
    </div>
    <script type="text/javascript" src="{{ URL::To('/'). '/js/login/password.js' }}"></script>
@endsection

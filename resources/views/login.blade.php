@extends('masterlogin')
@section('content')

        <div class="form" style="height: 470px;">

            <div class="form-toggle">
            </div>
            <div class="form-panel one">
                    @if(session()->has('warningmsj'))
                    <div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>{{ session('warningmsj') }}</div>
                @endif
                <div class="form-header">
                    <h1>Acceso al Kiosko</h1>
                </div>

                <div class="form-content">
                     <form action="{{ URL::To('/'). '/loginpost' }}" method="post">{{--action="javascript:login.login_in(this)" --}}
                        <input type="hidden" id="csrf-token" name="_token" class="token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="usuario">Usuario</label>
                            <input type="text" id="Usuario_NickName" name="Usuario_NickName" required="required">
                        </div>
                        <div class="form-group">
                            <label for="contrasena">Contraseña</label>
                            <input type="password" id="Usuario_Password" name="Usuario_Password">
                        </div>
                        <div class="form-group">
                            <button type="submit">Ingresar</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-panel two">
            </div>
        </div>
        <script type="text/javascript" src="{{ URL::To('/'). '/js/login/login2.js' }}"></script>
        @endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Estados</h5>
                <p class="font-14">Mantenimiento al catálogo de estados </p>
            </div>
            <div class="col-md-2" align="right">
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                Pais
                <select class="form-control" id="Id_Pais_C" name="Id_Pais_C"></select>
            </div>
        </div>
        <br>
        <div class="row">
            <table class="table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombre</th>
                        <th class="table-plus datatable-nosort">Pais</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog  modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="Id_Estado" id="Id_Estado" value="">
                            <div class="form-group">
                                <label for="Estado_Nombre" class="control-label">Nombre</label>
                                <input type="text" id="Estado_Nombre" name="Estado_Nombre" class="form-control">
                            </div>
                            <div class="form-group" >
                                <label for="Id_Pais" class="control-label">Pais</label>
                                <select class="form-control" id="Id_Pais" name="Id_Pais"></select>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="button" id="Id_Usuario" data-id="Id_Usuario" class="btn btn-primary" onclick="envio_usuario(this)">Guardar los cambios</button> --}}
                    <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Estado</button>
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/estados.js'}}"></script>
@endsection

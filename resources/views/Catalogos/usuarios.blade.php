@extends('paneles.ConfiguracionGeneral.master')
@section('content')
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h5 class="text-blue">Usuarios</h5>
                <p class="font-14">Mantenimiento al catálogo de Usuarios </p>
            </div>
        </div>
        <div class="row">

            <table class="table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombre</th>
                        <th class="table-plus datatable-nosort">A. Paterno</th>
                        <th class="table-plus datatable-nosort">A. Materno</th>
                        <th class="table-plus datatable-nosort">Nick Name</th>
                        <th class="table-plus datatable-nosort">Correo</th>
                        <th class="table-plus datatable-nosort">Perfil</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script src="{{ URL::To ('/'). '/js/Sistema/usuarios.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="row">
        <div class="col-md">
            <h5 class="text-blue">Incidencias</h5>
            <p class="font-14">Mantenimiento al catálogo de Incidencias </p>
        </div>
        <div class="col md" align="right">
            <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        </div>
    </div>   
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Nombre</th>
                    <th class="table-plus datatable-nosort">Tipo</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModal"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                                <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                <input type="hidden" name="Id_TipoIncidencia" id="Id_TipoIncidencia" value="">

                                <div class="form-group">
                                    <label for="TipoIncidencia_Nombre" class="control-label">Nombre</label>
                                    <input type="text" id="TipoIncidencia_Nombre" name="TipoIncidencia_Nombre" class="form-control">
                                </div>
                                <div class="form-group" >
                                    <label for="TipoIncidencia_DeduccionPercepcion" class="control-label">Tipo</label>
                                    <select class="form-control" id="TipoIncidencia_DeduccionPercepcion" name="TipoIncidencia_DeduccionPercepcion">
                                        <option selected value="2">Deducción</option>
                                        <option value="1">Percepción</option>
                                    </select>
                                </div>
                                <div class="col-6" >
                                    <div id="DivExistente">
                                        <div class="col-2">
                                            <label class="control-label">Maneja Horas</label>
                                        </div>
                                        <div class="col-4">
                                            <label class="switch switch-flat" >
                                                <input class="switch-input" type="checkbox" id="TipoIncidencia_ManejaTiempo"/>
                                                <span class="switch-label" data-on="Si" data-off="No"></span>
                                                <span class="switch-handle"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crear</button>
                <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/tipoincidencias.js'}}"></script>
@endsection

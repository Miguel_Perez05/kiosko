@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h5 class="text-blue">Grupos</h5>
                <p class="font-14">Mantenimiento al catálogo de Grupos </p>
            </div>
        </div>
        <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        <div class="row">
            <table class="data-table stripe hover nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombre</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($grupos as $grupo)
                        <tr>
                            <td>{{ $grupo->Grupo_Nombre}}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" data-id="{{ $grupo->Uid_Grupo }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a>
                                        @if(AuthUser::get_perfil()=='true')
                                            <a class="dropdown-item" data-id="{{ $grupo->Uid_Grupo }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                <input type="hidden" name="Uid_Grupo" id="Uid_Grupo" value="">

                                <div class="form-group">
                                    <label for="Grupo_Nombre" class="control-label">Nombre</label>
                                    <input type="text" id="Grupo_Nombre" name="Grupo_Nombre" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="button" id="Id_Usuario" data-id="Id_Usuario" class="btn btn-primary" onclick="envio_usuario(this)">Guardar los cambios</button> --}}
                    <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Grupo</button>
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edición(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/grupos.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
{{-- <script>
    window.onload=function()
    {
        console.log('algo');
        cargaGrafico("{{url('satisfaccion/grafico')}}");
    }
</script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Cursos</h5>
            <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="cargaGrafico()">Cargar</button>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <canvas id="chart"  width="600" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/satisfaccion.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="row">
        <div class="col-md">
            <h5 class="text-blue">Bajas</h5>
            <p class="font-14">Listado de estatus de bajas de empleados </p>
            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
        </div>
    </div>
    {{-- <div class="row">
        Pais
        <select class="form-control" id="Id_Pais_C" name="Id_Pais_C"></select>
    </div> --}}
    <br>
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Empleado</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="table-plus datatable-nosort">Fecha </th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModal"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">

                            <div class="form-group">
                                <label for="Baja_Causa" class="control-label">Causa</label>
                                <input type="text" id="Baja_Causa" name="Baja_Causa" class="form-control" readonly>
                            </div>

                            <div class="form-group">
                                <label for="Baja_Comentario" class="control-label">Comentarios</label>
                                <textarea id="Baja_Comentario" name="Baja_Comentario" class="form-control" readonly></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>               
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/bajas.js'}}"></script>
@endsection

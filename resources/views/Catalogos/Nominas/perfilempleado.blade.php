@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" href="{{ URL::To ('/'). '/css/sistema/empresas.css' }}">
<div class="row">
    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 mb-30" id="encabezado">
        <div class="pd-20 bg-white border-radius-4 box-shadow">
            <div class="profile-photo">
                <input type="hidden" name="Uid_Empleado" id="Uid_Empleado">
                <input type="hidden" name="Uid_Usuario" id="Uid_Usuario">
                <a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
                <img id="image"src="" alt="" class="avatar-photo" style="height:150px">
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body pd-5">
                                <div style="width: 100%; height: 100%; text-align:center;" class=" m-auto">
                                    <img id="img" src=""  class="img-responsive" >
                                </div>
                                <div class="form-group">
                                    <label id="LblLogo"></label>
                                    <div class="input-group">
                                        <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" type="hidden">
                                        <div class="input-group-btn">
                                            <input id="logo-id" accept="image/png, image/jpeg, image/gif" name="logo" type="file" class="attachment_upload" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" value="Actualizar" class="btn btn-primary" onclick="ActualizarAvatar()">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h5 class="text-center" id="label_Nombre"></h5>
            <p class="text-center text-muted"></p>
            <div class="profile-info">
                <h5 class="mb-20 weight-500">Información</h5>
                <ul>
                    <li>
                        <div class="row">
                            <div class="col-md">
                                <span>Email:</span>
                                <label id="label_e-mail"></label>
                            </div>
                            <div class="col-md-1">
                                <a href="#modal-mail" data-toggle="modal" data-target="#modal-mail" class="edit-avatar"><i class="fa fa-pencil"></i></a>
                            </div>
                        </div>

                    </li>
                    <li>
                        <span>Teléfono:</span>
                        <label id="label_telefono"></label>
                    </li>
                    <li>
                        <span>Departamento:</span>
                        <label id="label_departamento"></label>
                    </li>

                    <li>
                        <span>Puesto:</span>
                        <label id="label_puesto"></label>
                    </li>
                    {{-- <li align='center'>
                        <button type="button" id="BTN_Baja" data-id="" class="btn btn-primary" onclick="Baja()">Baja</button>
                    </li> --}}
                </ul>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-mail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body pd-5">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md">
                                <span>Ingresa el nuevo correo del empleado:</span>
                                <input type="text" name="newEmail" id="newEmail">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <input type="submit" value="Actualizar" class="btn btn-primary" onclick="ActualizarCorreo()">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

        <div class="col-xl-12 col-lg-8 col-md-8 col-sm-12 mb-30" id="datos">

        <div class="bg-white border-radius-4 box-shadow height-50-p">
            <div class="profile-tab height-50-p">
                <div class="tab height-50-p">
                    <ul class="nav nav-tabs customtab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Información Personal</a>
                        </li>
                        <li class="nav-item" id="tab_Contactos">
                            <a class="nav-link" data-toggle="tab" href="#contactos" role="tab">Contactos Personales</a>
                        </li>
                        <li class="nav-item" id="tab_Usuario">
                            <a class="nav-link" data-toggle="tab" href="#usuario" role="tab">Usuario</a>
                        </li>
                        <li class="nav-item" id="tab_Domicilio">
                            <a class="nav-link" data-toggle="tab" href="#domicilio" role="tab">Domicilio</a>
                        </li>
                        <li class="nav-item" id="tab_Identidad">
                            <a class="nav-link" data-toggle="tab" href="#identidad" role="tab">Identidad</a>
                        </li>
                        <li class="nav-item" id="tab_Academico">
                            <a class="nav-link" data-toggle="tab" href="#academico" role="tab">Escolaridad y Carrera</a>
                        </li>
                        <li class="nav-item" id="tab_Puesto">
                            <a class="nav-link" data-toggle="tab" href="#puesto" role="tab">Información del Puesto</a>
                        </li>
                        <li class="nav-item" id="tab_Compensacion">
                            <a class="nav-link" data-toggle="tab" href="#compensacion" role="tab">Compensación</a>
                        </li>
                        <li class="nav-item" id="tab_Formacion">
                            <a class="nav-link" data-toggle="tab" href="#formacion" role="tab">Perfil Talento</a>
                        </li>

                        {{-- <li class="nav-item" id="tab_Reconocimiento">
                            <a class="nav-link" data-toggle="tab" href="#reconocimiento" role="tab">Reconocimientos</a>
                        </li> --}}
                    </ul>

                    <div class="tab-content">
                        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">


                        @include('Catalogos.Nominas.Perfiles.personal')
                        @include('Catalogos.Nominas.Perfiles.contactos')
                        @include('Catalogos.Nominas.Perfiles.usuario')
                        @include('Catalogos.Nominas.Perfiles.domicilio')
                        @include('Catalogos.Nominas.Perfiles.identidad')
                        @include('Catalogos.Nominas.Perfiles.academico')
                        @include('Catalogos.Nominas.Perfiles.puesto')
                        @include('Catalogos.Nominas.Perfiles.formacion')
                        @include('Catalogos.Nominas.Perfiles.compensaciones')
                        {{-- @include('Catalogos.Nominas.Perfiles.reconocimientos') --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var perfilUsuario ={{AuthUser::get_perfil()}};
    var jefe = {{AuthUser::get_jefe()}};
    var usuarioUid="{{Auth::user()->Uid_Usuario}}";
</script>
<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/perfil.js'}}"></script>
{{-- <script src="{{ URL::To ('/'). '/js/Sistema/Empleados/empleados.js'}}"></script> --}}
<script src="{{ URL::To ('/'). '/js/Sistema/reconocimientos.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/entrenamientos.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/domina-idiomas.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/percepcionesdeducciones.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/sueldos.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/escolaridades.js'}}"></script>

<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/formacion.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/compensaciones.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/academico.js'}}"></script>
@endsection



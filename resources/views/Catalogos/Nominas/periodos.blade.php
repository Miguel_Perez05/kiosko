@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h5 class="text-blue">Periodos</h5>
                <p class="font-14">Mantenimiento al catálogo de Periodos </p>
            </div>
        </div>
        <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        <div class="row">
            <table class="data-table stripe hover nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Periodo</th>
                        <th class="table-plus datatable-nosort">Inicio</th>
                        <th class="table-plus datatable-nosort">Fin</th>
                        <th class="table-plus datatable-nosort">Ejercicio</th>
                        <th class="table-plus datatable-nosort">Estatus</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($periodos as $periodo)
                        <tr>
                            <td>{{ $periodo->Periodo_Nombre}}</td>
                            <td>{{ $periodo->Periodo_Inicial}}</td>
                            <td>{{ $periodo->Periodo_Final}}</td>
                            <td>{{ $periodo->Ejercicio_Nombre}}</td>
                            @if($periodo->Periodo_Estado==1)
                                <td>Actual</td>
                            @elseif($periodo->Periodo_Estado==2)
                                <td>Anterior</td>
                            @elseif($periodo->Periodo_Estado==3)
                                <td>Futuro</td>
                            @endif
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" data-id="{{ $periodo->Uid_Periodo }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a>
                                        @if(AuthUser::get_perfil()=='true')
                                            <a class="dropdown-item" data-id="{{ $periodo->Uid_Periodo }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                    <input type="hidden" name="Uid_Periodo" id="Uid_Periodo" value="">

                                    <div class="form-group">
                                        <label for="Periodo_Nombre" class="control-label">Numero</label>
                                        <input type="text" id="Periodo_Nombre" name="Periodo_Nombre" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Periodo_Inicial" class="control-label">Fecha Inicial</label>
                                        <input class="form-control date-picker" type="text" id="Periodo_Inicial" name="Periodo_Inicial" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="Periodo_Final" class="control-label">Fecha Final</label>
                                        <input class="form-control date-picker" type="text" id="Periodo_Final" name="Periodo_Final" class="form-control">
                                    </div>
                                    <div class="form-group" >
                                        <label for="Uid_Ejercicio" class="control-label">Ejercicio</label>
                                        <select class="form-control" id="Uid_Ejercicio" name="Uid_Ejercicio">
                                            <option selected></option>
                                            @foreach ($ejercicios as $ejercicio)
                                            <option value="{{ $ejercicio->Uid_Ejercicio }}">{{ $ejercicio->Ejercicio_Nombre }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" >
                                        <label for="Periodo_Estado" class="control-label">Estado</label>
                                        <select class="form-control" id="Periodo_Estado" name="Periodo_Estado">
                                            <option selected value="1">Actual</option>
                                            <option value="2">Anterior</option>
                                            <option value="3">Futuro</option>
                                        </select>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="button" id="Id_Usuario" data-id="Id_Usuario" class="btn btn-primary" onclick="envio_usuario(this)">Guardar los cambios</button> --}}
                    <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Periodo</button>
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/periodos.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
        <h5 class="text-blue">Solicitudes {{$tipoSolicitudes->TipoSolicitud_Nombre}}</h5>
            <p class="font-14"> </p>
        </div>
    </div>
    
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Colaborador</th>
                    <th class="table-plus datatable-nosort">Fecha Inicio</th>
                    <th class="table-plus datatable-nosort">Fecha Fin</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="modal_Comentarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModalComentarios"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                    <input type="hidden" name="Uid_Solicitud" id="Uid_Solicitud" value="">
                    <input type="hidden" name="Solicitud_StatusR" id="Solicitud_StatusR">
                    <div class="form-group" id="Motivo">
                        <label for="Solicitud_MotivoC" class="control-label">Motivo</label>
                        <textarea type="text" id="Solicitud_MotivoC" name="Solicitud_MotivoC" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="Solicitud_Observaciones" class="control-label">Observaciones  de la respuesta</label>
                        <textarea type="text" id="Solicitud_Observaciones" name="Solicitud_Observaciones" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="BTN_Status" data-id="" class="btn btn-primary" onclick="estatus(this)"></button>
            </div>
        </div>
    </div>
</div>
<script>
    const AuthUser="{{AuthUser::get_perfil()}}";
</script>
<script src="{{ URL::To ('/'). '/js/Sistema/solicitudes.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Puestos</h5>
                <p class="font-14">Mantenimiento al catálogo de Puestos </p>
            </div>
            <div class="col-md" align="right">
                <a>
                    <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                        <span class="oi oi-plus" style="color: white;"></span>
                    </button>
                </a>
            </div>
        </div>

        <br>
        <div class="row">
            <table class="table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombre</th>
                        <th class="table-plus datatable-nosort">Departamento</th>
                        <th class="table-plus datatable-nosort">Responsable</th>
                        <th class="table-plus datatable-nosort">Estatus</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                        <input type="hidden" name="Uid_Puesto" id="Uid_Puesto" value="">

                        <div class="row">
                            <div class="col-md">

                                <div class="form-group">
                                    <label for="Puesto_Nombre" class="control-label">Nombre</label>
                                    <input type="text" id="Puesto_Nombre" name="Puesto_Nombre" class="form-control">
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="Puesto_Descripcion" class="control-label">Descripción</label>
                                    <input type="text" id="Puesto_Descripcion" name="Puesto_Descripcion" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="Puesto_Sueldo_Min" class="control-label">Sueldo Minimo</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">$</span>
                                        <input type="number" class="form-control" style="text-align:right;" aria-label="Amount (to the nearest dollar)" name="Puesto_Sueldo_Min" id="Puesto_Sueldo_Min" >
                                        <span class="input-group-text">.00</span>
                                    </div>
                                    {{-- <input type="text" id="Puesto_Sueldo_Min" name="Puesto_Sueldo_Min" class="form-control"> --}}
                                </div>


                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="Puesto_Sueldo_Max" class="control-label">Sueldo Máximo</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">$</span>
                                        <input type="number" class="form-control" style="text-align:right;" aria-label="Amount (to the nearest dollar)" name="Puesto_Sueldo_Max" id="Puesto_Sueldo_Max" >
                                        <span class="input-group-text">.00</span>
                                    </div>
                                    {{-- <input type="number" id="Puesto_Sueldo_Max" name="Puesto_Sueldo_Max" class="form-control"> --}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group" >
                                    <label for="Uid_Responsable" class="control-label">Responsable</label>
                                    <input type="number" class="form-control" name="Responsable" id="Responsable" readonly>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group" >
                                    <label for="Uid_NivelEducativo" class="control-label">Nivel Educativo</label>
                                    <select class="form-control" id="Uid_NivelEducativo" name="Uid_NivelEducativo"></select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group" >
                                    <label for="Uid_Departamento" class="control-label">Departamento</label>
                                    <select class="form-control" id="Uid_Departamento" name="Uid_Departamento"></select>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group" >
                                    <label for="Uid_AreaFormacion" class="control-label">Area de Formación</label>
                                    <select class="form-control" id="Uid_AreaFormacion" name="Uid_AreaFormacion"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                    <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Puesto</button>
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/puestos.js'}}"></script>
@endsection

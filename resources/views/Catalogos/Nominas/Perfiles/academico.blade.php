<div class="tab-pane fade " id="academico" role="tabpanel">
        <div class="pd-20 profile-task-wrap">
            <div class="container pd-0">
                <div class="task-title row align-items-center">
                    <div class="col-md-8 col-sm-12">
                        <h4 class="text-blue mb-20">Estudios</h4>
                    </div>
                    @if(Auth::user()->Id_Perfil==1)
                        <div class="col-md-4 col-sm-12 text-right" id="Btn_Escolaridad">
                            <a href="task-add" data-toggle="modal" data-target="#task-add-carrera" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Nueva</a>
                        </div>
                    @endif
                </div>
                <div class="profile-timeline-list">
                    <div id="escolaridades"></div>
                </div>
                <br>
            </div>
            <div class="container pd-0">
                <div class="task-title row align-items-center">
                    <div class="col-md-8 col-sm-12">
                        <h4 class="text-blue mb-20">Carrera dentro de la Empresa</h4>
                    </div>
                </div>
                <div class="profile-timeline-list">
                    <div id="promociones"></div>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>

    <div class="modal fade customscroll" id="task-add-carrera" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registro de Nuevo Estudio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-0">
                    <div class="task-list-form">
                        <ul>
                            <li>
                                <div class="form-group row">
                                    <label class="col-md-4">Carrera</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="Uid_Carrera" name="Uid_Carrera"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4">Institución</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="Uid_Escuela" name="Uid_Escuela"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4">Concluida</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-flat" >
                                            <input class="switch-input" type="checkbox" id="Escolaridad_Concluida"/>
                                            <span class="switch-label" data-on="Si" data-off="No"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label>Año Ingreso</label>
                                        <select name="Escolaridad_AnoIngreso" id="Escolaridad_AnoIngreso"></select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Año Egreso</label>
                                            <select name="Escolaridad_AnoEgreso" id="Escolaridad_AnoEgreso"></select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="CrearEscolaridad()">Crear</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

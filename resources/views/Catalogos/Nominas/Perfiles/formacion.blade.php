<div class="tab-pane fade " id="formacion" role="tabpanel">
    <div class="pd-20 profile-task-wrap">
        <div class="container pd-0">
            <div class="task-title row align-items-center">
                <div class="col-md-8 col-sm-12">
                    <h4 class="text-blue mb-20">Formación</h4>
                </div>
                <div class="col-md-4 col-sm-12 text-right" id="Btn_Entrenamiento">
                    <a href="task-add" data-toggle="modal" data-target="#task-add" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Nueva</a>
                </div>
            </div>
            <div class="profile-timeline-list">
                <div id="entrenamientos"></div>
            </div>
            <br>
        </div>
        <div class="container pd-0">
            <div class="task-title row align-items-center">
                <div class="col-md-8 col-sm-12">
                    <h4 class="text-blue mb-20">Idiomas</h4>
                </div>
                <div class="col-md-4 col-sm-12 text-right" id="Btn_Idiomas">
                    <a href="task-add-reconocimiento" data-toggle="modal" data-target="#task-add-idiomas" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Nueva</a>
                </div>
            </div>
            <div class="profile-timeline-list">
                <div id="idiomas"></div>
            </div>
            <br>
        </div>
        <br>
        <div class="container pd-0">
            <div class="task-title row align-items-center">
                <div class="col-md-8 col-sm-12">
                    <h4 class="text-blue mb-20">Reconocimientos</h4>
                </div>
                <div class="col-md-4 col-sm-12 text-right" id="Btn_Reconocimientos">
                    <a href="task-add-reconocimiento" data-toggle="modal" data-target="#task-add-reconocimiento" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Nuevo</a>
                </div>
            </div>
            <div class="profile-timeline-list">
                <div id="reconocimientos"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade customscroll" id="task-add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de Nuevo Curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-0">
                <div class="task-list-form">
                    <ul>
                        <li>
                            <div class="form-group row">
                                <label class="col-md-4">Area Formación</label>
                                <div class="col-md-8">
                                    <select class="form-control" id="Uid_AreaFormacion" name="Uid_AreaFormacion"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Curso</label>
                                <div class="col-md-8">
                                    <select class="form-control" id="Uid_Curso" name="Uid_Curso" ></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Fecha</label>
                                <div class="col-md-8">
                                    <input type="date" id="Entrenamiento_Fecha" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Certificado</label>
                                <div class="col-md-8">
                                    <input type="text" id="Entrenamiento_Certificacion" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Duración (hrs)</label>
                                <div class="col-md-8">
                                    <input type="number" id="Entrenamiento_Duracion" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4">Calificación</label>
                                <div class="col-md-8">
                                    <input type="number" id="Entrenamiento_Calificacion" required>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="CrearEntrenamiento()">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade customscroll" id="task-add-reconocimiento" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de Nuevo Reconocimiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-0">
                <div class="task-list-form">
                    <ul>
                        <li>
                            <div class="form-group row">
                                <label class="col-md-4">Leyenda</label>
                                <div class="col-md-8">
                                    <textarea id="Reconocimiento_Titulo" class="form-control"></textarea>
                                </div>
                            </div>
                            <div  tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body pd-5">
                                            <div style="width: 100%; height: 100%; text-align:center;" class=" m-auto">
                                                <img id="img-R" src=""  class="img-responsive" >
                                            </div>
                                            <div class="form-group">
                                                <label id="LblLogo"></label>
                                                <div class="input-group">
                                                    <input id="fakeUploadLogo" class="form-control fake-shadow"  disabled="disabled" type="hidden">
                                                    <div class="input-group-btn">
                                                        <input id="logo-id-R" accept="image/png, image/jpeg, image/gif" name="logo" type="file" class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="CrearReconocimiento()">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade customscroll" id="task-add-idiomas" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de Idioma Dominado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-0">
                <div class="task-list-form">
                    <ul>
                        <li>
                                <div class="form-group row">
                                    <label class="col-md-6">Idioma</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="Uid_Idioma" name="Uid_Idioma"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Porcentaje Escritura / Lectura</label>
                                    <div class="col-md-6">
                                        <input type="number" id="DominaIdioma_PorcentajeEscritura" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Porcentaje Hablado</label>
                                    <div class="col-md-6">
                                        <input type="number" id="DominaIdioma_PorcentajeConversacion" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4">Fecha</label>
                                    <div class="col-md-8">
                                        <input type="date" id="DominaIdioma_Fecha" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="CrearDominaIdioma()">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

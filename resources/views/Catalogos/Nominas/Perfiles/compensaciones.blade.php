<div class="tab-pane fade " id="compensacion" role="tabpanel">
    <div class="pd-20 profile-task-wrap">
        <div class="container pd-0">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label style="font-weight:bold">Sueldo Mensual</label>
                        <input id="Empleado_SalarioMensual" name="Empleado_SalarioMensual" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="" >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label style="font-weight:bold">Ultima Actualización</label>
                        <input id="Sueldo_FechaC" name="Sueldo_FechaC" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="" >
                    </div>
                </div>
                <div class="col-md-3" id="Btn_ActualizaSueldo">
                    <div class="form-group">
                        <br>
                        <a href="task-add-sueldo" data-toggle="modal" data-target="#task-add-sueldo" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Actualizar</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-weight:bold">Tipo Empleado</label>
                        <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_TipoEmpleado" name="Uid_TipoEmpleado"></select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-weight:bold">Tipo de Nomina</label>
                        <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_TipoNomina" name="Uid_TipoNomina"></select>
                    </div>
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" id="BTN_Compensacion_Edicion" data-id="" class="btn btn-primary" onclick="EdicionCompensacion()">Editar</button>
                        <button type="button" id="BTN_Compensacion_Guardar" data-id="" class="btn btn-primary" onclick="GuardarCompensacion()">Guardar</button>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" id="BTN_Compensacion_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="container pd-0">
                {{-- <div class="task-title row align-items-center">
                    <div class="col-md-8 col-sm-12">
                        <h4 class="text-blue mb-20">Compensaciones</h4>
                    </div>
                    <div class="col-md-4 col-sm-12 text-right" id="Btn_Percepciones">
                        <a href="task-add-compensaciones" data-toggle="modal" data-target="#task-add-compensaciones" class="bg-light-blue btn text-blue weight-500">
                            <i class="ion-plus-round"></i>Nueva</a>
                    </div>
                </div> --}}
                <div class="profile-timeline-list">
                    <div id="percepciones"></div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<div class="modal fade customscroll" id="task-add-sueldo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Actualización de Sueldo Mensual</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-0">
                <div class="task-list-form">
                    <ul>
                        <li>
                                <div class="form-group row">
                                    <label class="col-md-6">Nuevo Sueldo</label>
                                    <div class="col-md-6">
                                        <input type="number" id="Sueldo_Monto">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Fecha de Aplicación</label>
                                    <div class="col-md-6">
                                        <input type="date" id="Sueldo_Fecha" value="{{ date("Y-m-d") }}">
                                    </div>
                                </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="ActualizarSueldo()">Actualizar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade customscroll" id="task-add-compensaciones" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de Compensaciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-0">
                <div class="task-list-form">
                    <ul>
                        <li>
                                <div class="form-group row">
                                    <label class="col-md-6">Monto</label>
                                    <div class="col-md-6">
                                        <input type="number" id="PercepcionDeduccion_Monto">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4">Tipo de Bono</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="Uid_TipoBono" name="Uid_TipoBono"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Comentario</label>
                                    <div class="col-md-6">
                                        <textarea id="PercepcionDeduccion_Comentario" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6">Fecha</label>
                                    <div class="col-md-6">
                                        <input type="date" id="PercepcionDeduccion_Fecha" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-tipo="5" onclick="CrearPercepcionDeduccion(this)">Crear</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

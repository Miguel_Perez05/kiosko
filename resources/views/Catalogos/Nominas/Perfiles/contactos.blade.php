<div class="tab-pane fade " id="contactos" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-50 col-md-12 ">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label style="font-weight:bold">Nombre de Contacto 1</label>
                                <input id="Empleado_Contacto1_Emergencia" name="Empleado_Contacto1_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Nombre Completo">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Teléfono Contacto 1</label>
                                <input id="Empleado_Telefono1_Emergencia" name="Empleado_Telefono1_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-weight:bold">Dirección Contacto 1</label>
                                <input id="Empleado_Domicilio1_Emergencia" name="Empleado_Domicilio1_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Calle y Numero">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label style="font-weight:bold">Nombre de Contacto 2</label>
                                <input id="Empleado_Contacto2_Emergencia" name="Empleado_Contacto2_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Nombre Completo">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Teléfono Contacto 2</label>
                                <input id="Empleado_Telefono2_Emergencia" name="Empleado_Telefono2_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-weight:bold">Dirección Contacto 2</label>
                                <input id="Empleado_Domicilio2_Emergencia" name="Empleado_Domicilio2_Emergencia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Calle y Numero">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Contactos_Edicion" data-id="" class="btn btn-primary" onclick="EdicionContactos()">Editar</button>
                                <button type="button" id="BTN_Contactos_Guardar" data-id="" class="btn btn-primary" onclick="GuardarContactos()">Guardar</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Contactos_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div>

<div class="tab-pane fade" id="puesto" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-500 col-md-12">
                    <div class="col-md-4">
                        <h4 class="text-blue mb-20">Información del Puesto</h4>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-4" id="_Estatus">
                            <label for="Id_Perfil" class="control-label" id="Lbl_Status"></label>
                            <div class="form-group" >
                                <div class="row">
                                    <div class="col-md-3">
                                        <h6>Activo</h6>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="switch switch-flat" >
                                            <input class="switch-input" type="checkbox" id="Id_Estatus"/>
                                            <span class="switch-label" data-on="Si" data-off="No"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-weight:bold">Turno</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_Turno" name="Uid_Turno"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Puesto</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_Puesto" name="Uid_Puesto"></select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Departamento</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_Departamento" name="Uid_Departamento"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Puesto_Edicion" data-id="" class="btn btn-primary" onclick="EdicionPuesto()">Editar</button>
                                <button type="button" id="BTN_Puesto_Guardar" data-id="" class="btn btn-primary" onclick="GuardarPuesto()">Guardar</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Puesto_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div>

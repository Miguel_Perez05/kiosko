<div class="tab-pane fade" id="domicilio" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-50 col-md-12 ">
                    <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                            <h4 class="text-blue mb-10">Domicilio Personal</h4>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label style="font-weight:bold">Domicilio</label>
                                <input id="Empleado_Calle" name="Empleado_Calle" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Calle y Número">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Numero</label>
                                <input id="Empleado_Numero" name="Empleado_Numero" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label style="font-weight:bold">Colonia</label>
                                <input id="Empleado_Colonia" name="Empleado_Colonia" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Código Postal</label>
                                <input id="Empleado_CP" name="Empleado_CP" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label style="font-weight:bold">Pais</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Id_Pais" name="Id_Pais"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label style="font-weight:bold">Estado</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Id_Estado" name="Id_Estado"></select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label style="font-weight:bold">Ciudad</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Id_Ciudad" name="Id_Ciudad"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Domicilio_Edicion" data-id="" class="btn btn-primary" onclick="EdicionDomicilio()">Editar</button>
                                <button type="button" id="BTN_Domicilio_Guardar" data-id="" class="btn btn-primary" onclick="GuardarDomicilio()">Guardar</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Domicilio_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div>

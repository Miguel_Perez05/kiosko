<div class="tab-pane fade height-100-p show active" id="personal" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-50 col-md-12 ">
                <div class="row">
                    <div class="col-md-8">
                            <div class="form-group">
                        <h4 class="text-blue mb-10">Información Personal</h4>
                            </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label style="font-weight:bold">Nombres</label>
                                <input id="Empleado_Nombre" name="Empleado_Nombre" class="form-control form-control-lg border-left-0 border-top-0 border-right-0 status" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label style="font-weight:bold">Género</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Empleado_Genero" name="Empleado_Genero">
                                    <option selected value="Femenino">Femenino</option>
                                    <option value="Masculino">Masculino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Apellido Paterno</label>
                                <input id="Empleado_APaterno" name="Empleado_APaterno" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Apellido Materno</label>
                                <input id="Empleado_AMaterno" name="Empleado_AMaterno" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="" required>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-4" id="divCorreo">
                            <div class="form-group">
                                <label style="font-weight:bold">Correo personal</label>
                                <input id="Usuario_Correo" name="Usuario_Correo" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="mail" placeholder="">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label style="font-weight:bold">RFC</label>
                                <input id="Empleado_RFC" name="Empleado_RFC" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="" required>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label style="font-weight:bold">CURP</label>
                                <input id="Empleado_CURP" name="Empleado_CURP" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label style="font-weight:bold">NSS IMSS</label>
                                <input id="Empleado_NSS" name="Empleado_NSS" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Estado Civil</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_EstadoCivil" name="Uid_EstadoCivil"></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Nacionalidad</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_Nacionalidad" name="Uid_Nacionalidad"></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">Teléfono</label>
                                <input id="Empleado_Telefono" name="Empleado_Telefono" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="+52 ()">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="font-weight:bold">F. Nacimiento</label>
                                <input id="Empleado_Fecha_Nacimiento" name="Empleado_Fecha_Nacimiento" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="date" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Personal_Edicion" data-id="" class="btn btn-primary" onclick="EdicionPersonal()">Editar</button>
                                <button type="button" id="BTN_Personal_Guardar" data-id="" class="btn btn-primary" onclick="GuardarPersonal()">Guardar</button>
                                <button type="button" id="BTN_Personal_Crear" data-id="" class="btn btn-primary" onclick="CrearEmpleado()">Crear</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Personal_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div>

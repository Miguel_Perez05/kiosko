<div class="tab-pane fade" id="identidad" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-500 col-md-12">
                    <div class="col-md-4">
                        <h4 class="text-blue mb-20">Identidad</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Fecha de Ingreso</label>
                                <input id="Empleado_Fecha_Ingreso" name="Empleado_Fecha_Ingreso" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="date" placeholder="" value={{ $empleado->Empleado_Fecha_Nacimiento ?? ''}}>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight:bold">Fecha de Reingreso</label>
                                <input id="Empleado_Fecha_ReIngreso" name="Empleado_Fecha_ReIngreso" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="date" placeholder="" value={{ $empleado->Empleado_Fecha_Nacimiento ?? ''}}>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-weight:bold">Registro Patronal</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_RegistroPatronal" name="Uid_RegistroPatronal"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-weight:bold">Empresa</label>
                                <select class="form-control border-left-0 border-top-0 border-right-0" id="Uid_Empresa" name="Uid_Empresa"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Identidad_Edicion" data-id="" class="btn btn-primary" onclick="EdicionIdentidad()">Editar</button>
                                <button type="button" id="BTN_Identidad_Guardar" data-id="" class="btn btn-primary" onclick="GuardarIdentidad()">Guardar</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Identidad_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div>

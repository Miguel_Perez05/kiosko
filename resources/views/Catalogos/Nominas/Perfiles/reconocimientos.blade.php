<div class="tab-pane fade " id="reconocimiento" role="tabpanel">
    <div class="pd-20 profile-task-wrap">
        <div class="container pd-0">
            <div class="task-title row align-items-center">
                <div class="col-md-8 col-sm-12">
                    <h4 class="text-blue mb-20">Reconocimientos</h4>
                </div>
                <div class="col-md-4 col-sm-12 text-right">
                    <a href="task-add" data-toggle="modal" data-target="#task-add" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i>Agregar</a>
                </div>
            </div>
            <div class="profile-task-list pb-30">
                <ul>
                    <li>
                        <div class="task-type">Email</div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ea earum.
                        <div class="task-assign">Assigned to Ferdinand M. <div class="due-date">due date <span>22 February 2019</span></div></div>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>

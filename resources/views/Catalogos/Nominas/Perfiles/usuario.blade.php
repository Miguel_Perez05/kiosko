<div class="tab-pane fade " id="usuario" role="tabpanel">
    <div class="profile-setting">
            <ul class="profile-edit-list row">
                <li class="weight-50 col-md-12 ">
                    <h4 class="text-blue mb-20">Personalice su información</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Correo</label>
                                <input type="hidden" id="Usuario_Correo_" name="Usuario_Correo_">
                                <input id="Usuario_Correo_E" name="Usuario_Correo_E" class="form-control form-control-lg" type="text" placeholder="Introduce tu correo" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nick Name</label>
                                <input id="Usuario_Apodo_" name="Usuario_Apodo_" type="hidden">
                                <input id="Usuario_Apodo_E" name="Usuario_Apodo_E" class="form-control form-control-lg" type="text" placeholder="Introduce tu nick name" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" id="BTN_Usuario_Edicion" data-id="" class="btn btn-primary" onclick="EdicionUsuario()">Editar</button>
                                <button type="button" id="BTN_Usuario_Guardar" data-id="" class="btn btn-primary" onclick="GuardarUsuario()">Guardar</button>
                            </div>
                            <div class="col-md-6" align="right">
                                <button type="button" id="BTN_Usuario_Cancelar" data-id="" class="btn btn-secondary" onclick="CancelarEdicion()">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </li>
            {{-- </ul>
            <ul class="profile-edit-list row"> --}}
                <li class="weight-500 col-md-12">
                    <h4 class="text-blue mb-20">Cambia contraseña</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contraseña:</label>
                                <input id="Password" name="Password" class="form-control form-control-lg" type="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Repite contraseña:</label>
                                <input id="Password1" name="Password1" class="form-control form-control-lg" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <button type="button" id="BTN_Password_Actualizar" data-id="" class="btn btn-primary" onclick="PasswordUsuario()">Actualizar</button>
                    </div>

                </li>
            </ul>
    </div>
</div>

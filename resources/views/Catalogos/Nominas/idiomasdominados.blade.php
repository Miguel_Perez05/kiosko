@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Idiomas Dominados</h5>
                <p class="font-14">Listado de Idiomas que dominan los empleados</p>
            </div>
            <div class="col-md-2" align="right">
                <a>
                    <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                        <span class="oi oi-plus" style="color: white;"></span>
                    </button>
                </a>
            </div>
        </div>
        <div class="row">
            <table class="data-table stripe hover nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Idioma</th>
                        <th class="table-plus datatable-nosort">Empleado</th>
                        <th class="table-plus datatable-nosort">% Escrito</th>
                        <th class="table-plus datatable-nosort">% Oral</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($d_idiomas as $d_idioma)
                        <tr>
                            <td>{{ $d_idioma->Idioma_Nombre}}</td>
                            <td>{{ $d_idioma->Empleado_Nombre}}</td>
                            <td>{{ $d_idioma->DominaIdioma_PorcentajeEscritura}}</td>
                            <td>{{ $d_idioma->DominaIdioma_PorcentajeConversación}}</td>
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" data-id="{{ $d_idioma->Uid_Dominio }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a>
                                        @if(AuthUser::get_perfil()=='true')
                                            <a class="dropdown-item" data-id="{{ $d_idioma->Uid_Dominio }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                <input type="hidden" name="Uid_Dominio" id="Uid_Dominio" value="">

                                <div class="form-group">
                                    <label for="idioma_Nombre" class="control-label">Idioma</label>
                                    <input type="text" id="Idioma_Nombre" name="Idioma_Nombre" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Empleado_Nombre" class="control-label">Empleado</label>
                                    <input type="text" id="Empleado_Nombre" name="Empleado_Nombre" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="DominaIdioma_PorcentajeEscritura" class="control-label">Porcentaje Escrito</label>
                                    <input type="text" id="DominaIdioma_PorcentajeEscritura" name="DominaIdioma_PorcentajeEscritura" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="DominaIdioma_PorcentajeConversación" class="control-label">Porcentaje Hablado</label>
                                    <input type="text" id="DominaIdioma_PorcentajeConversación" name="DominaIdioma_PorcentajeConversación" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                    {{-- <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Idioma</button> --}}
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edición(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/domina-idiomas.js'}}"></script>
@endsection


@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="row">
        <div class="col-md">
            <h5 class="text-blue">Registros Patronales</h5>
            <p class="font-14">Mantenimiento al catálogo de Registros Patronales </p>
        </div>
        <div class="col-md" align="right">
            <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        </div>
    </div>
    
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Identificador</th>
                    <th class="table-plus datatable-nosort">Empresa</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <label id="LabelModal"><h5 class="modal-title"></h5></label>
        </div>
        <div class="modal-body">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                        <input type="hidden" name="Uid_RegistroPatronal" id="Uid_RegistroPatronal" value="">

                        <div class="form-group">
                            <label for="RegistroPatronal_Nombre" class="control-label">Identificador</label>
                            <input type="text" id="RegistroPatronal_Nombre" name="RegistroPatronal_Nombre" class="form-control">
                        </div>

                        <div class="form-group" >
                            <label for="Uid_Empresa" class="control-label">Empresa</label>
                            <select class="form-control" id="Uid_Empresa" name="Uid_Empresa"></select>
                        </div>                            
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crear</button>
                <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/registrospatronales.js'}}"></script>
@endsection

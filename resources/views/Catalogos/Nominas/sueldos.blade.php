@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
        <h5 class="text-blue">Historial de cambios de Sueldo</h5>
            <p class="font-14"> </p>
        </div>
    </div>
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Colaborador</th>
                    <th class="table-plus datatable-nosort">Fecha de Actualización</th>
                    <th class="table-plus datatable-nosort">Monto</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/sueldos.js'}}"></script>
@endsection

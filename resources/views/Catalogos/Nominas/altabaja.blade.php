@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Empleados</h5>
                <p class="font-14">Mantenimiento al catálogo de Empleados </p>
            </div>
            <div class="col-md-2" align="right">
                <a>
                    <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                        <span class="oi oi-plus" style="color: white;"></span>
                    </button>
                </a>
            </div>
        </div>
    </div>
    
    <div class="row">
        <table class="data-table stripe hover nowrap">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Nombres</th>
                    <th class="table-plus datatable-nosort">RFC</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($historiales as $historial)
                    <tr>
                        <td>{{ $historial->Empleado_Nombre . " " . $historial->Empleado_APaterno . " " . $historial->Empleado_AMaterno}}</td>
                        <td>{{ $historial->Empleado_RFC}}</td>
                        @if($historial->Historial_Ingreso)
                            <td>Alta</td>
                        @elseif($historial->Historial_Egreso)
                            <td>Baja</td>
                        @elseif($historial->Historial_Reingreso)
                            <td>Reingreso</td>
                        @endif
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    {{-- @if($historial->Historial_Ingreso || $historial->Historial_Reingreso)
                                        <a class="dropdown-item" data-Uid_Empleado="{{$historial->Uid_Empleado}}" data-Uid_Empresa="{{$historial->Uid_Empresa}}" data-id="{{ $historial->Uid_Historial }}" data-status="Baja" onclick="aceptar(this)"><i class="fa fa-pencil"></i> Baja</a> --}}
                                    @if($historial->Historial_Egreso)
                                        <a class="dropdown-item" data-Uid_Empleado="{{$historial->Uid_Empleado}}" data-Uid_Empresa="{{$historial->Uid_Empresa}}" data-id="{{ $historial->Uid_Historial }}" data-status="Reingreso" onclick="aceptar(this)"><i class="fa fa-pencil"></i> Reingreso</a>
                                    @endif
                                    {{-- <a class="dropdown-item" data-id="{{ $historial->Uid_Historial }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a> --}}
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{{ URL::To('/').'/src/plugins/jquery-steps/build/jquery.steps.css'}}">
<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center font-18">
                <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                    <div class="clearfix">
                        <h4 class="text-blue" >Crea Empleado </h4>
                        <p class="mb-30 font-14">Llena los camps corresponientes</p>
                    </div>
                    <div class="wizard-content">
                        <form class="tab-wizard wizard-circle wizard vertical">
                            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                            <input type="hidden" name="Uid_TipoEmpleado" id="Uid_TipoEmpleado" value="">
                            <h5>General</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Empleado_Nombre" class="control-label">Nombres</label>
                                            <input type="text" id="Empleado_Nombre" name="Empleado_Nombre" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" >
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="Empleado_APaterno" class="control-label">Apellido Paterno</label>
                                                        <input type="text" id="Empleado_APaterno" name="Empleado_APaterno" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="Empleado_AMaterno" class="control-label">Apellido Materno</label>
                                                        <input type="text" id="Empleado_AMaterno" name="Empleado_AMaterno" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Fecha Nacimiento</label>
                                            <input type="date" id="Empleado_Fecha_Nacimiento" name="Empleado_Fecha_Nacimiento" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Empleado_RFC" class="control-label">RFC</label>
                                            <input type="text" id="Empleado_RFC" name="Empleado_RFC" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h5>Registro</h5>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Usuario_Correo" class="control-label">E-mail</label>
                                            <input type="email" id="Usuario_Correo" name="Usuario_Correo" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Uid_Empresa" class="control-label">Empresa</label>
                                            <select class="form-control" id="Uid_Empresa" name="Uid_Empresa">
                                                <option selected></option>
                                                @foreach ($empresas as $empresa)
                                                <option value="{{ $empresa->Uid_Empresa }}">{{ $empresa->Empresa_Nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Uid_RegistroPatronal" class="control-label">Registro Patronal</label>
                                            <select class="form-control" id="Uid_RegistroPatronal" name="Uid_RegistroPatronal">
                                                <option selected></option>
                                                @foreach ($registrospatronales as $registropatronal)
                                                <option value="{{ $registropatronal->Uid_RegistroPatronal }}">{{ $registropatronal->RegistroPatronal_Nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="Empleado_Fecha_Ingreso" class="control-label">Fecha Ingreso</label>
                                            <input type="date" id="Empleado_Fecha_Ingreso" name="Empleado_Fecha_Ingreso" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To('/').'/src/plugins/jquery-steps/build/jquery.steps.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/altasbajas.js'}}"></script>
@endsection

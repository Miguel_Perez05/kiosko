@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
        <h5 class="text-blue" >Bonos</h5>
        <input type="hidden"/>
            <p class="font-14">Mantenimiento al catálogo de Bonos</p>
        </div>
    </div>
    <a>
        <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
            <span class="oi oi-plus" style="color: white;"></span>
        </button>
    </a>
    <div class="row">
        <table class="data-table stripe hover nowrap">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Fecha</th>
                    <th class="table-plus datatable-nosort">Empleado</th>
                    <th class="table-plus datatable-nosort">Monto</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($bonos as $bono)
                    <tr>
                        <td>{{ $bono->PercepcionDeduccion_Fecha}}</td>
                        <td>{{ $bono->Empleado_Nombre}}</td>
                        <td>{{ $bono->PercepcionDeduccion_Monto}}</td>

                        <td>
                            <div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    {{-- <a class="dropdown-item" data-id="{{ $departamento->Uid_Departamento }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a> --}}
                                    @if(AuthUser::get_perfil()=='true')
                                        <a class="dropdown-item" data-id="{{ $bono->Uid_PercepcionDeduccion }}" data-tipo="{{ $bono->Id_TipoIncidencia }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



<script src="{{ URL::To ('/'). '/js/Sistema/percepcionesdeducciones.js'}}"></script>
@endsection

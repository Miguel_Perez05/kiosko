@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/dist/css/bootstrap-clockpicker.min.css' }}">
<link rel="stylesheet" href="{{ URL::To('/'). '/css/sistema/empresas.css' }}">
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="row">
        <div class="col-md">
            <h5 class="text-blue">Contratos</h5>
        </div>
        <div class="col-md-2" align="right">
            <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label>Empresa</label>
            <select class="form-control" id="Uid_Empresa" name="Uid_Empresa"></select>
        </div>
    </div>
    <br>
    <hr style="background-color: blue">
    <br>
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Folio</th>
                    <th class="table-plus datatable-nosort">Fecha Contrato</th>
                    <th class="table-plus datatable-nosort">Empleado</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/contratos.js'}}"></script>
@endsection

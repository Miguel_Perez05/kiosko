@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/dist/css/bootstrap-clockpicker.min.css' }}">
<link rel="stylesheet" href="{{ URL::To('/'). '/css/sistema/empresas.css' }}">
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Niveles Educativos</h5>
            <p class="font-14">Mantenimiento al catálogo de Niveles Educativos </p>
        </div>
    </div>
    <a>
        <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
            <span class="oi oi-plus" style="color: white;"></span>
        </button>
    </a>
    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Nombre</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModal"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                            <input type="hidden" name="Uid_NivelEducativo" id="Uid_NivelEducativo" value="">

                            <div class="form-group">
                                <label for="NivelEducativo_Nombre" class="control-label">Nombre</label>
                                <input type="text" id="NivelEducativo_Nombre" name="NivelEducativo_Nombre" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                {{-- <button type="button" id="Id_Usuario" data-id="Id_Usuario" class="btn btn-primary" onclick="envio_usuario(this)">Guardar los cambios</button> --}}
                <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Nivel Educativo</button>
                <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar los cambios</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/niveleseducativos.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <label for="Uid_Puesto" class="control-label">Selecciona el Puesto</label>
        <select class="form-control" id="Uid_Puesto" name="Uid_Puesto"></select>
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Candidatos</h5>
            <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="cargaGrafico()">Cargar</button>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <canvas id="chart"  width="600" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="postulacion" style="display:none">
        <label class="control-label" >Mejor Candidato</label>
        <label class="control-label" id="candidato"></label>
        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
        <input type="hidden" name="Uid_Puesto_" id="Uid_Puesto_" value="">
        <input type="hidden" name="Uid_Empleado" id="Uid_Empleado" value="">
        <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="asignarPuesto()">Asignar</button>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/Vacantes/graficos.js'}}"></script>
@endsection

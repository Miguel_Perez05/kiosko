@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Asignación de puesto</h5>
        </div>
    </div>
    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
    <input type="hidden" name="Uid_Puesto" id="Uid_Puesto">
    <div class="row">
        <div class="col-md">
            <label>Departamento</label>
            <input type="text" class="form-control" name="Departamento_Nombre" id="Departamento_Nombre" readonly>
        </div>
        <div class="col-md">
            <label>Puesto</label>
            <input type="text" class="form-control" name="Puesto_Nombre" id="Puesto_Nombre" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <label>Empleado</label>
            <select class="form-control" id="Uid_Empleado"></select>
        </div>
        <div class="col-md">
            <label>Sueldo</label>
            <div class="input-group mb-3">
                <span class="input-group-text">$</span>
                <input type="number" class="form-control" style="text-align:right;" aria-label="Amount (to the nearest dollar)" name="Asignacion_Sueldo" id="Asignacion_Sueldo" >
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col md" align="right">
            <button type="button" id="Generar" data-id="" class="btn btn-primary" onclick="asignarPuesto()">Asignar</button>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Vacantes/asignacion.js'}}"></script>



@endsection

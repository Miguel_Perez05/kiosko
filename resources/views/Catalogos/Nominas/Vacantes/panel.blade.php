@extends('master')
@section('content')
<div class="fh5co-gallery">
    <a class="gallery-item" href="{{ URL::To('/'). ('/vacantes') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Vacantes</h2>
            <span>Asigan el metodo de promoción de las vacantes</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/graficos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Estudio de Vacante</h2>
            <span>Analiza quien es mejor candidato para las vacantes internas</span>
        </span>
    </a>
</div>
@stop

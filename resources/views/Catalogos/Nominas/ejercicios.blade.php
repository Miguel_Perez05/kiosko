@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h5 class="text-blue">Ejercicios</h5>
                <p class="font-14">Mantenimiento al catálogo de Ejercicios </p>
            </div>
        </div>
        <a>
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        <div class="row">
            <table class="data-table stripe hover nowrap">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Ejercicio</th>
                        <th class="table-plus datatable-nosort">Estatus</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ejercicios as $ejercicio)
                        <tr>
                            <td>{{ $ejercicio->Ejercicio_Nombre}}</td>
                            @if($ejercicio->Ejercicio_Estado==1)
                                <td>Actual</td>
                            @elseif($ejercicio->Ejercicio_Estado==2)
                                <td>Anterior</td>
                            @elseif($ejercicio->Ejercicio_Estado==3)
                                <td>Futuro</td>
                            @endif
                            <td>
                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" data-id="{{ $ejercicio->Uid_Ejercicio }}" onclick="editar(this)"><i class="fa fa-pencil"></i> Edit</a>
                                        @if(AuthUser::get_perfil()=='true')
                                            <a class="dropdown-item" data-id="{{ $ejercicio->Uid_Ejercicio }}" onclick="eliminar(this)"><i class="fa fa-pencil"></i> Eliminar</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label id="LabelModal"><h5 class="modal-title"></h5></label>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                                <input type="hidden" name="Uid_Ejercicio" id="Uid_Ejercicio" value="">

                                <div class="form-group">
                                    <label for="Ejercicio_Nombre" class="control-label">Ejercicio</label>
                                    <input class="form-control date-picker" type="text" id="Ejercicio_Nombre" name="Ejercicio_Nombre" class="form-control">
                                </div>
                                <div class="form-group" >
                                    <label for="Ejercicio_Estado" class="control-label">Estado</label>
                                    <select class="form-control" id="Ejercicio_Estado" name="Ejercicio_Estado">
                                        <option selected value="1">Actual</option>
                                        <option value="2">Anterior</option>
                                        <option value="3">Futuro</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {{-- <button type="button" id="Id_Usuario" data-id="Id_Usuario" class="btn btn-primary" onclick="envio_usuario(this)">Guardar los cambios</button> --}}
                    <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crea Ejercicio</button>
                    <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edición(this)">Guardar los cambios</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/ejercicios.js'}}"></script>
@endsection

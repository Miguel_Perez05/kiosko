@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" href="{{ URL::To ('/'). '/css/sistema/empresas.css' }}">
<div class="row">
    <div class="col-xl-12 col-lg-8 col-md-8 col-sm-12 mb-30" id="datos">
        <div class="bg-white border-radius-4 box-shadow height-50-p">
            <div class="profile-tab height-50-p">
                <div class="tab height-50-p">
                    <ul class="nav nav-tabs customtab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Información de la Solicitud</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                        <input type="hidden" name="Uid_Solicitud" id="Uid_Solicitud">
                        <div class="profile-setting">
                                <ul class="profile-edit-list row">
                                    <li class="weight-50 col-md-12 ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="font-weight:bold">Empleado Solicitante</label>
                                                    <input id="Empleado_Nombres" name="Empleado_Nombres" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Nombre Completo">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label style="font-weight:bold">Motivo</label>
                                                    <input id="Solicitud_Motivo" name="Solicitud_Motivo" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="Calle y Numero">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label style="font-weight:bold">Fecha Solicitud</label>
                                                        <input id="Solicitud_Fecha" name="Solicitud_Fecha" class="form-control form-control-lg border-left-0 border-top-0 border-right-0" type="text" placeholder="">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                        <label for="Solicitud_Inicio_Auscencia" class="control-label">Fecha Inicio</label>
                                                        <input type="date" id="Solicitud_Inicio_Auscencia" name="Solicitud_Inicio_Auscencia" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                        <label for="Solicitud_Fin_Auscencia" class="control-label">Fecha Regreso</label>
                                                        <input type="date" id="Solicitud_Fin_Auscencia" name="Solicitud_Fin_Auscencia" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-0">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="button" id="BTN_Contactos_Edicion" class="btn btn-success" data-id="{{ $solicitud->Uid_Solicitud }}" data-status="Autorizado" onclick="cambiaEstatus(this)">Autorizar</button>

                                                </div>
                                                <div class="col-md-6" align="right">
                                                        <button type="button" id="BTN_Contactos_Guardar" class="btn btn-danger" data-id="{{ $solicitud->Uid_Solicitud }}" data-status="Rechazado" onclick="cambiaEstatus(this)">Rechazar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        var date = Date();
        date = $.datepicker.formatDate( "yy-mm-dd",new Date(date));
        document.getElementById("datos").className = "col-xl-12 col-lg-8 col-md-8 col-sm-12 mb-30";
        var solicitud={!! json_encode($solicitud)!!};
        $("#Empleado_Nombres").val(solicitud.Empleado_Nombres);
        $("#Solicitud_Fecha").val(solicitud.created_at);
        $("#Solicitud_Motivo").val(solicitud.Solicitud_Motivo);
        $("#Solicitud_Inicio_Auscencia").val(solicitud.Solicitud_Inicio_Auscencia);
        $("#Solicitud_Fin_Auscencia").val(solicitud.Solicitud_Fin_Auscencia);

    });
</script>
<div class="modal fade" id="modal_Comentarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModalComentarios"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                        <input type="hidden" name="Uid_Solicitud" id="Uid_Solicitud" value="">
                        <input type="hidden" name="Solicitud_StatusR" id="Solicitud_StatusR">
                        <div class="form-group" id="Motivo">
                            <label for="Solicitud_MotivoC" class="control-label">Motivo</label>
                            <textarea type="text" id="Solicitud_MotivoC" name="Solicitud_MotivoC" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="Solicitud_Observaciónes" class="control-label">Observaciónes  de la respuesta</label>
                            <textarea type="text" id="Solicitud_Observaciónes" name="Solicitud_Observaciónes" class="form-control"></textarea>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="BTN_Status" data-id="" class="btn btn-primary" onclick="estatus(this)"></button>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/solicitudes.js'}}"></script>
@endsection



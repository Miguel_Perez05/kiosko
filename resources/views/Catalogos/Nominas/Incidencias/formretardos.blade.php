@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Retardo</h5>
        </div>
    </div>
    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
    <div class="row">
        <div class="col-md">
            <label>Empleado</label>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md">
            <select class="form-control" style="width: 100%;" id="Uid_Empleado"></select>
        </div>
        <div class="col-md-2" align="right">
            <button type="button" class="btn btn-primary Save">Guardar</button>
            <button type="button" class="btn btn-primary Next">Continuar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="Incidencia_DiaInicio" class="control-label">Fecha Retardo</label>
                <input type="date" id="Incidencia_DiaInicio" name="Incidencia_DiaInicio" class="form-control" value="<?php echo date('Y-m-d'); ?>">
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label for="Incidencia_HoraEntrada" class="control-label">Hora de entrada</label>
                <input type="time" id="Incidencia_HoraEntrada" name="Incidencia_HoraEntrada" class="form-control" value="<?php echo date("h:i:s"); ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <label for="Incidencia_Comentario" class="control-label">Comentarios</label>
            <textarea id="Incidencia_Comentario" name="Incidencia_Comentario" class="form-control" 
                        rows="3" style="height: auto;resize: vertical;"></textarea>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Incidencias/retardosform.js'}}"></script>



@endsection

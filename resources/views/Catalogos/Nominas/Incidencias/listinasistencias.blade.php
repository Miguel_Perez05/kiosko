@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="row">
        <div class="col-md">
            <h5 class="text-blue">Inasistencias</h5>
            <p class="font-14">Listado de Empleados con Inasistencias</p>
            <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
        </div>
        <div class="col-md" align="right">
            <a>
                <button type="button" class="btn btn-info btn-sm NewVacation" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </a>
        </div>
    </div>

    <div class="row">
        <table class="table-striped hoverable responsive" id="table">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Tipo</th>
                    <th class="table-plus datatable-nosort">Empleado</th>
                    <th class="table-plus datatable-nosort">Fecha Inasistencia</th>
                    <th class="table-plus datatable-nosort">Estatus</th>
                    <th class="datatable-nosort"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="modal fade" id="modal_Comentarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label id="LabelModalComentarios"><h5 class="modal-title"></h5></label>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                                <input type="hidden" name="Uid_Incidencia" id="Uid_Incidencia" value="">
                                <div class="form-group">
                                    <label for="Incidencia_Comentarios" class="control-label">Comentarios</label>
                                    <textarea type="text" id="Incidencia_Comentarios" name="Incidencia_Comentarios" class="form-control"></textarea>
                                </div>
                                <div id="Justificado">
                                    <h4 align="center"> Justificación</h4>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="Incidencia_QuienJustifica" class="control-label">Persona que Justifica</label>
                                                <input type="text" id="Incidencia_QuienJustifica" name="Incidencia_QuienJustifica" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="Incidencia_FechaJustificacion" class="control-label">Fecha Justificación</label>
                                                <input type="date" id="Incidencia_FechaJustificacion" name="Incidencia_FechaJustificacion" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="Incidencia_ComentarioJustifica" class="control-label">Comentarios</label>
                                                <textarea type="text" id="Incidencia_ComentarioJustifica" name="Incidencia_ComentarioJustifica" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="BTN_Justificar" data-id="" class="btn btn-primary" onclick="justificar(this)">Justificar</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::To ('/'). '/js/Sistema/Incidencias/inasistencias.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" href="{{ URL::To ('/'). '/css/sistema/empresas.css' }}">
<div class="row">
    <div class="col-xl-12 col-lg-8 col-md-8 col-sm-12 mb-30" id="datos">
        <div class="bg-white border-radius-4 box-shadow height-50-p">
            <div class="profile-tab height-50-p">
                <div class="tab height-50-p">
                    <ul class="nav nav-tabs customtab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Información Personal</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                        @include('Catalogos.Nominas.Perfiles.personal')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Empleados/newemployee.js'}}"></script>
{{-- <script src="{{ URL::To ('/'). '/js/Sistema/Empleados/empleados.js'}}"></script> --}}
@endsection



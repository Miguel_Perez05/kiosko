@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/dist/css/bootstrap-clockpicker.min.css' }}">
<link rel="stylesheet" href="{{ URL::To('/'). '/css/sistema/empresas.css' }}">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">{{$typeString}}</h5>
                <p class="font-14">Mantenimiento al catálogo de {{$typeString}} </p>
            </div>
            <div class="col-md-2" align="right">
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombres</th>
                        <th class="table-plus datatable-nosort">A. Paterno</th>
                        <th class="table-plus datatable-nosort">A. Materno</th>
                        <th class="table-plus datatable-nosort">RFC</th>
                        <th class="table-plus datatable-nosort">Puesto</th>
                        <th class="table-plus datatable-nosort">Departamento</th>
                        <th class="table-plus datatable-nosort">Estatus</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="{{ URL::To ('/'). '/js/Sistema/Empleados/empleados.js'}}"></script>
@endsection

@extends('paneles.ConfiguracionGeneral.master')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/dist/css/bootstrap-clockpicker.min.css' }}">
<link rel="stylesheet" href="{{ URL::To('/'). '/css/sistema/empresas.css' }}">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Empleados Con Contrato</h5>
                <p class="font-14">Mantenimiento al catálogo de Empleados Con Contrato </p>
            </div>
            <div class="col-md-2" align="right">
                <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                    <span class="oi oi-plus" style="color: white;"></span>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group" >
                    <label for="Empresas" class="control-label">Seleccione la Empresa</label>
                    <select name="" id="Empresas" class="form-control"></select>
                </div>
            </div>

        </div>
        <div class="row">
            <table class="table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        <th class="table-plus datatable-nosort">Nombres</th>
                        <th class="table-plus datatable-nosort">Empresa</th>
                        <th class="table-plus datatable-nosort">RFC</th>
                        <th class="table-plus datatable-nosort">Puesto</th>
                        <th class="table-plus datatable-nosort">Departamento</th>
                        <th class="table-plus datatable-nosort">Estatus</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
    <script src="{{ URL::To ('/'). '/js/Sistema/Empleados/empleadoscontrato.js'}}"></script>
@endsection

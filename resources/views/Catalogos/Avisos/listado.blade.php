@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <h5 class="text-blue">Avisos</h5>
                <p class="font-14">Mantenimiento al catálogo de Avisos </p>
            </div>
            <div class="col-md" align="right">
                <a>
                    <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                        <span class="oi oi-plus" style="color: white;"></span>
                    </button>
                </a>
            </div>
        </div>
        <div class="row">
            <table class="table-striped hoverable responsive" id="table">
                <thead>
                    <tr>
                        {{-- <th class="table-plus datatable-nosort">Dirijido a:</th> --}}
                        <th class="table-plus datatable-nosort">Aviso</th>
                        <th class="table-plus datatable-nosort">Prioridad</th>
                        <th class="table-plus datatable-nosort">Fecha inicio</th>
                        <th class="table-plus datatable-nosort">Fecha Vigencia</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/Avisos/listado.js'}}"></script>
    <script>

        </script>
@endsection

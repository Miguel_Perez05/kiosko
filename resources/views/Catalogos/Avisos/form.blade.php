@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="css/sistema/empresas.css">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="row">
            <div class="col-md">
                <label id="LabelModal" name="LabelModal" class="control-label"></label>
                <p class="font-14">Formulario de Avisos </p>
            </div>
            <div class="col-md" align="right">
                <a>
                    <button type="button" class="btn btn-info btn-sm" onclick="nuevo()" title="Crear">
                        <span class="oi oi-plus" style="color: white;"></span>
                    </button>
                </a>
            </div>
        </div>
        <div class="row">

            <div class="col-12">

                <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
                <input type="hidden" name="Uid_Aviso" id="Uid_Aviso" value="">

                <div class="form-group">
                    <label for="Aviso_Titulo" class="control-label">Titulo</label>
                    <input type="text" id="Aviso_Titulo" name="Aviso_Titulo" class="form-control">
                </div>
                <div class="form-group">
                    <label for="Aviso_Asunto" class="control-label">Descripción</label>
                    <textarea rows="2" id="Aviso_Asunto" name="Aviso_Asunto" class="form-control"></textarea>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >Fecha de Inicio:</label>
                            </div>
                            <div class="form-group">
                                <input id="Aviso_Inicio" type="date" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >Fecha Final:</label>
                            </div>
                            <div class="form-group">
                                <input id="Aviso_Vigencia" type="date" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-6" >
                            <label for="Id_Perfil" class="control-label">Prioridad</label>
                        </div>
                        <div class="col-6" >
                                <div class="form-group" >
                                    <select class="form-control" id="Aviso_Prioridad" name="Aviso_Prioridad">

                                        <option value="1">Alta</option>
                                        <option value="2">Media</option>
                                        <option value="3">Baja</option>
                                        </select>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-6" >
                            <label for="Id_Perfil" class="control-label">General</label>
                        </div>
                        <div class="col-6" >
                            <label class="switch switch-flat" >
                                <input class="switch-input" type="checkbox" id="Aviso_General" onclick="general()"/>
                                <span class="switch-label" data-on="Si" data-off="No"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row" id="Departamento">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-3" >
                                <label for="Id_Perfil" class="control-label">Departamento</label>
                            </div>
                            <div class="col-2" >
                                <label class="switch switch-flat" >
                                    <input class="switch-input" type="checkbox" id="Aviso_Departamento" onclick="departamento()"/>
                                    <span class="switch-label" data-on="Si" data-off="No"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="col-7" >
                                <div class="form-group" >
                                    <select class="form-control" id="Uid_Departamento" name="Uid_Departamento"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="Grupo">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-3" >
                                <label for="Id_Perfil" class="control-label">Grupo</label>
                            </div>
                            <div class="col-2" >
                                <label class="switch switch-flat" >
                                    <input class="switch-input" type="checkbox" id="Aviso_Grupo" onclick="grupo()"/>
                                    <span class="switch-label" data-on="Si" data-off="No"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="col-7">
                                <div class="form-group" >
                                    <select class="form-control" id="Uid_Grupo" name="Uid_Grupo"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="Turno">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-3" >
                                <label for="Id_Perfil" class="control-label">Turno</label>
                            </div>
                            <div class="col-2" >
                                <label class="switch switch-flat" >
                                    <input class="switch-input" type="checkbox" id="Aviso_Turno" onclick="turno()"/>
                                    <span class="switch-label" data-on="Si" data-off="No"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="col-7">
                                <div class="form-group" >
                                    <select class="form-control" id="Uid_Turno" name="Uid_Turno"></select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row" id="Empleado">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-3" >
                                <label for="Id_Perfil" class="control-label">Empleado</label>
                            </div>
                            <div class="col-2" >
                                <label class="switch switch-flat" >
                                    <input class="switch-input" type="checkbox" id="Aviso_Empleado" onclick="empleado()"/>
                                    <span class="switch-label" data-on="Si" data-off="No"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="col-7">
                                <div class="form-group" >

                                    <select class="form-control" id="Uid_Empleado" name="Uid_Empleado"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <button type="button" id="Crear" data-id="" class="btn btn-primary" onclick="envio(this)">Crear Aviso</button>
                        <button type="button" id="Editar" data-id="" class="btn btn-primary" onclick="edicion(this)">Guardar los cambios</button>
                    </div>
                </div>
            </div>                        
        </div>
    </div>

    <script src="{{ URL::To ('/'). '/js/Sistema/Avisos/form.js'}}"></script>
    <script>

        </script>
@endsection

@extends('master')
@section('content')
<div class="fh5co-gallery">
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/nominas/4') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_9.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Horas Extras</h2>
                <span>Registra horas extras de los empleados</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/empleados?type=4') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_9.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Horas Extras</h2>
                <span>Conoce cuantas horas fueron autorizadas y cuales fueron dobles y triples</span>
            </span>
        </a>
    @endif
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="#">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_9.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Recibos de Nominas</h2>
                <span>Registra los recibos de nominas de los empleados</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="#">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_9.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Recibos de Nominas</h2>
                <span>Visualiza los recibos de nomina</span>
            </span>
        </a>
    @endif
</div>
@stop

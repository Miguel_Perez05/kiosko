@extends('master')
@section('content')
<div class="fh5co-gallery">
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramite?tipo=cartarecomendacion') }}" data-nombre="Carta de Recomendación">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Carta de Recomendación</h2>
                <span>Expide carta de recomendación.</span>
            </span>
        </a>
    @endif
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramite?tipo=contrato') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Contrato</h2>
                <span>Genera Contrato del empleado.</span>
            </span>
        </a>
    @endif
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramite?tipo=baja') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Baja de empleados</h2>
                <span>Genera baja de empleado</span>
            </span>
        </a>
    @endif
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramite?tipo=cartalaboral') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Carta Laboral</h2>
                <span>Expide carta laboral del empleado que selecciones</span>
            </span>
        </a>
    @endif
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/avisos') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Avisos</h2>
                <span>En esta sección puedes crear nuevos avisos de la empresa</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/avisosempresa') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Avisos</h2>
                <span>En esta sección puedes consultar los avisos de la empresa</span>
            </span>
        </a>
    @endif
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/tramites.js'}}"></script>
@stop

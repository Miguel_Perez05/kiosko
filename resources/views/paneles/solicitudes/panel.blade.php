@extends('master')
@section('content')
<div class="fh5co-gallery">
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/4') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Vacaciones</h2>
                <span>Recibe y dales continuidad a solicitudes de Vacaciones de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/4') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Vacaciones</h2>
                <span>Envia solicitudes de vacaciones y verifica el estado de ya existentes</span>
            </span>
        </a>
    @endif
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/2') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Retardos</h2>
                <span>Recibe y dales continuidad a Retardos de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/2') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Retardos</h2>
                <span>Verifica el estado de los Retardos ya existentes</span>
            </span>
        </a>
    @endif
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/3') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Permisos de Ausencia</h2>
                <span>Recibe y dales continuidad a solicitudes de permisos de ausencia de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes/s/3') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Permisos de Ausencia</h2>
                <span>Envia solicitudes de permiso de ausencia y verifica el estado de ya existentes</span>
            </span>
        </a>
    @endif
</div>
@stop

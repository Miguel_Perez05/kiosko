@extends('master')
@section('content')
<div class="fh5co-gallery">
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/nominas/2') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Retardos</h2>
                <span>Registra y/o justifica retardos de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/empleados?type=2') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Retardos</h2>
                <span>Verifica cuantos retardos tienes en el periodo</span>
            </span>
        </a>
    @endif
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/nominas/3') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_7.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Vacaciones</h2>
                <span>Mantenimiento de la información de vaciones de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/empleados?type=3') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_7.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Vacaciones</h2>
                <span>Obten el saldo de dias de vaciones</span>
            </span>
        </a>
    @endif
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/nominas/1') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_8.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Faltas</h2>
                <span>Regsitra las faltas de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/incidencias/empleados?type=1') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_8.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Faltas</h2>
                <span>Obten información de faltas y justificaciones</span>
            </span>
        </a>
    @endif
</div>
@stop

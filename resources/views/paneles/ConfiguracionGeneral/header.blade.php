<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="{{ URL::To('/') }}">
					<img src="{{ AuthUser::get_employee()->empresa->Empresa_Logo ?? URL::To('/'). ('/img/plantilla/logo-colored.png') }}" alt="" class="mobile-logo">
				</a>
            </div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
                        <span class="user-name">{{ Auth::user()->Usuario_NickName}}</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="{{ URL::To('/'). '/miperfil' }}"><i class="fa fa-user-md" aria-hidden="true"></i> Mi Perfil</a>
						<a class="dropdown-item" href="{{ URL::To('/'). '/logout' }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Cerrar Sesión</a>
					</div>
				</div>
			</div>
			<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="fa fa-bell" aria-hidden="true"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
                                @if(AuthUser::get_solicitudes())
                                    @foreach(AuthUser::get_solicitudes() as $solicitud))
                                        <li>
                                            <a href="{{ URL::To('/'). '/solicitud/detalle/'.$solicitud->Uid_Solicitud }}">
                                                <img src="{{ $solicitud->Usuario_Avatar }}" alt="">
                                                <h3 class="clearfix">{{ $solicitud->Empleado_Nombres}} <span>{{$solicitud->created_at}}</span></h3>
                                                <p>{{$solicitud->Solicitud_Motivo}}</p>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

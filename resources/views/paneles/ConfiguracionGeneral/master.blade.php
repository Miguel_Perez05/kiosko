<!DOCTYPE html>
<html>
    <head>
		<!-- Basic Page Info -->
        <meta charset="utf-8">
        <title>Kiosko</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::To('/'). '/vendors/styles/style.css'}}">

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-119386393-1');
        </script>
        <link href="{{URL::To('/').'/open-iconic/font/css/open-iconic-bootstrap.css'}}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/src/plugins/datatables/media/css/jquery.dataTables.css' }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/src/plugins/datatables/media/css/dataTables.bootstrap4.css' }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/src/plugins/datatables/media/css/responsive.dataTables.css' }}">
        @isset($TituloCalendar)
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/src/plugins/jquery-steps/build/jquery.steps.css' }}">
        @endisset
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
        <link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/plus.css' }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::To('/').'/css/switch.css' }}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link href="{{ URL::To ('/'). '/css/sweetalert.css'}}" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::To ('/'). '/css/plantilla/circle-menu.css'}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/css/toastr.min.css">
        {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" /> --}}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script type="text/javascript"> main_path = '{!! URL::To('/') !!}';</script>
    </head>
    <body>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @include('paneles.ConfiguracionGeneral.header')
        @include('paneles.ConfiguracionGeneral.sidebar')

        <div class="main-container">
            <div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
                <div class="min-height-200px">
                    @yield('content')
                    @if(Auth::user()->Id_Perfil==1)
                        @include('circle')
                    @endif
                </div>
                <div class="footer-wrap bg-white pd-20 mb-20 border-radius-5 box-shadow">
                    <small>&copy; 2021. All Rights Reserved.</span> <span>Designed by <a href="#" target="_blank">Inter Comp</a> </span> </small>
                </div>
            </div>

        </div>
        <script src="{{ URL::To ('/'). '/js/plantilla/circleMenu.min.js'}}"></script>
        <script>
            var el = '.js-menu';
            var myMenu = cssCircleMenu(el);
        </script>


        <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.min.js' }}"></script>
        <script src="{{ URL::To ('/'). '/js/Sistema/Helper/ordercombo.js'}}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/script.js' }}"></script>
        {{-- <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/scripts/script.js' }}"></script> --}}

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

        @isset($TituloCalendar)

            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/lang/es.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

            {!! $calendar->script() !!}
        @endisset
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/jquery.dataTables.min.js' }}"></script>
        {{-- <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/plus.js' }}"></script> --}}
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/dataTables.bootstrap4.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/dataTables.responsive.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/responsive.bootstrap4.js' }}"></script>
        <!-- buttons for Export datatable -->
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/dataTables.buttons.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/buttons.bootstrap4.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/buttons.print.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/buttons.html5.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/buttons.flash.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/pdfmake.min.js' }}"></script>
        <script type="text/javascript" src="{{ URL::To('/'). '/src/plugins/datatables/media/js/button/vfs_fonts.js' }}"></script>
        {{-- <script src="{{ URL::To('/'). '/js/sweetalert.min.js'}}"></script> --}}
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{{ URL::To ('/'). '/js/Sistema/Helper/message.js'}}"></script>
        <script src="{{ URL::To ('/'). '/js/Sistema/Helper/combos.js'}}"></script>
        <script src="{{ URL::To('/'). '/js/Sistema/principal.js'}}"></script>

        <script>
            var session = "{{AuthUser::get_perfil()}}";
        </script>
    </body>
</html>

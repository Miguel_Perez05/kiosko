<div class="left-side-bar">
    <div class="brand-logo">
        <a href="{{ URL::To('/') }}">
            <img src="{{ AuthUser::get_employee()->empresa->Empresa_Logo ?? URL::To('/'). '/vendors/images/deskapp-logo.png' }}" alt="">
        </a>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li>
                    <a href="{{ URL::To('/'). ('/') }}" class="dropdown-toggle no-arrow">
                        <span class="fa fa-bars"></span><span class="mtext">Principal</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-ellipsis-v"></span><span class="mtext">RRHH</span>
                    </a>
                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-users"></span><span class="mtext">Empleados</span>
                            </a>
                            <ul class="submenu">
                                <li><a href="{{ URL::To('/'). ('/empleados/create') }}">Alta de Empleados</a></li>
                                <li><a href="{{ URL::To('/'). ('/contratos') }}">Empleados con Contrato</a></li>
                                <li><a href="{{ URL::To('/'). ('/tramite?tipo=contrato&empresa=0') }}">Contratar Empleados</a></li>
                                <li><a href="{{ URL::To('/'). ('/empleados?type=1') }}">Empleados sin Contrato</a></li>
                                <li><a href="{{ URL::To('/'). ('/empleados?type=0') }}">Empleados de Baja</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-table"></span><span class="mtext">Trámites</span>
                            </a>
                            <ul class="submenu child">
                                @if(AuthUser::get_perfil()=='true')
                                    <li><a href="{{ URL::To('/'). ('/tramite?tipo=contrato') }}">Contratar Empleado</a></li>
                                    <li><a href="{{ URL::To('/'). ('/tramite?tipo=baja') }}">Baja de Empleado</a></li>
                                    <li><a href="{{ URL::To('/'). ('/tramite?tipo=cartarecomendacion') }}">Carta de Recomendación</a></li>
                                    <li><a href="{{ URL::To('/'). ('/tramite?tipo=cartalaboral') }}">Carta Laboral</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>

                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-table"></span><span class="mtext">Avisos</span>
                            </a>
                            <ul class="submenu child">
                                @if(AuthUser::get_perfil()=='true')
                                    <li><a href="{{ URL::To('/'). ('/avisos/from')}}">Nuevo Aviso</a></li>
                                    <li><a href="{{ URL::To('/'). ('/avisos')}}">Listado de Avisos</a></li>
                                @endif
                                <li><a href="{{ URL::To('/'). ('/avisosempresa')}}">Avisos públicos</a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-clock-o"></span><span class="mtext">Ausentismos</span>
                            </a>
                            <ul class="submenu child">
                                @if(Auth::user()->Id_Perfil==1)
                                    <li><a href="{{ URL::To('/'). ('/incidencias/nominas/3') }}">Vacaciones</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/nominas/1') }}">Faltas</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/nominas/2') }}">Retardos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/nominas/5') }}">Permisos de Ausencias</a></li>
                                @else
                                    <li><a href="{{ URL::To('/'). ('/incidencias/empleados?type=3') }}">Vacaciones</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/empleados?type=1') }}">Faltas</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/empleados?type=2') }}">Retardos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/incidencias/empleados?type=5') }}">Permisos de Ausencias</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>

                    @if(Auth::user()->Id_Perfil==1)
                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-clock-o"></span><span class="mtext">Vacantes</span>
                            </a>
                            <ul class="submenu child">
                                <li><a href="{{ URL::To('/'). ('/vacantes') }}">Vacantes</a></li>
                                <li><a href="{{ URL::To('/'). ('/graficos') }}">Estudio de postulación</a></li>
                            </ul>
                        </li>
                    </ul>
                    @endif

                    @if(Auth::user()->Id_Perfil==1)
                        <ul class="submenu">
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <span class="fa fa-cubes"></span><span class="mtext">Organización</span>
                                </a>
                                <ul class="submenu">
                                    <li><a href="{{ URL::To('/'). ('/turnos') }}">Turnos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/tiponominas') }}">Tipos de Nominas</a></li>
                                    <li><a href="{{ URL::To('/'). ('/puestos') }}">Puestos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/departamentos') }}">Departamentos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/tipoempleados') }}">Tipos de Empleados</a></li>
                                    <li><a href="{{ URL::To('/'). ('/registrospatronales') }}">Registros Patronales</a></li>
                                    <li><a href="{{ URL::To('/'). ('/festivos') }}">Días Festivos</a></li>
                                </ul>
                            </li>
                        </ul>


                        {{-- <ul class="submenu">
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <span class="fa fa-users"></span><span class="mtext">Informes</span>
                                </a>
                                <ul class="submenu">
                                    <li><a href="{{ URL::To('/'). ('/reportes/empleados') }}">Empleados</a></li>
                                    <li><a href="{{ URL::To('/'). ('/reportes/puestos') }}">Puestos</a></li>
                                    <li><a href="{{ URL::To('/'). ('/reportes/solicitudes') }}">Solictudes</a></li>
                                </ul>
                            </li>
                        </ul> --}}
                    @endif
                </li>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-ellipsis-v"></span><span class="mtext">Nómina</span>
                    </a>
                    <ul class="submenu">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle">
                                <span class="fa fa-calendar-check-o"></span><span class="mtext">Compensación</span>
                            </a>
                            <ul class="submenu">
                                <li><a href="{{ URL::To('/'). ('/incidencias/nominas/4') }}">Horas Extras</a></li>
                                <li><a href="{{ URL::To('/'). ('/tipobonos') }}">Tipos de Bonos</a></li>
                                <li><a href="{{ URL::To('/'). ('/sueldos') }}">Cambios de Sueldo</a></li>
                                <li><a href="{{ URL::To('/'). ('/percepcionesdeducciones') }}">Listado de Bonos</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                @if(Auth::user()->Id_Perfil==1)
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle">
                            <span class="fa fa-cogs"></span><span class="mtext">Catálogos Generales</span>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ URL::To('/'). ('/empresas') }}">Empresas</a></li>
                            <li><a href="{{ URL::To('/'). ('/usuarios') }}">Usuarios</a></li>
                            <li><a href="{{ URL::To('/'). ('/perfiles') }}">Perfiles</a></li>
                            <li><a href="{{ URL::To('/'). ('/paises') }}">Paises</a></li>
                            <li><a href="{{ URL::To('/'). ('/estados') }}">Estados</a></li>
                            <li><a href="{{ URL::To('/'). ('/ciudades') }}">Ciudades</a></li>
                            <li><a href="{{ URL::To('/'). ('/nacionalidades') }}">Nacionalidades</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>

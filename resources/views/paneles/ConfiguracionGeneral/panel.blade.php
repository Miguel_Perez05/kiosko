@extends('master')
@section('content')
<div class="fh5co-gallery">

    <a class="gallery-item" href="{{ URL::To('/'). ('/paises') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Paises</h2>
            <span>Mantenimiento a la información del catálogo de Paises</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/estados') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Estados</h2>
            <span>Mantenimiento a la información del catálogo de Estados</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/ciudades') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Ciudades</h2>
            <span>Mantenimiento a la información del catálogo de Ciudades</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/empresas') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Empresas</h2>
            <span>Mantenimiento a la información del catálogo de Empresas</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/perfiles') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Perfiles</h2>
            <span>Mantenimiento a la información del catálogo de Perfiles</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/usuarios') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Usuarios</h2>
            <span>Mantenimiento a la información del catálogo de Usuarios</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/festivos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_7.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Dias Festivos</h2>
            <span>Mantenimiento a la información del catálogo de Dias Festivos</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/cursos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_8.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Cursos</h2>
            <span>Mantenimiento a la información del catálogo de Cursos</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/areasformacion') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Areas de Formación</h2>
            <span>Mantenimiento a la información del catálogo de Areas de Formación</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/niveleseducativos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Niveles Educativos</h2>
            <span>Mantenimiento a la información del catálogo de Niveles Educativos</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/carreras') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Carreras</h2>
            <span>Mantenimiento a la información del catálogo de Carreras</span>
        </span>
    </a>

</div>
@stop

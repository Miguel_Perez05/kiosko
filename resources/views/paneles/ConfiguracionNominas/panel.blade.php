@extends('master')
@section('content')
<div class="fh5co-gallery">
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/turnos') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Turnos</h2>
                <span>Mantenimiento a la información del catálogo de Turnos</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/tipoempleados') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Tipos de Empleados</h2>
                <span>Mantenimiento a la información del catálogo de Tipos de Empleados</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/estadosciviles') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Estados Civiles</h2>
                <span>Mantenimiento a la información del catálogo de Estados Civiles</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/empresas') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Empresas</h2>
                <span>Mantenimiento a la información del catálogo de Empresas</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/registrospatronales') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Registros Patronales</h2>
                <span>Mantenimiento a la información del catálogo de Registros Patronales</span>
            </span>
        </a>
    @endif
    <a class="gallery-item" href="{{ URL::To('/'). ('/empleados?type=1') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Empleados sin Contrato</h2>
            <span>Mantenimiento a la información del catálogo de Empleados sin Contrato</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/empleados?type=2') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Empleados con Contrato</h2>
            <span>Mantenimiento a la información del catálogo de Empleados con Contrato</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/bajas') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Empleados dados de Baja</h2>
            <span>Mantenimiento a la información del catálogo de Empleados dados de Baja</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/contratos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Contratos</h2>
            <span>Listado de Empleados con contrato</span>
        </span>
    </a>
    <a class="gallery-item" href="{{ URL::To('/'). ('/contratos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Nuevo contrato</h2>
            <span>Contratar Empleado</span>
        </span>
    </a>
    @if(AuthUser::get_perfil()=='true')
        <a class="gallery-item" href="{{ URL::To('/'). ('/puestos') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_8.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Puestos</h2>
                <span>Mantenimiento a la información del catálogo de Puestos</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/departamentos') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_9.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Departamentos</h2>
                <span>Mantenimiento a la información del catálogo de Departamentos</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/tiposolicitudes') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_10.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Tipos Solicitudes</h2>
                <span>Mantenimiento a la información del catálogo de Tipos Solictudes</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/tiponominas') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Tipos de Nominas</h2>
                <span>Mantenimiento a la información del catálogo de Tipos de Nominas</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/festivos') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Nacionalidades</h2>
                <span>Mantenimiento a la información del catálogo de Nacionalidades</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/idiomas') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Idiomas</h2>
                <span>Mantenimiento a la información del catálogo de Idiomas</span>
            </span>
        </a>
        <a class="gallery-item" href="{{ URL::To('/'). ('/dominaidiomas') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Idiomas Dominados</h2>
                <span>Listado de Idiomas que dominan los empleados</span>
            </span>
        </a>
    @endif
</div>
@stop

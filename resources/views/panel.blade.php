@extends('master')
@section('content')
<div class="fh5co-gallery">
    <a class="gallery-item" href="{{ URL::To('/'). ('/miperfil') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Mi Perfil</h2>
            <span>Visualización y edición de mi perfil.</span>
        </span>
    </a>
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/configuracion') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Configuración General</h2>
                <span>Mantenimiento a la información de la empresa y empleados.</span>
            </span>
        </a>
    @endif

    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/configuracionNominas') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Configuración de Nominas</h2>
                <span>Mantenimiento a la información de las nomina.</span>
            </span>
        </a>
    @endif

    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Solicitudes</h2>
                <span>Continuidad a solicitudes de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/solicitudes') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Solicitudes</h2>
                <span>Envia solicitudes y verifica el estado de ya existentes</span>
            </span>
        </a>
    @endif

    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramites') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Trámites</h2>
                <span>Continuidad a trámites de los colaboradores</span>
            </span>
        </a>
    @else
        <a class="gallery-item" href="{{ URL::To('/'). ('/tramites') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Trámites</h2>
                <span>Envia trámites y verifica el estado de ya existentes</span>
            </span>
        </a>
    @endif

    {{-- <a class="gallery-item" href="#">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_10.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Buzón</h2>
            <span>Envía y recibe mensajes</span>
        </span>
    </a> --}}
    <a class="gallery-item" href="{{ URL::To('/'). ('/diasfestivos') }}">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_11.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Cumpleaños y Dias Festivos</h2>
            <span>Visualiza los cumpleaños del mes y dias festivos</span>
        </span>
    </a>
    {{-- <a class="gallery-item" href="#">
        <img src="{{ URL::To('/'). ('/img/plantilla/work_12.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
        <span class="overlay">
            <h2>Capacitación</h2>
            <span>Ingresa a la sección de capacitación</span>
        </span>
    </a> --}}
    @if(Auth::user()->Id_Perfil==1)
        <a class="gallery-item" href="{{ URL::To('/'). ('/panelvacantes') }}">
            <img src="{{ URL::To('/'). ('/img/plantilla/work_12.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
            <span class="overlay">
                <h2>Vacantes</h2>
                <span>Ingresa a la sección de satisfacción</span>
            </span>
        </a>
    @endif


</div>
@stop

<html>

<head>
<meta http-equiv=Content-Type content="text/html;">

</head>

<body>

<div>
<p><img width="10%" src="{{ $datosdocumento->Empresa_Logo ?? URL::To('/'). '/vendors/images/deskapp-logo.png' }}" alt=""></p>

<p align='right'>{{$datosdocumento->Empresa_LugarEmision}}</p>

<p align='right'>{{$Fecha}}</p>
<p></p>
<br>
<p></p>

<p><h4>A QUIEN CORRESPONDA:</h4></p>
<p></p>
<br>
<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Por medio de la presente hago de su
conocimiento que <b>{{$datosdocumento->Empleado_Nombres}}</b> laboró en esta
empresa desde {{ Carbon\Carbon::parse($historial->Historial_Ingreso ?? $historial->Historial_Reingreso)->toFormattedDateString() }} hasta
{{ $historial->Historial_Historial_Egreso ? Carbon\Carbon::parse($historial->Historial_Historial_Egreso)->toFormattedDateString() :  ' la fecha' }},
desempeñandose de manera satisfactoria en el puesto de <b>{{$datosdocumento->Puesto_Nombre}}.</b></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Hago constar que durante el tiempo
que {{$datosdocumento->Empleado_Nombres}} prestó sus servicios en esta empresa siempre mostró una buena actitud
de trabajo y de respeto hacia sus jefes y demás compañeros, siempre se manejó
con honestidad y responsabilidad en sus labores diarias, por lo que no tengo impedimento
alguno en recomendarlo ampliamente.</p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Se extiende la presente constancia
para los fines que al interesado convengan.</p>

<p></p>
<br>
<p></p>
<br>
<p></p>
<br>

<p align=center><b>ATENTAMENTE</b></p>

<p></p>
<br>
<p></p>

<p align=center>________________________________________</p>

<p align=center>{{$emisor->Empleado_Nombre.' '.$emisor->Empleado_APaterno.' '.$emisor->Empleado_AMaterno}}</p>

<p align=center>{{$emisor->Puesto_Nombre}}</p>

</div>

</body>

</html>

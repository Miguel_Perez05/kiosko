<html>

<head>
<meta http-equiv=Content-Type content="text/html;">

<title></title>
<body>
<div>
<img width="10%" src="{{ $datosdocumento->Empresa_Logo ?? URL::To('/'). '/vendors/images/deskapp-logo.png' }}" alt="">

<p align='right'>Fecha: <b>{{$Fecha}}</b></p>

<p><b>{{$datosdocumento->Empresa_Razon_Social}}</b></p>

<p><b>{{$datosdocumento->Empresa_Domicilio}}</b></p>

<p><b>{{$datosdocumento->Empresa_LugarEmision}}.</b></p>

<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Con esta fecha y por convenir así a mis
intereses doy por terminadas voluntariamente con la empresa {{$datosdocumento->Empresa_Razon_Social}}, mis
relaciones de trabajo haciendo uso del
derecho que me concede la fracción I del artículo 53 de la ley
Federal del Trabajo y por mutuo consentimiento con la propia empresa.</p>

<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Reconozco expresamente que durante el tiempo
en que les preste mis servicios recibí
el pago puntual y oportuno de todas y cada una de las prestaciones a que tuve
derecho, por lo que no me reservo en su contra ninguna acción por concepto de
salarios ordinarios, extraordinarios, descansos legales y semanarios,
vacaciones, prima de vacaciones, aguinaldo anual, participación en las
utilidades, prima legal de antigüedad ni por ningún otro concepto que pudiere
derivarse de mis relaciones de trabajo y de la ley, por cuyas razones les
otorgo el finiquito más amplio que en derecho proceda y relevo a la empresa en
cuestión de responsabilidad derivada de mi contratación.</p>

<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Tambión para los fines que procedan, hago
constar que durante el tiempo de mis expresados servicios, estuve inscrito en
el Instituto Mexicano del Seguro Social y que la empresa cumplió con las
disposiciones del Instituto del Fondo Nacional de la Vivienda para los
trabajadores.</p>

<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Por último estoy firmando por separado mi
recibo finiquito de que común acuerdo he aceptado de {{$datosdocumento->Empresa_Razon_Social}}.
Por los conceptos que en la misma se señalan.</p>

<p></p>

<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Manifiesto tambión que los fines que procedan
y bajo protesta de decir la verdad que no presentado demanda alguna en contra
de esta compañía toda vez que no tengo que reclamar, En el evento de que
hubiese presentado demanda o reclamación legal en contra de esta empresa,
expresamente desisto por este conducto de las acciones intentadas y revoco
cualquier conferido para este efecto.</p>

<p></p>
<br>

<table  style="width:100%"  align='center'>
    <tr align='center'>
        <td style="width:50%" colspan="3">EMPLEADO</td>
        <td style="width:50%" colspan="3">LA EMPRESA</td>
    </tr>

    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>
    <tr align='center'>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="3">{{$datosdocumento->Empleado_Nombres}}</td>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="3">&nbsp;</td>
    </tr>

    <tr align='center'>
        <td></td>
        <td style="width:50%; border-top-style: solid;">Nombre:</td>
        <td></td>
        <td></td>
        <td style="width:50%; border-top-style: solid;">Firma:</td>
        <td></td>
    </tr>


    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>


    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" style="line-height:10px;" colspan="6">&nbsp;</td>
    </tr>

    <tr align='center'>
        <td></td>
        <td  style="width:50%; border-top-style: solid;">TESTIGO</td>
        <td></td>
        <td></td>
        <td  style="width:50%; border-top-style: solid;">TESTIGO</td>
        <td></td>
    </tr>

</table>

</div>

</body>

</html>

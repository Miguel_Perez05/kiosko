<html>

<head>
<meta http-equiv=Content-Type content="text/html;">


</head>

<body>

<div>

<p><img width="10%" src="{{ $datosdocumento->Empresa_Logo ?? URL::To('/'). '/vendors/images/deskapp-logo.png' }}" alt=""></p>

<p align='right'>{{$datosdocumento->Empresa_LugarEmision}}</p>

<p align='right'>{{$Fecha}}</p>
<p></p>
<br>
<p></p>

<p><h4>A QUIEN CORRESPONDA:</h4></p>
<p></p>
<br>
<p></p>



<p align='justify'>&nbsp;&nbsp;&nbsp;&nbsp;Por medio de la presente hacemos constar que el empleado <b>{{$datosdocumento->Empleado_Nombres}} </b>
labora en esta empresa {{$datosdocumento->Empresa_Razon_Social}}, con registro Patronal
<b>{{$datosdocumento->RegistroPatronal_Nombre}}</b> y con Registro Federal
de Contribuyentes <b>{{$datosdocumento->Empresa_RFC}}, </b>en
el puesto de <b>{{$datosdocumento->Puesto_Nombre}} </b>con número de afiliación
<b>{{$datosdocumento->Empleado_NSS}} </b>y CURP <b>{{$datosdocumento->Empleado_CURP}} </b>en el departamento
de <b>{{$datosdocumento->Departamento_Nombre}} </b>con una
antigüedad desde el día <b> {{ $historial->Historial_Ingreso ?? $historial->Historial_Reingreso}} a la fecha </b>
y un sueldo mensual de ${{$importeLetra}}</p>

<p></p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;Se extiende la presente a petición del interesado y para los fines que
juzgue conveniente.</p>

<p></p>
<br>
<p></p>
<br>
<p></p>
<br>
<p></p>
<br>

<p align=center><b>ATENTAMENTE</b></p>

<p></p>
<br>
<p></p>

<p align=center>________________________________________</p>

<p align=center>{{$emisor->Empleado_Nombre.' '.$emisor->Empleado_APaterno.' '.$emisor->Empleado_AMaterno}}</p>

<p align=center>{{$emisor->Puesto_Nombre}}</p>


</div>

</body>

</html>

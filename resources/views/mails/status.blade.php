<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>{{ $Tipo }}</title>
</head>
<body>
    <p>Hola! Han {{ $Status }} a tu solicitud de {{ $Tipo }}.</p>
    <ul>
        <li>Quien cambio el estatus de tu solicitud: {{ $Empleado_Nombre }}</li>
        <li>Departamento: {{ $Departamento_Nombre }}</li>
        <li>Puesto: {{ $Puesto_Nombre }}</li>
    </ul>
    <p>Como dato adicional:</p>
    <ul>
        <li>El motivo de tu solicitud: {{ $Motivo }}</li>
        <li>Fecha: {{ $Solicitud_Inicio_Auscencia }}</li>
        @if($Solicitud_Fin_Auscencia)
            <li>Fecha Regreso: {{ $Solicitud_Fin_Auscencia }}</li>
        @endif
        <li>Comentarios Adicionales: {{ $Solicitud_Comentario }}</li>
    </ul>
</body>
</html>

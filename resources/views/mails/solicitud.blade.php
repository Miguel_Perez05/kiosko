<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>{{ $Tipo }}</title>
</head>
<body>
    <p>Hola! Se ha registrado un nuevo {{ $Tipo }}.</p>
    <p>Estos son los datos del empleado:</p>
    <ul>
        <li>Nombre: {{ $Empleado_Nombre }}</li>
        <li>Departamento: {{ $Departamento_Nombre }}</li>
        <li>Puesto: {{ $Puesto_Nombre }}</li>
    </ul>
    <p>Como dato adicional:</p>
    <ul>
        <li>Motivo: {{ $Motivo }}</li>
        <li>Fecha: {{ $Solicitud_Inicio_Auscencia }}</li>
        @if($Solicitud_Fin_Auscencia)
            <li>Fecha Regreso: {{ $Solicitud_Fin_Auscencia }}</li>
        @endif

    </ul>
</body>
</html>

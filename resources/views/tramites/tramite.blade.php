@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Trámites</h5>
            <p class="font-14" id="labelTramite"></p>
        </div>
    </div>
    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
    <input type="hidden" name="StatusEmployee" id="StatusEmployee">
    <div class="row">
        <div class="col-md">
            <label>Empleado</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md">
            <select class="form-control" style="width: 50%; height: 38px;" id="Empleado"></select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col md">
            <button type="button" id="Generar" data-id="" class="btn btn-primary" onclick="generaDocumento(this)"></button>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Tramites/tramites.js'}}"></script>



@endsection

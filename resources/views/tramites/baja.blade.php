@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::To('/'). '/css/plantilla/bootstrap-switch.css' }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Generación de Baja</h5>
        </div>
    </div>
    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
    <input type="hidden" name="StatusEmployee" id="StatusEmployee">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md">
                <label>Empleado</label>
                <select class="form-control" id="Empleado" widht="100%"></select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md">
                        <label>Tipo Baja</label>
                        <select class="form-control" id="Baja_Causa" name="Baja_Causa">
                            <option value="Voluntario">Voluntaria</option>
                            <option value="Despido">Despido</option>
                            <option value="Otra">Otra</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <br>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="AplicaBaja">
                            <label class="custom-control-label" for="AplicaBaja">Aplica Baja</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md">
                <label>Comentarios</label>
                <textarea class="form-control" name="Baja_Comentario" id="Baja_Comentario" cols="30" rows="3"></textarea>
            </div>        
        </div>
        <br>
        {{-- <div class="row">
            <div class="col-md">
                <br>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="Recontratable" name="Recontratable">
                    <label class="custom-control-label" for="Recontratable">Recontratable</label>
                </div>
            </div>
            <div class="col-md-8">
                <label>Comentarios no recontratable</label>
                <textarea class="form-control" name="Recontratable_Causa" id="Recontratable_Causa" cols="30" rows="3"></textarea>
            </div>
        </div> --}}
    </div>
    <br>
    <div class="row">
        <div class="col md" align="right">
            <button type="button" id="Generar" data-id="" class="btn btn-primary" onclick="generaDocumento()"></button>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/plantilla/bootstrap-switch.min.js'}}"></script>
<script src="{{ URL::To ('/'). '/js/Sistema/Tramites/creabaja.js'}}"></script>
@endsection

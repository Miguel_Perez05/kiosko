@extends('paneles.ConfiguracionGeneral.master')
@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">Nuevo Contrato</h5>
        </div>
    </div>
    <input type="hidden" name="_token" class="token" id="CSRF-token" value="{{ csrf_token()}}">
    <input type="hidden" name="StatusEmployee" id="StatusEmployee">
    <div class="row">
        <div class="col-md-7">
            <label>Empleado</label>
            <select class="form-control" id="Empleado"></select>
        </div>
        <div class="col-md-1">
            <br>
            <a href="javascript:void(0);" onclick="editEmployee()" class="edit-avatar"><i class="fa fa-pencil"></i></a>
        </div>
        <div class="col-md-2">
            <label>Folio</label>
            <input type="text" class="form-control" id="Contrato_Folio">
        </div>
        <div class="col-md-2">
            <label>Tipo Empleado</label>
            <select class="form-control" id="Uid_TipoEmpleado" name="Uid_TipoEmpleado"></select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md">
            <label>Empresa</label>
            <select class="form-control" id="Uid_Empresa" name="Uid_Empresa"></select>
        </div>
        <div class="col-md">
            <label>Registro Patronal</label>
            <select class="form-control" id="Uid_RegistroPatronal" name="Uid_RegistroPatronal"></select>
        </div>
        <div class="col-md">
            <label>Tipo Contrato</label>
            <select class="form-control" id="Id_TipoContrato" name="Id_TipoContrato"></select>
        </div>
        <div class="col-md">
            <label>Tipo Nomina</label>
            <select class="form-control" id="Uid_TipoNomina" name="Uid_TipoNomina"></select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md">
            <label>Turno</label>
            <select class="form-control" id="Uid_Turno" name="Uid_Turno"></select>
        </div>
        <div class="col-md">
            <label>Departamento</label>
            <select class="form-control" id="Uid_Departamento" name="Uid_Departamento"></select>
        </div>
        <div class="col-md">
            <label>Puesto</label>
            <select class="form-control" id="Uid_Puesto" name="Uid_Puesto"></select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md">
            <label>Sueldo</label>
            <input type="number" class="form-control" name="Contrato_Salario" id="Contrato_Salario">
        </div>
        <div class="col-md">
            <label>Fecha Inicio</label>
            <input type="date" class="form-control" name="Contrato_Inicio" id="Contrato_Inicio" value="<?php echo date('Y-m-d'); ?>">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col md" align="right">
            <button type="button" id="Generar" data-id="" class="btn btn-primary" onclick="generaDocumento()"></button>
        </div>
    </div>
</div>
<script src="{{ URL::To ('/'). '/js/Sistema/Tramites/creacontrato.js'}}"></script>
@endsection

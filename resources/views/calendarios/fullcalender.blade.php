<!doctype html>
<html lang="en">
<head>
    <script src="{{ URL::To('/'). '/js/jquery.min.js' }}"></script>
    <script src="{{ URL::To('/'). '/js/moment.min.js' }}"></script> 
    <script src="{{ URL::To('/'). '/js/fullcalendar.min.js' }}"></script>
    <script type="text/javascript" src="{{ URL::To('/'). '/js/es.js' }}"></script>
<script>

    $(document).ready(function() {

        $('#calendar').fullCalendar({
        });

    });

</script>
    <link rel="stylesheet" href="{{ URL::To('/'). '/css/fullcalendar.min.css' }}"/>


    <style>
        /* ... */
    </style>
</head>
<body>
    {!! $calendar->calendar() !!}
    {!! $calendar->script() !!}
</body>
</html>

<aside>
    <h5>Cumpleaños del Mes</h5>
    <ul>
	    @if(!empty($cumpleanos))
	      @foreach($cumpleanos as $cumpleano)
	        <li><div style="color:#0000FF"<time>{{ $cumpleano->Empleados_fecha_nacimiento }}</time> {{ $cumpleano->Empleados_apodo }}</li></div></br>
	      @endforeach
	    @endif
    </ul>
</aside>

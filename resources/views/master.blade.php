<!DOCTYPE html>
    <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiosko</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
	<meta name="keywords" content="free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="FreeHTML5.co" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Animate.css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/animate.css' }}">

	<link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/icomoon.css' }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/bootstrap.css' }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/owl.carousel.min.css' }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/owl.theme.default.min.css' }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/css/plantilla/style.css' }}">
    <link rel="stylesheet" href="{{ URL::To ('/'). '/css/plantilla/circle-menu.css'}}">

    <link rel="stylesheet" type="text/css" href="{{ URL::To ('/'). '/vendors/styles/style.css' }}">

    <!-- Modernizr JS -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/modernizr-2.6.2.min.js' }}"></script>
    <script type="text/javascript"> main_path = '{!! URL::To('/') !!}';</script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="fh5co-page">
		<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
		<aside id="fh5co-aside" role="complementary" class="border js-fullheight">
			<h1 id="fh5co-logo"><a href="{{ URL::To('/') }}"><img src="{{ AuthUser::get_employee()->empresa->Empresa_Logo ?? URL::To('/'). ('/img/plantilla/logo-colored.png') }}" alt="Free HTML5 Bootstrap Website Template"></a></h1>
            <span class="user-icon"><i class="fa fa-user-o"></i></span>
            <h3 align='center' class="user-name">Hola {{AuthUser::get_username()->empleado->Empleado_Nombre ?? AuthUser::get_username()->Usuario_NickName}}</h3 align='center'>
            <h4 align='center' class="user-name">@if(AuthUser::get_username()->empleado)@if(AuthUser::get_username()->empleado->Empleado_Genero=='Femenino')Bienvenida @else Bienvenido @endif @else Bienvenido @endif  </h4 align='center'>
            @include('menu')

			<div class="fh5co-footer">
                <p>
                    <small>
                    <span><a href="{{ URL::To('/'). ('/logout') }}" >Cerrar Sesion.</a> </span>
                    </small>
                </p>
                <br><br><br>
				<p><small>&copy; 2021. All Rights Reserved.</span> <span>Designed by <a href="#" target="_blank">Inter Comp</a> </span> </small></p>

			</div>

		</aside>

		<div id="fh5co-main">
            @yield('content')

            @if(Auth::user()->Id_Perfil==1)
                @include('circle')
            @endif
        </div>

	</div>
    <script src="{{ URL::To ('/'). '/js/plantilla/circleMenu.min.js'}}"></script>
    <script>
        var el = '.js-menu';
        var myMenu = cssCircleMenu(el);
        </script>
    <!-- jQuery -->
    {{-- <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.min.js' }}"></script> --}}
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>


    <!-- jQuery Easing -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.easing.1.3.js' }}"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/bootstrap.min.js' }}"></script>
    <!-- Carousel -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/owl.carousel.min.js' }}"></script>
    <!-- Stellar -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.stellar.min.js' }}"></script>
    <!-- Waypoints -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.waypoints.min.js' }}"></script>
    <!-- Counters -->
    <script type="text/javascript" src="{{ URL::To('/'). '/js/plantilla/jquery.countTo.js' }}"></script>



	<!-- MAIN JS -->
	<script src="{{ URL::To('/'). '/js/plantilla/main.js' }}"></script>

	</body>
</html>



<?php

namespace Database\Factories;

use App\empleados;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Factories\Factory;

class empleadosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = empleados::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // $faker = Factory::create();
        return [
            'Uid_Empleado' =>  Uuid::generate()->string,
            'Uid_Empresa' => '36f2e9f0-c08c-11e9-8f51-a16d8b25865d',
            'Uid_RegistroPatronal' => '3a16ba90-bfdd-11e9-8e54-bf6639924a72',
            'Empleado_Nombre' => '$faker->name',
            'Empleado_APaterno' => '$faker->lastName',
            'Empleado_AMaterno' => '$faker->lastName',
            'Uid_Turno' => '$faker->lastName',
            'Id_Pais' => 1,
            'Id_Estado' => 1,
            'Id_Ciudad' => 1,
            'Empleado_RFC'=>'',
            'Id_Estatus' => 2,
            'Uid_Usuario_Crea'=>'cc88fa10-b8bd-11e9-b2c9-a3b93cc28a8f',
            'Uid_Usuario_Edita'=>'cc88fa10-b8bd-11e9-b2c9-a3b93cc28a8f',
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ];
    }
}

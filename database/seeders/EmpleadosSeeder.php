<?php

namespace Database\Seeders;

use App\empleados;
use Illuminate\Database\Seeder;

class EmpleadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        empleados::factory()->count(10)->create();
    }
}

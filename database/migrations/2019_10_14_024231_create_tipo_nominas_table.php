<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoNominasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_nominas', function (Blueprint $table) {
            $table->uuid('Uid_TipoNomina');
            $table->string('TipoNomina_Nombre');
            $table->Integer('TipoNomina_Dias');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_nominas');
    }
}

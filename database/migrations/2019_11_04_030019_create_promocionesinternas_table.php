<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromocionesinternasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promociones_internas', function (Blueprint $table) {
            $table->uuid('Uid_PromocionInterna');
            $table->string('Uid_Empleado');
            $table->string('Uid_Puesto');
            $table->string('PromocionInterna_Fecha');
            $table->integer('Id_Estatus');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocionesinternas');
    }
}

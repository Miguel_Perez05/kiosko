<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avisos', function (Blueprint $table) {
            $table->uuid('Uid_Aviso')->primary();
            $table->uuid('Uid_Empleado_Emisor');
            $table->uuid('Uid_Empleado_Receptor')->nullable();
            $table->uuid('Uid_Grupo')->nullable();
            $table->uuid('Uid_Puesto')->nullable();
            $table->uuid('Uid_Departamento')->nullable();
            $table->uuid('Uid_Sucursal')->nullable();
            $table->uuid('Uid_Turno')->nullable();
            $table->string('Aviso_Titulo');
            $table->string('Aviso_Asunto');
            $table->integer('Aviso_Prioridad');
            $table->string('Aviso_Tipo');
            $table->datetime('Aviso_Vigencia');
            $table->boolean('Aviso_Visto');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avisos');
    }
}

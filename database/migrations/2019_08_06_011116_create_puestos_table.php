<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puestos', function (Blueprint $table) {
            $table->uuid('Uid_Puesto')->primary();
            $table->string('Puesto_Nombre');
            $table->string('Puesto_Descripcion');
            $table->float('Puesto_Sueldo_Min',20,2);
            $table->float('Puesto_Sueldo_Max',20,2);
            $table->uuid('Uid_Responsable');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puestos');
    }
}

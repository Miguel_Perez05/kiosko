<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->uuid('Uid_Empleado')->primary();
            $table->uuid('Uid_Empresa');
            $table->uuid('Uid_RegistroPatronal');
            $table->uuid('Uid_Turno');
            $table->uuid('Uid_TipoEmpleado');
            $table->uuid('Uid_EstadoCivil');
            $table->uuid('Uid_Grupo')->nullable();
            $table->string('Empleado_Nombre');
            $table->string('Empleado_APaterno');
            $table->string('Empleado_AMaterno');
            $table->string('Empleado_Telefono');
            $table->string('Empleado_Apodo');
            $table->enum('Empleado_Genero', ['Masculino', 'Femenino']);
            $table->date('Empleado_Fecha_Nacimiento');
            $table->integer('Id_Pais');
            $table->integer('Id_Estado');
            $table->integer('Id_Ciudad');
            $table->string('Empleado_NSS',13);
            $table->string('Empleado_CURP',20);
            $table->string('Empleado_RFC',13);
            $table->integer('Id_Estatus');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}

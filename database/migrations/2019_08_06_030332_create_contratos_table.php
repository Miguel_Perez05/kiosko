<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->uuid('Uid_Contrato')->primary();
            $table->uuid('Uid_Empleado');
            $table->uuid('Uid_Departamento');
            $table->uuid('Uid_Puesto');
            $table->uuid('Id_TipoContrato');
            $table->date('Contrato_Inicio');
            $table->date('Contrato_Fin');
            $table->float('Contrato_Salario',202);
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePercepcionesDeduccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percepciones_deducciones', function (Blueprint $table) {
            $table->uuid('Uid_PercepcionDeduccion')->primary();
            $table->integer('PercepcionDeduccion_Tipo');
            $table->float('PercepcionDeduccion_Monto',20,2);
            $table->uuid('Uid_Periodo')->nullable();          
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percepciones_deducciones');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->uuid('Uid_Empresa')->primary();
            $table->string('Empresa_Nombre');
            $table->string('Empresa_RFC',13);
            $table->string('Empresa_Domicilio');
            $table->string('Empresa_Integracion');
            $table->integer('Empresa_Padre');
            $table->string('Empresa_Razon_Social');
            $table->string('Empresa_Pagina_Web');
            $table->string('Empresa_Domicilio_fiscal');
            $table->string('Empresa_Estatus');
            $table->string('Empresa_Guid_Dsl')->nullable();
            $table->string('Empresa_Logo')->nullable();
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}

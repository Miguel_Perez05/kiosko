<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Incidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->uuid('Uid_Incidencia')->primary();
            $table->string('Incidencia_Nombre');
            $table->integer('Incidencia_DeduccionPercepcion');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencias');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiudadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciudades', function (Blueprint $table) {
            $table->Increments('Id_Ciudad');
            $table->Integer('Id_Estado');
            $table->Integer('Id_Pais');
            $table->string('Cuidad_Nombre');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciudades');
    }
}

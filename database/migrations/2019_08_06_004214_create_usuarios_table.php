<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->uuid('Uid_Usuario')->primary();
            $table->uuid('Uid_Empleado');
            $table->string('Usuario_Nombre');
            $table->string('Usuario_APaterno');
            $table->string('Usuario_AMaterno');
            $table->string('Usuario_NickName');
            $table->string('Usuario_Correo');
            $table->string('Usuario_Password');            
            $table->integer('Id_Perfil');
            $table->text('Usuario_Avatar')->nullable();
            $table->integer('Id_Estatus');
            $table->integer('Usuario_Activo');
            $table->integer('Usuario_Nuevo');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_contratos', function (Blueprint $table) {
            $table->uuid('Id_TipoContrato')->primary();
            $table->string('TipoContrato_Nombre');
            $table->text('TipoContrato_Plantilla');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_contratos');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->uuid('Uid_Curso')->primary();
            $table->string('Curso_Nombre');
            $table->float('Curso_Porcentaje',3,2);
            $table->string('Curso_Proposito');
            $table->string('Curso_Origen');
            $table->uuid('Uid_Instructor');
            $table->uuid('Uid_Usuario_Crea');         
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}

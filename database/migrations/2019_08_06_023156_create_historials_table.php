<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial', function (Blueprint $table) {
            $table->uuid('Uid_Historial')->primary();
            $table->uuid('Uid_Empleado');
            $table->uuid('Uid_Puesto')->nullable();
            $table->uuid('Uid_Departamento')->nullable();
            $table->uuid('Uid_Sucursal')->nullable();
            $table->uuid('Uid_Turno')->nullable();
            $table->uuid('Uid_TipoEmpleado');
            $table->uuid('Id_TipoContrato');
            $table->uuid('Uid_Empresa');
            $table->integer('Id_Estatus');
            $table->integer('Id_Pais');
            $table->integer('Id_Estado');
            $table->integer('Id_Ciudad');
            $table->uuid('Uid_Recontratable');
            $table->uuid('Uid_PercepcionDeduccion');
            $table->uuid('Uid_ActaAdministrativa');
            $table->uuid('Uid_Comentario');
            $table->uuid('Uid_Sueldo');
            $table->date('Historial_Ingreso');
            $table->date('Historial_Egreso');
            $table->date('Historial_Reingreso');
            $table->uuid('Uid_Jefe_Directo');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial');
    }
}

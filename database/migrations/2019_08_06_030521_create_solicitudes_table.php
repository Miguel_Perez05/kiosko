<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->uuid('Uid_Solicitud')->primary();
            $table->uuid('Uid_Empleado_Emisor');
            $table->uuid('Uid_Empleado_Receptor');
            $table->uuid('Uid_TipoSolicitud');
            $table->enum('Solicitud_Status',['Pendiente','Rechazado','Autorizado']);
            $table->date('Solicitud_Inicio_Auscencia');
            $table->date('Solicitud_Fin_Auscencia');
            $table->string('Solicitud_Motivo');
            $table->string('Solicitud_Observaciones');
            $table->uuid('Uid_Comentarios');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}


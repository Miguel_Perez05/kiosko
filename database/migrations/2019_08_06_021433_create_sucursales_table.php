<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->uuid('Uid_Sucursal')->primary();
            $table->uuid('Uid_Empresa');
            $table->integer('Id_Pais');
            $table->integer('Id_Estado');
            $table->integer('Id_Ciudad');
            $table->string('Sucursal_Nombre');
            $table->string('Sucursal_Domicilio');
            $table->string('Sucursal_Estatus');
            $table->uuid('Uid_Usuario_Crea');
            $table->uuid('Uid_Usuario_Edita');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales');
    }
}

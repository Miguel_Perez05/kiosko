<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estados extends Model
{
    protected $primaryKey = 'Id_Estado';
    protected $fillable = [
        'Estado_Nombre',
        'Id_Pais',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

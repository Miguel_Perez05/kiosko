<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departamentos extends Model
{
    protected $primaryKey = 'Uid_Departamento';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Departamento',
        'Departamento_Nombre',
        'Departamento_Descripcion',
        'Departamento_Jefe',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

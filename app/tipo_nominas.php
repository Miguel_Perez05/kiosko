<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_nominas extends Model
{
    protected $primaryKey = 'Uid_TipoNomina';
    public $incrementing = false;
    protected $fillable = [
        'Uid_TipoNomina',
        'TipoNomina_Nombre',
        'TipoNomina_Dias',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

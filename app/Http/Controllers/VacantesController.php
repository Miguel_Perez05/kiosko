<?php

namespace App\Http\Controllers;

use App\kardex;
use App\puestos;
use App\empleados;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\promociones_internas;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class VacantesController extends Controller
{
    public function panel()
    {
        return view('Catalogos.Nominas.Vacantes.panel');
    }

    public function index()
    {
        return view('Catalogos.Nominas.Vacantes.vacantes');
    }

    public function assign()
    {
        return view('Catalogos.Nominas.Vacantes.asignacion');
    }

    public function show(puestos $puestos, $estatus)
    {
        $puestosLst=$puestos->LeftJoin('departamentos', 'puestos.Uid_Departamento', 'departamentos.Uid_Departamento')
                            ->Select('puestos.*', 'departamentos.Departamento_Nombre');
        if($estatus>0)
            $puestosLst=$puestosLst->where('puestos.Id_Estatus',$estatus);
        return $puestosLst->get();
    }

    public function estudio(puestos $puestos, $Uid_Puesto)
    {
        $puesto=$puestos::find($Uid_Puesto);

        $sql = "select e.Uid_Empleado ,e.Empleado_Nombre ,
        (select NivelEducativo_Nivel from niveles_educativos where Uid_NivelEducativo=c.Uid_NivelEducativo order by NivelEducativo_Nivel desc limit 1) as Nivel,
        (select count(Uid_Entrenamiento) from entrenamientos where Uid_Empleado=e.Uid_Empleado and Uid_Curso=cu.Uid_Curso) as cursos,
        (select DominaIdioma_PorcentajeEscritura from domina_idiomas where e.Uid_Empleado=Uid_Empleado) as idioma_Escritura,
        (select DominaIdioma_PorcentajeConversacion from domina_idiomas where e.Uid_Empleado=Uid_Empleado) as idioma_Conversacional,
        (select count(Uid_PromocionInterna) from promociones_internas where Uid_Empleado=e.Uid_Empleado) as promociones,
        (select count(Uid_Reconocimiento) from reconocimientos where Uid_Empleado=e.Uid_Empleado) as reconocimientos,
        (select NivelEducativo_Nivel from niveles_educativos where Uid_NivelEducativo=c.Uid_NivelEducativo order by NivelEducativo_Nivel desc limit 1)+
        (select count(Uid_Entrenamiento) from entrenamientos where Uid_Empleado=e.Uid_Empleado and Uid_Curso=cu.Uid_Curso)+
        (select DominaIdioma_PorcentajeEscritura from domina_idiomas where e.Uid_Empleado=Uid_Empleado)+
        (select DominaIdioma_PorcentajeConversacion from domina_idiomas where e.Uid_Empleado=Uid_Empleado)+
        (select count(Uid_PromocionInterna) from promociones_internas where Uid_Empleado=e.Uid_Empleado)+
        (select count(Uid_Reconocimiento) from reconocimientos where Uid_Empleado=e.Uid_Empleado) AS Total
        from empleados e
        left join sueldos as s on e.Uid_Empleado = s.Uid_Empleado
        left join escolaridades AS es ON e.Uid_Empleado= es.Uid_Empleado
        join carreras as c on es.Uid_Carrera=c.Uid_Carrera and c.Uid_AreaFormacion='{$puesto->Uid_AreaFormacion}'
        left join cursos as cu on cu.Uid_AreaFormacion='{$puesto->Uid_AreaFormacion}'
        WHERE e.Id_Estatus=2 and e.Empleado_SalarioMensual<='{$puesto->Puesto_Sueldo_Max}'
        and e.Uid_Empleado != 'cc88fb90-b8bd-11e9-a0be-51c4189b47aa'
        order by Total desc";

        return DB::select(DB::raw($sql));
    }

    public function graficos()
    {
        return view('Catalogos.Nominas.Vacantes.graficos');
    }

    public function asignacion(Request $request, puestos $puestos, $uid)
    {
        // return $request->Uid_Responsable;
        try{
            $puesto=$puestos::where('Uid_Responsable',$request->Uid_Responsable)->first();
            if($puesto)
            {
                $puesto->Id_Estatus=4;
                $puesto->Uid_Responsable=null;
                $puesto->update();
                promociones_internas::create(['Uid_Puesto'=>$uid,
                                            'Uid_Empleado'=>$request->Uid_Responsable,
                                            'PromocionInterna_Fecha'=>Carbon::now()]);
            }
            $puesto=$puestos::where('Uid_Puesto',$uid)->first();
            $puesto->update(['Uid_Responsable'=>$request->Uid_Responsable,
                            'Id_Estatus'=>1]);

            empleados::where('Uid_Empleado',$request->Uid_Responsable)
                    ->update(['Uid_Puesto'=>$uid,
                            'Uid_Departamento'=>$puesto->Uid_Departamento,
                            'Id_Estatus'=>2]);

            kardex::create([
                'Kardex_Descripcion'=>'Vacante cubierta',
                'Kardex_Uid_Registro'=>$uid
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Vacante cubierta correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al cubrir la vacante: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$uid
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

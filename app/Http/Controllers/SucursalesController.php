<?php

namespace App\Http\Controllers;

use App\sucursales;
use Illuminate\Http\Request;
use App\kardex;

class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function show(sucursales $sucursales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function edit(sucursales $sucursales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sucursales $sucursales)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function destroy(sucursales $sucursales)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\perfiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PerfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.perfiles');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, perfiles $perfiles)
    {
        try
        {
            $perfil=$perfiles::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Perfil Creado',
                'Kardex_Uid_Registro'=>$perfil->Id_Perfil
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Perfil se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Perfil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\perfiles  $perfiles
     * @return \Illuminate\Http\Response
     */
    public function show(perfiles $perfiles, $id)
    {
        return $perfiles::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\perfiles  $perfiles
     * @return \Illuminate\Http\Response
     */
    public function edit(perfiles $perfiles, $id)
    {
        return $perfiles::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\perfiles  $perfiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, perfiles $perfiles, $id)
    {
        try{
            $perfiles::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Perfil Editado',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Perfil editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Perfil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\perfiles  $perfiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(perfiles $perfiles, $id)
    {
        try{
            $perfiles::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Perfil",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Perfil eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Perfil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\estados_civiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class EstadosCivilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estadosciviles=Estados_Civiles::where('Id_Estatus',1)->get();
        return view('Catalogos.Nominas.estadosciviles', compact('estadosciviles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $estado=Estados_Civiles::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Estado Civil",
                'Kardex_Uid_Registro'=>$estado->Uid_EstadoCivil
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Estado Civil se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Estado Civil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estados_civiles  $estados_civiles
     * @return \Illuminate\Http\Response
     */
    public function show(estados_civiles $estados_civiles)
    {
        return $estados_civiles::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estados_civiles  $estados_civiles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Estados_Civiles::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estados_civiles  $estados_civiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            Estados_Civiles::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Estado Civil",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Estado Civil editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Estado Civil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estados_civiles  $estados_civiles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            estados_civiles::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Estado Civil eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Estado Civil eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Estado Civil: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

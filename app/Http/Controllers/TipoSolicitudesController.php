<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_incidencias;
use App\tipo_solicitudes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TipoSolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.tiposolicitudes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, tipo_solicitudes $tipo_solicitudes)
    {
        try
        {
            $tipo_solicitud=$tipo_solicitudes::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Tipo de Solicitud Creada",
                'Kardex_Uid_Registro'=> $tipo_solicitud->Uid_TipoSolicitud
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo de Solicitud se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Tipo de Solicitud: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoSolicitudes  $TipoSolicitudes
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_solicitudes $tipo_solicitudes,$status)
    {
        $tipo_solicitudesLst= $tipo_solicitudes->Join('tipo_incidencias','tipo_solicitudes.Id_TipoIncidencia','tipo_incidencias.Id_TipoIncidencia')
                                                ->select('tipo_solicitudes.*','TipoIncidencia_Nombre');
        if($status>0)
            $tipo_solicitudesLst= $tipo_solicitudesLst->where('Id_Estatus',$status);
        return $tipo_solicitudesLst->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_solicitudes  $tipo_solicitudes
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_solicitudes $tipo_solicitudes, $id)
    {
        return $tipo_solicitudes::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_solicitudes  $tipo_solicitudes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_solicitudes $tipo_solicitudes, $id)
    {
        try{
            $tipo_solicitudes::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Tipo de Solicitud Editada",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo de Solicitud editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo de Solicitud: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_solicitudes  $tipo_solicitudes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, tipo_solicitudes $tipo_solicitudes, $id)
    {
        $statusAcción=$request->Id_Estatus==1?'Activar':'Desactivar';
        $status=$request->Id_Estatus==1?'Activada':'Desactivada';
        try{
            $tipo_solicitudes::where('Uid_TipoSolicitud',$id)
                            ->update(['Id_Estatus'=>$request->Id_Estatus]);
            kardex::create([
                'Kardex_Descripcion'=>"Tipo de Solicitud $status",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>$statusAcción,'Message'=>"Tipo de Solicitud $status"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al $statusAcción la Tipo de Solicitud: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

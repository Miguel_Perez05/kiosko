<?php

namespace App\Http\Controllers;

use App\periodos;
use App\ejercicios;
use Illuminate\Http\Request;
use App\kardex;

class PeriodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ejercicios=ejercicios::all();
        $periodos=periodos::
        Join('ejercicios','periodos.Uid_Ejercicio','=','ejercicios.Uid_Ejercicio')
        ->select('Periodo_Nombre', 'Ejercicio_Nombre','Uid_Periodo','Periodo_Inicial'
        ,'Periodo_Final','Periodo_Estado')->where('periodos.Id_Estatus',1)->get();
        return view('Catalogos.Nominas.periodos', compact('periodos','ejercicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            periodos::create($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Periodo se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            return response()->json(array('status'=>"warning", 'Accion'=>'Creación','Message'=>"Hubo un problema al registrar Periodo: {$e->getMessage()}"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function show(periodos $periodos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return periodos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            periodos::find($id)->update($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Periodo editado correctamente"));
        }
        catch(\Exception $e){
            return response()->json(array('status'=>"warning", 'Accion'=>'Edición','Message'=>"Hubo un problema al editar el Periodo: {$e->getMessage()}"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            periodos::find($id)->delete();
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Periodo",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Periodo eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Periodo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"warning", 'Accion'=>'Eliminación','Message'=>"Hubo un problema al eliminar el Periodo"));
        }
    }
}

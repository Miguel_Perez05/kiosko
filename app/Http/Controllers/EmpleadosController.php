<?php

namespace App\Http\Controllers;

use App\cursos;
use App\kardex;
use App\puestos;
use App\sueldos;
use App\festivos;
use App\usuarios;
use App\empleados;
use App\historial;
use App\escolaridades;
use App\nacionalidades;
use App\estados_civiles;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\promociones_internas;
use Illuminate\Support\Carbon;
use App\percepciones_deducciones;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;
use LaravelFullCalendar\Facades\Calendar;

class EmpleadosController extends Controller
{

    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public function cumpleanos()
    {
        $events = [];
        $data = empleados::where('Id_Estatus','=',1)
        ->select('Empleado_Nombre',
        DB::raw('concat(DATE_FORMAT(Empleado_Fecha_Nacimiento, "%d-%m-"),Year(curdate())) as Empleado_Fecha_Nacimiento'))
        ->whereRaw('Month(Empleado_Fecha_Nacimiento)=Month(curdate())')
        ->get();

        $dias = festivos::select('Festivo_Nombre',
        DB::raw('concat(DATE_FORMAT(Festivo_Dia, "%d-%m-"),Year(curdate())) as Festivo_Dia'))
        ->where('Id_Estatus','=',1)
        ->whereRaw('Year(Festivo_Dia)=Year(curdate())')
        ->get();

        foreach ($data as $key => $value) {
            $events[] = Calendar::event(
                $value->Empleado_Nombre,
                true,
                new \DateTime($value->Empleado_Fecha_Nacimiento),
                new \DateTime($value->Empleado_Fecha_Nacimiento),
                null,
                [
                    'Color' => 'Blue',
                    'textColor'=>'White'
                ]
            );
        }

        foreach ($dias as $key => $value) {
            $events[] = Calendar::event(
                $value->Festivo_Nombre,
                true,
                new \DateTime($value->Festivo_Dia),
                new \DateTime($value->Festivo_Dia),
                null,
                [
                    'Color' => 'Green',
                    'textColor'=>'White'
                ]
            );
        }
        $calendar = Calendar::addEvents($events)->setOptions(['firstDay' => 7, 'lang'=> 'es',
        'header' =>
        [
            'left' => '',
            'center' => 'title',
            'right' => '',
        ]]);

        $TituloCalendar='Cumpleaños del mes';

        return view('kiosko.fullcalendar', compact('calendar','TituloCalendar'));
    }

    public function index(Request $request)
    {
        $typeString='Todos los Empleados';
        $plantilla="empleados";
        if($request->type=="0")
            $typeString='Empleados Inactivos';
        else if($request->type=="1")
            $typeString='Empleados Sin Contrato';
        else if($request->type=="2")
        {
            $typeString='Empleados Con Contrato';
            $plantilla="empleadoscontrato";
        }
        // else if($request->type==2)
        //     $typeString='Empleados Con Contrato';
        $type=$request->type;

        return view("Catalogos.Empleados.$plantilla",compact('type','typeString'));//'empleados',
    }

    public function listado(Request $request, empleados $empleados)
    {
        return $empleados::select('empleados.Uid_Empleado as id',
                                    DB::raw("CONCAT(Empleado_Nombre,' ',Empleado_APaterno,' ',Empleado_AMaterno) as text"))
                                    ->join('usuarios','empleados.Uid_Empleado','=','usuarios.Uid_Empleado')
                                    ->where(function($query) use($request) {
                                        if($request->type<"4")
                                            $query->where('Id_Estatus',$request->type);
                                        else if($request->type=="2")
                                        {
                                            $query->where('Id_Estatus','>','0');
                                        }
                                        else if($request->type=="all")
                                        {
                                            $query->where('empleados.Id_Estatus','<','2');
                                        }
                                    })
                                    ->where(function($query) use($request) {
                                        if($request->term!=null)
                                        {
                                            $query->orWhere('Empleado_Completo','like',"%$request->term%")
                                                ->orWhere('Empleado_APaterno','like',"%$request->term%")
                                                ->orWhere('Empleado_AMaterno','like',"%$request->term%");
                                        }
                                    }
                                )->orderBy('text')->take(20)->get();
    }

    public function create()
    {
        return view('Catalogos.Nominas.nuevoempleado');
    }

    public function store(Request $request)
    {
        try
        {
            $empleado=empleados::create($request->all());
            $usuario = usuarios::create([
                'Usuario_NickName'=>str_limit($request['Empleado_Nombre'], $limit = 1, $end = '').''.str_limit($request['Empleado_APaterno'], $limit = 2, $end = '').''.str_limit($request['Empleado_AMaterno'], $limit = 2, $end = ''),
                'Usuario_Password'=>bcrypt(str_replace('-','',$request['Empleado_Fecha_Nacimiento'])),
                'Usuario_Correo'=>$request['Usuario_Correo'],
                'Uid_Empleado'=>$empleado->Uid_Empleado,
                'Id_Perfil'=>2,
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Empleado",
                'Kardex_Uid_Registro'=>$empleado->Uid_Empleado
                ]);
            return $usuario;
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Empleado se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Empleado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function showCombo(empleados $empleados)
    {
        return $empleados::where('Id_Estatus',1)->get();
    }

    public function show(Request $request, empleados $empleados, $type)
    {
        $Uid_Jefe='';
        if(\AuthUser::get_perfil()=='false' && \AuthUser::get_jefe()==true)
             $Uid_Jefe=\AuthUser::get_employee()->Uid_Departamento;

        $empleadosList=$empleados::Join('usuarios','empleados.Uid_Empleado','usuarios.Uid_Empleado')
             ->LeftJoin('departamentos','empleados.Uid_Departamento','departamentos.Uid_Departamento')
             ->LeftJoin('puestos','empleados.Uid_Puesto','puestos.Uid_Puesto')
             ->LeftJoin('empresas','empleados.Uid_Empresa','empresas.Uid_Empresa')
             ->leftJoin('historial',function($query)
             {
                 $query->on('empleados.Uid_Empleado','historial.Uid_Empleado')
                         ->where('historial.Uid_Empleado','=','empleados.Uid_Empleado')
                         ->whereRaw('historial.Historial_Ingreso = (select max(Historial_Ingreso)
                         from historial where historial.Uid_Empleado=Uid_Empleado )')
                         ->orwhereRaw('historial.Historial_Reingreso = (select max(Historial_Reingreso)
                         from historial where historial.Uid_Empleado=Uid_Empleado )');
             })
             ->where(function ($query) use ($Uid_Jefe,$type)
             {
                 if($Uid_Jefe!='')
                 {
                     $query->where('empleados.Uid_Departamento',$Uid_Jefe)
                     ->where('empleados.Id_Estatus',1)
                     ->orwhere('empleados.Uid_Departamento',null);
                 }

                 if($type!="")
                 {
                     $query->where('empleados.Id_Estatus',$type);
                 }

             })
             ->select('empleados.Empleado_Nombre', 'empleados.Empleado_APaterno', 'empleados.Empleado_AMaterno', 'empleados.Id_Estatus'
             ,'usuarios.Uid_Usuario','puestos.Puesto_Nombre', 'departamentos.Departamento_Nombre');
        return datatables()->eloquent($empleadosList)->toJson();
    }

    public function perfil()
    {
        return view('Catalogos.Nominas.perfilempleado');
    }

    public function edit(empleados $empleados, $id)
    {
        $cursos=cursos::where('cursos.Id_Estatus',1)
                        ->join('puestos', 'cursos.Uid_AreaFormacion','puestos.Uid_AreaFormacion')
                        ->where('puestos.Uid_Puesto',$empleados->Uid_Puesto)->get();

        $percepciones= percepciones_deducciones::Leftjoin('tipo_bonos','percepciones_deducciones.Uid_TipoBono','tipo_bonos.Uid_TipoBono')
        ->where('percepciones_deducciones.Id_Estatus',1)
        ->where('Id_TipoIncidencia',5)->where('Uid_Empleado',$empleados->Uid_Empleado)->get();

        $escolaridades=escolaridades::Join('carreras','escolaridades.Uid_Carrera','carreras.Uid_Carrera')
                ->where('Uid_Empleado','=',$empleados->Uid_Empleado)
                ->where('escolaridades.Id_Estatus',1)->get();

        $promociones=promociones_internas::Join('puestos','promociones_internas.Uid_puesto','puestos.Uid_puesto')
                ->where('Uid_Empleado','=',$empleados->Uid_Empleado)->get();

        return view('Catalogos.Nominas.perfilempleado',compact('cursos','percepciones', 'escolaridades','promociones'));
    }

    public function showperfil(empleados $empleados, $Uid_Usuario)
    {
        $usuario=usuarios::where('Uid_Usuario',$Uid_Usuario)->first();

        return $empleados::from('empleados as e')
                            ->Join('usuarios as u','e.Uid_Empleado','u.Uid_Empleado')
                            ->leftJoin('puestos as p','e.Uid_Puesto','p.Uid_Puesto')
                            ->leftJoin('departamentos as d','e.Uid_Departamento','d.Uid_Departamento')
                            ->leftJoin('historial as in', 'e.Uid_Empleado', 'in.Uid_Empleado')
                            ->leftJoin(DB::raw("(Select Uid_Empleado, Sueldo_Fecha, Sueldo_Monto from sueldos where Uid_Empleado = '$usuario->Uid_Empleado' order by Sueldo_Fecha desc LIMIT 1) as s"),function($join){
                                $join->on('e.Uid_Empleado', 's.Uid_Empleado');
                            })
                            ->select('u.Usuario_NickName', 'Usuario_Correo as Correo','Uid_Usuario','e.Empleado_Telefono as Telefono',
                                DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Usuario_Nombre"),
                                'p.Puesto_Nombre as Puesto','Departamento_Nombre as Departamento',DB::raw('(
                                    CASE WHEN Usuario_Avatar is null
                                    THEN "/img/sistema/No-Foto.png"
                                    ELSE Usuario_Avatar
                                    END) AS Avatar'),
                                'e.*','p.*','d.*','u.*', 's.Sueldo_Fecha','s.Sueldo_Monto'
                                ,DB::raw('(Select Historial_Ingreso from historial where Uid_Empleado = `e`.`Uid_Empleado` order by created_at asc LIMIT 1) AS Fecha_Ingreso')
                                ,DB::raw('(Select Historial_Reingreso from historial where Uid_Empleado = `e`.`Uid_Empleado` order by created_at desc LIMIT 1) AS Fecha_Reingreso')
                                )
                            ->where('u.Uid_Usuario',"$Uid_Usuario")->first();
    }

    public function update(Request $request, empleados $empleados, $Uid_Empleado)
    {
        try
        {
            $empleados::find($Uid_Empleado)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Empleado",
                'Kardex_Uid_Registro'=>$Uid_Empleado
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Empleado editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Empleado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$Uid_Empleado
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function destroy(Request $request,empleados $empleados, $Uid_Usuario)
    {
        // try
        // {
        //     $empleado=$empleados::where('Uid_Empleado','=',$request->Uid_Empleado)->first();
        //     $empleado->update([
        //         'Id_Estatus'=>0
        //     ]);

        //     usuarios::find($Uid_Usuario)->update(['Id_Estatus'=>0]);

        //     historial::create([
        //         'Uid_Empleado'=>$request->Uid_Empleado,
        //         'Historial_Egreso'=>Carbon::now(),
        //         'Uid_Empresa'=>$empleado->Uid_Empresa
        //     ]);

        //     puestos::where('Uid_Responsable',$request->Uid_Empleado)->update([
        //         'Uid_Responsable'=>null
        //         ]);

        //     kardex::create([
        //         'Kardex_Descripcion'=>"Baja de Empleado",
        //         'Kardex_Uid_Registro'=>$request->Uid_Empleado
        //         ]);
        //     return response()->json(array('status'=>"success", 'Accion'=>'Baja','Message'=>"Empleado dado de baja correctamente"));
        // }
        // catch(\Exception $e){
        //     $error="Hubo un problema al dar de baja al Empleado: {$e->getMessage()}";
        //     kardex::create([
        //         'Kardex_Descripcion'=>$error,
        //         'Kardex_Uid_Registro'=>$request->Uid_Empleado
        //         ]);
        //     return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        // }
    }

    public function updateemployees(empleados $empleados){
        $usuarios=usuarios::select('Uid_Empleado')->get()->toArray();
        $empleadosList= $empleados->wherenotin('Uid_Empleado',$usuarios)->take(500)->get();

        return $empleadosList->map(function($items){
            return usuarios::create([
                'Usuario_NickName'=>str_limit($items->Empleado_Nombre, $limit = 1, $end = '').''.str_limit($items->Empleado_APaterno, $limit = 2, $end = '').''.str_limit($items->Empleado_AMaterno, $limit = 2, $end = ''),
                'Usuario_Password'=>bcrypt(str_replace('-','',$items->Empleado_Fecha_Nacimiento)),
                'Usuario_Correo'=>$items->Empleado_Nombre.'_'.$items->Empleado_APaterno.'_'.$items->Empleado_AMaterno.'@mail.com',
                'Uid_Empleado'=>$items->Uid_Empleado,
                'Id_Perfil'=>2,
            ]);
        });
    }

    public function getUsuario($Uid_Empleado)
    {
        return usuarios::where('Uid_Empleado',$Uid_Empleado)->first();
    }
}

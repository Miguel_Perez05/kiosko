<?php

namespace App\Http\Controllers;

use App\bajas;
use App\kardex;
use App\comentarios;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comentarios=Comentarios::all();
        return view('Comentarios.comentarios', compact('comentarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $request->Uid_Comentarios=Uuid::generate();
            $request->Uid_Usuario_Crea=Auth::user()->Uid_Usuario;
            $request->Uid_Usuario_Edita=Auth::user()->Uid_Usuario;
            bajas::create($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Baja se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Baja: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\comentarios  $comentarios
     * @return \Illuminate\Http\Response
     */
    public function show(comentarios $comentarios)
    {
        return Comentarios::findOrFail($comentarios->Uid_Comentarios);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\comentarios  $comentarios
     * @return \Illuminate\Http\Response
     */
    public function edit(comentarios $comentarios)
    {
        return Comentarios::findOrFail($comentarios->Uid_Comentarios);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\comentarios  $comentarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $request->Uid_Usuario_Edita=Auth::user()->Uid_Usuario;
            $comentario=Comentarios::findOrFile($id);
            $comentario->update($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Comenatrio editado correctamente"));
        }
        catch(\Exception $e){
            return response()->json(array('status'=>"alert", 'Accion'=>'Edición','Message'=>"Hubo un problema al editar el Comenatrio"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\comentarios  $comentarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(comentarios $comentarios)
    {
        try{
            $comentarios->Id_Estatus=0;
            $comentarios->Uid_Usuario_Edita=Auth::user()->Uid_Usuario;
            $comentario=Comentarios::findOrFile($comentarios->Uid_Comentarios);
            $comentario->update($comentarios->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Comentario eliminado"));
        }
        catch(\Exception $e){
            return response()->json(array('status'=>"alert", 'Accion'=>'Eliminación','Message'=>"Hubo un problema al eliminar el Comentario"));
        }
    }
}

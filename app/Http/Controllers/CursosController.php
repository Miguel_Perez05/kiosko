<?php

namespace App\Http\Controllers;

use App\cursos;
use App\kardex;
use App\areas_formacion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\CursoRequest;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('Catalogos.cursos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $request, cursos $cursos)
    {
        try
        {
            $curso=$cursos::create($request->all());

            kardex::create([
                'Kardex_Descripcion'=>"Edición del Curso",
                'Kardex_Uid_Registro'=> $curso->Uid_Curso
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Curso se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Curso: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('responseJSON'=> array('message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, cursos $cursos)
    {
        return $cursos::where('cursos.Uid_AreaFormacion',$request->Uid_AreaFormacion)
                        ->where('cursos.Id_Estatus',1)
                        ->join('areas_formacion','cursos.Uid_AreaFormacion','areas_formacion.Uid_AreaFormacion')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function edit(cursos $cursos, $uid)
    {
        return $cursos::findOrFail($uid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cursos $cursos, $id)
    {
        try{
            $cursos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Curso",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Curso editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Curso: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cursos  $cursos
     * @return \Illuminate\Http\Response
     */
    public function destroy(cursos $cursos, $id)
    {
        try{
            $cursos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Curso",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Revocación','Message'=>"Curso eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Curso: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

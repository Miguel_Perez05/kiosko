<?php

namespace App\Http\Controllers;

use App\kardex;
use App\usuarios;
use App\escolaridades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class EscolaridadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escolaridades=escolaridades::where('Id_Estatus',1)->get();
        return view('Empleados.escolaridades', compact('escolaridades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, escolaridades $escolaridades)
    {
        try
        {
            $escolaridad=$escolaridades::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Escolaridad",
                'Kardex_Uid_Registro'=>$escolaridad->Uid_Escolaridad
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Escolaridad creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Escolaridad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, escolaridades $escolaridades, $uid)
    {
        $empleado=usuarios::where('Uid_Usuario','=',$uid)->first();
        return $escolaridades::Join('carreras','escolaridades.Uid_Carrera','carreras.Uid_Carrera')
                            ->where('Uid_Empleado','=',$empleado->Uid_Empleado)
                            ->where('escolaridades.Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return \Illuminate\Http\Response
     */
    public function edit(escolaridades $escolaridades)
    {
        return escolaridades::findOrFail($escolaridades->Uid_Escolaridad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\escolaridades  $escolaridades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, escolaridades $escolaridades, $id)
    {
        try{
            $escolaridades::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de la Escolaridad",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Escolaridad editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Escolaridad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return \Illuminate\Http\Response
     */
    public function destroy(escolaridades $escolaridades, $id)
    {
        try{
            $escolaridades::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Escolaridad eliminada",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Escolaridad eliminada"));
        }
        catch(\Exception $e){

            $error="Hubo un problema al eliminar la Escolaridad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\Helpers;
use App\perfiles;
use App\usuarios;
use App\empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class UsuariosController extends Controller
{

    function __construct(){
        $this->middleware('auth');
    }

    public function MiPerfil()
    {
        return redirect()->route('empleados.edit', ['empleado' => Auth::user()->Uid_Usuario]);
    }

    public function index()
    {
        return view('Catalogos.usuarios');
    }

    public function create()
    {
        return view('/register');
    }

    public function store(Request $request, usuarios $usuarios)
    {
        try
        {
            $usuario=$usuarios::create([
                'Usuario_NickName'=>$request['Usuario_NickName'],
                'Usuario_Correo'=>$request['Usuario_Correo'],
                'Usuario_Password'=>bcrypt(''),
                'Id_Perfil'=>$request['Id_Perfil'],
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Usuario",
                'Kardex_Uid_Registro'=>$usuario->Uid_Usuario
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Usuario se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al Crear el Usuario: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function show(usuarios $usuarios)
    {
        return datatables($usuarios::from('usuarios as u')
                        ->Join('empleados as e', 'u.Uid_Empleado','e.Uid_Empleado')
                        ->Join('perfiles as p','u.Id_Perfil','p.Id_Perfil')
                        ->select('u.Usuario_NickName', 'u.Usuario_Correo','Uid_Usuario','e.Empleado_Nombre','e.Empleado_APaterno','e.Empleado_AMaterno',
                        // DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Usuario_Nombre"),
                        'p.Perfil_Nombre as Usuario_Perfil')
                        ->where('u.Id_Estatus',1)
                        ->orderby('Empleado_Nombre'))->toJson();
    }

    public function edit(usuarios $usuarios, $id)
    {
        return $usuarios::from('usuarios as u')
        ->leftJoin('empleados as e', 'u.Uid_Empleado','e.Uid_Empleado')
        ->select('u.Usuario_NickName', 'Usuario_Correo','Uid_Usuario','Id_Perfil','u.Id_Estatus',
        DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Usuario_Nombre")
        ,DB::raw('(
            CASE WHEN Usuario_Avatar is null
            THEN "img/sistema/No-Foto.png"
            ELSE Usuario_Avatar
            END) AS Avatar'))
        ->where('u.Uid_Usuario',$id)->first();
    }

    public function update(Request $request, usuarios $usuarios, $id)
    {
        try{
            $usuarios::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Usuario",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Usuario editado correctamente",'Uid_Empleado'=>$id));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Usuario: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function updatePassword(Request $request, usuarios $usuarios)
    {
        try{
            if($request->input('Usuario_Password')!=$request->input('Usuario_Password_'))
            {
                return back()->with('warningmsj','Las contraseñas deben coincidir');
            }

            $usuario=$usuarios::findOrFail($request->input('Uid_Usuario'));
            $usuario->update([
                'Usuario_Password'=>bcrypt($request['Usuario_Password']),
                'Usuario_Nuevo'=>0,
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Actualización de contraseña de Usuario",
                'Kardex_Uid_Registro'=> $request->input('Uid_Usuario')
                ]);
            return redirect('/logout');
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"La contraseña se ha actualizado con éxito. <br>Es necesario iniciar con la nueva contraseña.",'Uid_Empleado'=>$request->input('Uid_Usuario')));
        }
        catch(\Exception $e){
            $error="Hubo un problema al actualizar la contraseña el Usuario: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$usuarios->Uid_Usuario
            ]);
            return back()->with('warningmsj','Hubo un problema al actualizar la contraseña');
        }

    }

    public function updateAvatar(Request $request, usuarios $usuarios, $idU, $idE)
    {
        try{
            $usuarios::find($idU)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Usuario",
                'Kardex_Uid_Registro'=>$idU
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Avatar actualizado correctamente",'Uid_Empleado'=>$idE));
        }
        catch(\Exception $e){
            $error="Hubo un problema al actualizar el avatar el Usuario: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$idU
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function updateEmail(Request $request, usuarios $usuarios, $idU, $idE)
    {
        try{
            $usuarios::find($idU)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Usuario",
                'Kardex_Uid_Registro'=>$idU
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Email actualizado correctamente",'Uid_Empleado'=>$idE));
        }
        catch(\Exception $e){
            $error="Hubo un problema al actualizar el email el Usuario: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$idU
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function NewPassword()
    {
        return view('usuarios.passwordnuevo');//C:\laragon\www\Kiosko\resources\views\usuarios\reset.blade.php
    }

    public function master(){
        return view('master');
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\puestos;
use App\empleados;
use Carbon\Carbon;
use App\areas_formacion;
use App\niveles_educativos;
use Illuminate\Http\Request;
use App\promociones_internas;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PuestosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.puestos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, puestos $puestos)
    {
        try
        {
            $puesto=$puestos::create($request->all());

            kardex::create([
                'Kardex_Descripcion'=>'Puesto creado',
                'Kardex_Uid_Registro'=>$puesto->Uid_Puesto
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Puesto se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Puesto: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\puestos  $puestos
     * @return \Illuminate\Http\Response
     */
    public function show(puestos $puestos,$id_Estatus)
    {
        $puestosList=$puestos->LeftJoin('empleados', 'Uid_Responsable', 'Uid_Empleado')
                            ->LeftJoin('departamentos', 'puestos.Uid_Departamento', 'departamentos.Uid_Departamento')
                            ->Select('puestos.*', 'departamentos.Departamento_Nombre',
                            DB::raw("CONCAT(empleados.Empleado_Nombre,' ', empleados.Empleado_APaterno,' ', empleados.Empleado_AMaterno) AS Responsable"));
        if($id_Estatus>0)
            return $puestosList->where('puestos.Id_Estatus',$id_Estatus)->get();
        else
            return $puestosList->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\puestos  $puestos
     * @return \Illuminate\Http\Response
     */
    public function edit(puestos $puestos, $id)
    {
        return $puestos->where('puestos.Uid_Puesto',$id)
                        ->LeftJoin('empleados', 'Uid_Responsable', 'Uid_Empleado')
                        ->LeftJoin('departamentos', 'puestos.Uid_Departamento', 'departamentos.Uid_Departamento')
                        ->Select('puestos.*', 'departamentos.Departamento_Nombre',
                        DB::raw("CONCAT(empleados.Empleado_Nombre,' ', empleados.Empleado_APaterno,' ', empleados.Empleado_AMaterno) AS Responsable"))
                        ->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\puestos  $puestos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, puestos $puestos, $id)
    {
        try{
            $puestos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Puesto Editado',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Puesto editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Puesto: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\puestos  $puestos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, puestos $puestos, $id)
    {
        try{
            $puestos::find($id)->update(['Id_Estatus'=>$request->Id_Estatus,
                                        'Uid_Responsable'=>null]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Puesto",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Puesto eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Puesto: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

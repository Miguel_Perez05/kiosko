<?php

namespace App\Http\Controllers;

use App\campos_extras;
use Illuminate\Http\Request;

class CamposExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\campos_extras  $campos_extras
     * @return \Illuminate\Http\Response
     */
    public function show(campos_extras $campos_extras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\campos_extras  $campos_extras
     * @return \Illuminate\Http\Response
     */
    public function edit(campos_extras $campos_extras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\campos_extras  $campos_extras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, campos_extras $campos_extras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\campos_extras  $campos_extras
     * @return \Illuminate\Http\Response
     */
    public function destroy(campos_extras $campos_extras)
    {
        //
    }
}

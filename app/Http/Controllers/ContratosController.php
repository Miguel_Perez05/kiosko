<?php

namespace App\Http\Controllers;

use App\bajas;
use App\kardex;
use App\puestos;
use App\sueldos;
use App\usuarios;
use App\contratos;
use App\empleados;
use App\historial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.contratos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, contratos $contratos)
    {
        try
        {
            $solicitudes=new SolicitudesController();
            $respuesta=$solicitudes->InfoTramite($request->Uid_Empleado, "contrato");
            if($respuesta->status() !=200)
                return $respuesta;
            
            $contrato=$contratos::create($request->all());

            $sueldo=sueldos::create([
                                'Uid_Empleado'=>$request->Uid_Empleado,
                                'Sueldo_Fecha'=>$request->Contrato_Inicio,
                                'Sueldo_Monto'=>$request->Contrato_Salario
                            ]);

            historial::create([
                            'Uid_Empleado'=>$request->Uid_Empleado,
                            'Uid_Puesto'=>$request->Uid_Puesto,
                            'Uid_Departamento'=>$request->Uid_Departamento,
                            'Uid_Turno'=>$request->Uid_Turno,
                            'Uid_TipoEmpleado'=>$request->Uid_TipoEmpleado,
                            'Uid_TipoContrato'=>$request->Uid_TipoContrato,
                            'Uid_Empresa'=>$request->Uid_Empresa,
                            'Historial_Ingreso'=>Carbon::now(),
                            'Uid_Sueldo'=>$sueldo->Uid_Sueldo
                        ]);

            empleados::find($request->Uid_Empleado)
                    ->update([
                        'Uid_Empresa'=>$request->Uid_Empresa,
                        'Uid_RegistroPatronal'=>$request->Uid_RegistroPatronal,
                        'Uid_Departamento'=>$request->Uid_Departamento,
                        'Uid_Puesto'=>$request->Uid_Puesto,
                        'Uid_TipoEmpleado'=>$request->Uid_TipoEmpleado,
                        'Empleado_SalarioMensual'=>$request->Contrato_Salario,
                        'Uid_Turno'=>$request->Uid_Turno,
                        'Uid_TipoNomina'=>$request->Uid_TipoNomina,
                        'Id_Estatus'=>2
                    ]);

            kardex::create([
                        'Kardex_Descripcion'=>"Creación del Contrato",
                        'Kardex_Uid_Registro'=>$contrato->Uid_Contrato
                    ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Contrato se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Contrato: {$e->getMessage()}";
            kardex::create([
                        'Kardex_Descripcion'=>$error
                    ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\contratos  $contratos
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,contratos $contratos,$id)
    {
        $contratos= $contratos::join('empleados', 'contratos.Uid_Empleado','empleados.Uid_Empleado')
                            ->where('empleados.Uid_Empresa',$request->Uid_Empresa)
                            ->where('contratos.Id_Estatus',1);
        return datatables()->eloquent($contratos)->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\contratos  $contratos
     * @return \Illuminate\Http\Response
     */
    public function edit(contratos $contratos,$id)
    {
        if($id>0)
        {
            return $contratos::join('empleados', 'contratos.Uid_Empleado','empleados.Uid_Empleado')->findOrFail($id);
        }
        else
            return $contratos::orderBy('created_at', 'desc')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\contratos  $contratos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, contratos $contratos, $id)
    {
        try{
            $contratos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Contrato editado correctamente",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Contrato editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Contrato: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\contratos  $contratos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, contratos $contratos,$Uid_Empleado)
    {
        
    }
}

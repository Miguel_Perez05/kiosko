<?php

namespace App\Http\Controllers;

use App\kardex;
use App\empleados;
use Carbon\Carbon;
use App\incidencias;
use App\Observers\Tipo_IncidenciasObserver;
use App\tipo_incidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class IncidenciasController extends Controller
{

    public function index($tipo)
    {
        $tiposIncidencias=Tipo_Incidencias::where('Id_TipoIncidencia',$tipo)->first();
        $form=$tiposIncidencias->TipoIncidencia_List;

        return view("Catalogos.Nominas.Incidencias.$form");
    }

    public function empleados(incidencias $incidencias, $tipo)
    {
        $incidencia= $incidencias::IncidenciasEmpleados()
                                    ->where(function($incidencia) use ($tipo)  {
                                        if($tipo>0) {
                                            $incidencia->where('Incidencia_Tipo',$tipo);
                                        }
                                        else{
                                            $incidencia->where('Incidencia_Tipo',">",4);
                                        }
                                    })
                                    ->where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                                    ->get();

        $Titulo=null;
        $suma=null;
        if($tipo>0)
        {
            $Titulo=tipo_incidencias::where('Id_TipoIncidencia',$tipo)
                                ->select('TipoIncidencia_Nombre','TipoIncidencia_ManejaTiempo')
                                ->first();
            if($Titulo->TipoIncidencia_ManejaTiempo==0)
                $suma=incidencias::TotalDias($tipo);
            else
                $suma=incidencias::TotalHoras($tipo);
        }
        return view('kiosko.asistencia', compact('incidencias', 'Titulo','suma'));
    }

    public function create($tipo)
    {
        $tiposIncidencias=Tipo_Incidencias::where('Id_TipoIncidencia',$tipo)->first();
        $form=$tiposIncidencias->TipoIncidencia_Form;

        return view("Catalogos.Nominas.Incidencias.$form");
    }

    public function store(Request $request, incidencias $incidencias)
    {
        try
        {
            $incidencia=$incidencias::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creacion de incidencia",
                'Kardex_Uid_Registro'=>$incidencia->Uid_Incidencia
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Incidencia se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar la Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function show(incidencias $incidencias, $id)
    {
        $incidenciasLst=$incidencias::IncidenciasNominas();
        if($id>0)
            $incidenciasLst=$incidenciasLst->where('Incidencia_Tipo',$id);
        else
            $incidenciasLst=$incidenciasLst->where('Incidencia_Tipo',">",4);
        return $incidenciasLst->get();
    }

    public function edit(incidencias $incidencias, $id)
    {
        return $incidencias::leftjoin('usuarios','Uid_UsuarioJustifica','Uid_Usuario')
        ->leftjoin('empleados','usuarios.Uid_Empleado','empleados.Uid_Empleado')
        ->select('Incidencia_Comentario','Incidencia_FechaJustificacion','Incidencia_ComentarioJustifica','Empleado_Nombre',
        'Empleado_APaterno','Empleado_AMaterno','usuarios.Uid_Empleado','Usuario_NickName')
        ->findOrFail($id);
    }

    public function update(Request $request, incidencias $incidencias, $id)
    {
        try
        {
            $incidencias::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de incidencia",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Incidencia editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function justificacion(Request $request, incidencias $incidencias, $id)
    {

        try
        {
            $incidencia=$incidencias::findOrFail($id);
            $incidencia->update([
                'Incidencia_FechaJustificacion'=>Carbon::now(),
                'Uid_UsuarioJustifica'=>Auth::user()->Uid_Usuario,
                'Incidencia_ComentarioJustifica'=>$request['Incidencia_ComentarioJustifica']
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Justificación de incidencia",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Incidencia justificada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al justificar la Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function autorizacion(Request $request, incidencias $incidencias, $id)
    {

        try
        {
            $incidencia=$incidencias::findOrFail($id);
            $incidencia->update([
                'Incidencia_FechaJustificacion'=>Carbon::now(),
                'Uid_UsuarioJustifica'=>Auth::user()->Uid_Usuario,
                'Incidencia_ComentarioJustifica'=>$request->Incidencia_ComentarioJustifica
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Autorización de vacaciones",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Vacaciones autorizadas correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al autorizar la vacaciones: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function destroy(incidencias $incidencias, $id)
    {
        try{
            $incidencias::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Rechazo de las vacaciones",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Rechazar','Message'=>"Vacaciones Rechazada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al rechazar las vacaciones: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

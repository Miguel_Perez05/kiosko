<?php

namespace App\Http\Controllers;

use App\funciones;
use Illuminate\Http\Request;

class FuncionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\funciones  $funciones
     * @return \Illuminate\Http\Response
     */
    public function show(funciones $funciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\funciones  $funciones
     * @return \Illuminate\Http\Response
     */
    public function edit(funciones $funciones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\funciones  $funciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, funciones $funciones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\funciones  $funciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(funciones $funciones)
    {
        //
    }
}

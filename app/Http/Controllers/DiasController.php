<?php

namespace App\Http\Controllers;

use App\dias;
use App\kardex;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dias=dias::where('Id_Estatus',1)->OrderBy('Festivo_Dia')->get();
        return view('Empleados.dias', compact('dias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $dia=dias::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Dia",
                'Kardex_Uid_Registro'=> $dia->Uid_Dia
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Dia se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Dia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function show(dias $dias)
    {
        return dias::findOrFail($dias->Uid_Dia);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function edit(dias $dias)
    {
        return dias::findOrFail($dias->Uid_Dia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            dias::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Dia",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Dia editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Dia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            dias::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Dia eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Dia eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Dia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

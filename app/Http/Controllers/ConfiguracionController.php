<?php

namespace App\Http\Controllers;

use Input;

use Redirect;
use App\usuarios;
use App\empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\facades\Validator;

class ConfiguracionController extends Controller
{
    public function index()
    {
        return view('paneles.ConfiguracionGeneral.panel');
    }
    public function vista(){

        $empleados = empleados::where('Uid_Empleado','=',Auth::user()
        ->Uid_Empleado)->get();
        //$info = HabilitaInformacion::where('Uid_empresa','=',Auth::user()->Uid_empresa)->get();

        $datos = [ 'empleados' => $empleados];

        /*return view('configuracion.config')->with('datos',$datos);*/
        return view('configuracion.config',compact('empleados'));
    }

    /************AVATAR**************/

    public function vista_avatar(){
    	return view('configuracion.avatar');
    }

    public function avatar(Request $request){
        //obtenemos el campo file definido en el formulario
        $archivo = $request->file('archivo');
        $id=$request->input('Usuario_Avatar');

        /*print_r($request->file('archivo'));*/

        $arch = array('image'=>Input::file('archivo'));

        $reglas = array('image' =>'mimes:jpeg,png|max:1024');

        $validacion = Validator::make($arch,  $reglas);

        if ($validacion->fails()){
            dd('error, El formato del archivo no es el correcto');
        }


        //obtenemos el nombre del archivo
        $nombre = str_random(30) . '-' . $request->file('archivo')->getClientOriginalName();

        //indicamos que queremos guardar un nuevo archivo en el disco local
        \Storage::disk('local')->put($nombre,  \File::get($archivo));

        $directorio = '/storage/app/'.$nombre;
        Storage::makeDirectory($directorio);


        $usuario = usuarios::find($id);
        $usuario->Usuario_avatar = $nombre;
        $usuario->save();


       return "archivo guardado";

    }


    /************CONTRASEÑA**************/

    public function vista_password(){
    	return view('configuracion.password');
    }

    public function edita_password(Request $request, usuarios $usuarios){
        $data = $request->all();
        

        if($data != [''])
        {
        $validacion= \Validator::make(
                $data,
                    array('password'=>'min:6'),
                    array('password.min'=>'la contraseña no puede ser menor a 6 caracteres')
                );

                if ($validacion->fails()) {
                    return Response::json(array('ResponseStatus'=> array('Message'=>$validacion->messages())), 403);
                }
                else
                {
                    $usuarios::find($data['id'])->update(['Usuario_password'=>bcrypt($request->password)]);
                    return response()->json(array('status'=>"success", 'Accion'=>'Actualización','Message'=>"La contraseña ha sido actualizada"));
            }
        }
    }


    /************CORREO**************/

    public function vista_correo(){
    	return view('configuracion.correo');
    }

    public function edita_correo(Request $request){
        $data = $request->all();
        $usuario = usuarios::find($data['id']);

        if($data != [''])
        {
        $validacion= \Validator::make(
                $data,
                    array('correo'=>'email'),
                    array('correo.email'=>'te falta ingresar un correo')
                );

                if ($validacion->fails()) {
                    return response()->json(['status'=>false, 'message'=>$validacion->messages()]);
                }
                else
                {

                    $usuario->Usuario_correo = $data['correo'];
                    $usuario->save();

                    return response()->json(['status'=>true, 'message'=>'El correo ha sido actualizado con exito ']);
            }
        }
    }


    public function subir_imagen_usuario(Request $request)
    {

        $id=$request->input('Usuario_Avatar');
        $archivo = $request->file('archivo');
        $input  = array('image' => $archivo) ;
        $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif|max:2000');
        $validacion = Validator::make($input,  $reglas);
        if ($validacion->fails())
        {
          return view("mensajes.msj_rechazado")->with("msj","El archivo no es una imagen valida");
        }
        else
        {

            $nombre_original=$archivo->getClientOriginalName();
            $extension=$archivo->getClientOriginalExtension();
            $nuevo_nombre="userimagen-".$id.".".$extension;
            $r1=Storage::disk('fotografias')->put($nuevo_nombre,  \File::get($archivo) );
            $rutadelaimagen="../storage/fotografias/".$nuevo_nombre;

            if ($r1){

                $usuario=usuarios::find($id);
                $usuario->imagenurl=$rutadelaimagen;
                $r2=$usuario->save();
                return view("mensajes.msj_correcto")->with("msj","Imagen agregada correctamente");
            }
            else
            {
                return "nada";
            }


        }

    }


/*************************************++++configuracion de interfaz******************/

    public function vista_interfaz()
    {
        return view('configuracion.interfaz');
    }

}

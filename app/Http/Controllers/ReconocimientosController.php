<?php

namespace App\Http\Controllers;

use App\kardex;
use App\usuarios;
use App\recontratables;
use App\reconocimientos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ReconocimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, reconocimientos $reconocimientos)
    {
        try
        {
            $reconocimiento=$reconocimientos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Reconocimiento",
                'Kardex_Uid_Registro'=>$reconocimiento->Uid_Reconocimiento
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Reconocimiento del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Reconocimiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function storePU(Request $request, reconocimientos $reconocimientos, $Uid_Usuario)
    {
        try
        {
            $reconocimiento=$reconocimientos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Reconocimiento",
                'Kardex_Uid_Registro'=>$reconocimiento->Uid_Reconocimiento
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Reconocimiento del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Reconocimiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return \Illuminate\Http\Response
     */
    public function show(reconocimientos $reconocimientos, $Uid_Usuario)
    {
        $usuarios=usuarios::where('Uid_Usuario',$Uid_Usuario)->first();
        return $reconocimientos::where('Uid_Empleado',$usuarios->Uid_Empleado)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return \Illuminate\Http\Response
     */
    public function edit(reconocimientos $reconocimientos, $Uid)
    {
        return $reconocimientos::findOrFail($Uid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\reconocimientos  $reconocimientos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, reconocimientos $reconocimientos, $id)
    {
        try{
            $reconocimientos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Reconocimiento",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Reconocimiento editado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Reconocimiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return \Illuminate\Http\Response
     */
    public function destroy(reconocimientos $reconocimientos, $id)
    {
        try{
            $reconocimientos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Reconocimiento eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Reconocimiento eliminado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Reconocimiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);

        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\empresas;
use App\usuarios;
use App\empleados;
use App\historial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\registros_patronales;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class HistorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function altasbajas()
    {
        $historiales=historial::HistorialNominas()->get();
        $empleados=empleados::where('Id_Estatus',1)->get();
        $empresas=empresas::where('Id_Estatus',1)->get();
        $registrospatronales=registros_patronales::where('Id_Estatus',1)->get();
        return view('Catalogos.Nominas.altabaja',
        compact('historiales','empleados','empresas','registrospatronales'));
    }



    public function historialEmpleado()
    {
        $now=Carbon::now();
        $ingreso=historial::where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                ->whereRaw('historial.Historial_Ingreso = (select max(Historial_Ingreso)
                from historial where historial.Uid_Empleado=Uid_Empleado )')
                ->orwhereRaw('historial.Historial_Reingreso = (select max(Historial_Reingreso)
                from historial where historial.Uid_Empleado=Uid_Empleado )')
                ->orderBy('created_at', 'desc')->first();

         $historial=historial::where('updated_at','>=',$ingreso->updated_at)->get();

        $date=Carbon::parse($ingreso->Historial_Ingreso ?? $ingreso->Historial_Reingreso);
        $antiguedad=$date->diffInMonths($now);

        $empleado=Empleados::where('Uid_Empleado','=',Auth::user()->Uid_Empleado)->first();
        // $empresas=Empresas::where('Id_Estatus',1)->get();
        // $registrospatronales=Registros_Patronales::where('Id_Estatus',1)->get();
        return response()->json(array('historial'=>$historial, 'antiguedad'=>$antiguedad,'empleado'=>$empleado));

    }

    public function index(historial $historial)
    {
        return $historial->HistorialNominas()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, historial $historial)
    {
        try
        {
            $empleado=empleados::create([
                'Uid_Empresa'=>$request['Uid_Empresa'],
                'Uid_RegistroPatronal'=>$request['Uid_RegistroPatronal'],
                'Empleado_Nombre'=>$request['Empleado_Nombre'],
                'Empleado_APaterno'=>$request['Empleado_APaterno'],
                'Empleado_AMaterno'=>$request['Empleado_AMaterno'],
                'Empleado_Fecha_Nacimiento'=>$request['Empleado_Fecha_Nacimiento'],
                'Empleado_RFC'=>$request['Empleado_RFC'],
                ]);
            usuarios::create([
                'Usuario_NickName'=>str_limit($request['Empleado_Nombre'], $limit = 1, $end = '').''.str_limit($request['Empleado_APaterno'], $limit = 2, $end = '').''.str_limit($request['Empleado_AMaterno'], $limit = 2, $end = ''),
                'Usuario_Password'=>bcrypt(str_replace('-','',$request['Empleado_Fecha_Nacimiento'])),
                'Usuario_Correo'=>$request['Usuario_Correo'],
                'Uid_Empleado'=>$empleado->Uid_Empleado,
                'Id_Perfil'=>2,
                ]);
            $historia=$historial::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creacion de Historial",
                'Kardex_Uid_Registro'=>$historia->Uid_Historial
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Empresa se ha creada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al crear el registro Historial: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function storedCambio(Request $request, historial $historial)
    {
        try
        {
            empleados::find($request['Uid_Empleado'])
            ->update(['Id_Estatus'=>$request['Id_Estado']]);
            $usuario=usuarios::where('Uid_Empleado','=',$request['Uid_Empleado'])->first();
            $usuario->update(['Id_Estatus'=>$request['Id_Estado']]);
            $historia=$historial::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creacion de Historial",
                'Kardex_Uid_Registro'=>$historia->Uid_Historial
                ]);

            return response()->json(array('status'=>"success", 'Accion'=>'Estatus','Message'=>"Estatus cambiado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al crear el registro Historial: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\historial  $historial
     * @return \Illuminate\Http\Response
     */
    public function show(historial $historial, $id)
    {
        return $historial->HistorialNominas()->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\historial  $historial
     * @return \Illuminate\Http\Response
     */
    public function edit(historial $historial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\historial  $historial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\historial  $historial
     * @return \Illuminate\Http\Response
     */
    public function destroy(historial $historial)
    {
        //
    }
}

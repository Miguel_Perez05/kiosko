<?php

namespace App\Http\Controllers;

use App\kardex;
use Illuminate\Http\Request;

class KardexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            kardex::create($request->all());
        }
        catch(\Exception $e)
        {

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kardex  $kardex
     * @return \Illuminate\Http\Response
     */
    public function show(kardex $kardex)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kardex  $kardex
     * @return \Illuminate\Http\Response
     */
    public function edit(kardex $kardex)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kardex  $kardex
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kardex $kardex)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kardex  $kardex
     * @return \Illuminate\Http\Response
     */
    public function destroy(kardex $kardex)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\usuarios;
use App\entrenamientos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\EntrenamientoRequest;

class EntrenamientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Empleados.entrenamientos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntrenamientoRequest $request,entrenamientos $entrenamientos, $Uid_Usuario)
    {
        try
        {
            $entrenamiento=$entrenamientos::create($request->all());

            kardex::create([
                'Kardex_Descripcion'=>"Registro de Entrenamiento",
                'Kardex_Uid_Registro'=> $entrenamiento->Uid_Entrenamiento
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Entrenamiento se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Entrenamiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return \Illuminate\Http\Response
     */
    public function show(entrenamientos $entrenamientos,$Uid_Usuario)
    {
        $usuarios=usuarios::where('Uid_Usuario',$Uid_Usuario)->first();
        return $entrenamientos::join('cursos','entrenamientos.Uid_Curso','cursos.Uid_Curso')->where('Uid_Empleado',$usuarios->Uid_Empleado)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return \Illuminate\Http\Response
     */
    public function edit(entrenamientos $entrenamientos, $id)
    {
        return $entrenamientos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\entrenamientos  $entrenamientos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, entrenamientos $entrenamientos, $id)
    {
        try{
            $entrenamientos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Entrenamiento",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Entrenamiento editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Entrenamiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return \Illuminate\Http\Response
     */
    public function destroy(entrenamientos $entrenamientos,$id)
    {
        try{
            $entrenamientos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Entrenamiento eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Entrenamiento eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Entrenamiento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

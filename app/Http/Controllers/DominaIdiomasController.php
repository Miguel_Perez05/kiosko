<?php

namespace App\Http\Controllers;

use App\kardex;
use App\usuarios;
use App\domina_idiomas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class DominaIdiomasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d_idiomas=domina_idiomas::join('empleados','empleados.Uid_Empleado','domina_idiomas.Uid_Empleado')
                                ->join('idiomas','idiomas.Uid_Idioma','domina_idiomas.Uid_Idioma')
                                ->select('Idioma_Nombre','domina_idiomas.Uid_Dominio',
                                DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombre")
                                        ,'domina_idiomas.DominaIdioma_PorcentajeEscritura','domina_idiomas.DominaIdioma_PorcentajeConversacion')
                                        ->where('domina_idiomas.Id_Estatus',1)->get();
        return view('Catalogos.Nominas.idiomasdominados', compact('d_idiomas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, domina_idiomas $domina_idiomas)
    {
        try
        {
            $dominia_idioma=$domina_idiomas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Dominio del idioma Uid {$dominia_idioma->Uid_Dominio}",
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Registro del Dominio del idioma",
                'Kardex_Uid_Registro'=> $dominia_idioma->Uid_Dominio
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El dominio de idioma del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Dominio del idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function storePU(Request $request, domina_idiomas $domina_idiomas, $Uid_Usuario)
    {
        try
        {
            $dominia_idioma=$domina_idiomas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Dominio del idioma Uid {$dominia_idioma->Uid_Dominio}",
                ]);
            kardex::create([
                'Kardex_Descripcion'=>"Registro del Dominio del idioma",
                'Kardex_Uid_Registro'=> $dominia_idioma->Uid_Dominio
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El dominio de idioma del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Dominio del idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\domina_idiomas  $domina_idiomas
     * @return \Illuminate\Http\Response
     */
    public function show(domina_idiomas $domina_idiomas, $Uid_Usuario)
    {
        $usuarios=usuarios::where('Uid_Usuario',$Uid_Usuario)->first();
        return $domina_idiomas::join('idiomas','domina_idiomas.Uid_Idioma','idiomas.Uid_Idioma')->where('Uid_Empleado',$usuarios->Uid_Empleado)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\domina_idiomas  $domina_idiomas
     * @return \Illuminate\Http\Response
     */
    public function edit(domina_idiomas $domina_idiomas, $uid)
    {
        return $domina_idiomas::join('empleados','empleados.Uid_Empleado','domina_idiomas.Uid_Empleado')
                                ->join('idiomas','idiomas.Uid_Idioma','domina_idiomas.Uid_Idioma')
                                ->select('Idioma_Nombre','domina_idiomas.Uid_Dominio',
                                DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombre")
                                        ,'domina_idiomas.DominaIdioma_PorcentajeEscritura','domina_idiomas.DominaIdioma_PorcentajeConversacion')
                                ->findOrFail($uid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\domina_idiomas  $domina_idiomas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, domina_idiomas $domina_idiomas, $id)
    {
        try{
            $domina_idiomas::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Dominio del idioma",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Dominio del idioma editado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Dominio del idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\domina_idiomas  $domina_idiomas
     * @return \Illuminate\Http\Response
     */
    public function destroy(domina_idiomas $domina_idiomas, $id)
    {
        try{
            $domina_idiomas::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Dominio del idioma eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Dominio del idioma eliminado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Dominio del idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Kardex;
use App\tipo_bonos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TipoBonosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.tipobonos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, tipo_bonos $tipo_bonos)
    {
        try
        {
            $tipo_bono=tipo_bonos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Tipo de Bono",
                'Kardex_Uid_Registro'=> $tipo_bono->Uid_TipoBono
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo de Bono se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Tipo de Bono: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_bonos  $tipo_bonos
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_bonos $tipo_bonos)
    {
        return $tipo_bonos::where('Id_Estatus','!=',0)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_bonos  $tipo_bonos
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_bonos $tipo_bonos, $id)
    {
        return $tipo_bonos::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_bonos  $tipo_bonos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_bonos $tipo_bonos, $id)
    {
        try{
            $tipo_bonos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Tipo de Bono",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo de Bono editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo de Bono: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_bonos  $tipo_bonos
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_bonos $tipo_bonos, $id)
    {
        try{
            $tipo_bonos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Tipo de Bono",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Tipo de Bono eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Tipo de Bono: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

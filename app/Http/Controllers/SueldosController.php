<?php

namespace App\Http\Controllers;

use App\kardex;
use App\sueldos;
use App\empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class SueldosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('Catalogos.Nominas.sueldos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function storePU(Request $request, sueldos $sueldos, $Uid_Usuario)
    {
        try
        {
            $sueldo=$sueldos::create($request->all());
            empleados::find($request->Uid_Empleado)
                    ->update(['Empleado_SalarioMensual'=>$request->Sueldo_Monto]);

            kardex::create([
                'Kardex_Descripcion'=>"Actualización del Sueldo",
                'Kardex_Uid_Registro'=>$sueldo->Uid_Sueldo
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Sueldo del empleado se ha registrado correctamente.",'Uid_Empleado'=>$request->Uid_Usuario));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Sueldo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sueldos  $sueldos
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, sueldos $sueldos)
    {
        return $sueldos=sueldos::Join('empleados','sueldos.Uid_Empleado','empleados.Uid_Empleado')
        ->where('sueldos.Id_Estatus','!=',0)
        ->select(DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombre"),
        'sueldos.*')->orderby('Empleado_Nombre')->get();
        return $sueldos::where('Uid_Empleado','=',$request->Uid_Empleado)
                        ->orderBy('Sueldo_Fecha', 'desc')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sueldos  $sueldos
     * @return \Illuminate\Http\Response
     */
    public function edit(sueldos $sueldos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sueldos  $sueldos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sueldos $sueldos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sueldos  $sueldos
     * @return \Illuminate\Http\Response
     */
    public function destroy(sueldos $sueldos)
    {
        //
    }
}

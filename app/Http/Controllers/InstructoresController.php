<?php

namespace App\Http\Controllers;

use App\kardex;
use App\instructores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class InstructoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructores=instructores::where('Id_Estatus',1)->get();
        return view('Catalogos.Nominas.instructores', compact('instructores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, instructores $instructores)
    {
        try
        {
            $instructor=$instructores::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Instructor",
                'Kardex_Uid_Registro'=>$instructor->Uid_Instructor
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Instructor se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Instructor: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instructores  $instructores
     * @return \Illuminate\Http\Response
     */
    public function show(Instructores $instructores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instructores  $instructores
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return instructores::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instructores  $instructores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, instructores $instructores, $id)
    {
        try{
            $instructores::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Edición del Instructor',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Instructor editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Instructor: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instructores  $instructores
     * @return \Illuminate\Http\Response
     */
    public function destroy(instructores $instructores, $id)
    {
        try{
            $instructores::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Instructor",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Instructor eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Instructor: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
                return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

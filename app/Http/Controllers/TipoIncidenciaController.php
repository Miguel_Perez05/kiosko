<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_incidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TipoIncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo_incidencias= tipo_incidencias::all();

        return view('Catalogos.Nominas.tipoincidencias', compact('tipo_incidencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, tipo_incidencias $tipo_Incidencias)
    {
        try
        {
            $tipo_Incidencia=$tipo_Incidencias::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"El Tipo de Incidencia se ha creado correctamente",
                'Kardex_Uid_Registro'=> $tipo_Incidencia->Id_TipoIncidencia
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo de Incidencia se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Tipo de Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tipo_Incidencia  $tipo_Incidencia
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_incidencias $tipo_Incidencias, $status)
    {
        $tipo_IncidenciasLst= $tipo_Incidencias;
        if($status>0)
            $tipo_IncidenciasLst= $tipo_IncidenciasLst->where('Id_Estatus',$status);
        return $tipo_IncidenciasLst->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipo_Incidencia  $tipo_Incidencia
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_incidencias $tipo_Incidencias,$id)
    {
        return $tipo_Incidencias::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipo_Incidencia  $tipo_Incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_incidencias $tipo_Incidencias, $id)
    {
        try{
            $tipo_Incidencias::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Tipo de Incidencia editada correctamente",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo de Incidencia editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo de Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipo_Incidencia  $tipo_Incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, tipo_incidencias $tipo_Incidencias,$id)
    {
        $statusAcción=$request->Id_Estatus==1?'Activar':'Desactivar';
        $status=$request->Id_Estatus==1?'Activada':'Desactivada';
        try{
            $tipo_Incidencias::where('Id_TipoIncidencia',$id)->update(['Id_Estatus'=>$request->Id_Estatus]);
            kardex::create([
                'Kardex_Descripcion'=>"Tipo de Incidencia $status",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>$statusAcción,'Message'=>"Tipo de Incidencia $status"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al $status el Tipo de Incidencia: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\areas_formacion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AreasFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Catalogos.Nominas.Vacantes.areasformacion');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, areas_formacion $areas_formacion)
    {
        try
        {
            $areasformacion=$areas_formacion::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Area de Formación creado',
                'Kardex_Uid_Registro'=>$areasformacion->Uid_AreaFormacion
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Area de Formación se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Area de Formación: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\areas_formacion  $areas_formacion
     * @return \Illuminate\Http\Response
     */
    public function show(areas_formacion $areas_formacion)
    {
        return $areas_formacion::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\areas_formacion  $areas_formacion
     * @return \Illuminate\Http\Response
     */
    public function edit(areas_formacion $areas_formacion, $id)
    {
        return $areas_formacion::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\areas_formacion  $areas_formacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, areas_formacion $areas_formacion, $id)
    {
        try{
            $areas_formacion::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Area de Formación Editado',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Area de Formación editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Area de Formación: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\areas_formacion  $areas_formacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(areas_formacion $areas_formacion, $id)
    {
        try{
            $areas_formacion::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Area de Formación",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Area de Formación eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Area de Formación: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

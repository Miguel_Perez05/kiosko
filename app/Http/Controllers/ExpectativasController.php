<?php

namespace App\Http\Controllers;

use App\expectativas;
use App\kardex;
use Illuminate\Http\Request;

class ExpectativasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\expectativas  $expectativas
     * @return \Illuminate\Http\Response
     */
    public function show(expectativas $expectativas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\expectativas  $expectativas
     * @return \Illuminate\Http\Response
     */
    public function edit(expectativas $expectativas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\expectativas  $expectativas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, expectativas $expectativas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\expectativas  $expectativas
     * @return \Illuminate\Http\Response
     */
    public function destroy(expectativas $expectativas)
    {
        //
    }
}

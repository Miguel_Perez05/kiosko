<?php

namespace App\Http\Controllers;

use App\bajas;
use App\kardex;
use App\puestos;
use App\usuarios;
use App\contratos;
use App\empleados;
use App\historial;
use Carbon\Carbon;
use App\recontratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BajasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catalogos.Nominas.bajas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, bajas $bajas)
    {
        try
        {
            $baja=$bajas->where('Uid_Empleado',$request->Uid_Empleado)
                        ->where('Id_Estatus',1)->first();
            if($baja)
                return Response::json(array('ResponseStatus'=> array('Message'=>"Existe una baja sin aplicar")), 403);;
            $baja=new bajas();
            
            $baja->Uid_Empleado=$request->Uid_Empleado;            
            $baja->Baja_Causa=$request->Baja_Causa;
            $baja->Baja_Comentario=$request->Baja_Comentario;
            $baja->Id_Estatus=1;
            // return $request->AplicaBaja;
            if($request->AplicaBaja==1)
            {
                
                puestos::where('Uid_Responsable',$request->Uid_Empleado)
                    ->update([
                        'Id_Estatus'=>4,
                        'Uid_Responsable'=>null
                    ]);
            
                $empleado=empleados::where('Uid_Empleado',$request->Uid_Empleado)->first();
                $empleado->update([
                                'Id_Estatus'=>0
                            ]);

                usuarios::where('Uid_Empleado',$request->Uid_Empleado)->update(['Id_Estatus'=>0]);
        
                contratos::where('Uid_Empleado',$request->Uid_Empleado)
                            ->where('Id_Estatus',)
                            ->update([
                                'Id_Estatus'=>0,
                                'Contrato_Fin'=>Carbon::now()
                            ]);

                $historial=historial::create([
                                        'Uid_Empleado'=>$request->Uid_Empleado,
                                        'Uid_Empresa'=>$empleado->Uid_Empresa,
                                        'Historial_Egreso'=>Carbon::now()
                                    ]);
                
                // recontratables::create([
                //                     'Recontratable_Estatus'=>intval($request->Recontratable),
                //                     'Recontratable_Causa'=>$request->Recontratable_Causa
                //                 ]);

                $baja->Baja_Fecha=Carbon::now();
                $baja->Uid_Historial=$historial->Uid_Historial;
                $baja->Id_Estatus=2;
            }

            $baja->save();
            kardex::create([
                        'Kardex_Descripcion'=>"Creación de la Baja",
                        'Kardex_Uid_Registro'=>$baja->Uid_Baja
                    ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Baja se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Baja: {$e->getMessage()}";
            kardex::create([
                        'Kardex_Descripcion'=>$error
                    ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bajas  $bajas
     * @return \Illuminate\Http\Response
     */
    public function show(bajas $bajas)
    {
	    return $bajas::Join('empleados', 'bajas.Uid_Empleado', 'empleados.Uid_Empleado')
                        ->Select('empleados.Empleado_Nombre', 'empleados.Empleado_APaterno',
                        'empleados.Empleado_AMaterno', 'bajas.*')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bajas  $bajas
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, bajas $bajas, $uid_Baja)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bajas  $bajas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bajas $bajas, $uid_Baja)
    {
        // return $request;
        try
        {
            $baja=$bajas->Where('Uid_Baja',$uid_Baja)->first();
                
            puestos::where('Uid_Responsable',$baja->Uid_Empleado)
                ->update([
                    'Id_Estatus'=>4,
                    'Uid_Responsable'=>null
                ]);
        
            $empleado=empleados::where('Uid_Empleado',$baja->Uid_Empleado)->first();
            $empleado->update([
                            'Id_Estatus'=>0
                        ]);

            usuarios::where('Uid_Empleado',$baja->Uid_Empleado)->update(['Id_Estatus'=>0]);
    
            contratos::where('Uid_Empleado',$baja->Uid_Empleado)
                        ->where('Id_Estatus',1)
                        ->update([
                            'Id_Estatus'=>0,
                            'Contrato_Fin'=>Carbon::now()
                        ]);

            $historial=historial::create([
                                    'Uid_Empleado'=>$baja->Uid_Empleado,
                                    'Uid_Empresa'=>$empleado->Uid_Empresa,
                                    'Historial_Egreso'=>Carbon::now()
                                ]);
            
            // recontratables::create([
            //                     'Recontratable_Estatus'=>intval($request->Recontratable_Estatus),
            //                     'Recontratable_Causa'=>$request->Recontratable_Causa
            //                 ]);

            $baja->Baja_Fecha=Carbon::now();
            $baja->Uid_Historial=$historial->Uid_Historial;
            $baja->Id_Estatus=2;

            $baja->save();
            kardex::create([
                        'Kardex_Descripcion'=>"Aplicación de Baja",
                        'Kardex_Uid_Registro'=>$baja->Uid_Baja
                    ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Aplicación','Message'=>"La Baja se ha aplicado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al aplicar la Baja: {$e->getMessage()}";
            kardex::create([
                        'Kardex_Descripcion'=>$error
                    ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bajas  $bajas
     * @return \Illuminate\Http\Response
     */
    public function destroy(bajas $bajas)
    {

    }
}

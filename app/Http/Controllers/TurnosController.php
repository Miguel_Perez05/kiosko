<?php

namespace App\Http\Controllers;

use App\kardex;
use App\turnos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TurnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Catalogos.Nominas.turnos');
    }

    /**
     * Show the form for creating a new resource.
     *  
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, turnos $turnos)
    {
        try
        {
            $turnos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Turno",
                'Kardex_Uid_Registro'=> $turnos->Uid_Turno
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Turno se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Turno: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\turnos  $turnos
     * @return \Illuminate\Http\Response
     */
    public function show(turnos $turnos,$status)
    {
        $turnosList=$turnos::select('Uid_Turno','Turno_Nombre','Id_Estatus',
                                    DB::raw('Time_Format(Turno_Hora_Fin,"%H:%i") AS Turno_Hora_Fin'),
                                    DB::raw('Time_Format(Turno_Hora_Inicio,"%H:%i") AS Turno_Hora_Inicio'));
        if($status>0)
            $turnosList=$turnosList->where('Id_Estatus',$status);
        return $turnosList->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\turnos  $turnos
     * @return \Illuminate\Http\Response
     */
    public function edit(turnos $turnos,$id)
    {
        return $turnos::select('Uid_Turno','Turno_Nombre','Id_Estatus',
                        DB::raw('Time_Format(Turno_Hora_Fin,"%H:%i") AS Turno_Hora_Fin'),
                        DB::raw('Time_Format(Turno_Hora_Inicio,"%H:%i") AS Turno_Hora_Inicio'))
                        ->where('Uid_Turno','=',$id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\turnos  $turnos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, turnos $turnos, $id)
    {
        try{

            $turnos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Turno",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Turno editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Turno: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\turnos  $turnos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, turnos $turnos, $id)
    {
        $statusAccion=$request->Id_Estatus==1?'Activar':'Desactivar';
        $status=$request->Id_Estatus==1?'Activado':'Desactivado';
        try{
            $turnos::where('Uid_Turno',$id)->update(['Id_Estatus'=>$request->Id_Estatus]);
            kardex::create([
                'Kardex_Descripcion'=>"Turno $status",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>$statusAccion,'Message'=>"Turno $status"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al $statusAccion el Turno: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

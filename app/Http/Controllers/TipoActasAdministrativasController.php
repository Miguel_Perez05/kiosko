<?php

namespace App\Http\Controllers;

use App\tipo_actas_administrativas;
use Illuminate\Http\Request;
use App\kardex;

class TipoActasAdministrativasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipos_actas_administrativas  $tipos_actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_actas_administrativas $tipos_actas_administrativas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipos_actas_administrativas  $tipos_actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_actas_administrativas $tipos_actas_administrativas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipos_actas_administrativas  $tipos_actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_actas_administrativas $tipos_actas_administrativas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipos_actas_administrativas  $tipos_actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_actas_administrativas $tipos_actas_administrativas)
    {
        //
    }
}

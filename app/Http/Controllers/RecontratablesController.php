<?php

namespace App\Http\Controllers;

use App\kardex;
use App\recontratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class RecontratablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, recontratables $recontratables)
    {
        try
        {
            $recontratable=$recontratables::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Estatus Recontratable",
                'Kardex_Uid_Registro'=>$recontratable->Uid_Recontratable
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"Estatus Recontratable se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el estatus Recontratable: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\recontratables  $recontratables
     * @return \Illuminate\Http\Response
     */
    public function show(recontratables $recontratables)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\recontratables  $recontratables
     * @return \Illuminate\Http\Response
     */
    public function edit(recontratables $recontratables, $id)
    {
        return $recontratables::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\recontratables  $recontratables
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, recontratables $recontratables, $id)
    {
        try{
            $recontratables::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Estatus Recontratable",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Estatus Recontratable editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el estatus Recontratable: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\recontratables  $recontratables
     * @return \Illuminate\Http\Response
     */
    public function destroy(recontratables $recontratables)
    {
        //
    }
}

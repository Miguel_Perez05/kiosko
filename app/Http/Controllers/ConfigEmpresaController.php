<?php

namespace App\Http\Controllers;

use App\kardex;
use App\Empresas;
use App\Usuarios;
use App\Licencias;
use Webpatser\Uuid\Uuid;
use App\HabilitaInformacion;
use Illuminate\Http\Request;
use App\registros_patronales;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class ConfigEmpresaController extends Controller
{
	public function register(){
    	return view('ConfiguracionEmpresas.register');
    }

    public function vista(){
        return view('ConfiguracionEmpresas.config');
    }

    public function mostrar(){
      $empresas = Empresas::where('Uid_empresa','=',Auth::user()->Uid_empresa)->get();
      return view('ConfiguracionEmpresas.empresas')->with('empresas',$empresas);
    }

    public function almacenar(Request $request, empresas $empresas){
    	$data = $request->all();

    	$consulta = Usuarios::find(Auth::user('Uid_empresa'));

    	if ($data != ['']) {
    		$validacion= \Validator::make(
    			$data,
    				array(  'nombre'=>'required|unique:empresa_jano,Empresa_nombre',
    						'razon_social'=>'required|unique:empresa_jano,Empresa_razon_social',
    						'rfc'=>'required|min:12|max:13|unique:empresa_jano,Empresa_rfc',
    						'domicilio'=>'required',
    						'fiscal'=>'required',
    					),
    				array(  'nombre.required'=>'Ingresa el nombre de la empresa ',
    						'nombre.unique'=>'El nombre que se ingreso ya se encuentra en uso ',
    						'razon_social.required'=>'Ingresa la razon social de la empresa ',
    						'razon_social.unique'=>'La razon social que ingreso ya se encuentra en uso ',
    						'rfc.required'=>'Ingresa el RFC de la empresa ',
    						'rfc.unique'=>'El RFC ya se encuentra en uso ',
    						'rfc.max'=>'El RFC no debe tener una longitud mayor de 13 caracteres ',
    						'rfc.min'=>'El RFC debe tener una longitud minima de 12 caracteres ',
    						'domicilio.required'=>'Ingresa la direccion de la empresa ',
    						'fiscal.required'=>'Ingresa el domicilio fiscal de la empresa '
    					)
    		);

    		if ($validacion->fails()) {
                return Response::json(array('ResponseStatus'=> array('Message'=>$validacion->messages())), 403);
    		}
            $empresa=$empresas->create(['Empresa_nombre'=>$request->nombre,
                                        'Empresa_RFC'=>$request->rfc,
                                        'Empresa_RFC'=>$request->rfc,
                                        'Empresa_domicilio'=>$request->domicilio,
                                        'Empresa_padre'=>0,
                                        'Empresa_razon_social'=>$request->razon_social,
                                        'Empresa_integracion'=>$consulta->Id_empresa,
                                        'Empresa_pagina_web'=>$request->web,
                                        'Empresa_direccion_fiscal'=>$request->fiscal]);
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Empresa",
                'Kardex_Uid_Registro'=>$empresa->Uid_Empresa
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La empresa se ha creado correctamente"));
    	}
    }


    public function editar(Request $request){
        $id=$request->input('id');
        $empresas = new Empresas;
        return $empresas::find($id);
    }


    public function almacena_edicion(Request $request){
        $data=$request->all();
        $empresa = Empresas::find($data['id']);

        if ($data['nombre'] != '') {

            $validacion= \Validator::make(
              $data,
                  array(
                       'nombre'=>'unique:empresas,Empresa_nombre',
                       ),

                  array(
                        'nombre.unique'=>'Ya existe una empresa con ese nombre',
                        )
              );
              if ($validacion->fails()) {
                return response()->json(['status'=>false, 'message'=>$validacion->messages()]);
              }else{

                $empresa->Empresa_nombre = $data['nombre'];
              }
          }

        elseif ($data['r_social'] != '') {

            $validacion= \Validator::make(
              $data,
                  array(
                       'r_social'=>'unique:empresas,Empresa_razon_social',
                       ),

                  array(
                        'r_social.unique'=>'La razon social ya se encuentra en uso',
                        )
              );
              if ($validacion->fails()) {
                return response()->json(['status'=>false, 'message'=>$validacion->messages()]);
              }else{
                $empresa->Empresa_razon_social = $data['r_social'];
              }
          }

        if ($data != ['']) {

            $validacion = \Validator::make(
                $data,
                    array(  'domicilio'=>'required',
                            'dir_fiscal'=>'required',
                            'dir_envio'=>'required',
                            'web'=>'required',
                        ),
                    array(  'domicilio.required'=>'Ingresa la direccion de la empresa',
                            'dir_fiscal.required'=>'Ingresa la direccion fiscal de la empresa',
                            'dir_envio.required'=>'Ingresa la direccion de envio',
                            'web.required'=>'Ingresa la pagina web de la empresa'
                        )
            );

            if($validacion->fails()){
                return response()->json(['status'=>false,'Message'=>$validacion->messages()]);
            }else{

                $empresa->Empresa_domicilio = $data['domicilio'];
                $empresa->Empresa_domicilio_fiscal = $data['dir_fiscal'];
                $empresa->Empresa_pagina_web = $data['web'];
                $empresa->save();

                return response()->json(['status'=>true,'Message'=>'La empresa '.$empresa->Empresa_nombre.' ha sido actualizada con exito']);
            }
        }
    }

    public function destroy(Request $request){
        $data = $request->input('id');
        $empresa = new Empresas();
        $empresa::destroy($data);

        return response()->json(['status'=>true,'Message'=>'La empresa se elimino correctamente']);

    }

    public function registros(){
        $patronales =registros_patronales::from('registros_patronales as r')
        ->leftJoin('empresas as e1','r.Uid_Empresa','e1.Uid_Empresa')
        ->leftJoin('empresas as e2','r.Uid_Empresa','e1.UUid_Empresa')
        ->select('r.RegistroPatronal_Nombre', 'e.Empresa_nombre')
        ->where('e1.Uid_Empresa',Auth::user()->Uid_empresa);
        return view('ConfiguracionEmpresas.patronal')->with('patronales',$patronales);
    }


    public function licencia(){
        $informacion = Licencias::where('Uid_empresa','=',Auth::user()->Uid_empresa)->get();
        return view('ConfiguracionEmpresas.licencia')->with('informacion',$informacion);
    }

}

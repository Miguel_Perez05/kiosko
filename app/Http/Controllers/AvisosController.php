<?php

namespace App\Http\Controllers;

use App\avisos;
use App\kardex;
use App\empleados;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AvisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Avisos.listado');
    }

    public function avisos()
    {
        return view('kiosko.avisos');
    }

    public function view(avisos $avisos)
    {
        $now = Carbon::now()->format('Y-m-d');
        $empleado=empleados::where('Uid_Empleado','=', Auth::user()->Uid_Empleado)->first();

        return $avisos::from('avisos as a')
                ->leftjoin('empleados as e', 'a.Uid_Empleado_Receptor', '=','e.Uid_Empleado','a.Uid_Empleado_Receptor', '=',$empleado->Uid_Empleado)//-> where('a.Uid_Empleado_Receptor', '=',$empleado->Uid_Empleado)
                ->leftjoin('grupos as g', 'a.Uid_Grupo', '=', 'g.Uid_Grupo','a.Uid_Grupo', '=',$empleado->Uid_Grupo)//-> where('a.Uid_Grupo', '=',$empleado->Uid_Grupo)
                ->leftjoin('departamentos as d', 'a.Uid_Departamento', '=', 'd.Uid_Departamento','a.Uid_Departamento', '=',$empleado->Uid_Departamento)//-> where('a.Uid_Departamento', '=',$empleado->Uid_Departamento)
                ->leftjoin('turnos as t', 'a.Uid_Turno', '=', 't.Uid_Turno','a.Uid_Turno', '=',$empleado->Uid_Turno)//-> where()
                ->select('Aviso_Titulo', 'Aviso_Asunto', 'Aviso_Asunto', 'Aviso_Vigencia',
                DB::raw('(case when a.Aviso_Tipo is not null then a.Aviso_Tipo when a.Uid_Empleado_Receptor is not null then "Personal" when Concat("Grupo ",a.Uid_Grupo) is not null then g.Grupo_Nombre when a.Uid_Departamento is not null then Concat("Departamento ",d.Departamento_Nombre) when a.Uid_Turno is not null then Concat("Turno ",t.Turno_Nombre) end) as receptor'))
                ->where('Aviso_Vigencia', '>=', $now)
                ->orderBy('receptor','asc','aviso_Vigencia')
                ->get();
    }

    public function create()
    {
        return view('Catalogos.Avisos.form');
    }

    public function form()
    {
        return view('Catalogos.Avisos.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, avisos $avisos)
    {
        try
        {
            $avisos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Aviso",
                'Kardex_Uid_Registro'=>$avisos->Uid_Aviso
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Aviso se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Aviso: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\avisos  $avisos
     * @return \Illuminate\Http\Response
     */
    public function show(avisos $avisos)
    {
        return $avisos::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\avisos  $avisos
     * @return \Illuminate\Http\Response
     */
    public function edit(avisos $avisos,$uid)
    {
        return $avisos::where('Uid_Aviso',$uid)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\avisos  $avisos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, avisos $avisos, $id)
    {
        try{
            $avisos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Aviso Uid",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Aviso editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Aviso: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\avisos  $avisos
     * @return \Illuminate\Http\Response
     */
    public function destroy(avisos $avisos, $id)
    {
        try{
            $avisos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Registro Patronal",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Registro Patronal eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Registro Patronal: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

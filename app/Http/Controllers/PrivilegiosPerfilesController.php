<?php

namespace App\Http\Controllers;

use App\privilegios_perfiles;
use Illuminate\Http\Request;
use App\kardex;

class PrivilegiosPerfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            privilegios_perfiles::create($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Privilegio del Perfil se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            return response()->json(array('status'=>"warning", 'Accion'=>'Creación','Message'=>"Hubo un problema al registrar Privilegio del Perfil: {$e->getMessage()}"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\privilegios_perfiles  $privilegios_perfiles
     * @return \Illuminate\Http\Response
     */
    public function show(privilegios_perfiles $privilegios_perfiles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\privilegios_perfiles  $privilegios_perfiles
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return privilegios_perfiles::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\privilegios_perfiles  $privilegios_perfiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            privilegios_perfiles::find($id)->update($request->all());
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Privilegio del Perfil editado correctamente"));
        }
        catch(\Exception $e){
            return response()->json(array('status'=>"warning", 'Accion'=>'Edición','Message'=>"Hubo un problema al editar el Privilegio del Perfil: {$e->getMessage()}"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\privilegios_perfiles  $privilegios_perfiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(privilegios_perfiles $privilegios_perfiles)
    {
        //
    }
}

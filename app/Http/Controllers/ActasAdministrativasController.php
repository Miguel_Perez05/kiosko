<?php

namespace App\Http\Controllers;

use App\actas_administrativas;
use App\avisos;
use App\empleados;
use App\usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActasAdministrativasController extends Controller
{
    public function avisos()
  {
      return view('contacto');
  }

  public function buzonrec()
  {
    $avisos=new avisos();
    $avisos=$avisos->getListaAvisosRE();
    return view('listadobuzon')->with('avisos',$avisos);
  }

  public function buzonenv()
  {
    $avisos=new avisos();
    $avisos=$avisos->getListaAvisosEN();
    return view('listadobuzon')->with('avisos',$avisos);
  }

  public function mensaje_respuesta($id)
  {
    $aviso=avisos::find($id);
    $aviso->Avisos_visto=1;
    $aviso->save();
    $avisos=new avisos();
    $avisos=$avisos->getListaAvisosSM($id,'');
    return view('contestacion')->with('avisos',$avisos);
  }

  public function lista_empleados()
  {
    $empleados = new empleados();
    $empleados=$empleados->getEmpleados();
    return view('listaempleados')->with('empleados',$empleados);
  }

  public function muestra_avisos_sm(avisos $avisos,$tipo,$vista)
  {
    $avisoslst=$avisos->getListaAvisosSM('',$vista);
    if($tipo==1)
    {
      return view('buzon')->with('avisos',$avisoslst);
    }
    if($tipo==2)
    {
      return view('listabuzon')->with('avisos',$avisoslst);
    }
  }

  public function buzon()
  {
    return view('listabuzon');
  }

  public function muestra_avisos_detalle(avisos $avisos, $id)
  {
    $avisos::find($id)->update(['Avisos_visto'=>1]);
    $avisoslst=$avisos->getListaAvisosSM($id,'');
    return view('detallebuzon')->with('avisos',$avisoslst);
  }

  public function envia_aviso(Request $request,avisos $avisos)
  {
    $data= $request->all();
    if ($data != "[]")
    {
      $avisos->create(['Uid_Empleado_Emisor'=>Auth::user()->Id_empleado,
                      'Uid_Empleado_Receptor'=>$data['Id_remite'],
                      'Avisos_titulo'=>$data['titulo'],
                      'Avisos_asunto'=>$data['asunto'],
                      'Aviso_Vigencia'=>$data['fechacaducidad']]);
      return response()->json(array('status'=>true, 'message'=>"Se ha enviado existosamente tu mensaje",'codigo'=>202));
    }
  }

  public function tipo_usuario(Request $request,avisos $avisos)
  {
    $data=$request->all();
    $users= usuarios::where('Usuarios_rol','=',$request['t_empleado'])->get();
    foreach($users as $user)
    {
      $avisos->create(['Uid_Empleado_Emisor'=>Auth::user()->Id_empleado,
                      'Uid_Empleado_Receptor'=>$user['Id_empleado'],
                      'Avisos_titulo'=>$data['titulo'],
                      'Avisos_asunto'=>$data['asunto']]);
    }
    return response()->json(array('status'=>true, 'message'=>"Se ha enviado existosamente tu mensaje",'codigo'=>202));
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\actas_administrativas  $actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function show(actas_administrativas $actas_administrativas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\actas_administrativas  $actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function edit(actas_administrativas $actas_administrativas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\actas_administrativas  $actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, actas_administrativas $actas_administrativas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\actas_administrativas  $actas_administrativas
     * @return \Illuminate\Http\Response
     */
    public function destroy(actas_administrativas $actas_administrativas)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    // use AuthenticatesUsers;

    public function showLoginForm() {
        $date = Carbon::now()->locale('es');
        $fecha = \Carbon\Carbon::parse($date);
        $mes = $fecha->formatLocalized('%B');
        $mes='/img/login/'.$mes.'.jpg';
        return view('login',compact('mes'));
    }

    public function login(){
        if (Auth::user()) {
            return Redirect::to('/');
        }
        $date = Carbon::now()->locale('es');
        $fecha = \Carbon\Carbon::parse($date);
        $mes = $fecha->formatLocalized('%B');
        $mes='/img/login/'.$mes.'.jpg';
        return view('login',compact('mes'));
    }

    public function login_in(Request $request)
    {
        $user= DB::table('usuarios')
        ->where('Usuario_NickName', $request->Usuario_NickName)
        ->first();
        if($user)
        {
            if((bool)password_verify($request->input('Usuario_Password'), $user->Usuario_Password) )
            {
                if($user->Id_Estatus!=1)
                    return back()->with('warningmsj','El usuario esta inhabilitado');
                if($user->Usuario_Nuevo==1)
                {
                    if (Auth::loginUsingId($user->Uid_Usuario))
                    {

                        $date = Carbon::now()->locale('es');
                        $fecha = \Carbon\Carbon::parse($date);
                        $mes = $fecha->formatLocalized('%B');
                        $mes='/img/login/'.$mes.'.jpg';
                        return view('passwordnuevo',compact('mes'));
                    }
                }
                if (Auth::loginUsingId($user->Uid_Usuario))
                {
                    $notification = array(
                        'message' => 'Bienvenido '. $request->input('Usuario_NickName'),
                        'alert-type' => 'success'
                    );
                    return redirect('/')->with($notification);
                }
            }
        }
        return back()->with('warningmsj','Credenciales incorrectas');
    }

    public function logout() {
        Auth::logout();
        $date = Carbon::now()->locale('es');
        $fecha = \Carbon\Carbon::parse($date);
        $mes = $fecha->formatLocalized('%B');
        $mes='/img/login/'.$mes.'.jpg';
        return redirect('/login');
        // return view('login',compact('mes'));
    }

    public function forgot(){
        return view('reset');
    }
}

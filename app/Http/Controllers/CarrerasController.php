<?php

namespace App\Http\Controllers;

use App\kardex;
use App\carreras;
use App\areas_formacion;
use App\niveles_educativos;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarrerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.Vacantes.carreras');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, carreras $carreras)
    {
        try
        {
            $carrera=$carreras::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Carrera creada',
                'Kardex_Uid_Registro'=>$carrera->Uid_Carrera
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Carrera se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Carrera: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\carreras  $carreras
     * @return \Illuminate\Http\Response
     */
    public function show(carreras $carreras)
    {
        return $carreras::Join('areas_formacion','carreras.Uid_AreaFormacion','areas_formacion.Uid_AreaFormacion')
                        ->Join('niveles_educativos','carreras.Uid_NivelEducativo','niveles_educativos.Uid_NivelEducativo')
                        ->where('carreras.Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\carreras  $carreras
     * @return \Illuminate\Http\Response
     */
    public function edit(carreras $carreras, $id)
    {
        return $carreras::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carreras  $carreras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, carreras $carreras, $id)
    {
        try{
            $carreras::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Carrera Editada',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Carrera editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Carrera: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\carreras  $carreras
     * @return \Illuminate\Http\Response
     */
    public function destroy(carreras $carreras, $id)
    {
        try{
            $carreras::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Eliminación de la Carrera",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Carrera eliminada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la Carrera: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

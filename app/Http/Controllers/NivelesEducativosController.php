<?php

namespace App\Http\Controllers;

use App\kardex;
use App\niveles_educativos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class NivelesEducativosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.Vacantes.niveleseducativos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, niveles_educativos $niveles_educativos)
    {
        try
        {
            $niveleducativo=$niveles_educativos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Nivel Educativo creado',
                'Kardex_Uid_Registro'=>$niveleducativo->Uid_NivelEducativo
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Nivel Educativo se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Nivel Educativo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\niveles_educativos  $niveles_educativos
     * @return \Illuminate\Http\Response
     */
    public function show(niveles_educativos $niveles_educativos)
    {
        return $niveles_educativos::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\niveles_educativos  $niveles_educativos
     * @return \Illuminate\Http\Response
     */
    public function edit(niveles_educativos $niveles_educativos, $id)
    {
        return niveles_educativos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\niveles_educativos  $niveles_educativos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, niveles_educativos $niveles_educativos, $id)
    {
        try{
            $niveles_educativos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Nivel Educativo Editado',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Nivel Educativo editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Nivel Educativo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\niveles_educativos  $niveles_educativos
     * @return \Illuminate\Http\Response
     */
    public function destroy(niveles_educativos $niveles_educativos, $id)
    {
        try{
            $niveles_educativos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Nivel Educativo",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Nivel Educativo eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Nivel Educativo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmpleadosExport;
use App\Exports\DepartamentosExport;
use App\Exports\PuestosExport;
use App\Exports\SolicitudesExport;

class ReportesController extends Controller
{
    public function empleados()
    {
        return Excel::download(new EmpleadosExport, 'empleados.xlsx');
    }

    public function puestos()
    {
        return Excel::download(new PuestosExport, 'puestos.xlsx');
    }

    public function solicitudes()
    {
        return Excel::download(new SolicitudesExport, 'solicitudes.xlsx');
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\escuelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class EscuelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escuelas=escuelas::where('Id_Estatus',1)->get();
        return view('Catalogos.escuelas', compact('escuelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,escuelas $escuelas)
    {
        try
        {
            $escuela=$escuelas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Escuela",
                'Kardex_Uid_Registro'=>$escuela->Uid_Escuela
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Escuela se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Escuela: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function show(escuelas $escuelas)
    {
        return $escuelas::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function edit(escuelas $escuelas,$id)
    {
        return $escuelas::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, escuelas $escuelas, $id)
    {
        try{
            $escuelas::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de la Escuela",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Escuela editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Escuela: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\escuelas  $escuelas
     * @return \Illuminate\Http\Response
     */
    public function destroy(escuelas $escuelas, $id)
    {
        try{
            $escuelas::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Escuela eliminada",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Escuela eliminada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la Escuela: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

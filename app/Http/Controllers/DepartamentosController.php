<?php

namespace App\Http\Controllers;

use App\kardex;
use App\empleados;
use App\departamentos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.departamentos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, departamentos $departamentos)
    {
        try
        {
            $departamento=$departamentos::create($request->all());

            kardex::create([
                'Kardex_Descripcion'=>"Registro del Departamento",
                'Kardex_Uid_Registro'=> $departamento->Uid_Departamento
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Departamento se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Departamento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\departamentos  $departamentos
     * @return \Illuminate\Http\Response
     */
    public function show(departamentos $departamentos, $id)
    {
        return $departamentos::from('departamentos as d')
                ->leftJoin('empleados as e', 'd.Departamento_Jefe', 'e.Uid_Empleado')
                ->where('d.Id_Estatus',1)
                ->select('d.Uid_Departamento', 'd.Departamento_Nombre',
                DB::raw("CONCAT(e.Empleado_Nombre,' ', e.Empleado_APaterno,' ', e.Empleado_AMaterno) AS Encargado"))->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\departamentos  $departamentos
     * @return \Illuminate\Http\Response
     */
    public function edit(departamentos $departamentos,$id)
    {
        return $departamentos::from('departamentos as d')
            ->leftJoin('empleados as e', 'd.Departamento_Jefe', 'e.Uid_Empleado')
            ->where('d.Id_Estatus',1)
            ->where('d.Uid_departamento',$id)
            ->Select('d.Departamento_Nombre','d.Departamento_Descripcion','d.Uid_Departamento',
            DB::raw("CONCAT(e.Empleado_Nombre,' ', e.Empleado_APaterno,' ', e.Empleado_AMaterno) AS Encargado"))
            ->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\departamentos  $departamentos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, departamentos $departamentos, $id)
    {
        try{
            $departamentos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Departamento",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Departamento editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Departamento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\departamentos  $departamentos
     * @return \Illuminate\Http\Response
     */
    public function destroy(departamentos $departamentos, $id)
    {
        try{
            $departamentos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Departamento",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Revocación','Message'=>"Departamento eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Departamento: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\paises;
use App\estados;
use App\ciudades;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CiudadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.ciudades');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ciudades $ciudades)
    {
        try
        {
            $ciudad=$ciudades::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Ciudad",
                'Kardex_Uid_Registro'=>$ciudad->Id_Ciudad
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Ciudad se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la ciudad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ciudades $ciudades, $id)
    {
        return $ciudades::where('ciudades.Id_Estatus',1)
                            ->Join('estados','ciudades.Id_Estado','=','estados.Id_Estado')
                            ->Join('paises','ciudades.Id_Pais','=','paises.Id_Pais')
                            ->where('estados.Id_Pais','=',$request->Id_Pais)
                            ->where('estados.Id_Estado','=',$request->Id_Estado)
                            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function edit(ciudades $ciudades, $id)
    {
        return $ciudades::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ciudades $ciudades, $id)
    {
        try{
            $ciudades::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de la Ciudad",
                'Kardex_Uid_Registro'=>$id

                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Ciudad editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la ciudad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function destroy(ciudades $ciudades, $id)
    {
        try{
            $ciudades::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Eliminación de la Ciudad",
                'Kardex_Uid_Registro'=>$id

                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Ciudad eliminada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la ciudad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_incidencias;
use Illuminate\Http\Request;
use App\percepciones_deducciones;
use App\usuarios;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PercepcionesDeduccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bonos=percepciones_deducciones::Join('empleados','percepciones_deducciones.Uid_Empleado','empleados.Uid_Empleado')
        ->where('Id_TipoIncidencia',5)
        ->where('percepciones_deducciones.Id_Estatus','!=',0)
        ->select(DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombre"),
        'percepciones_deducciones.*')->get();
        return view('Catalogos.Nominas.bonos', compact('bonos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, percepciones_deducciones $percepciones_deducciones)
    {
        try
        {
            $tipo=tipo_incidencias::where('Id_TipoIncidencia',$request->Id_TipoIncidencia)->first();
            $percepcionesdeducciones=$percepciones_deducciones::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"{$tipo->TipoIncidencia_Nombre} Creado",
                'Kardex_Uid_Registro'=>$percepcionesdeducciones->Uid_PercepcionDeduccion
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La {$tipo->TipoIncidencia_Nombre} del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la {$tipo->TipoIncidencia_Nombre}: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function storePU(Request $request, percepciones_deducciones $percepciones_deducciones, $Uid_Usuario)
    {
        try
        {
            $tipo=tipo_incidencias::where('Id_TipoIncidencia',$request->Id_TipoIncidencia)->first();
            $percepcionesdeducciones=$percepciones_deducciones::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"{$tipo->TipoIncidencia_Nombre} Creado",
                'Kardex_Uid_Registro'=>$percepcionesdeducciones->Uid_PercepcionDeduccion
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La {$tipo->TipoIncidencia_Nombre} del empleado se ha registrado correctamente."));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la {$tipo->TipoIncidencia_Nombre}: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\percepciones_deducciones  $percepciones_deducciones
     * @return \Illuminate\Http\Response
     */
    public function show(percepciones_deducciones $percepciones_deducciones,$Uid_Usuario)
    {
        $usuarios=usuarios::where('Uid_Usuario',$Uid_Usuario)->first();
        return $percepciones_deducciones::Leftjoin('tipo_bonos','percepciones_deducciones.Uid_TipoBono','tipo_bonos.Uid_TipoBono')
                                        ->where('percepciones_deducciones.Id_Estatus',1)
                                        ->where('Id_TipoIncidencia',5)->where('Uid_Empleado',$usuarios->Uid_Empleado)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\percepciones_deducciones  $percepciones_deducciones
     * @return \Illuminate\Http\Response
     */
    public function edit(percepciones_deducciones $percepciones_deducciones, $Uid)
    {
        return $percepciones_deducciones::findOrFail($Uid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\percepciones_deducciones  $percepciones_deducciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, percepciones_deducciones $percepciones_deducciones, $id)
    {
        try
        {
            $tipo=tipo_incidencias::where('Id_TipoIncidencia',$request->Id_TipoIncidencia)->first();
            $percepciones_deducciones::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"{$tipo->TipoIncidencia_Nombre} Editado",
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"La {$tipo->TipoIncidencia_Nombre} del empleado se ha editado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la {$tipo->TipoIncidencia_Nombre}: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\percepciones_deducciones  $percepciones_deducciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, percepciones_deducciones $percepciones_deducciones,$id)
    {
        try{

            $tipo=tipo_incidencias::find($request->Id_TipoIncidencia);
            $percepciones_deducciones::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"{$tipo->TipoIncidencia_Nombre} Eliminado",
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"La {$tipo->TipoIncidencia_Nombre} del empleado se ha eliminado correctamente."));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la {$tipo->TipoIncidencia_Nombre}: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\paises;
use App\estados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class EstadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.estados');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, estados $estados)
    {
        try
        {
            $estado=$estados::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Estado",
                'Kardex_Uid_Registro'=>$estado->Id_Estado
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Estado se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Estado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function show(estados $estados, $id)
    {
        return $estados::Join('paises','estados.Id_Pais','=','paises.Id_Pais')->where('estados.Id_Pais','=',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function edit(estados $estados, $id)
    {
        return $estados::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estados $estados, $id)
    {
        try{
            $estados::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Estado",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Estado editado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al editar el Estado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function destroy(estados $estados, $id)
    {
        try{
            $estados::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Eliminación del Estado",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Estado eliminado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al eliminar el Estado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

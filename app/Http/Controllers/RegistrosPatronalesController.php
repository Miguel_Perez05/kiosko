<?php

namespace App\Http\Controllers;

use App\kardex;
use App\empresas;
use Illuminate\Http\Request;
use App\registros_patronales;
use Illuminate\Support\Facades\Response;

class RegistrosPatronalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.registrospatronales');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, registros_patronales $registros_patronales)
    {
        try
        {
            $registro_patronal=$registros_patronales::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Registro Patronal",
                'Kardex_Uid_Registro'=> $registro_patronal->Uid_RegistroPatronal
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Registro Patronal se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Registro Patronal: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\registros_patronales  $registros_patronales
     * @return \Illuminate\Http\Response
     */
    public function show(registros_patronales $registros_patronales,$estatus)
    {
        $registrospatronalesLst=$registros_patronales::Join('empresas','registros_patronales.Uid_Empresa','=','empresas.Uid_Empresa')
                                                    ->select('RegistroPatronal_Nombre', 'Empresa_Nombre','Uid_RegistroPatronal','registros_patronales.Id_Estatus');
        if($estatus>0)                            
            $registrospatronalesLst=$registrospatronalesLst->where('registros_patronales.Id_Estatus',1);
        return $registrospatronalesLst->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\registros_patronales  $registros_patronales
     * @return \Illuminate\Http\Response
     */
    public function edit(registros_patronales $registros_patronales, $id)
    {
        return $registros_patronales::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\registros_patronales  $registros_patronales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, registros_patronales $registros_patronales, $id)
    {
        try{
            $registros_patronales::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Registro Patronal",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Pais editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Registro Patronal: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\registros_patronales  $registros_patronales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,registros_patronales $registros_patronales, $id)
    {
        try{
            $registros_patronales::find($id)->update(['Id_Estatus'=>$request->status]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Registro Patronal",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Registro Patronal eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Registro Patronal: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

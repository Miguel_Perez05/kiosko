<?php

namespace App\Http\Controllers;

use App\kardex;
use App\puestos;
use App\sueldos;
use App\usuarios;
use Illuminate\Http\Request;
use App\promociones_internas;
use Illuminate\Support\Facades\Response;

class PromocionesinternasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, promociones_internas $promociones_internas, $Uid_Puesto)
    {
        try
        {
            $promocion_interna=$promociones_internas::create([
                                                        'Uid_Empleado'=>$request->Uid_Empleado,
                                                        'Uid_Puesto'=>$request->Uid_Puesto,
                                                        'PromocionInterna_Fecha'=>$request->PromocionInterna_Fecha
                                                    ]);

            puestos::where('Uid_Responsable',$request->Uid_Empleado)
                    ->update([
                        'Id_Estatus'=>4,
                        'Uid_Responsable'=>null
                    ]);

            puestos::find($Uid_Puesto)
                    ->update([
                        'Uid_Responsable'=>$request->Uid_Empleado,
                        'Id_Estatus'=>1,
                    ]);

            sueldos::create([
                        'Uid_Empleado'=>$request->Uid_Empleado,
                        'Sueldo_Fecha'=>$request->PromocionInterna_Fecha,
                        'Sueldo_Monto'=>$request->Monto
                    ]);

            kardex::create([
                        'Kardex_Descripcion'=>'Empleado Promovido',
                        'Kardex_Uid_Registro'=>$promocion_interna->Uid_PromocionInterna
                    ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El empleado ha sido Promovido correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al Promover al empleado: {$e->getMessage()}";
            kardex::create([
                        'Kardex_Descripcion'=>$error,
                    ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, promociones_internas $promocionesinternas, $uid)
    {
        $empleado=usuarios::where('Uid_Usuario','=',$uid)->first();
        return $promocionesinternas::Join('puestos','promociones_internas.Uid_puesto','puestos.Uid_puesto')
                                    ->where('Uid_Empleado','=',$empleado->Uid_Empleado)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return \Illuminate\Http\Response
     */
    public function edit(promociones_internas $promocionesinternas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, promociones_internas $promocionesinternas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return \Illuminate\Http\Response
     */
    public function destroy(promociones_internas $promocionesinternas)
    {
        //
    }
}

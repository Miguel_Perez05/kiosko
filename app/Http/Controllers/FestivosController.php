<?php

namespace App\Http\Controllers;

use App\kardex;
use App\festivos;
use App\empleados;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use LaravelFullCalendar\Facades\Calendar;

class FestivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $festivos=festivos::FestivosEjercicio()->where('Id_Estatus',1)->orderBy('Festivo_Dia','desc')->get();
        return view('Catalogos.Nominas.festivos', compact('festivos'));
    }

    public function festivos(festivos $festivos){
        $events = [];
        $festivosList= $festivos::select('Festivo_Nombre',
        DB::raw('concat(DATE_FORMAT(Festivo_Dia, "%d-%m-"),Year(curdate())) as Festivo_Dia'))
        ->where('Id_Estatus',1)
                        ->orderBy('Festivo_Dia','desc')->get();
        $collection=collect($festivosList->where('Festivo_Ciclico',1));

        foreach ($festivosList->where('Festivo_Ciclico','<',1) as $valor)
        {
            $year=Carbon::create($valor->Festivo_Dia)->year;
            $month=Carbon::create($valor->Festivo_Dia)->month;
            $day=Carbon::create($valor->Festivo_Dia)->day;
            $yearToday=Carbon::now()->year;
            if($year==$yearToday)
            {
                $valor->Festivo_Dia="$year-$month-$day";
                $collection->push($valor);
            }
        }
        $data=$festivosList;

        $empleados = empleados::where('Id_Estatus','=',2)
        ->select('Empleado_Nombre',
        DB::raw('concat(DATE_FORMAT(Empleado_Fecha_Nacimiento, "%d-%m-"),Year(curdate())) as Empleado_Fecha_Nacimiento'))
        ->get();
        foreach ($empleados as $key => $value) {
            $events[] = Calendar::event(
                $value->Empleado_Nombre,
                true,
                new \DateTime($value->Empleado_Fecha_Nacimiento),
                new \DateTime($value->Empleado_Fecha_Nacimiento),
                null,
                [
                    'backgroundColor' => 'Green',
                    'textColor'=>'White'
                ]
            );
        }
        foreach ($data as $key => $value) {
            $events[] = Calendar::event(
                $value->Festivo_Nombre,
                true,
                new \DateTime($value->Festivo_Dia),
                new \DateTime($value->Festivo_Dia),
                null,
                [
                    'backgroundColor' => '#CD5C5C',
                    'textColor'=>'White'
                ]
            );
        }
        $calendar = Calendar::addEvents($events)->setOptions(['firstDay' => 7, 'lang'=> 'es']);

        $TituloCalendar='';

        return view('kiosko.fullcalendar', compact('calendar','TituloCalendar'));
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $festivo=festivos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Dia Festivo",
                'Kardex_Uid_Registro'=>$festivo->Uid_Festivo
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El dia festivo se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Dia Festivo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Festivos  $festivos
     * @return \Illuminate\Http\Response
     */
    public function show(Festivos $festivos)
    {
        $festivosList= $festivos::where('Id_Estatus',1)
                        ->orderBy('Festivo_Dia','desc')->get();
        $collection=collect((object)$festivosList->where('Festivo_Ciclico',1));

        foreach ($festivosList->where('Festivo_Ciclico','<',1) as $valor)
        {
            $year=Carbon::create($valor->Festivo_Dia)->year;
            $yearToday=Carbon::now()->year;
            if($year==$yearToday)
            {
                $collection->prepend((object)$valor);
            }
        }
        return $collection->all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Festivos  $festivos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return festivos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Festivos  $festivos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            festivos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Dia Festivo",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Dia festivo editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Dia Festivo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Festivos  $festivos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            festivos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Dia Festivo eliminado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Dia Festivo eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Dia Festivo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
                return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

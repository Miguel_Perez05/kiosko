<?php

namespace App\Http\Controllers;

use App\satisfaccion;
use Illuminate\Http\Request;
use App\kardex;

class SatisfaccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Catalogos.Nominas.satisfaccion');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return satisfaccion::join('empleados','satisfaccion.Uid_Empleado','empleados.Uid_Empleado')
        ->join('expectativas','satisfaccion.Uid_Expectativa','expectativas.Uid_Expectativa')
        ->where('satisfaccion.Uid_Empleado','cc88fb90-b8bd-11e9-a0be-51c4189b47ac')
        ->select('satisfaccion.Satisfaccion_Valor','expectativas.Expectativa_Nombre')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\satisfaccion  $satisfaccion
     * @return \Illuminate\Http\Response
     */
    public function show(satisfaccion $satisfaccion)
    {
        return satisfaccion::join('empleados','satisfaccion.Uid_Empleado','empleados.Uid_Empleado')
        ->join('expectativas','satisfaccion.Uid_Expectativa','expectativas.Uid_Expectativa')
        ->where('satisfaccion.Uid_Empleado','cc88fb90-b8bd-11e9-a0be-51c4189b47ac')
        ->select('satisfaccion.Satisfaccion_Valor','expectativas.Expectativa_Nombre')->get();
    }

    public function grafico()
    {

        return satisfaccion::join('empleados','satisfaccion.Uid_Empleado','empleados.Uid_Empleado')
        ->join('expectativas','satisfaccion.Uid_Expectativa','expectativas.Uid_Expectativa')
        ->where('satisfaccion.Uid_Empleado','cc88fb90-b8bd-11e9-a0be-51c4189b47ac')
        ->select('satisfaccion.Satisfaccion_Valor','expectativas.Expectativa_Nombre')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\satisfaccion  $satisfaccion
     * @return \Illuminate\Http\Response
     */
    public function edit(satisfaccion $satisfaccion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\satisfaccion  $satisfaccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, satisfaccion $satisfaccion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\satisfaccion  $satisfaccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(satisfaccion $satisfaccion)
    {
        //
    }
}

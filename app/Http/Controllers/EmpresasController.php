<?php

namespace App\Http\Controllers;

use App\kardex;
use App\empresas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas= empresas::where('Id_Estatus',1)->get();
        return view('Catalogos.empresas', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $empresa=empresas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Empresa",
                'Kardex_Uid_Registro'=>$empresa->Uid_Empresa
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Empresa se ha creada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al crear la Empresa: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function show(empresas $empresas, $id)
    {
        return $empresas::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function edit(empresas $empresas,$id)
    {
        return $empresas::where('Uid_Empresa',$id)
                        ->select('*',DB::raw('(
                            CASE WHEN Empresa_Logo is null
                            THEN "img/sistema/no-logo.jpg"
                            ELSE Empresa_Logo
                            END) AS Logo'))->first();
    }

    /*              Concat("data:image/bmp;base64, ",Empresa_Logo)
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            empresas::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de la Empresa",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Empresa editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Empresa: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\empresas  $empresas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            empresas::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Empresa eliminada",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Empresa eliminada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la Empresa: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>  $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

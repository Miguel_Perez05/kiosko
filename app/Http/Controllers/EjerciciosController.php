<?php

namespace App\Http\Controllers;

use App\ejercicios;
use Illuminate\Http\Request;
use App\kardex;

class EjerciciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ejercicios=ejercicios::all();
        return view('Catalogos.Nominas.ejercicios', compact('ejercicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $ejercicio=ejercicios::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Ejercicio Uid {$ejercicio->Uid_Ejercicio}",
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Ejercicio se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            return response()->json(array('status'=>"warning", 'Accion'=>'Creación','Message'=>"Hubo un problema al registrar Ejercicio: {$e->getMessage()}"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ejercicios  $ejercicios
     * @return \Illuminate\Http\Response
     */
    public function show(ejercicios $ejercicios)
    {
        return ejercicios::findOrFail($ejercicios->Uid_Ejercicio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ejercicios  $ejercicios
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ejercicios::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ejercicios  $ejercicios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            ejercicios::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de Ejercicio Uid {$id}",
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Ejercicio editado correctamente"));
        }
        catch(\Exception $e){
            return response()->json(array('status'=>"warning", 'Accion'=>'Edición','Message'=>"Hubo un problema al editar el Ejercicio: {$e->getMessage()}"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ejercicios  $ejercicios
     * @return \Illuminate\Http\Response
     */
    public function destroy(ejercicios $ejercicios)
    {
        //
    }
}

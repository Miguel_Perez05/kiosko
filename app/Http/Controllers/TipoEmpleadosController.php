<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TipoEmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.tipoempleados');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, tipo_empleados $tipo_empleados)
    {
        try
        {
            $tipo_empleado=$tipo_empleados::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Tipo de Empleado",
                'Kardex_Uid_Registro'=> $tipo_empleado->Uid_TipoEmpleado
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo de Empleado se ha creada correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Tipo de Empleado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_empleados  $tiposEmpleados
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_empleados $tipo_empleados,$status)
    {
        $tipo_empleadosLst= $tipo_empleados;
        if($status>0)
            $tipo_empleadosLst= $tipo_empleadosLst->where('Id_Estatus',$status);
        return $tipo_empleadosLst->get();

        // return $tipo_empleados::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_empleados  $tiposEmpleados
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_empleados $tipo_empleados, $id)
    {
        return $tipo_empleados::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_empleados  $tiposEmpleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_empleados $tipo_empleados, $id)
    {
        try{

            $tipo_empleados::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Tipo de Empleado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo de Empleado editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo de Empleado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_empleados  $tiposEmpleados
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_empleados $tipo_empleados, $id)
    {
        try{
            $tipo_empleados::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Tipo de Empleado",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Tipo de Empleado eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Tipo de Empleado: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

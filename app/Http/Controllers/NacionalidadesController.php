<?php

namespace App\Http\Controllers;

use App\kardex;
use App\paises;
use App\nacionalidades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class NacionalidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nacionalidades=nacionalidades::where('Id_Estatus',1)->get();
        $paises=paises::all();
        return view('Catalogos.nacionalidades', compact('nacionalidades','paises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, nacionalidades $nacionalidades)
    {
        try
        {
            $nacionalidad=$nacionalidades::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Nacionalidad Creada',
                'Kardex_Uid_Registro'=>$nacionalidad->Uid_Nacionalidad
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Nacionalidad se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Nacionalidad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\nacionalidades  $nacionalidades
     * @return \Illuminate\Http\Response
     */
    public function show(nacionalidades $nacionalidades,$status)
    {
        $nacionalidadesLst= $nacionalidades::join('paises','nacionalidades.Id_pais','paises.Id_pais')
                                            ->select('nacionalidades.*','paises.Pais_Nombre');
        if($status>0)
            $nacionalidadesLst= $nacionalidadesLst->where('Id_Estatus',$status);
        return $nacionalidadesLst->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\nacionalidades  $nacionalidades
     * @return \Illuminate\Http\Response
     */
    public function edit(nacionalidades $nacionalidades, $id)
    {
        return $nacionalidades::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nacionalidades  $nacionalidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, nacionalidades $nacionalidades, $id)
    {
        try{
            $nacionalidades::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Nacionalidad Editada',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Nacionalidad editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Nacionalidad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nacionalidades  $nacionalidades
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, nacionalidades $nacionalidades, $id)
    {
        $statusAcción=$request->Id_Estatus==1?'Activar':'Desactivar';
        $status=$request->Id_Estatus==1?'Activada':'Desactivada';
        try{
            $nacionalidades::where('Uid_Nacionalidad',$id)
                            ->update(['Id_Estatus'=>$request->Id_Estatus]);
            kardex::create([
                'Kardex_Descripcion'=>"Nacionalidad $status",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>$statusAcción,'Message'=>"Nacionalidad $status"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al $statusAcción la Nacionalidad: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

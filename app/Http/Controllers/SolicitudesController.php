<?php

namespace App\Http\Controllers;

use App\kardex;
use App\turnos;
use App\sueldos;
use App\usuarios;
use App\contratos;
use App\empleados;
use App\historial;
use Carbon\Carbon;
use App\incidencias;
use App\solicitudes;
use NumerosEnLetras;
use App\nacionalidades;
use App\estados_civiles;
use App\tipo_solicitudes;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class SolicitudesController extends Controller
{
    public function buzon()
    {
        return view('Solicitudes.listadosolicitudes');
    }

    public function nuevasolicitud()
    {
        return view('Solicitudes.nuevasolicitud');
    }

    public function CartaLaboralpdf($Uid_Empleado,$tipo)
    {
        $datosdocumento = empleados::Join('empresas', 'empleados.Uid_Empresa', 'empresas.Uid_Empresa')
                        ->join('registros_patronales','empleados.Uid_RegistroPatronal','registros_patronales.Uid_RegistroPatronal')
                        ->join('departamentos', 'empleados.Uid_Departamento','departamentos.Uid_Departamento')
                        ->join('puestos', 'empleados.Uid_Puesto','puestos.Uid_Puesto')
                        ->Where('empleados.Uid_Empleado',$Uid_Empleado)
                        ->select(DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombres"),
                        'empleados.*','empresas.*','departamentos.*','puestos.*','registros_patronales.*')
                        ->first();


        if(!$datosdocumento)
        {
            return Response::json(array('ResponseStatus'=> array('Message'=>"Falta informacion personal o de registro de la empresa")), 403);
        }
        else
        {
            $nacionalidad=nacionalidades::where('Uid_Nacionalidad',$datosdocumento->Uid_Nacionalidad)->first();
            $estadocivil=estados_civiles::where('Uid_EstadoCivil',$datosdocumento->Uid_EstadoCivil)->first();
            $sueldo=sueldos::where('Uid_Empleado','=',$Uid_Empleado)
                ->orderBy('Sueldo_Fecha', 'desc')->first();
            if($sueldo==null)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"No tiene sueldo asignado")), 403);
            }
            $importeLetra=NumerosEnLetras::convertir($sueldo->Sueldo_Monto,'M.N.',true);
            $turnos=turnos::where('Uid_Turno',$datosdocumento->Uid_Turno)->first();
            $emisor=empleados::where('Uid_Empleado',Auth::user()->Uid_Empleado)
                            ->join('puestos', 'empleados.Uid_Puesto','puestos.Uid_Puesto')
                            ->first();
            $date = Carbon::now()->locale('es_MX');
            $Fecha=$date->isoFormat('MMMM D, YYYY');

            $edad = Carbon::createFromDate($datosdocumento->Empleado_Fecha_Nacimiento)->age;

            $historial=historial::where('Uid_Empleado','=',$Uid_Empleado)
                    ->whereRaw('historial.Historial_Ingreso = (select max(Historial_Ingreso)
                    from historial where historial.Uid_Empleado=Uid_Empleado )')
                    ->orwhereRaw('historial.Historial_Reingreso = (select max(Historial_Reingreso)
                    from historial where historial.Uid_Empleado=Uid_Empleado )')
                    ->orderBy('created_at', 'desc')->first();
            if($nacionalidad==null)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Nacionalidad")), 403);
            }
            elseif(!$estadocivil)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Estado Civil")), 403);
            }
            elseif(!$turnos)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información del turno")), 403);
            }
            elseif(!$emisor)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información del administrador")), 403);
            }
            elseif(!$historial)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información sobre el alta del empleado")), 403);
            }
            else
            {
                $pdf = PDF::loadView('plantillas.'.$tipo, compact('datosdocumento','emisor','Fecha','importeLetra','historial','edad',
                        'nacionalidad','estadocivil','turnos'));

                return $pdf->stream($tipo.'.pdf',array('Attachment'=>false));
            }
        }
    }

    public function InfoTramite($Uid_Empleado,$tipo)
    {
        $datosdocumento = empleados::Where('empleados.Uid_Empleado',$Uid_Empleado)
                                    ->select(DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombres"),
                                    'empleados.*')
                                    ->first();
        if(!$datosdocumento)
        {
            return Response::json(array('ResponseStatus'=> array('Message'=>"Falta informacion personal")), 403);
        }
        else
        {
            $nacionalidad=nacionalidades::where('Uid_Nacionalidad',$datosdocumento->Uid_Nacionalidad)->first();
            $estadocivil=estados_civiles::where('Uid_EstadoCivil',$datosdocumento->Uid_EstadoCivil)->first();

            if($nacionalidad==null)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Nacionalidad")), 403);
            }
            elseif(!$estadocivil)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Estado Civil")), 403);
            }
            else
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Sin falta de información")), 200);
            }
        }
    }

    public function lista_estatus(solicitudes $solicitudes)
    {
        return $solicitudes::from('solicitudes as s')
            ->join('empleados as e','s.Uid_Empleado_Emisor','e.Uid_Empleado')
            ->join('departamentos as d','e.Uid_Departamento','d.Uid_Departamento')
            ->where('d.Departamento_Jefe',Auth::user()->Uid_Empleado)
            ->where('s.Id_Estatus',1)->get();
    }

    public function detalle_solicitud(solicitudes $solicitudes, $Uid)
    {
        $solicitud=$solicitudes::from('solicitudes as s')
            ->join('empleados as e','s.Uid_Empleado_Emisor','e.Uid_Empleado')
            ->where('s.Uid_Solicitud',$Uid)
            ->select((DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Empleado_Nombres")),
            's.*')
            ->first();
        return view('Catalogos.Nominas.detallesolicitud',compact('solicitud'));
    }

    public function comentarios(Request $request, solicitudes $solicitudes)
    {
        $data= $request->all();
        if ($data != "[]")
        {
            $solicitud=$solicitudes::find($data['Id_solicitudes']);
            $solicitud->Solicitud_comentarios_rh=$data['comentario'];
            $solicitud->save();
            return response()->json(array('status'=>true, 'message'=>"La solicitud se ha ".$data['estatus']));
        }
    }

    public function index()
    {
        return view('paneles.solicitudes.panel');
    }

    public function indexTramites()
    {
        return view('paneles.tramites.panel');
    }

    public function solicitudes(solicitudes $solicitudes, $tipo)
    {

        $tipoEmpleado=Auth::user()->Id_Perfil;
        $tipoSolicitudes=tipo_solicitudes::where('TipoSolicitud_Index',$tipo)->first();

        if($tipoEmpleado>1)
        {
            return view('kiosko.solicitudes', compact('tipoSolicitudes'));
        }
        else
        {
            return view('Catalogos.Nominas.solicitudes', compact('tipoSolicitudes'));
        }
    }

    public function ContratoShow(contratos $contratos, $Uid_Contrato)
    {
        $datosdocumento=$contratos->Join('empleados', 'contratos.Uid_Empleado', 'empleados.Uid_Empleado')
                            ->Join('empresas', 'empleados.Uid_Empresa', 'empresas.Uid_Empresa')
                            ->join('registros_patronales','empleados.Uid_RegistroPatronal','registros_patronales.Uid_RegistroPatronal')
                            ->join('departamentos', 'empleados.Uid_Departamento','departamentos.Uid_Departamento')
                            ->join('puestos', 'empleados.Uid_Puesto','puestos.Uid_Puesto')
                            ->Where('contratos.Uid_Contrato',$Uid_Contrato)
                            ->select(DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APaterno,' ',empleados.Empleado_AMaterno) as Empleado_Nombres"),
                            'empresas.*','departamentos.*','puestos.*','registros_patronales.*','contratos.*', 'empleados.Uid_Nacionalidad', 'empleados.Uid_EstadoCivil','empleados.Uid_Turno')
                            ->first();

        if(!$datosdocumento)
        {
            return Response::json(array('ResponseStatus'=> array('Message'=>"Falta informacion personal o de registro de la empresa")), 403);
        }
        else
        {
            $nacionalidad=nacionalidades::where('Uid_Nacionalidad',$datosdocumento->Uid_Nacionalidad)->first();
            $estadocivil=estados_civiles::where('Uid_EstadoCivil',$datosdocumento->Uid_EstadoCivil)->first();
            $sueldo=sueldos::where('Uid_Empleado','=',$datosdocumento->Uid_Empleado)
                ->orderBy('Sueldo_Fecha', 'desc')->first();
            if($sueldo==null)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"No tiene sueldo asignado")), 403);
            }
            $importeLetra=NumerosEnLetras::convertir($sueldo->Sueldo_Monto,'M.N.',true);
            $turnos=turnos::where('Uid_Turno',$datosdocumento->Uid_Turno)->first();
            $emisor=empleados::where('Uid_Empleado',Auth::user()->Uid_Empleado)
                            ->join('puestos', 'empleados.Uid_Puesto','puestos.Uid_Puesto')
                            ->first();
            $date = Carbon::parse($datosdocumento->Contrato_Inicio)->locale('es_MX');
            $Fecha=$date->isoFormat('MMMM D, YYYY');

            $edad = Carbon::createFromDate($datosdocumento->Empleado_Fecha_Nacimiento)->age;

            $historial=historial::where('Uid_Empleado','=',$datosdocumento->Uid_Empleado)
                    ->whereRaw('historial.Historial_Ingreso = (select max(Historial_Ingreso)
                    from historial where historial.Uid_Empleado=Uid_Empleado )')
                    ->orwhereRaw('historial.Historial_Reingreso = (select max(Historial_Reingreso)
                    from historial where historial.Uid_Empleado=Uid_Empleado )')
                    ->orderBy('created_at', 'desc')->first();
            if($nacionalidad==null)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Nacionalidad")), 403);
            }
            elseif(!$estadocivil)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información de Estado Civil")), 403);
            }
            elseif(!$emisor)
            {
                return Response::json(array('ResponseStatus'=> array('Message'=>"Falta información del administrador")), 403);
            }
            else
            {
                $pdf = PDF::loadView('plantillas.contrato', compact('datosdocumento','emisor','Fecha','importeLetra','historial','edad',
                        'nacionalidad','estadocivil','turnos'));

                return $pdf->stream('contrato.pdf',array('Attachment'=>false));
            }
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request, solicitudes $solicitudes)
    {
        $solicitudVerifica=$solicitudes::where('Uid_TipoSolicitud',$request->Uid_TipoSolicitud)
                ->where('Solicitud_Inicio_Auscencia',$request->Solicitud_Inicio_Auscencia)->first();
        if($solicitudVerifica==null)
        {
            try
            {

                $solicitudes=$solicitudes::create($request->all());

                $tiposolicitud=tipo_solicitudes::where('Uid_TipoSolicitud',$solicitudes->Uid_TipoSolicitud)->first();
                $envio=empleados::from('empleados as e')
                                ->join('usuarios as u', 'e.Uid_Empleado','u.Uid_Empleado')
                                ->join('departamentos as d', 'e.Uid_Departamento','d.Uid_Departamento')
                                ->join('empleados as j', 'd.Departamento_Jefe', 'j.Uid_Empleado')
                                ->join('usuarios as uj', 'j.Uid_Empleado','uj.Uid_Empleado')
                                ->join('puestos as p', 'e.Uid_Puesto','p.Uid_Puesto')
                                ->select('u.Usuario_Correo as ECorreo','uj.Usuario_Correo as UCorreo',
                                DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Empleado_Nombre"),
                                'd.Departamento_Nombre','p.Puesto_Nombre')
                                ->where('e.Uid_Empleado',Auth::user()->Uid_Empleado)->first();
                $data=array(
                    'Tipo'=>$tiposolicitud->TipoSolicitud_Nombre,
                    'Empleado_Nombre'=>$envio->Empleado_Nombre,
                    'Departamento_Nombre'=>$envio->Departamento_Nombre,
                    'Puesto_Nombre'=>$envio->Puesto_Nombre,
                    'Motivo'=>$solicitudes->Solicitud_Motivo,
                    'Solicitud_Inicio_Auscencia'=>$solicitudes->Solicitud_Inicio_Auscencia,
                    'Solicitud_Fin_Auscencia'=>$solicitudes->Solicitud_Fin_Auscencia,
                );

                kardex::create([
                    'Kardex_Descripcion'=>"Creación de la Solicitud",
                    'Kardex_Uid_Registro'=>$solicitudes->Uid_Solicitud
                    ]);

                \AuthUser::envioSolicitudes($envio->UCorreo,'',$data,'mails.solicitud','Registro de ' . $tiposolicitud->TipoSolicitud_Nombre);
                return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>'La Solicitud se ha creado correctamente.'));
            }
            catch(\Exception $e)
            {
                $error="Hubo un problema al registrar la Solicitud: {$e->getMessage()}";
                kardex::create([
                    'Kardex_Descripcion'=>$error
                    ]);
                return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
            }

        }
        return Response::json(array('ResponseStatus'=> array('Message'=>'Ya existe una solicitud de este tipo para esta fecha.')), 403);
    }

    public function show(solicitudes $solicitudes, $tipo)
    {

        $tipoEmpleado=Auth::user()->Id_Perfil;

        if($tipoEmpleado>1)
        {
            return $solicitudes::empleados()->where('TipoSolicitud_Index',$tipo)
                                            ->where('solicitudes.Id_Estatus',1)->get();
        }
        else
        {
            return $solicitudes::nominas()->where('TipoSolicitud_Index',$tipo)
                                            ->where('solicitudes.Id_Estatus',1)
                                            ->get();
        }
    }

    public function edit(solicitudes $solicitudes, $id)
    {
        return $solicitudes::findOrFail($id);
    }

    public function update(Request $request, solicitudes $solicitudes)
    {
        //
    }

    public function destroy(solicitudes $solicitudes, $id)
    {
        try{
            $solicitudes::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación de la Solicitud",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Solicitud eliminada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la Solicitud: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    public function estatus(Request $request)
    {
        $error='';
        try{
            $solicitudes=solicitudes::find($request['Uid_Solicitud']);
            $solicitudes->update([
                'Solicitud_Status'=>$request['Solicitud_Status'],
                'Solicitud_Observaciones'=>$request['Solicitud_Observaciones'],
                'Uid_Empleado_Receptor'=>Auth::user()->Uid_Empleado,
            ]);
            $tiposolicitud=tipo_solicitudes::find($solicitudes->Uid_TipoSolicitud);
            if($request['Solicitud_Status']=='Autorizado')
            {
                if($tiposolicitud->Id_TipoIncidencia)
                {
                    $incidencia=incidencias::create([
                        'Incidencia_Comentario'=>$request['Solicitud_Observaciones'],
                        'Incidencia_Tipo'=>$tiposolicitud->Id_TipoIncidencia,
                        'Incidencia_DiaInicio'=>$solicitudes->Solicitud_Inicio_Auscencia,
                        'Uid_Empleado'=> $solicitudes->Uid_Empleado_Emisor
                        ]);
                    kardex::create([
                        'Kardex_Descripcion'=>"Incidencia",
                        'Kardex_Uid_Registro'=>$incidencia->Uid_Incidencia
                        ]);
                }
            }
            kardex::create([
                'Kardex_Descripcion'=>"Status de la Solicitud",
                'Kardex_Uid_Registro'=>$solicitudes->Uid_Solicitud
                ]);
            $envio=empleados::from('empleados as e')
                ->join('usuarios as u', 'e.Uid_Empleado','u.Uid_Empleado')
                ->leftjoin('departamentos as d', 'e.Uid_Departamento','d.Uid_Departamento')
                ->leftjoin('puestos as p', 'e.Uid_Puesto','p.Uid_Puesto')
                ->select('u.Usuario_Correo as UCorreo','d.Departamento_Nombre','p.Puesto_Nombre',
                DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Empleado_Nombre"))
                ->where('e.Uid_Empleado',$solicitudes->Uid_Empleado_Emisor)
                ->first();

            $data=array(
                'Tipo'=>$tiposolicitud->TipoSolicitud_Nombre,
                'Empleado_Nombre'=>$envio->Empleado_Nombre,
                'Departamento_Nombre'=>$envio->Departamento_Nombre,
                'Puesto_Nombre'=>$envio->Puesto_Nombre,
                'Motivo'=>$solicitudes->Solicitud_Motivo,
                'Solicitud_Comentario'=>$request['Solicitud_Observaciones'],
                'Solicitud_Inicio_Auscencia'=>$solicitudes->Solicitud_Inicio_Auscencia,
                'Solicitud_Fin_Auscencia'=>$solicitudes->Solicitud_Fin_Auscencia,
                'Status'=>$request['Solicitud_Status']
            );
            //\AuthUser::envioSolicitudes($envio->UCorreo,'',$data,'mails.status',$request['Solicitud_Status'] . ' de ' .$tiposolicitud->TipoSolicitud_Nombre . $tiposolicitud->TipoSolicitud_Nombre);
            return response()->json(array('status'=>"success", 'Accion'=>"Solicitud {$request['Solicitud_Status']}",'Message'=>$request['Solicitud_Status']));
        }
        catch(\Exception $e){
            $error="Hubo un problema al actualizar el estatus de la Solicitud: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$solicitudes->Uid_Solicitud
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

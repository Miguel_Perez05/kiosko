<?php

namespace App\Http\Controllers;

use App\imagenes_mes;
use Illuminate\Http\Request;

class ImagenesMesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\imagenes_mes  $imagenes_mes
     * @return \Illuminate\Http\Response
     */
    public function show(imagenes_mes $imagenes_mes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\imagenes_mes  $imagenes_mes
     * @return \Illuminate\Http\Response
     */
    public function edit(imagenes_mes $imagenes_mes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\imagenes_mes  $imagenes_mes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, imagenes_mes $imagenes_mes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\imagenes_mes  $imagenes_mes
     * @return \Illuminate\Http\Response
     */
    public function destroy(imagenes_mes $imagenes_mes)
    {
        //
    }
}

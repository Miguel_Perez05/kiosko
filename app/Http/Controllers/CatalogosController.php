<?php

namespace App\Http\Controllers;

use App\catalogos;
use Illuminate\Http\Request;

class CatalogosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, catalogos $catalogos)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\catalogos  $catalogos
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, catalogos $catalogos)
    {
        return $catalogos::where('Status',1)
                        ->where('CatalogCode',$request->CatalogCode)
                        ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\catalogos  $catalogos
     * @return \Illuminate\Http\Response
     */
    public function edit(catalogos $catalogos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\catalogos  $catalogos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, catalogos $catalogos, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\catalogos  $catalogos
     * @return \Illuminate\Http\Response
     */
    public function destroy(catalogos $catalogos, $id)
    {
        
    }
}

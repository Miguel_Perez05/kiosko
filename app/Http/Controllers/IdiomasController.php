<?php

namespace App\Http\Controllers;

use App\kardex;
use App\idiomas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class IdiomasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        return view('Catalogos.Nominas.idiomas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, idiomas $idiomas)
    {
        try
        {
            $idioma=$idiomas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Idioma",
                'Kardex_Uid_Registro'=>$idioma->Uid_Idioma
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Idioma se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\idiomas  $idiomas
     * @return \Illuminate\Http\Response
     */
    public function show(idiomas $idiomas, $estatus)
    {
        $idiomasLst=$idiomas;
        if($estatus>0)
            $idiomasLst=$idiomasLst->where('Id_Estatus',$estatus);
        return $idiomasLst->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\idiomas  $idiomas
     * @return \Illuminate\Http\Response
     */
    public function edit(idiomas $idiomas, $id)
    {
        return $idiomas::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\idiomas  $idiomas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, idiomas $idiomas, $id)
    {
        try{
            $idiomas::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Idioma",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Idioma editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\idiomas  $idiomas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, idiomas $idiomas, $id)
    {
        try{
            $idiomas::find($id)->update(['Id_Estatus'=>$request->Id_Estatus]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Idioma",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Idioma eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Idioma: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);    
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\kardex;
use App\jornadas_laborales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class JornadasLaboralesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jornadas_laborales=jornadas_laborales::where('Id_Estatus',1)->get();
        return view('Catalogos.Nominas.jornadas_laborales', compact('jornadas_laborales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, jornadas_laborales $jornadas_laborales)
    {
        try
        {
            $jornada=$jornadas_laborales::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación de la Jornada Laboral",
                'Kardex_Uid_Registro'=>$jornada->Uid_JornadaLaboral
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"La Jornada Laboral se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear la Jornada Laboral: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jornadas_laborales  $jornadas_laborales
     * @return \Illuminate\Http\Response
     */
    public function show(jornadas_laborales $jornadas_laborales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jornadas_laborales  $jornadas_laborales
     * @return \Illuminate\Http\Response
     */
    public function edit(jornadas_laborales $jornadas_laborales, $id)
    {
        return $jornadas_laborales::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jornadas_laborales  $jornadas_laborales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jornadas_laborales $jornadas_laborales, $id)
    {
        try{
            $jornadas_laborales::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición de la Jornada Laboral",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Jornada Laboral editada correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar la Jornada Laboral: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jornadas_laborales  $jornadas_laborales
     * @return \Illuminate\Http\Response
     */
    public function destroy(jornadas_laborales $jornadas_laborales, $id)
    {
        try{
            $jornadas_laborales::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación de la Jornada",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Jornada eliminada"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar la Jornada: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

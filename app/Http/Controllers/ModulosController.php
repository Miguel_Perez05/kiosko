<?php

namespace App\Http\Controllers;

use App\modulos;
use Illuminate\Http\Request;

class ModulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\modulos  $modulos
     * @return \Illuminate\Http\Response
     */
    public function show(modulos $modulos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\modulos  $modulos
     * @return \Illuminate\Http\Response
     */
    public function edit(modulos $modulos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\modulos  $modulos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, modulos $modulos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\modulos  $modulos
     * @return \Illuminate\Http\Response
     */
    public function destroy(modulos $modulos)
    {
        //
    }
}

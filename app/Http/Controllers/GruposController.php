<?php

namespace App\Http\Controllers;

use App\grupos;
use App\kardex;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class GruposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Catalogos.Nominas.grupos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, grupos $grupos)
    {
        try
        {
            $grupos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Grupo",
                'Kardex_Uid_Registro'=>$grupos->Uid_Grupo
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Grupo se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Grupo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\grupos  $grupos
     * @return \Illuminate\Http\Response
     */
    public function show(grupos $grupos)
    {
        return $grupos::where('Id_Estatus',1)->get();;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\grupos  $grupos
     * @return \Illuminate\Http\Response
     */
    public function edit(grupos $grupos, $id)
    {
        return $grupos::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\grupos  $grupos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, grupos $grupos, $id)
    {
        try{
            $grupos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Grupo",
                'Kardex_Uid_Registro'=>$id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Grupo editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Grupo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\grupos  $grupos
     * @return \Illuminate\Http\Response
     */
    public function destroy(grupos $grupos, $id)
    {
        try{
            $grupos::find($id)->update(['Id_Estatus'=>0]);
            kardex::create([
                'Kardex_Descripcion'=>"Elimnación del Grupo",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Grupo eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Grupo: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }
}

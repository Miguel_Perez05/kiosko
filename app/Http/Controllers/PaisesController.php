<?php

namespace App\Http\Controllers;

use App\paises;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\kardex;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.paises');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, paises $paises)
    {
        try
        {
            $pais=$paises::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Pais Creado',
                'Kardex_Uid_Registro'=>$pais->Id_pais
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Pais se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Pais: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return response()->json(array('status'=>"alert", 'Accion'=>'Creación','Message'=>"Hubo un problema al crear el Pais"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function show(paises $paises)
    {
        return $paises::where('Id_Estatus',1)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function edit(paises $paises, $id)
    {
        return $paises::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, paises $paises, $id)
    {
//        return $id;
        try{
            $paises::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>'Pais Editado',
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Pais editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Pais: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return response()->json(array('status'=>"warning", 'Accion'=>'Edición','Message'=>"Hubo un problema al editar el Pais"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paises  $paises
     * @return \Illuminate\Http\Response
     */
    public function destroy(paises $paises, $id)
    {
        try{
            $paises::find($id)->delete();
            kardex::create([
                'Kardex_Descripcion'=>"Eliminación del Pais",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Eliminación','Message'=>"Pais eliminado"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al eliminar el Pais: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"warning", 'Accion'=>'Eliminación','Message'=>"Hubo un problema al eliminar el Pais"));
        }
    }
}

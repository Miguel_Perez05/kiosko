<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_nominas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TipoNominasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.tiponominas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, tipo_nominas $tipo_nominas)
    {
        try
        {
            $tipo_nomina=$tipo_nominas::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Tipo de Nominas",
                'Kardex_Uid_Registro'=> $tipo_nomina->Uid_TipoNomina
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo de Nomina se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al registrar el Tipo de Nomina: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_nominas  $tipo_nominas
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_nominas $tipo_nominas,$id)
    {
        return $tipo_nominas::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_nominas  $tipo_nominas
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_nominas $tipo_nominas,$id)
    {
        return $tipo_nominas::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_nominas  $tipo_nominas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_nominas $tipo_nominas, $id)
    {
        try{
            $tipo_nominas::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Tipo de Nominas",
                'Kardex_Uid_Registro'=> $id
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo de Nomina editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo de Nomina: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=> $id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_nominas  $tipo_nominas
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_nominas $tipo_nominas)
    {
        //
    }
}

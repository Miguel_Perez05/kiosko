<?php

namespace App\Http\Controllers;

use App\kardex;
use App\tipo_contratos;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TipoContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Catalogos.Nominas.tipocontrato');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,tipo_contratos $tipo_contratos)
    {
        try
        {
            $tipo_contratos::create($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Creación del Tipo Contrato Id {$tipo_contratos->Id_TipoContrato}",
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Creación','Message'=>"El Tipo Contrato se ha creado correctamente"));
        }
        catch(\Exception $e)
        {
            $error="Hubo un problema al crear el Tipo Contrato: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_contratos $tipo_contratos)
    {
        return $tipo_contratos->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_contratos $tipo_contratos, $id)
    {
        return $tipo_contratos->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_contratos $tipo_contratos,$id)
    {
        try{
            $tipo_contratos::find($id)->update($request->all());
            kardex::create([
                'Kardex_Descripcion'=>"Edición del Tipo Contrato Id {$id}",
                ]);
            return response()->json(array('status'=>"success", 'Accion'=>'Edición','Message'=>"Tipo Contrato editado correctamente"));
        }
        catch(\Exception $e){
            $error="Hubo un problema al editar el Tipo Contrato: {$e->getMessage()}";
            kardex::create([
                'Kardex_Descripcion'=>$error,
                'Kardex_Uid_Registro'=>$id
            ]);
            return Response::json(array('ResponseStatus'=> array('Message'=>$error)), 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_contratos $tipo_contratos)
    {
        //
    }
}

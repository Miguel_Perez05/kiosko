<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Curso_Nombre' => 'required',
            'Uid_AreaFormacion' => 'exists:areas_formacion,Uid_AreaFormacion',
        ];
    }

    public function messages()
    {
        return [
            'Curso_Nombre.required' => 'El nombre del curso es obligatorio.',
            'Uid_AreaFormacion.exists' => 'Selecciona un area de formación valido'
        ];
    }

    // public function attributes()
    // {
    //     return [
    //         'Curso_Nombre' => 'nombre del curso',
    //         'Uid_AreaFormacion' => 'area de formación',
    //     ];
    // }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntrenamientoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Uid_Curso' => 'required',
            'Entrenamiento_Porcentaje' => 'required',
            'Entrenamiento_Calificacion' => 'required',
            'Entrenamiento_Fecha' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'Uid_Curso.required' => 'Selecciona un curso valido.',
            'Entrenamiento_Porcentaje.required' => 'El porcentaje es obligatorio.',
            'Entrenamiento_Calificacion.required' => 'La calificación es obligatoria.',
            'Entrenamiento_Fecha.required' => 'La fecha es obligatoria.'
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registros_patronales extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'Uid_RegistroPatronal';

    protected $fillable = [
        'Uid_RegistroPatronal',
        'Uid_Empresa',
        'RegistroPatronal_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

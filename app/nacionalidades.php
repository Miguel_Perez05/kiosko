<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nacionalidades extends Model
{
    protected $primaryKey = 'Uid_Nacionalidad';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Nacionalidad',
        'Nacionalidad_Nombre',
        'Id_Pais',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

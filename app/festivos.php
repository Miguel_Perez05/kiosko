<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class festivos extends Model
{
    protected $primaryKey = 'Uid_Festivo';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Festivo',
        'Festivo_Nombre',
        'Festivo_Dia',
        'Festivo_Ciclico',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];

    public static function FestivosEjercicio()
    {
        return static::where(function($query) {
                            if('Festivo_Ciclico' > 1)
                                $query->whereRaw('Year(Festivo_Dia)=Year(curdate())');
                            });
    }
}

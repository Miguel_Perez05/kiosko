<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_bonos extends Model
{
    protected $primaryKey = 'Uid_TipoBono';
    public $incrementing = false;
    protected $fillable = [
        'Uid_TipoBono',
        'TipoBono_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

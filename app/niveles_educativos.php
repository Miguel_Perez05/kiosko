<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class niveles_educativos extends Model
{
    protected $primaryKey = 'Uid_NivelEducativo';
    public $incrementing = false;
    protected $fillable = [
        'Uid_NivelEducativo',
        'NivelEducativo_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

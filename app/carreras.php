<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carreras extends Model
{
    protected $primaryKey = 'Uid_Carrera';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Carrera',
        'Uid_AreaFormacion',
        'Uid_NivelEducativo',
        'Carrera_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\avisos::observe(\App\Observers\AvisosObserver::class);
        \App\bajas::observe(\App\Observers\BajasObserver::class);
        \App\ciudades::observe(\App\Observers\CiudadesObserver::class);
        \App\cursos::observe(\App\Observers\CursosObserver::class);
        \App\departamentos::observe(\App\Observers\DepartamentosObserver::class);
        \App\dias::observe(\App\Observers\DiasObserver::class);
        \App\domina_idiomas::observe(\App\Observers\Domina_IdiomasObserver::class);
        \App\escolaridades::observe(\App\Observers\EscolaridadesObserver::class);
        \App\empleados::observe(\App\Observers\EmpleadosObserver::class);
        \App\empresas::observe(\App\Observers\EmpresasObserver::class);
        \App\entrenamientos::observe(\App\Observers\EntrenamientosObserver::class);
        \App\escuelas::observe(\App\Observers\EscuelasObserver::class);
        \App\estados_civiles::observe(\App\Observers\Estados_CivilesObserver::class);
        \App\estados::observe(\App\Observers\EstadosObserver::class);
        \App\festivos::observe(\App\Observers\FestivosObserver::class);
        \App\grupos::observe(\App\Observers\GruposObserver::class);
        \App\historial::observe(\App\Observers\HistorialObserver::class);
        \App\idiomas::observe(\App\Observers\IdiomasObserver::class);
        \App\incidencias::observe(\App\Observers\IncidenciasObserver::class);
        \App\instructores::observe(\App\Observers\InstructoresObserver::class);
        \App\jornadas_laborales::observe(\App\Observers\Jornadas_LaboralesObserver::class);
        \App\kardex::observe(\App\Observers\KardexObserver::class);
        \App\paises::observe(\App\Observers\PaisesObserver::class);
        \App\percepciones_deducciones::observe(\App\Observers\Percepciones_DeduccionesObserver::class);
        \App\perfiles::observe(\App\Observers\PerfilesObserver::class);
        \App\periodos::observe(\App\Observers\PeriodosObserver::class);
        \App\privilegios_perfiles::observe(\App\Observers\Privilegios_PerfilesObserver::class);
        \App\puestos::observe(\App\Observers\PuestosObserver::class);
        \App\recontratables::observe(\App\Observers\RecontratablesObserver::class);
        \App\registros_patronales::observe(\App\Observers\Registros_PatronalesObserver::class);
        \App\solicitudes::observe(\App\Observers\SolicitudesObserver::class);
        \App\tipo_empleados::observe(\App\Observers\Tipo_EmpleadosObserver::class);
        \App\tipo_incidencias::observe(\App\Observers\Tipo_IncidenciasObserver::class);
        \App\tipo_solicitudes::observe(\App\Observers\Tipo_SolicitudesObserver::class);
        \App\turnos::observe(\App\Observers\TurnosObserver::class);
        \App\usuarios::observe(\App\Observers\UsuarioObserver::class);
        \App\nacionalidades::observe(\App\Observers\NacionalidadesObserver::class);
        \App\tipo_nominas::observe(\App\Observers\Tipo_NominasObserver::class);
        \App\reconocimientos::observe(\App\Observers\ReconocimientosObserver::class);
        \App\entrenamientos::observe(\App\Observers\EntrenamientosObserver::class);
        \App\niveles_educativos::observe(\App\Observers\NivelesEducativosObserver::class);
        \App\areas_formacion::observe(\App\Observers\AreasFormacionObserver::class);
        \App\carreras::observe(\App\Observers\CarrerasObserver::class);
        \App\sueldos::observe(\App\Observers\SueldosObserver::class);
        \App\escolaridades::observe(\App\Observers\EscolaridadesObserver::class);
        \App\tipo_bonos::observe(\App\Observers\TiposBonosObserver::class);
        \App\promociones_internas::observe(\App\Observers\PromocionesInternasObserver::class);
        \App\contratos::observe(\App\Observers\ContratosObserver::class);
        \App\tipo_contratos::observe(\App\Observers\Tipo_ContratosObserver::class);
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sueldos extends Model
{
    protected $primaryKey = 'Uid_Sueldo';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Sueldo',
        'Uid_Empleado',
        'Sueldo_Fecha',
        'Sueldo_Monto',
        'Id_Status',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

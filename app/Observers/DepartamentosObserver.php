<?php

namespace App\Observers;

use App\departamentos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class DepartamentosObserver
{
    /**
     * Handle the departamentos "created" event.
     *
     * @param  \App\departamentos  $departamentos
     * @return void
     */
    public function creating(departamentos $departamentos)
    {
        $departamentos->Uid_Departamento = Uuid::generate()->string;
        $departamentos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $departamentos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $departamentos->Id_Estatus = 1;

    }

    /**
     * Handle the departamentos "updated" event.
     *
     * @param  \App\departamentos  $departamentos
     * @return void
     */
    public function updating(departamentos $departamentos)
    {
        $departamentos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;

    }

    /**
     * Handle the departamentos "deleted" event.
     *
     * @param  \App\departamentos  $departamentos
     * @return void
     */
    public function deleted(departamentos $departamentos)
    {
        //
    }

    /**
     * Handle the departamentos "restored" event.
     *
     * @param  \App\departamentos  $departamentos
     * @return void
     */
    public function restored(departamentos $departamentos)
    {
        //
    }

    /**
     * Handle the departamentos "force deleted" event.
     *
     * @param  \App\departamentos  $departamentos
     * @return void
     */
    public function forceDeleted(departamentos $departamentos)
    {
        //
    }
}

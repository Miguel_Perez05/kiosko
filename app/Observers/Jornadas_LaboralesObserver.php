<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\jornadas_laborales;
use Illuminate\Support\Facades\Auth;

class Jornadas_LaboralesObserver
{
    /**
     * Handle the jornadas_laborales "created" event.
     *
     * @param  \App\jornadas_laborales  $jornadasLaborales
     * @return void
     */
    public function creating(jornadas_laborales $jornadasLaborales)
    {
        $jornadasLaborales->Uid_JornadaLaboral = Uuid::generate()->string;
        $jornadasLaborales->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $jornadasLaborales->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $jornadasLaborales->Id_Estatus=1;
    }

    /**
     * Handle the jornadas_laborales "updated" event.
     *
     * @param  \App\jornadas_laborales  $jornadasLaborales
     * @return void
     */
    public function updating(jornadas_laborales $jornadasLaborales)
    {
        $jornadasLaborales->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the jornadas_laborales "deleted" event.
     *
     * @param  \App\jornadas_laborales  $jornadasLaborales
     * @return void
     */
    public function deleted(jornadas_laborales $jornadasLaborales)
    {
        //
    }

    /**
     * Handle the jornadas_laborales "restored" event.
     *
     * @param  \App\jornadas_laborales  $jornadasLaborales
     * @return void
     */
    public function restored(jornadas_laborales $jornadasLaborales)
    {
        //
    }

    /**
     * Handle the jornadas_laborales "force deleted" event.
     *
     * @param  \App\jornadas_laborales  $jornadasLaborales
     * @return void
     */
    public function forceDeleted(jornadas_laborales $jornadasLaborales)
    {
        //
    }
}

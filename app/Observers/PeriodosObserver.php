<?php

namespace App\Observers;

use App\periodos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class PeriodosObserver
{
    /**
     * Handle the periodos "created" event.
     *
     * @param  \App\periodos  $periodos
     * @return void
     */
    public function creating(periodos $periodos)
    {
        $periodos->Uid_Periodo = Uuid::generate()->string;
        $periodos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $periodos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the periodos "updated" event.
     *
     * @param  \App\periodos  $periodos
     * @return void
     */
    public function updating(periodos $periodos)
    {
        $periodos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the periodos "deleted" event.
     *
     * @param  \App\periodos  $periodos
     * @return void
     */
    public function deleted(periodos $periodos)
    {
        //
    }

    /**
     * Handle the periodos "restored" event.
     *
     * @param  \App\periodos  $periodos
     * @return void
     */
    public function restored(periodos $periodos)
    {
        //
    }

    /**
     * Handle the periodos "force deleted" event.
     *
     * @param  \App\periodos  $periodos
     * @return void
     */
    public function forceDeleted(periodos $periodos)
    {
        //
    }
}

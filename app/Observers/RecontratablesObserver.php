<?php

namespace App\Observers;

use App\recontratables;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class RecontratablesObserver
{
    /**
     * Handle the recontratables "created" event.
     *
     * @param  \App\recontratables  $recontratables
     * @return void
     */
    public function creating(recontratables $recontratables)
    {
        $recontratables->Uid_Recontratable = Uuid::generate()->string;
        $recontratables->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $recontratables->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the recontratables "updated" event.
     *
     * @param  \App\recontratables  $recontratables
     * @return void
     */
    public function updating(recontratables $recontratables)
    {
        $recontratables->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the recontratables "deleted" event.
     *
     * @param  \App\recontratables  $recontratables
     * @return void
     */
    public function deleted(recontratables $recontratables)
    {
        //
    }

    /**
     * Handle the recontratables "restored" event.
     *
     * @param  \App\recontratables  $recontratables
     * @return void
     */
    public function restored(recontratables $recontratables)
    {
        //
    }

    /**
     * Handle the recontratables "force deleted" event.
     *
     * @param  \App\recontratables  $recontratables
     * @return void
     */
    public function forceDeleted(recontratables $recontratables)
    {
        //
    }
}

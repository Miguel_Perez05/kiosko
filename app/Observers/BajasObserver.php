<?php

namespace App\Observers;

use App\bajas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class BajasObserver
{
    /**
     * Handle the bajas "created" event.
     *
     * @param  \App\bajas  $bajas
     * @return void
     */
    public function creating(bajas $bajas)
    {
        $bajas->Uid_Baja = Uuid::generate()->string;
        $bajas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $bajas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the bajas "updated" event.
     *
     * @param  \App\bajas  $bajas
     * @return void
     */
    public function updating(bajas $bajas)
    {
        $bajas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the bajas "deleted" event.
     *
     * @param  \App\bajas  $bajas
     * @return void
     */
    public function deleted(bajas $bajas)
    {
        //
    }

    /**
     * Handle the bajas "restored" event.
     *
     * @param  \App\bajas  $bajas
     * @return void
     */
    public function restored(bajas $bajas)
    {
        //
    }

    /**
     * Handle the bajas "force deleted" event.
     *
     * @param  \App\bajas  $bajas
     * @return void
     */
    public function forceDeleted(bajas $bajas)
    {
        //
    }
}

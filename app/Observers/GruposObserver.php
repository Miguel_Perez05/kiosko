<?php

namespace App\Observers;

use App\grupos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class GruposObserver
{
    /**
     * Handle the grupos "created" event.
     *
     * @param  \App\grupos  $grupos
     * @return void
     */
    public function creating(grupos $grupos)
    {
        $grupos->Uid_Grupo = Uuid::generate()->string;
        $grupos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $grupos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $grupos->Estatus=1;
    }

    /**
     * Handle the grupos "updated" event.
     *
     * @param  \App\grupos  $grupos
     * @return void
     */
    public function updating(grupos $grupos)
    {
        $grupos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the grupos "deleted" event.
     *
     * @param  \App\grupos  $grupos
     * @return void
     */
    public function deleted(grupos $grupos)
    {
        //
    }

    /**
     * Handle the grupos "restored" event.
     *
     * @param  \App\grupos  $grupos
     * @return void
     */
    public function restored(grupos $grupos)
    {
        //
    }

    /**
     * Handle the grupos "force deleted" event.
     *
     * @param  \App\grupos  $grupos
     * @return void
     */
    public function forceDeleted(grupos $grupos)
    {
        //
    }
}

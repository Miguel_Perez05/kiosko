<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\promociones_internas;
use Illuminate\Support\Facades\Auth;

class PromocionesInternasObserver
{
    /**
     * Handle the promocionesinternas "created" event.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return void
     */
    public function creating(promociones_internas $promocionesinternas)
    {
        $promocionesinternas->Uid_PromocionInterna = Uuid::generate()->string;
        $promocionesinternas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $promocionesinternas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $promocionesinternas->Id_Estatus=1;
    }

    /**
     * Handle the promocionesinternas "updated" event.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return void
     */
    public function updating(promociones_internas $promocionesinternas)
    {
        //
    }

    /**
     * Handle the promocionesinternas "deleted" event.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return void
     */
    public function deleted(promociones_internas $promocionesinternas)
    {
        //
    }

    /**
     * Handle the promocionesinternas "restored" event.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return void
     */
    public function restored(promociones_internas $promocionesinternas)
    {
        //
    }

    /**
     * Handle the promocionesinternas "force deleted" event.
     *
     * @param  \App\promocionesinternas  $promocionesinternas
     * @return void
     */
    public function forceDeleted(promociones_internas $promocionesinternas)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\areas_formacion;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class AreasFormacionObserver
{
    /**
     * Handle the areas_formacion "created" event.
     *
     * @param  \App\areas_formacion  $areasFormacion
     * @return void
     */
    public function creating(areas_formacion $areasFormacion)
    {
        $areasFormacion->Uid_AreaFormacion = Uuid::generate()->string;
        $areasFormacion->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $areasFormacion->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $areasFormacion->Id_Estatus = 1;
    }

    /**
     * Handle the areas_formacion "updated" event.
     *
     * @param  \App\areas_formacion  $areasFormacion
     * @return void
     */
    public function updating(areas_formacion $areasFormacion)
    {
        $areasFormacion->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the areas_formacion "deleted" event.
     *
     * @param  \App\areas_formacion  $areasFormacion
     * @return void
     */
    public function deleted(areas_formacion $areasFormacion)
    {
        //
    }

    /**
     * Handle the areas_formacion "restored" event.
     *
     * @param  \App\areas_formacion  $areasFormacion
     * @return void
     */
    public function restored(areas_formacion $areasFormacion)
    {
        //
    }

    /**
     * Handle the areas_formacion "force deleted" event.
     *
     * @param  \App\areas_formacion  $areasFormacion
     * @return void
     */
    public function forceDeleted(areas_formacion $areasFormacion)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\carreras;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class CarrerasObserver
{
    /**
     * Handle the carreras "created" event.
     *
     * @param  \App\carreras  $carreras
     * @return void
     */
    public function creating(carreras $carreras)
    {
        $carreras->Uid_Carrera = Uuid::generate()->string;
        $carreras->Id_Estatus = 1;
        $carreras->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $carreras->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the carreras "updated" event.
     *
     * @param  \App\carreras  $carreras
     * @return void
     */
    public function updating(carreras $carreras)
    {
        $carreras->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the carreras "deleted" event.
     *
     * @param  \App\carreras  $carreras
     * @return void
     */
    public function deleted(carreras $carreras)
    {
        //
    }

    /**
     * Handle the carreras "restored" event.
     *
     * @param  \App\carreras  $carreras
     * @return void
     */
    public function restored(carreras $carreras)
    {
        //
    }

    /**
     * Handle the carreras "force deleted" event.
     *
     * @param  \App\carreras  $carreras
     * @return void
     */
    public function forceDeleted(carreras $carreras)
    {
        //
    }
}

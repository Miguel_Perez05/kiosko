<?php

namespace App\Observers;

use App\tipo_bonos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class TiposBonosObserver
{
    /**
     * Handle the tipo_bonos "created" event.
     *
     * @param  \App\tipo_bonos  $tiposBonos
     * @return void
     */
    public function creating(tipo_bonos $tiposBonos)
    {
        $tiposBonos->Uid_TipoBono = Uuid::generate()->string;
        $tiposBonos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $tiposBonos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $tiposBonos->Id_Estatus=1;
    }

    /**
     * Handle the tipo_bonos "updated" event.
     *
     * @param  \App\tipo_bonos  $tiposBonos
     * @return void
     */
    public function updating(tipo_bonos $tiposBonos)
    {
        $tiposBonos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_bonos "deleted" event.
     *
     * @param  \App\tipo_bonos  $tiposBonos
     * @return void
     */
    public function deleted(tipo_bonos $tiposBonos)
    {
        //
    }

    /**
     * Handle the tipo_bonos "restored" event.
     *
     * @param  \App\tipo_bonos  $tiposBonos
     * @return void
     */
    public function restored(tipo_bonos $tiposBonos)
    {
        //
    }

    /**
     * Handle the tipo_bonos "force deleted" event.
     *
     * @param  \App\tipo_bonos  $tiposBonos
     * @return void
     */
    public function forceDeleted(tipo_bonos $tiposBonos)
    {
        //
    }
}

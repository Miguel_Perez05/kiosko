<?php

namespace App\Observers;

use App\tipo_nominas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class Tipo_NominasObserver
{
    /**
     * Handle the tipo_nominas "created" event.
     *
     * @param  \App\tipo_nominas  $tipoNominas
     * @return void
     */
    public function creating(tipo_nominas $tipoNominas)
    {
        $tipoNominas->Uid_TipoNomina = Uuid::generate()->string;
        $tipoNominas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $tipoNominas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_nominas "updated" event.
     *
     * @param  \App\tipo_nominas  $tipoNominas
     * @return void
     */
    public function updating(tipo_nominas $tipoNominas)
    {
        $tipoNominas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_nominas "deleted" event.
     *
     * @param  \App\tipo_nominas  $tipoNominas
     * @return void
     */
    public function deleted(tipo_nominas $tipoNominas)
    {
        //
    }

    /**
     * Handle the tipo_nominas "restored" event.
     *
     * @param  \App\tipo_nominas  $tipoNominas
     * @return void
     */
    public function restored(tipo_nominas $tipoNominas)
    {
        //
    }

    /**
     * Handle the tipo_nominas "force deleted" event.
     *
     * @param  \App\tipo_nominas  $tipoNominas
     * @return void
     */
    public function forceDeleted(tipo_nominas $tipoNominas)
    {
        //
    }
}

<?php

namespace App\Observers;
use App\usuarios;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class UsuarioObserver
{
    public function creating(usuarios $usuario)
    {
        $usuario->Usuario_Nuevo=1;
        $usuario->Usuario_Activo=0;
        $usuario->Id_Estatus=1;
        $usuario->Uid_Usuario = Uuid::generate()->string;
        $usuario->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $usuario->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(usuarios $usuario)
    {
        $usuario->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

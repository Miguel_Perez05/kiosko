<?php

namespace App\Observers;

use App\entrenamientos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class EntrenamientosObserver
{
    /**
     * Handle the entrenamientos "created" event.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return void
     */
    public function creating(entrenamientos $entrenamientos)
    {
        $entrenamientos->Uid_Entrenamiento = Uuid::generate()->string;
        $entrenamientos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $entrenamientos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $entrenamientos->Id_Estatus=1;
    }

    /**
     * Handle the entrenamientos "updated" event.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return void
     */
    public function updating(entrenamientos $entrenamientos)
    {
        $entrenamientos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the entrenamientos "deleted" event.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return void
     */
    public function deleted(entrenamientos $entrenamientos)
    {
        //
    }

    /**
     * Handle the entrenamientos "restored" event.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return void
     */
    public function restored(entrenamientos $entrenamientos)
    {
        //
    }

    /**
     * Handle the entrenamientos "force deleted" event.
     *
     * @param  \App\entrenamientos  $entrenamientos
     * @return void
     */
    public function forceDeleted(entrenamientos $entrenamientos)
    {
        //
    }
}

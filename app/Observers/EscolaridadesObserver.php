<?php

namespace App\Observers;

use App\escolaridades;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class EscolaridadesObserver
{
    /**
     * Handle the escolaridades "created" event.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return void
     */
    public function creating(escolaridades $escolaridades)
    {
        $escolaridades->Id_Estatus=1;
        $escolaridades->Uid_Escolaridad = Uuid::generate()->string;
        $escolaridades->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $escolaridades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the escolaridades "updated" event.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return void
     */
    public function updating(escolaridades $escolaridades)
    {
        $escolaridades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the escolaridades "deleted" event.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return void
     */
    public function deleted(escolaridades $escolaridades)
    {
        //
    }

    /**
     * Handle the escolaridades "restored" event.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return void
     */
    public function restored(escolaridades $escolaridades)
    {
        //
    }

    /**
     * Handle the escolaridades "force deleted" event.
     *
     * @param  \App\escolaridades  $escolaridades
     * @return void
     */
    public function forceDeleted(escolaridades $escolaridades)
    {
        //
    }
}

<?php

namespace App\Observers;
use App\avisos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class AvisosObserver
{
    public function creating(avisos $aviso)
    {
        $aviso->Uid_Aviso = Uuid::generate()->string;
        $aviso->Uid_Empleado_Emisor = Auth::user()->Uid_Usuario;
        $aviso->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $aviso->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $aviso->Aviso_Visto = 0;
        $aviso->Id_Estatus = 1;
    }

    public function updating(avisos $aviso)
    {
        $aviso->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

<?php

namespace App\Observers;

use App\escuelas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class EscuelasObserver
{
    /**
     * Handle the escuelas "created" event.
     *
     * @param  \App\escuelas  $escuelas
     * @return void
     */
    public function creating(escuelas $escuelas)
    {
        $escuelas->Uid_Escuela = Uuid::generate()->string;
        $escuelas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $escuelas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $escuelas->Id_Estatus=1;
    }

    /**
     * Handle the escuelas "updated" event.
     *
     * @param  \App\escuelas  $escuelas
     * @return void
     */
    public function updating(escuelas $escuelas)
    {
        $escuelas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the escuelas "deleted" event.
     *
     * @param  \App\escuelas  $escuelas
     * @return void
     */
    public function deleted(escuelas $escuelas)
    {
        //
    }

    /**
     * Handle the escuelas "restored" event.
     *
     * @param  \App\escuelas  $escuelas
     * @return void
     */
    public function restored(escuelas $escuelas)
    {
        //
    }

    /**
     * Handle the escuelas "force deleted" event.
     *
     * @param  \App\escuelas  $escuelas
     * @return void
     */
    public function forceDeleted(escuelas $escuelas)
    {
        //
    }
}

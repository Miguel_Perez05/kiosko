<?php

namespace App\Observers;

use App\dias;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class DiasObserver
{
    /**
     * Handle the dias "created" event.
     *
     * @param  \App\dias  $dias
     * @return void
     */
    public function creating(dias $dias)
    {
        $dias->Uid_Dia = Uuid::generate()->string;
        $dias->Id_Estatus=1;
        $dias->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $dias->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the dias "updated" event.
     *
     * @param  \App\dias  $dias
     * @return void
     */
    public function updating(dias $dias)
    {
        $dias->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the dias "deleted" event.
     *
     * @param  \App\dias  $dias
     * @return void
     */
    public function deleted(dias $dias)
    {
        //
    }

    /**
     * Handle the dias "restored" event.
     *
     * @param  \App\dias  $dias
     * @return void
     */
    public function restored(dias $dias)
    {
        //
    }

    /**
     * Handle the dias "force deleted" event.
     *
     * @param  \App\dias  $dias
     * @return void
     */
    public function forceDeleted(dias $dias)
    {
        //
    }
}

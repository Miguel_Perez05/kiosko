<?php

namespace App\Observers;

use App\sueldos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class SueldosObserver
{
    /**
     * Handle the sueldos "created" event.
     *
     * @param  \App\sueldos  $sueldos
     * @return void
     */
    public function creating(sueldos $sueldos)
    {
        $sueldos->Uid_Sueldo = Uuid::generate()->string;
        $sueldos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $sueldos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $sueldos->Id_Estatus = 1;
    }

    /**
     * Handle the sueldos "updated" event.
     *
     * @param  \App\sueldos  $sueldos
     * @return void
     */
    public function updating(sueldos $sueldos)
    {
        $sueldos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the sueldos "deleted" event.
     *
     * @param  \App\sueldos  $sueldos
     * @return void
     */
    public function deleted(sueldos $sueldos)
    {
        //
    }

    /**
     * Handle the sueldos "restored" event.
     *
     * @param  \App\sueldos  $sueldos
     * @return void
     */
    public function restored(sueldos $sueldos)
    {
        //
    }

    /**
     * Handle the sueldos "force deleted" event.
     *
     * @param  \App\sueldos  $sueldos
     * @return void
     */
    public function forceDeleted(sueldos $sueldos)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\kardex;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class KardexObserver
{
    /**
     * Handle the kardex "created" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function creating(kardex $kardex)
    {
        $kardex->Uid_Kardex = Uuid::generate()->string;
        $kardex->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the kardex "updated" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function updated(kardex $kardex)
    {
        //
    }

    /**
     * Handle the kardex "deleted" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function deleted(kardex $kardex)
    {
        //
    }

    /**
     * Handle the kardex "restored" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function restored(kardex $kardex)
    {
        //
    }

    /**
     * Handle the kardex "force deleted" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function forceDeleted(kardex $kardex)
    {
        //
    }
}

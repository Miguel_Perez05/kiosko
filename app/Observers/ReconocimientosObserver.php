<?php

namespace App\Observers;

use App\reconocimientos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class ReconocimientosObserver
{
    /**
     * Handle the reconocimientos "created" event.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return void
     */
    public function creating(reconocimientos $reconocimientos)
    {
        $reconocimientos->Uid_Reconocimiento = Uuid::generate()->string;
        $reconocimientos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $reconocimientos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $reconocimientos->Id_Estatus = 1;
    }

    /**
     * Handle the reconocimientos "updated" event.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return void
     */
    public function updating(reconocimientos $reconocimientos)
    {
        $reconocimientos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the reconocimientos "deleted" event.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return void
     */
    public function deleted(reconocimientos $reconocimientos)
    {
        //
    }

    /**
     * Handle the reconocimientos "restored" event.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return void
     */
    public function restored(reconocimientos $reconocimientos)
    {
        //
    }

    /**
     * Handle the reconocimientos "force deleted" event.
     *
     * @param  \App\reconocimientos  $reconocimientos
     * @return void
     */
    public function forceDeleted(reconocimientos $reconocimientos)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\puestos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class PuestosObserver
{
    /**
     * Handle the puestos "created" event.
     *
     * @param  \App\puestos  $puestos
     * @return void
     */
    public function creating(puestos $puestos)
    {
        $puestos->Uid_Puesto = Uuid::generate()->string;
        $puestos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $puestos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $puestos->Id_Estatus=4;
    }

    /**
     * Handle the puestos "updated" event.
     *
     * @param  \App\puestos  $puestos
     * @return void
     */
    public function updating(puestos $puestos)
    {
        $puestos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the puestos "deleted" event.
     *
     * @param  \App\puestos  $puestos
     * @return void
     */
    public function deleted(puestos $puestos)
    {
        //
    }

    /**
     * Handle the puestos "restored" event.
     *
     * @param  \App\puestos  $puestos
     * @return void
     */
    public function restored(puestos $puestos)
    {
        //
    }

    /**
     * Handle the puestos "force deleted" event.
     *
     * @param  \App\puestos  $puestos
     * @return void
     */
    public function forceDeleted(puestos $puestos)
    {
        //
    }
}

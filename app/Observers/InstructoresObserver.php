<?php

namespace App\Observers;

use App\instructores;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class InstructoresObserver
{
    /**
     * Handle the instructores "created" event.
     *
     * @param  \App\instructores  $instructores
     * @return void
     */
    public function creating(instructores $instructores)
    {
        $instructores->Uid_Instructor = Uuid::generate()->string;
        $instructores->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $instructores->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $instructores->Id_Estatus=1;
    }

    /**
     * Handle the instructores "updated" event.
     *
     * @param  \App\instructores  $instructores
     * @return void
     */
    public function updating(instructores $instructores)
    {
        $instructores->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the instructores "deleted" event.
     *
     * @param  \App\instructores  $instructores
     * @return void
     */
    public function deleted(instructores $instructores)
    {
        //
    }

    /**
     * Handle the instructores "restored" event.
     *
     * @param  \App\instructores  $instructores
     * @return void
     */
    public function restored(instructores $instructores)
    {
        //
    }

    /**
     * Handle the instructores "force deleted" event.
     *
     * @param  \App\instructores  $instructores
     * @return void
     */
    public function forceDeleted(instructores $instructores)
    {
        //
    }
}

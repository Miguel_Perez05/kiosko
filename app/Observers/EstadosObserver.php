<?php

namespace App\Observers;

use App\estados;
use Uuid;
use Illuminate\Support\Facades\Auth;

class EstadosObserver
{
    /**
     * Handle the estados "created" event.
     *
     * @param  \App\estados  $estados
     * @return void
     */
    public function creating(estados $estados)
    {
        $estados->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $estados->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $estados->Id_Estatus=1;
    }

    /**
     * Handle the estados "updated" event.
     *
     * @param  \App\estados  $estados
     * @return void
     */
    public function updating(estados $estados)
    {
        $estados->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the estados "deleted" event.
     *
     * @param  \App\estados  $estados
     * @return void
     */
    public function deleted(estados $estados)
    {
        //
    }

    /**
     * Handle the estados "restored" event.
     *
     * @param  \App\estados  $estados
     * @return void
     */
    public function restored(estados $estados)
    {
        //
    }

    /**
     * Handle the estados "force deleted" event.
     *
     * @param  \App\estados  $estados
     * @return void
     */
    public function forceDeleted(estados $estados)
    {
        //
    }
}

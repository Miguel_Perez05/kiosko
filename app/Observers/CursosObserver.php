<?php

namespace App\Observers;

use App\cursos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class CursosObserver
{
    public function creating(cursos $cursos)
    {
        $cursos->Uid_Curso = Uuid::generate()->string;
        $cursos->Id_Estatus = 1;
        $cursos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $cursos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(cursos $cursos)
    {
        $cursos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

<?php

namespace App\Observers;
use App\empleados;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class EmpleadosObserver
{
    public function creating(empleados $empleado)
    {
        $empleado->Id_Estatus=1;
        $empleado->Uid_Empleado = Uuid::generate()->string;
        $empleado->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $empleado->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(empleados $empleado)
    {
        $empleado->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;

    }
}

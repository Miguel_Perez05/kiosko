<?php

namespace App\Observers;

use App\estados_civiles;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class Estados_CivilesObserver
{
    /**
     * Handle the estados_civiles "created" event.
     *
     * @param  \App\estados_civiles  $estadosCiviles
     * @return void
     */
    public function creating(estados_civiles $estadosCiviles)
    {
        $estadosCiviles->Id_Estatus=1;
        $estadosCiviles->Uid_EstadoCivil = Uuid::generate()->string;
        $estadosCiviles->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $estadosCiviles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the estados_civiles "updated" event.
     *
     * @param  \App\estados_civiles  $estadosCiviles
     * @return void
     */
    public function updating(estados_civiles $estadosCiviles)
    {
        $estadosCiviles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the estados_civiles "deleted" event.
     *
     * @param  \App\estados_civiles  $estadosCiviles
     * @return void
     */
    public function deleted(estados_civiles $estadosCiviles)
    {
        //
    }

    /**
     * Handle the estados_civiles "restored" event.
     *
     * @param  \App\estados_civiles  $estadosCiviles
     * @return void
     */
    public function restored(estados_civiles $estadosCiviles)
    {
        //
    }

    /**
     * Handle the estados_civiles "force deleted" event.
     *
     * @param  \App\estados_civiles  $estadosCiviles
     * @return void
     */
    public function forceDeleted(estados_civiles $estadosCiviles)
    {
        //
    }
}

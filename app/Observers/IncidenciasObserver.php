<?php

namespace App\Observers;
use App\incidencias;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class IncidenciasObserver
{
    public function creating(incidencias $incidencia)
    {
        $incidencia->Uid_Incidencia = Uuid::generate()->string;
        $incidencia->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $incidencia->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $incidencia->Id_Estatus=1;
    }

    public function updating(incidencias $incidencia)
    {
        $incidencia->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

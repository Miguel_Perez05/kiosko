<?php

namespace App\Observers;

use App\turnos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class TurnosObserver
{
    /**
     * Handle the turnos "created" event.
     *
     * @param  \App\turnos  $turnos
     * @return void
     */
    public function creating(turnos $turnos)
    {
        $turnos->Uid_Turno = Uuid::generate()->string;
        $turnos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $turnos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $turnos->Id_Estatus=1;
    }

    /**
     * Handle the turnos "updated" event.
     *
     * @param  \App\turnos  $turnos
     * @return void
     */
    public function updating(turnos $turnos)
    {
        $turnos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the turnos "deleted" event.
     *
     * @param  \App\turnos  $turnos
     * @return void
     */
    public function deleted(turnos $turnos)
    {
        //
    }

    /**
     * Handle the turnos "restored" event.
     *
     * @param  \App\turnos  $turnos
     * @return void
     */
    public function restored(turnos $turnos)
    {
        //
    }

    /**
     * Handle the turnos "force deleted" event.
     *
     * @param  \App\turnos  $turnos
     * @return void
     */
    public function forceDeleted(turnos $turnos)
    {
        //
    }
}

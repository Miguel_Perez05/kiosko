<?php

namespace App\Observers;
use App\tipo_incidencias;
use Illuminate\Support\Facades\Auth;

class Tipo_IncidenciasObserver
{
    public function creating(tipo_incidencias $incidencia)
    {
        $incidencia->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $incidencia->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(tipo_incidencias $incidencia)
    {
        $incidencia->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

<?php

namespace App\Observers;

use App\idiomas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class IdiomasObserver
{
    /**
     * Handle the idiomas "created" event.
     *
     * @param  \App\idiomas  $idiomas
     * @return void
     */
    public function creating(idiomas $idiomas)
    {
        $idiomas->Uid_Idioma = Uuid::generate()->string;
        $idiomas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $idiomas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $idiomas->Id_Estatus=1;
    }

    /**
     * Handle the idiomas "updated" event.
     *
     * @param  \App\idiomas  $idiomas
     * @return void
     */
    public function updating(idiomas $idiomas)
    {
        $idiomas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the idiomas "deleted" event.
     *
     * @param  \App\idiomas  $idiomas
     * @return void
     */
    public function deleted(idiomas $idiomas)
    {
        //
    }

    /**
     * Handle the idiomas "restored" event.
     *
     * @param  \App\idiomas  $idiomas
     * @return void
     */
    public function restored(idiomas $idiomas)
    {
        //
    }

    /**
     * Handle the idiomas "force deleted" event.
     *
     * @param  \App\idiomas  $idiomas
     * @return void
     */
    public function forceDeleted(idiomas $idiomas)
    {
        //
    }
}

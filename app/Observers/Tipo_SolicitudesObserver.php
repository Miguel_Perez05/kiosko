<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\tipo_solicitudes;
use Illuminate\Support\Facades\Auth;

class Tipo_SolicitudesObserver
{
    /**
     * Handle the tipo_solicitudes "created" event.
     *
     * @param  \App\tipo_solicitudes  $tipoSolicitudes
     * @return void
     */
    public function creating(tipo_solicitudes $tipoSolicitudes)
    {
        $tipoSolicitudes->Uid_TipoSolicitud = Uuid::generate()->string;
        $tipoSolicitudes->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $tipoSolicitudes->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $tipoSolicitudes->Id_Estatus=1;
    }

    /**
     * Handle the tipo_solicitudes "updated" event.
     *
     * @param  \App\tipo_solicitudes  $tipoSolicitudes
     * @return void
     */
    public function updating(tipo_solicitudes $tipoSolicitudes)
    {
        //
    }

    /**
     * Handle the tipo_solicitudes "deleted" event.
     *
     * @param  \App\tipo_solicitudes  $tipoSolicitudes
     * @return void
     */
    public function deleted(tipo_solicitudes $tipoSolicitudes)
    {
        //
    }

    /**
     * Handle the tipo_solicitudes "restored" event.
     *
     * @param  \App\tipo_solicitudes  $tipoSolicitudes
     * @return void
     */
    public function restored(tipo_solicitudes $tipoSolicitudes)
    {
        //
    }

    /**
     * Handle the tipo_solicitudes "force deleted" event.
     *
     * @param  \App\tipo_solicitudes  $tipoSolicitudes
     * @return void
     */
    public function forceDeleted(tipo_solicitudes $tipoSolicitudes)
    {
        //
    }
}

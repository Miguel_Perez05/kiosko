<?php

namespace App\Observers;

use App\empresas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class EmpresasObserver
{
    /**
     * Handle the empresas "created" event.
     *
     * @param  \App\Empresas  $empresas
     * @return void
     */
    public function creating(empresas $empresas)
    {
        $empresas->Id_Estatus=1;
        $empresas->Empresa_Padre=1;
        $empresas->Uid_Empresa = Uuid::generate()->string;
        $empresas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $empresas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the empresas "updated" event.
     *
     * @param  \App\Empresas  $empresas
     * @return void
     */
    public function updating(empresas $empresas)
    {
        $empresas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the empresas "deleted" event.
     *
     * @param  \App\Empresas  $empresas
     * @return void
     */
    public function deleted(empresas $empresas)
    {
        //
    }

    /**
     * Handle the empresas "restored" event.
     *
     * @param  \App\Empresas  $empresas
     * @return void
     */
    public function restored(empresas $empresas)
    {
        //
    }

    /**
     * Handle the empresas "force deleted" event.
     *
     * @param  \App\Empresas  $empresas
     * @return void
     */
    public function forceDeleted(empresas $empresas)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\ciudades;
use Uuid;
use Illuminate\Support\Facades\Auth;

class CiudadesObserver
{
    /**
     * Handle the ciudades "created" event.
     *
     * @param  \App\ciudades  $ciudades
     * @return void
     */
    public function creating(ciudades $ciudades)
    {
        $ciudades->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $ciudades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $ciudades->Id_Estatus=1;
    }

    /**
     * Handle the ciudades "updated" event.
     *
     * @param  \App\ciudades  $ciudades
     * @return void
     */
    public function updating(ciudades $ciudades)
    {
        $ciudades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the ciudades "deleted" event.
     *
     * @param  \App\ciudades  $ciudades
     * @return void
     */
    public function deleted(ciudades $ciudades)
    {
        //
    }

    /**
     * Handle the ciudades "restored" event.
     *
     * @param  \App\ciudades  $ciudades
     * @return void
     */
    public function restored(ciudades $ciudades)
    {
        //
    }

    /**
     * Handle the ciudades "force deleted" event.
     *
     * @param  \App\ciudades  $ciudades
     * @return void
     */
    public function forceDeleted(ciudades $ciudades)
    {
        //
    }
}

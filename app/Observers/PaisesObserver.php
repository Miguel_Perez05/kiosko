<?php

namespace App\Observers;

use App\paises;
use Uuid;
use Illuminate\Support\Facades\Auth;

class PaisesObserver
{
    /**
     * Handle the paises "created" event.
     *
     * @param  \App\paises  $paises
     * @return void
     */
    public function creating(paises $paises)
    {
        $paises->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $paises->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $paises->Id_Estatus=1;
    }

    /**
     * Handle the paises "updated" event.
     *
     * @param  \App\paises  $paises
     * @return void
     */
    public function updating(paises $paises)
    {
        $paises->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the paises "deleted" event.
     *
     * @param  \App\paises  $paises
     * @return void
     */
    public function deleted(paises $paises)
    {
        //
    }

    /**
     * Handle the paises "restored" event.
     *
     * @param  \App\paises  $paises
     * @return void
     */
    public function restored(paises $paises)
    {
        //
    }

    /**
     * Handle the paises "force deleted" event.
     *
     * @param  \App\paises  $paises
     * @return void
     */
    public function forceDeleted(paises $paises)
    {
        //
    }
}

<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\registros_patronales;
use Illuminate\Support\Facades\Auth;

class Registros_PatronalesObserver
{
    /**
     * Handle the registros_patronales "created" event.
     *
     * @param  \App\registros_patronales  $registrosPatronales
     * @return void
     */
    public function creating(registros_patronales $registrosPatronales)
    {
        $registrosPatronales->Id_Estatus=1;
        $registrosPatronales->Uid_RegistroPatronal = Uuid::generate()->string;
        $registrosPatronales->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $registrosPatronales->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the registros_patronales "updated" event.
     *
     * @param  \App\registros_patronales  $registrosPatronales
     * @return void
     */
    public function updating(registros_patronales $registrosPatronales)
    {
        $registrosPatronales->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the registros_patronales "deleted" event.
     *
     * @param  \App\registros_patronales  $registrosPatronales
     * @return void
     */
    public function deleted(registros_patronales $registrosPatronales)
    {
        //
    }

    /**
     * Handle the registros_patronales "restored" event.
     *
     * @param  \App\registros_patronales  $registrosPatronales
     * @return void
     */
    public function restored(registros_patronales $registrosPatronales)
    {
        //
    }

    /**
     * Handle the registros_patronales "force deleted" event.
     *
     * @param  \App\registros_patronales  $registrosPatronales
     * @return void
     */
    public function forceDeleted(registros_patronales $registrosPatronales)
    {
        //
    }
}

<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\percepciones_deducciones;
use Illuminate\Support\Facades\Auth;

class Percepciones_DeduccionesObserver
{
    /**
     * Handle the percepciones_deducciones "created" event.
     *
     * @param  \App\percepciones_deducciones  $percepcionesDeducciones
     * @return void
     */
    public function creating(percepciones_deducciones $percepcionesDeducciones)
    {
        $percepcionesDeducciones->Uid_PercepcionDeduccion = Uuid::generate()->string;
        $percepcionesDeducciones->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $percepcionesDeducciones->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $percepcionesDeducciones->Id_Estatus=1;
    }

    /**
     * Handle the percepciones_deducciones "updated" event.
     *
     * @param  \App\percepciones_deducciones  $percepcionesDeducciones
     * @return void
     */
    public function updating(percepciones_deducciones $percepcionesDeducciones)
    {
        $percepcionesDeducciones->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the percepciones_deducciones "deleted" event.
     *
     * @param  \App\percepciones_deducciones  $percepcionesDeducciones
     * @return void
     */
    public function deleted(percepciones_deducciones $percepcionesDeducciones)
    {
        //
    }

    /**
     * Handle the percepciones_deducciones "restored" event.
     *
     * @param  \App\percepciones_deducciones  $percepcionesDeducciones
     * @return void
     */
    public function restored(percepciones_deducciones $percepcionesDeducciones)
    {
        //
    }

    /**
     * Handle the percepciones_deducciones "force deleted" event.
     *
     * @param  \App\percepciones_deducciones  $percepcionesDeducciones
     * @return void
     */
    public function forceDeleted(percepciones_deducciones $percepcionesDeducciones)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\tipo_empleados;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class Tipo_EmpleadosObserver
{
    /**
     * Handle the tipo_empleados "created" event.
     *
     * @param  \App\tipo_empleados  $tipoEmpleados
     * @return void
     */
    public function creating(tipo_empleados $tipoEmpleados)
    {
        $tipoEmpleados->Uid_TipoEmpleado = Uuid::generate()->string;
        $tipoEmpleados->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $tipoEmpleados->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $tipoEmpleados->Id_Estatus=1;
    }

    /**
     * Handle the tipo_empleados "updated" event.
     *
     * @param  \App\tipo_empleados  $tipoEmpleados
     * @return void
     */
    public function updating(tipo_empleados $tipoEmpleados)
    {
        $tipoEmpleados->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_empleados "deleted" event.
     *
     * @param  \App\tipo_empleados  $tipoEmpleados
     * @return void
     */
    public function deleted(tipo_empleados $tipoEmpleados)
    {
        //
    }

    /**
     * Handle the tipo_empleados "restored" event.
     *
     * @param  \App\tipo_empleados  $tipoEmpleados
     * @return void
     */
    public function restored(tipo_empleados $tipoEmpleados)
    {
        //
    }

    /**
     * Handle the tipo_empleados "force deleted" event.
     *
     * @param  \App\tipo_empleados  $tipoEmpleados
     * @return void
     */
    public function forceDeleted(tipo_empleados $tipoEmpleados)
    {
        //
    }
}

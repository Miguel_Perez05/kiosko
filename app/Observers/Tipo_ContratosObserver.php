<?php

namespace App\Observers;

use App\tipo_contratos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class Tipo_ContratosObserver
{
    /**
     * Handle the tipo_contratos "created" event.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return void
     */
    public function creating(tipo_contratos $tipo_contratos)
    {
        $tipo_contratos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $tipo_contratos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_contratos "updated" event.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return void
     */
    public function updating(tipo_contratos $tipo_contratos)
    {
        $tipo_contratos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the tipo_contratos "deleted" event.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return void
     */
    public function deleted(tipo_contratos $tipo_contratos)
    {
        //
    }

    /**
     * Handle the tipo_contratos "restored" event.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return void
     */
    public function restored(tipo_contratos $tipo_contratos)
    {
        //
    }

    /**
     * Handle the tipo_contratos "force deleted" event.
     *
     * @param  \App\tipo_contratos  $tipo_contratos
     * @return void
     */
    public function forceDeleted(tipo_contratos $tipo_contratos)
    {
        //
    }
}

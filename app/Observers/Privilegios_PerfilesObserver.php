<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\privilegios_perfiles;
use Illuminate\Support\Facades\Auth;

class Privilegios_PerfilesObserver
{
    /**
     * Handle the privilegios_perfiles "created" event.
     *
     * @param  \App\privilegios_perfiles  $privilegiosPerfiles
     * @return void
     */
    public function creating(privilegios_perfiles $privilegiosPerfiles)
    {
        $privilegiosPerfiles->Uid_Privilegio = Uuid::generate()->string;
        $privilegiosPerfiles->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $privilegiosPerfiles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the privilegios_perfiles "updated" event.
     *
     * @param  \App\privilegios_perfiles  $privilegiosPerfiles
     * @return void
     */
    public function updating(privilegios_perfiles $privilegiosPerfiles)
    {
        $privilegiosPerfiles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the privilegios_perfiles "deleted" event.
     *
     * @param  \App\privilegios_perfiles  $privilegiosPerfiles
     * @return void
     */
    public function deleted(privilegios_perfiles $privilegiosPerfiles)
    {
        //
    }

    /**
     * Handle the privilegios_perfiles "restored" event.
     *
     * @param  \App\privilegios_perfiles  $privilegiosPerfiles
     * @return void
     */
    public function restored(privilegios_perfiles $privilegiosPerfiles)
    {
        //
    }

    /**
     * Handle the privilegios_perfiles "force deleted" event.
     *
     * @param  \App\privilegios_perfiles  $privilegiosPerfiles
     * @return void
     */
    public function forceDeleted(privilegios_perfiles $privilegiosPerfiles)
    {
        //
    }
}

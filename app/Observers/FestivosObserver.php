<?php

namespace App\Observers;
use App\festivos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class FestivosObserver
{
    public function creating(festivos $festivo)
    {
        $festivo->Uid_Festivo = Uuid::generate()->string;
        $festivo->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $festivo->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $festivo->Id_Estatus=1;
    }

    public function updating(festivos $festivo)
    {
        $festivo->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

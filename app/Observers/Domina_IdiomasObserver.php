<?php

namespace App\Observers;

use App\domina_idiomas;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class Domina_IdiomasObserver
{
    /**
     * Handle the domina_idiomas "created" event.
     *
     * @param  \App\domina_idiomas  $dominaIdiomas
     * @return void
     */
    public function creating(domina_idiomas $dominaIdiomas)
    {
        $dominaIdiomas->Uid_Dominio = Uuid::generate()->string;
        $dominaIdiomas->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $dominaIdiomas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $dominaIdiomas->Id_Estatus = 1;
    }

    /**
     * Handle the domina_idiomas "updated" event.
     *
     * @param  \App\domina_idiomas  $dominaIdiomas
     * @return void
     */
    public function updating(domina_idiomas $dominaIdiomas)
    {
        $dominaIdiomas->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the domina_idiomas "deleted" event.
     *
     * @param  \App\domina_idiomas  $dominaIdiomas
     * @return void
     */
    public function deleted(domina_idiomas $dominaIdiomas)
    {
        //
    }

    /**
     * Handle the domina_idiomas "restored" event.
     *
     * @param  \App\domina_idiomas  $dominaIdiomas
     * @return void
     */
    public function restored(domina_idiomas $dominaIdiomas)
    {
        //
    }

    /**
     * Handle the domina_idiomas "force deleted" event.
     *
     * @param  \App\domina_idiomas  $dominaIdiomas
     * @return void
     */
    public function forceDeleted(domina_idiomas $dominaIdiomas)
    {
        //
    }
}

<?php

namespace App\Observers;

use App\nacionalidades;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class NacionalidadesObserver
{
    public function creating(nacionalidades $nacionalidades)
    {
        $nacionalidades->Uid_Nacionalidad = Uuid::generate()->string;
        $nacionalidades->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $nacionalidades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $nacionalidades->Id_Estatus=1;
    }

    /**
     * Handle the kardex "updated" event.
     *
     * @param  \App\kardex  $kardex
     * @return void
     */
    public function updating(nacionalidades $nacionalidades)
    {
        $nacionalidades->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

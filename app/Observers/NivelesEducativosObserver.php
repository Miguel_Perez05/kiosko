<?php

namespace App\Observers;

use Webpatser\Uuid\Uuid;
use App\niveles_educativos;
use Illuminate\Support\Facades\Auth;

class NivelesEducativosObserver
{
    /**
     * Handle the niveles_educativos "created" event.
     *
     * @param  \App\niveles_educativos  $nivelesEducativos
     * @return void
     */
    public function creating(niveles_educativos $nivelesEducativos)
    {
        $nivelesEducativos->Uid_NivelEducativo = Uuid::generate()->string;
        $nivelesEducativos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $nivelesEducativos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $nivelesEducativos->Id_Estatus = 1;
    }

    /**
     * Handle the niveles_educativos "updated" event.
     *
     * @param  \App\niveles_educativos  $nivelesEducativos
     * @return void
     */
    public function updating(niveles_educativos $nivelesEducativos)
    {
        $nivelesEducativos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the niveles_educativos "deleted" event.
     *
     * @param  \App\niveles_educativos  $nivelesEducativos
     * @return void
     */
    public function deleted(niveles_educativos $nivelesEducativos)
    {
        //
    }

    /**
     * Handle the niveles_educativos "restored" event.
     *
     * @param  \App\niveles_educativos  $nivelesEducativos
     * @return void
     */
    public function restored(niveles_educativos $nivelesEducativos)
    {
        //
    }

    /**
     * Handle the niveles_educativos "force deleted" event.
     *
     * @param  \App\niveles_educativos  $nivelesEducativos
     * @return void
     */
    public function forceDeleted(niveles_educativos $nivelesEducativos)
    {
        //
    }
}

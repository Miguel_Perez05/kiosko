<?php

namespace App\Observers;

use App\contratos;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class ContratosObserver
{
    /**
     * Handle the contratos "created" event.
     *
     * @param  \App\contratos  $contratos
     * @return void
     */
    public function creating(contratos $contratos)
    {
        $contratos->Uid_Contrato= Uuid::generate()->string;
        $contratos->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $contratos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
        $contratos->Id_Estatus=1;
    }

    /**
     * Handle the contratos "updated" event.
     *
     * @param  \App\contratos  $contratos
     * @return void
     */
    public function updating(contratos $contratos)
    {
        $contratos->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the contratos "deleted" event.
     *
     * @param  \App\contratos  $contratos
     * @return void
     */
    public function deleted(contratos $contratos)
    {
        //
    }

    /**
     * Handle the contratos "restored" event.
     *
     * @param  \App\contratos  $contratos
     * @return void
     */
    public function restored(contratos $contratos)
    {
        //
    }

    /**
     * Handle the contratos "force deleted" event.
     *
     * @param  \App\contratos  $contratos
     * @return void
     */
    public function forceDeleted(contratos $contratos)
    {
        //
    }
}

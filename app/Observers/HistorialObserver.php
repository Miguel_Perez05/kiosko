<?php

namespace App\Observers;
use App\historial;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class HistorialObserver
{
    public function creating(historial $historial)
    {
        $historial->Uid_Historial = Uuid::generate()->string;
        $historial->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $historial->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(historial $historial)
    {
        $historial->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

<?php

namespace App\Observers;
use App\solicitudes;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class SolicitudesObserver
{
    public function creating(solicitudes $solicitudes)
    {
        $solicitudes->Solicitud_Status='Pendiente';
        $solicitudes->Uid_Solicitud = Uuid::generate()->string;
        $solicitudes->Uid_Empleado_Emisor = Auth::user()->Uid_Empleado;
        $solicitudes->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $solicitudes->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    public function updating(solicitudes $solicitudes)
    {
        $solicitudes->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }
}

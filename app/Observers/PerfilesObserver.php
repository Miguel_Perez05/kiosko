<?php

namespace App\Observers;

use App\perfiles;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;

class PerfilesObserver
{
    /**
     * Handle the perfiles "created" event.
     *
     * @param  \App\perfiles  $perfiles
     * @return void
     */
    public function creating(perfiles $perfiles)
    {
        $perfiles->Id_Estatus=1;        
        $perfiles->Uid_Usuario_Crea = Auth::user()->Uid_Usuario;
        $perfiles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the perfiles "updated" event.
     *
     * @param  \App\perfiles  $perfiles
     * @return void
     */
    public function updating(perfiles $perfiles)
    {
        $perfiles->Uid_Usuario_Edita = Auth::user()->Uid_Usuario;
    }

    /**
     * Handle the perfiles "deleted" event.
     *
     * @param  \App\perfiles  $perfiles
     * @return void
     */
    public function deleted(perfiles $perfiles)
    {
        //
    }

    /**
     * Handle the perfiles "restored" event.
     *
     * @param  \App\perfiles  $perfiles
     * @return void
     */
    public function restored(perfiles $perfiles)
    {
        //
    }

    /**
     * Handle the perfiles "force deleted" event.
     *
     * @param  \App\perfiles  $perfiles
     * @return void
     */
    public function forceDeleted(perfiles $perfiles)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_solicitudes extends Model
{
    protected $primaryKey = 'Uid_TipoSolicitud';
    public $incrementing = false;
    protected $fillable = [
        'Uid_TipoSolicitud',
        'TipoSolicitud_Nombre',
        'Id_TipoIncidencia',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instructores extends Model
{
    protected $primaryKey = 'Uid_Instructor';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Instructor',
        'Instructor_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

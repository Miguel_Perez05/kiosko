<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class promociones_internas extends Model
{
    protected $primaryKey = 'Uid_PromocionInterna';
    public $incrementing = false;
    protected $fillable = [
        'Uid_PromocionInterna',
        'Uid_Empleado',
        'Uid_Puesto',
        'PromocionInterna_Fecha',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

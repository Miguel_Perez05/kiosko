<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ciudades extends Model
{
    protected $primaryKey = 'Id_Ciudad';
    protected $fillable = [
        'Ciudad_Nombre',
        'Id_Estado',
        'Id_Pais',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];

    public static function allCiudades()
    {
        return static::Join('Estados','Ciudades.Id_Estado','=','Estados.Id_Estado')
        ->Join('Paises','Estados.Id_Pais','=','Paises.Id_Pais');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class percepciones_deducciones extends Model
{
    protected $primaryKey = 'Uid_PercepcionDeduccion';
    public $incrementing = false;
    protected $fillable = [
        'Uid_PercepcionDeduccion',
        'Id_TipoIncidencia',
        'PercepcionDeduccion_Monto',
        'PercepcionDeduccion_Fecha',
        'PercepcionDeduccion_Comentario',
        'Uid_Empleado',
        'Uid_TipoBono',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

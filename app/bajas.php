<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bajas extends Model
{
    protected $primaryKey = 'Uid_Baja';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Baja',
        'Uid_Empleado',
        'Uid_Historial',
        'Baja_Fecha',
        'Uid_Causa',
        'Baja_Comentario',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

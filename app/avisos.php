<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class avisos extends Model
{
    protected $primaryKey = 'Uid_Aviso';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Aviso',
        'Uid_Empleado_Emisor',
        'Uid_Empleado_Receptor',
        'Uid_Grupo',
        'Uid_Departamento',
        'Uid_Turno',
        'Aviso_Titulo',
        'Aviso_Asunto',
        'Aviso_Prioridad',
        'Aviso_Tipo',
        'Aviso_Inicio',
        'Aviso_Vigencia',
        'Aviso_Visto',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];

    public static function AvisosEmpresa($empleado)
    {
        return static::from('Avisos as a')
	    ->leftjoin('Empleados as e', 'a.Uid_Empleado_Receptor', '=','e.Uid_Empleado')-> where('a.Uid_Empleado_Receptor', '=',$empleado->Uid_Empleado)
		->leftjoin('Grupos as g', 'a.Uid_Grupo', '=', 'g.Uid_Grupo')-> where('a.Uid_Grupo', '=',$empleado->Uid_Grupo)
		->leftjoin('Departamentos as d', 'a.Uid_Departamento', '=', 'd.Uid_Departamento')-> where('a.Uid_Departamento', '=',$empleado->Uid_Departamento)
		->leftjoin('Turnos as t', 'a.Uid_Turno', '=', 't.Uid_Turno')-> where('a.Uid_Turno', '=',$empleado->Uid_Turno)
        ->select('Aviso_Titulo', 'Aviso_Asunto', 'Aviso_Asunto', 'Aviso_Vigencia',
        DB::raw('(case when a.Aviso_Tipo is not null then a.Aviso_Tipo when a.Uid_Empleado_Receptor is not null then "Personal" when a.Uid_Grupo is not null then g.Grupo_Nombre when a.Uid_Departamento is not null then d.Departamento_Nombre when a.Uid_Turno is not null then t.Turno_Nombre end) as receptor'))
	    ->orderBy('receptor','asc','aviso_Vigencia');
    }
}

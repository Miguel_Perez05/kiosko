<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cursos extends Model
{
    protected $primaryKey = 'Uid_Curso';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Curso',
        'Curso_Nombre',
        'Curso_Porcentaje',
        'Curso_Proposito',
        'Curso_Origen',
        'Uid_Instructor',
        'Uid_AreaFormacion',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

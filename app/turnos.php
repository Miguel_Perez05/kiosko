<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class turnos extends Model
{
    protected $primaryKey = 'Uid_Turno';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Turno',
        'Turno_Nombre',
        'Turno_Hora_Inicio',
        'Turno_Hora_Fin',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Empresa_Logo',
        'Id_Estatus'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reconocimientos extends Model
{
    protected $primaryKey = 'Uid_Reconocimiento';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Reconocimiento',
        'Reconocimiento_Titulo',
        'Reconocimiento_Image',
        'Uid_Empleado',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

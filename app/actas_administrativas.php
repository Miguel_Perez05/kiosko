<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actas_administrativas extends Model
{
    protected $primaryKey = 'Uid_ActaAdministrativa';

    public function usuarios(){
        return $this->belongsTo('App\Usuarios','Uid_Usuario_Crea','Uid_Usuario');
     }
}

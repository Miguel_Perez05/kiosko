<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class domina_idiomas extends Model
{
    protected $primaryKey = 'Uid_Dominio';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Dominio',
        'Uid_Empleado',
        'Uid_Idioma',
        'DominaIdioma_PorcentajeEscritura',
        'DominaIdioma_PorcentajeConversacion',
        'DominaIdioma_Fecha',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

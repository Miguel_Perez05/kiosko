<?php

namespace App;

use Carbon\Carbon;
//use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class Auth extends Model
{
    use RedirectsUsers, ThrottlesLogins;


  public function showLoginForm()
  {
        $date = Carbon::now()->locale('es');
        $fecha = \Carbon\Carbon::parse($date);
        $mes = $fecha->formatLocalized('%B');
        $mes='/img/login/'.$mes.'.jpg';
        return view('login',compact('mes'));
  }

  /**
   * Handle a login request to the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
   */
  public function login(Request $request)
  {
      $this->validateLogin($request);

      // If the class is using the ThrottlesLogins trait, we can automatically throttle
      // the login attempts for this application. We'll key this by the username and
      // the IP address of the client making these requests into this application.
      if ($this->hasTooManyLoginAttempts($request)) {
          $this->fireLockoutEvent($request);

          return $this->sendLockoutResponse($request);
      }

      if ($this->attemptLogin($request)) {
          return $this->sendLoginResponse($request);
      }

      // If the login attempt was unsuccessful we will increment the number of attempts
      // to login and redirect the user back to the login form. Of course, when this
      // user surpasses their maximum number of attempts they will get locked out.
      $this->incrementLoginAttempts($request);

      return $this->sendFailedLoginResponse($request);
  }

  /**
   * Validate the user login request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return void
   */
  protected function validateLogin(Request $request)
  {
      $this->validate($request, [
          $this->username() => 'required|string',
          'Usuario_Password' => 'required|string',
      ]);
  }

  /**
   * Attempt to log the user into the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return bool
   */
  protected function attemptLogin(Request $request)
  {
      return $this->guard()->attempt(
          $this->credentials($request), $request->has('remember')
      );
  }

  /**
   * Get the needed authorization credentials from the request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  protected function credentials(Request $request)
  {
      return $request->only($this->username(), 'Usuario_Password');
  }

  /**
   * Send the response after the user was authenticated.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  protected function sendLoginResponse(Request $request)
  {
      $request->session()->regenerate();

      $this->clearLoginAttempts($request);

      return $this->authenticated($request, $this->guard()->user())
              ?: redirect()->intended($this->redirectPath());
  }

  /**
   * The user has been authenticated.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  mixed  $user
   * @return mixed
   */
  protected function authenticated(Request $request, $user)
  {
      //
  }

  /**
   * Get the failed login response instance.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\RedirectResponse
   */
  protected function sendFailedLoginResponse(Request $request)
  {
      $errors = [$this->username() => trans('auth.failed')];

      if ($request->expectsJson()) {
          return response()->json($errors, 422);
      }

      return redirect()->back()
          ->withInput($request->only($this->username(), 'remember'))
          ->withErrors($errors);
  }

  /**
   * Get the login username to be used by the controller.
   *
   * @return string
   */
  public function username()
  {
      return 'Usuario_NickName';
  }

  public function getAuthPassword()
    {
        return $this->Usuario_Password;
    }



    public function setPasswordAttribute($value)
    {
        $this->attributes['Usuario_Password'] = bcrypt($value);
    }


    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'Usuario_Password');
    }
  /**
   * Log the user out of the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function logout(Request $request)
  {
      $this->guard()->logout();

      $request->session()->invalidate();

      return redirect('/');
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
      return Auth::guard();
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catalogos extends Model
{
    protected $primaryKey = 'CatalogUid';
    public $incrementing = false;
    protected $fillable = [
        'CatalogUid',
        'CatalogCode',
        'DisplayOrder',
        'OptionCode',
        'DisplayText',
        'Status',
        'Parameters',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

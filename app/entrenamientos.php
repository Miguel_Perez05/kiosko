<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class entrenamientos extends Model
{
    protected $primaryKey = 'Uid_Entrenamiento';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Entrenamiento',
        'Uid_Empleado',
        'Uid_Curso',
        'Entrenamiento_Porcentaje',
        'Entrenamiento_Calificacion',
        'Entrenamiento_Fecha',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

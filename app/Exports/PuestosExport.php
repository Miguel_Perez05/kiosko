<?php

namespace App\Exports;

use App\puestos;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class PuestosExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return puestos::leftJoin('departamentos','puestos.Uid_Departamento','departamentos.Uid_Departamento')
                        ->leftJoin('empleados','puestos.Uid_Puesto','empleados.Uid_Puesto')
                        ->leftJoin('turnos','empleados.Uid_Turno','turnos.Uid_Turno')
                        ->leftJoin('niveles_educativos','puestos.Uid_NivelEducativo','niveles_educativos.Uid_NivelEducativo')
                        ->leftJoin('areas_formacion','puestos.Uid_AreaFormacion','areas_formacion.Uid_AreaFormacion')
                        ->select('puestos.Puesto_Nombre', DB::raw("(CASE WHEN puestos.Id_Estatus = 2 THEN 'Vacante' Else
                        CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APAterno,' ',empleados.Empleado_AMaterno) end) As Estado"),
                        'puestos.Puesto_Sueldo_Min','puestos.Puesto_Sueldo_Max','niveles_educativos.NivelEducativo_Nombre','areas_formacion.AreaFormacion_Nombre',
                        'departamentos.Departamento_Nombre','turnos.Turno_Nombre')->get();

    }

    public function headings(): array
    {
        return [
            'Puesto',
            'Responsable',
            'Sueldo Min',
            'Sueldo Max',
            'Nivel Estudios',
            'Area Formación',
            'Departamento',
            'Turno',
        ];
    }
}

<?php

namespace App\Exports;

use App\departamentos;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class DepartamentosExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return departamentos::leftJoin('empleados','Departamento_Jefe','empleados.Uid_Empleado')
        ->select('Departamento_Nombre','Departamento_Descripcion',
        DB::raw("CONCAT(empleados.Empleado_Nombre,' ',empleados.Empleado_APAterno,' ',empleados.Empleado_AMaterno)"))->get();
    }

    public function headings(): array
    {
        return [
            'Nombre',
            'Descripción',
            'Jefe',
        ];
    }
}

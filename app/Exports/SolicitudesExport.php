<?php

namespace App\Exports;

use App\solicitudes;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class SolicitudesExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return solicitudes::Join('tipo_solicitudes','solicitudes.Uid_TipoSolicitud','tipo_solicitudes.Uid_TipoSolicitud')
                    ->Join('empleados as e','solicitudes.Uid_Empleado_Emisor','e.Uid_Empleado')
                    ->leftJoin('empleados as r','solicitudes.Uid_Empleado_Receptor','r.Uid_Empleado')
                    ->select('TipoSolicitud_Nombre', DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APAterno,' ',e.Empleado_AMaterno) as Emisor"),
                    DB::raw("CONCAT(r.Empleado_Nombre,' ',r.Empleado_APAterno,' ',r.Empleado_AMaterno) As Receptor"),
                    'solicitudes.created_at', 'Solicitud_Status','Solicitud_Inicio_Auscencia','Solicitud_Fin_Auscencia',
                    'Solicitud_Motivo','Solicitud_Observaciones')->get();
    }

    public function headings(): array
    {
        return [
            'Tipo Solicitud',
            'Emisor',
            'Receptor',
            'Fecha Emision',
            'Estatus',
            'Fecha Inicio',
            'Fecha Fin',
            'Motivo',
            'Observaciones',
        ];
    }
}

<?php

namespace App\Exports;

use App\empleados;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class EmpleadosExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return empleados::Join('puestos','empleados.Uid_Puesto','puestos.Uid_Puesto')
            ->Join('departamentos','empleados.Uid_Departamento','departamentos.Uid_Departamento')
            ->Join('turnos','empleados.Uid_Turno','turnos.Uid_Turno')
            ->where('empleados.Id_Estatus',1)
            ->select('empleados.Empleado_Nombre', 'empleados.Empleado_APaterno','empleados.Empleado_AMaterno',
                    'empleados.Empleado_Fecha_Nacimiento','empleados.Empleado_CURP','empleados.Empleado_RFC',
                    'puestos.Puesto_Nombre','departamentos.Departamento_Nombre', 'turnos.Turno_Nombre'
                    ,DB::raw("CONCAT(empleados.Empleado_Calle,' ',empleados.Empleado_Numero)"),'empleados.Empleado_SalarioMensual')->get();
    }

    public function headings(): array
    {
        return [
            'Nombres',
            'Apellido Paterno',
            'Apellido Materno',
            'Fecha Nacimiento',
            'CURP',
            'RFC',
            'Puesto',
            'Departamento',
            'Turno',
            'Domicilio',
            'Salario Mensual'
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class escuelas extends Model
{
    protected $primaryKey = 'Uid_Escuela';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Escuela',
        'Escuela_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

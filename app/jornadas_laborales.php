<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jornadas_laborales extends Model
{
    protected $primaryKey = 'Uid_Jornada';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Dia',
        'Uid_Turno',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

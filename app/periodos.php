<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class periodos extends Model
{
    protected $primaryKey = 'Uid_Periodo';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Periodo',
        'Periodo_Nombre',
        'Periodo_Estado',
        'Periodo_Inicial',
        'Periodo_Final',
        'Uid_Ejercicio',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

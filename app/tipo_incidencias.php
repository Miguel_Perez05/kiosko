<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_incidencias extends Model
{
    protected $primaryKey = 'Id_TipoIncidencia';

    protected $fillable = [
        'Id_TipoIncidencia',
        'TipoIncidencia_Nombre',
        'TipoIncidencia_DeduccionPercepcion',
        'TipoIncidencia_ManejaTiempo',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

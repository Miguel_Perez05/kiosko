<?php

namespace App;

use App\empresas;
use App\usuarios;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class empleados extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'Uid_Empleado';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Empleado',
        'Uid_Empresa',
        'Uid_RegistroPatronal',
        'Uid_Turno',
        'Uid_TipoEmpleado',
        'Uid_EstadoCivil',
        'Uid_Grupo',
        'Empleado_Nombre',
        'Empleado_APaterno',
        'Empleado_AMaterno',
        'Empleado_Telefono',
        'Empleado_Calle',
        'Empleado_Numero',
        'Empleado_Colonia',
        'Empleado_CP',
        'Empleado_Genero',
        'Empleado_Fecha_Nacimiento',
        'Empleado_Telefono1_Emergencia',
        'Empleado_Contacto1_Emergencia',
        'Empleado_Domicilio1_Emergencia',
        'Empleado_Telefono2_Emergencia',
        'Empleado_Contacto2_Emergencia',
        'Empleado_Domicilio2_Emergencia',
        'Empleado_Telefono3_Emergencia',
        'Empleado_Contacto3_Emergencia',
        'Empleado_Domicilio3_Emergencia',
        'Empleado_SalarioMensual',
        'Id_Pais',
        'Id_Estado',
        'Id_Ciudad',
        'Empleado_NSS',
        'Empleado_CURP',
        'Empleado_RFC',
        'Uid_Nacionalidad',
        'Id_Estatus',
        'Uid_Puesto',
        'Uid_Departamento',
        'Uid_Jefe_Inmediato',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Uid_TipoNomina'
    ];

    public function usuario(){
        return $this->hasOne(usuarios::class);
    }

    public function empresa(){
        return $this->belongsTo(empresas::class,'Uid_Empresa','Uid_Empresa');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class puestos extends Model
{
    protected $primaryKey = 'Uid_Puesto';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Puesto',
        'Puesto_Nombre',
        'Puesto_Descripcion',
        'Puesto_Sueldo_Min',
        'Puesto_Sueldo_Max',
        'Uid_Responsable',
        'Uid_NivelEducativo',
        'Uid_AreaFormacion',
        'Uid_Departamento',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',        
        'Empresa_Logo',
        'Id_Estatus'
    ];
}

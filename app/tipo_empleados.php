<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_empleados extends Model
{
    protected $primaryKey = 'Uid_TipoEmpleado';
    public $incrementing = false;
    protected $fillable = [
        'Uid_TipoEmpleado',
        'TipoEmpleado_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

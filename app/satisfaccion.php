<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class satisfaccion extends Model
{
    protected $table='satisfaccion';
    protected $primaryKey = 'Uid_Satisfaccion';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Satisfaccion',
        'Uid_Empleado',
        'Uid_Expectativa',
        'Satisfaccion_Valor',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expectativas extends Model
{
    protected $primaryKey = 'Uid_Expectativa';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Expectativa',
        'Expectativa_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

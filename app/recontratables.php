<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recontratables extends Model
{
    protected $primaryKey = 'Uid_Recontratable';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Recontratable',
        'Recontratable_Estatus',
        'Recontratable_Causa',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'];
}

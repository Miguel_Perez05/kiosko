<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dias extends Model
{
    protected $primaryKey = 'Uid_Dia';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Dia',
        'Dia_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ejercicios extends Model
{
    protected $primaryKey = 'Uid_Ejercicio';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Ejercicio',
        'Ejercicio_Nombre',
        'Ejercicio_Estado',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Uid_Puesto',
        'Uid_Departamento'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class escolaridades extends Model
{
    protected $primaryKey = 'Uid_Escolaridad';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Escolaridad',
        'Uid_Empleado',
        'Uid_Escuela',
        'Uid_Carrera',
        'Escolaridad_AnoIngreso',
        'Escolaridad_AnoEgreso',
        'Escolaridad_Concluida',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

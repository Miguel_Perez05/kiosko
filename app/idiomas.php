<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class idiomas extends Model
{
    protected $primaryKey = 'Uid_Idioma';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Idioma',
        'Idioma_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

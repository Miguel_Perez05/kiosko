<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contratos extends Model
{
    protected $primaryKey = 'Uid_Contrato';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'Uid_Contrato',
        'Contrato_Folio',
        'Uid_Empleado',
        'Uid_Empresa',
        'Uid_Departamento',
        'Uid_Puesto',
        'Id_TipoContrato',
        'Contrato_Inicio',
        'Contrato_Fin',
        'Contrato_Salario',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
    ];
}

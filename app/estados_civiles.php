<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estados_civiles extends Model
{
    protected $primaryKey = 'Uid_EstadoCivil';
    public $incrementing = false;
    protected $fillable = [
        'Uid_EstadoCivil',
        'EstadoCivil_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

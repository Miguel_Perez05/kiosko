<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empresas extends Model
{
    protected $primaryKey = 'Uid_Empresa';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Empresa',
        'Empresa_Nombre',
        'Empresa_Razon_Social',
        'Empresa_RFC',
        'Empresa_Domicilio',
        'Empresa_Padre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Empresa_Logo',
        'Id_Estatus'
    ];
    public function empleado(){
        return $this->hasOne(empleados::class);
    }
}

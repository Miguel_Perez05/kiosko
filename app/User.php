<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $incrementing = false;
    protected $primaryKey = 'Uid_Usuario';
    protected $table = 'usuarios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Usuario_Nombre',
        'Usuario_APaterno',
        'Usuario_AMaterno',
        'Usuario_Nickname',
        'Usuario_Correo',
        'Id_Perfil',
        'Usuario_Avatar',
        'Id_Estatus',
        'Usuario_Activo',
        'Usuario_Nuevo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Usuario_Password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function getAuthPassword()
    // {
    //     return $this->Usuario_Password;
    // }
}

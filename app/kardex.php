<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kardex extends Model
{
    protected $primaryKey = 'Uid_Kardex';
    public $incrementing = false;
    protected  $table='kardex';
    protected $fillable = [
        'Uid_Kardex',
        'Kardex_Descripcion',
        'Kardex_Uid_Registro',
        'Uid_Usuario_Crea'
    ];
}

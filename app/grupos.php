<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grupos extends Model
{
    protected $primaryKey = 'Uid_Grupo';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Grupo',
        'Grupo_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

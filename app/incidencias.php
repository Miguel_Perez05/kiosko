<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class incidencias extends Model
{
    protected $primaryKey = 'Uid_Incidencia';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Incidencia',
        'Incidencia_Comentario',
        'Incidencia_Tipo',
        'Incidencia_DiaInicio',
        'Incidencia_DiaFin',
        'Uid_Empleado',
        'Incidencia_Minutos',
        'Incidencia_HoraEntrada',
        'Incidencia_Monto',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Incidencia_ComentarioJustifica',
        'Incidencia_FechaJustificacion',
        'Uid_UsuarioJustifica',
        'Id_Estatus'
    ];

    public static function IncidenciasEmpleados()
    {
        $fromDate=Carbon::now()->subDays(30);
        $toDate=Carbon::now();
        return static::Join('tipo_incidencias','Id_TipoIncidencia','Incidencia_Tipo')
        ->where('Incidencia_DiaInicio','<=',$toDate)->where('Incidencia_DiaFin','>=',$fromDate)
        ->Select(DB::raw('DATE_FORMAT(Incidencia_DiaInicio, "%d %b") as Incidencia_DiaInicio'),DB::raw('DATE_FORMAT(Incidencia_DiaFin, "%d %b") as Incidencia_DiaFin'),
        'Incidencia_Minutos','TipoIncidencia_Nombre', 'Incidencia_Comentario','Incidencia_FechaJustificacion','TipoIncidencia_ManejaTiempo');
    }

    public static function IncidenciasNominas()
    {
        return static::Join('empleados', 'incidencias.Uid_Empleado','empleados.Uid_Empleado')
        ->Join('tipo_incidencias','Id_TipoIncidencia','Incidencia_Tipo')
        ->Select('Uid_Incidencia','Incidencia_DiaInicio','Incidencia_DiaFin','Incidencia_Minutos','Incidencia_HoraEntrada','TipoIncidencia_Nombre','Empleado_Nombre',
        'Empleado_APaterno','Empleado_AMaterno','Incidencia_FechaJustificacion','incidencias.Id_Estatus');
    }

    public static function TotalDias($tipo)
    {
        $fromDate=Carbon::now()->subDays(30);
        $toDate=Carbon::now();
        return static::where('Incidencia_DiaInicio','<=',$toDate)
                        ->where('Incidencia_DiaFin','>=',$fromDate)
                        ->where('Incidencia_Tipo',$tipo)
                        ->whereNull('Incidencia_FechaJustificacion')
                        ->where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                        ->count();
    }

    public static function TotalDiasVacaciones($tipo)
    {
        $fromDate='';
        $toDate=Carbon::now();
        $ingreso= historial::where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                            ->whereRaw('historial.updated_at = (select max(`updated_at`) from historial where historial.Uid_Empleado=Uid_Empleado)')->first();
        if($ingreso->Historial_Ingreso)
            $fromDate=$ingreso->Historial_Ingreso;
        else
            $fromDate=$ingreso->Historial_Reingreso;
        return static::where('Incidencia_DiaInicio','<=',$toDate)
                        ->where('Incidencia_DiaFin','>=',$fromDate)
                        ->where('Incidencia_Tipo',$tipo)
                        ->whereNull('Incidencia_FechaJustificacion')
                        ->where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                        ->count();
    }

    public static function TotalHoras($tipo)
    {
        $fromDate=Carbon::now()->subDays(30);
        $toDate=Carbon::now();
        return static::where('Incidencia_DiaInicio','<=',$toDate)
                        ->where('Incidencia_DiaFin','>=',$fromDate)
                        ->where('Incidencia_Tipo',$tipo)
                        ->whereNull('Incidencia_FechaJustificacion')
                        ->where('Uid_Empleado','=',Auth::user()->Uid_Empleado)
                        ->sum('Incidencia_Minutos');
    }
}

<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use App\solicitudes;
use App\departamentos;
use DB;

class User
{
    public static function get_username() {
        if(Auth::user())
        {
            return
            \App\usuarios::with('empleado')
            ->where('Uid_Usuario',Auth::user()->Uid_Usuario)
            ->first();
        }
        else
        {return null;}

    }

    public static function get_employee()
    {
        if(Auth::user())
        {
            return (User::get_username()->empleado ?
            \App\empleados::with('empresa')
            ->where('Uid_Empresa',User::get_username()
            ->empleado->Uid_Empresa)->first() : null);
        }
        else
        {return null;}
    }

    public static function get_perfil()
    {
        if(Auth::user())
        {
            if(Auth::user()->Id_Perfil==1)
                return 'true';
            if(User::get_username()->Uid_Puesto=='2b36b740-e3e7-11e9-aa8d-0f7f68ecb547')
                return 'true';
            return 'false';
        }
    }

    public static function get_jefe()
    {
        if(Auth::user())
        {
            $departamentos=departamentos::where('Departamento_Jefe',Auth::user()->Uid_Empleado)->first();
            if($departamentos)
                return 'true';
            return 'false';
        }
    }

    public static function get_solicitudes()
    {
        if(Auth::user())
        {
            return (User::get_username()->empleado ?
             solicitudes::from('solicitudes as s')
            ->join('empleados as e','s.Uid_Empleado_Emisor','e.Uid_Empleado')
            ->join('usuarios as u', 'e.Uid_Empleado', 'u.Uid_Empleado')
            ->join('departamentos as d','e.Uid_Departamento','d.Uid_Departamento')
            ->where('d.Departamento_Jefe',User::get_username()->Uid_Empleado)
            ->where('s.Solicitud_Status','Pendiente')
            ->select((DB::raw("CONCAT(e.Empleado_Nombre,' ',e.Empleado_APaterno,' ',e.Empleado_AMaterno) as Empleado_Nombres")),
            'u.Usuario_Avatar','s.Solicitud_Motivo', 's.created_at','s.Uid_Solicitud')
            ->get() : null );
        }
        else
        {return null;}

    }

    public static function envioSolicitudes($para,$empresa,$data,$plantilla,$asunto)
    {
        $from = env('MAIL_USERNAME');

        \Mail::send($plantilla, $data, function ($message) use ($from,$para,$empresa,$asunto )
        {
            $message->from($from, $empresa);
            $message->to($para)->subject($asunto);
        });
        return 1;
    }
}

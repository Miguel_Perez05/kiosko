<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class perfiles extends Model
{
    protected $primaryKey = 'Id_Perfil';
    public $incrementing = true;
    protected $fillable = [
        'Id_Perfil',
        'Perfil_Nombre',
        'Id_Estatus',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_contratos extends Model
{
    protected $primaryKey = 'Id_TipoContrato';
    public $incrementing = true;
    protected $fillable = [
        'Id_TipoContrato',
        'TipoContrato_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];
}

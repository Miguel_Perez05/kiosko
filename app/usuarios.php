<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use DB;
use App\empleados;

class usuarios extends Authenticatable
{
    public $incrementing = false;
    protected $primaryKey = 'Uid_Usuario';

    public function empleado()
    {
        return $this->hasOne(empleados::class,'Uid_Empleado','Uid_Empleado');
    }

    protected $fillable = [
        'Uid_Usuario',
        'Usuario_NickName',
        'Usuario_Correo',
        'Id_Perfil',
        'Usuario_Avatar',
        'Id_Estatus',
        'Usuario_Activo',
        'Usuario_Nuevo',
        'Usuario_Password',
        'Uid_Empleado'
    ];
}

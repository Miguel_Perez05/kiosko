<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class solicitudes extends Model
{
    protected $primaryKey = 'Uid_Solicitud';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Solicitud',
        'Uid_Empleado_Emisor',
        'Uid_Empleado_Receptor',
        'Uid_TipoSolicitud',
        'Solicitud_Status',
        'Solicitud_Inicio_Auscencia',
        'Solicitud_Fin_Auscencia',
        'Solicitud_Motivo',
        'Solicitud_Observaciones',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];

    public static function empleados()
    {
        return static::Join('tipo_solicitudes','solicitudes.Uid_TipoSolicitud','tipo_solicitudes.Uid_TipoSolicitud');
    }

    public static function nominas()
    {
        return static::LeftJoin('tipo_solicitudes','solicitudes.Uid_TipoSolicitud','tipo_solicitudes.Uid_TipoSolicitud')
                    ->LeftJoin('empleados','Uid_Empleado_Emisor','Uid_Empleado')
                    ->select('Empleado_Nombre','Empleado_APaterno','Empleado_AMaterno','Solicitud_Status',
                    'Solicitud_Inicio_Auscencia','Solicitud_Fin_Auscencia','TipoSolicitud_Nombre','Uid_Solicitud');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class areas_formacion extends Model
{
    protected $primaryKey = 'Uid_AreaFormacion';
    protected $table='areas_formacion';
    public $incrementing = false;
    protected $fillable = [
        'Uid_AreaFormacion',
        'AreaFormacion_Nombre',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita',
        'Id_Estatus'
    ];
}

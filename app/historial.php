<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class historial extends Model
{
    protected $primaryKey = 'Uid_Historial';
    protected $table='historial';
    public $incrementing = false;
    protected $fillable = [
        'Uid_Historial',
        'Uid_Empleado',
        'Uid_Puesto',
        'Uid_Departamento',
        'Uid_Turno',
        'Uid_TipoEmpleado',
        'Id_TipoContrato',
        'Uid_Empresa',
        'Uid_Recontratable',
        'Uid_PercepcionDeduccion',
        'Uid_ActaAdministrativa',
        'Uid_Sueldo',
        'Historial_Ingreso',
        'Historial_Egreso',
        'Historial_Reingreso',
        'Uid_Usuario_Crea',
        'Uid_Usuario_Edita'
    ];

    public static function HistorialNominas()
    {
        return static::Join('empleados', 'historial.Uid_Empleado','empleados.Uid_Empleado')
        ->Select('Uid_Historial','Historial_Ingreso','Historial_Egreso','Historial_Reingreso','Empleado_Nombre',
        'Empleado_APaterno','Empleado_AMaterno','Empleado_RFC','historial.Uid_Empleado','historial.Uid_Empresa')
        ->whereRaw('historial.updated_at = (select max(`updated_at`) from historial where historial.Uid_Empleado=empleados.Uid_Empleado)');
    }
}

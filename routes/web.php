<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

Route::get('update/employee','EmpleadosController@updateemployees');

Route::group(['middleware'=>['auth']],function ()
{
    Route::get('/', function () {
        return view('panel');
    });

    Route::resource('configuracion', 'ConfiguracionController');
    Route::resource('configuracionNominas', 'ConfiguracionNominasController');

    Route::resource('paises', 'PaisesController');
    Route::resource('estados', 'EstadosController');
    Route::resource('ciudades', 'CiudadesController');
    Route::resource('empresas', 'EmpresasController');
    Route::resource('registrospatronales', 'RegistrosPatronalesController');
    Route::resource('turnos', 'TurnosController');
    Route::resource('tipoempleados', 'TipoEmpleadosController');
    Route::resource('estadosciviles', 'EstadosCivilesController');
    Route::resource('grupos', 'GruposController');
    Route::resource('ejercicios', 'EjerciciosController');
    Route::resource('periodos', 'PeriodosController');
    Route::resource('puestos', 'PuestosController');
    Route::resource('departamentos', 'DepartamentosController');
    Route::resource('perfiles', 'PerfilesController');
    Route::resource('tipoincidencias', 'TipoIncidenciaController');
    Route::resource('tiposolicitudes', 'TipoSolicitudesController');
    Route::resource('tiponominas', 'TipoNominasController');
    Route::resource('nacionalidades', 'NacionalidadesController');
    Route::resource('escolaridades', 'EscolaridadesController');
    Route::resource('escuelas', 'EscuelasController');
    Route::resource('tiposbonos', 'TipoBonosController');
    Route::resource('promociones', 'PromocionesinternasController');
    Route::resource('contratos', 'ContratosController');
    Route::resource('tipocontratos', 'TipoContratosController');

    Route::post('sueldos/PU/{Uid}', 'SueldosController@storePU');
    Route::resource('sueldos', 'SueldosController');

    Route::post('percepcionesdeducciones/PU/{Uid}', 'PercepcionesDeduccionesController@storePU');
    Route::resource('percepcionesdeducciones', 'PercepcionesDeduccionesController');

    Route::get('employeescombo', 'EmpleadosController@showCombo');

    Route::resource('vacantes', 'VacantesController');
    Route::get('panelvacantes', 'VacantesController@panel');
    Route::get('estudio/{id}','VacantesController@estudio');
    Route::get('graficos','VacantesController@graficos');
    Route::get('vacantes/{uid}','VacantesController@asignacion');
    Route::put('asignacion/{uid}','VacantesController@asignacion');
    Route::get('assign','VacantesController@assign');

    Route::resource('carreras', 'CarrerasController');
    Route::resource('areasformacion', 'AreasFormacionController');
    Route::resource('niveleseducativos', 'NivelesEducativosController');

    Route::resource('cursos', 'CursosController');
    Route::resource('idiomas', 'IdiomasController');

    Route::resource('reconocimientos', 'ReconocimientosController');
    Route::post('reconocimientos/PU/{Uid}', 'ReconocimientosController@storePU');

    Route::post('dominaidiomas/PU/{Uid}', 'DominaIdiomasController@storePU');
    Route::resource('dominaidiomas', 'DominaIdiomasController');

    Route::post('entrenamientos/PU/{Uid}', 'EntrenamientosController@store');
    Route::resource('entrenamientos', 'EntrenamientosController');

    Route::resource('festivos', 'FestivosController');
    Route::get('diasfestivos', 'FestivosController@festivos');

    Route::resource('tramite', 'TramitesController');

    Route::resource('solicitudes', 'SolicitudesController');
    Route::post('solicitudes/estatus', 'SolicitudesController@estatus');
    Route::get('solicitudes/s/{tipo}', 'SolicitudesController@solicitudes');
    Route::get('tramites', 'SolicitudesController@indexTramites');
    Route::get('tramitesolicitado/{Uid}/{tipo}','SolicitudesController@CartaLaboralpdf');
    Route::get('ContratoShow/{Uid}','SolicitudesController@ContratoShow');
    Route::get('lista_estatus','SolicitudesController@lista_estatus');
    Route::get('solicitud/detalle/{Uid}','SolicitudesController@detalle_solicitud');


    Route::post('altasbajas/cambio', 'HistorialController@storedCambio');
    Route::get('altasbajas', 'HistorialController@altasbajas');
    Route::get('historialEmpleado', 'HistorialController@historialEmpleado');
    Route::resource('historial', 'HistorialController');

    Route::post('empleados/baja', 'BajasController@store');
    Route::resource('bajas', 'BajasController');


    Route::put('empleados/actualiza/{Uid}', 'EmpleadosController@update');
    Route::get('empleados/show/{Uid}/perfil', 'EmpleadosController@showperfil');
    Route::get('empleado/{Uid_Empleado}/usuario','EmpleadosController@getUsuario');
    Route::resource('empleados', 'EmpleadosController');
    Route::get('cumpleanos','EmpleadosController@cumpleanos');
    Route::get('listado','EmpleadosController@listado');

    Route::resource('usuarios', 'UsuariosController');
    Route::get('miperfil', 'UsuariosController@MiPerfil');
    Route::get('nuevopassword', 'UsuariosController@NewPassword');
    Route::post('updatepassword', 'UsuariosController@updatePassword');
    Route::put('updateemail/{UidU}/{UidE}', 'UsuariosController@updateEmail');
    Route::put('updateAvatar/{UidU}/{UidE}', 'UsuariosController@updateAvatar');

    Route::put('incidencia/{uid}/autorizacion', 'IncidenciasController@autorizacion');
    Route::resource('incidencias', 'IncidenciasController');
    Route::get('incidencias/empleados/{tipo}', 'IncidenciasController@empleados');
    Route::get('retardos', 'IncidenciasController@retardos');
    Route::get('vacaciones', 'IncidenciasController@vacaciones');
    Route::get('horaextra', 'IncidenciasController@horaextra');
    Route::post('incidencias/justificacion/{id}', 'IncidenciasController@justificacion');
    Route::get('incidencias/nominas/{id}', 'IncidenciasController@index');
    Route::get('incidencias/nominas/{id}/create', 'IncidenciasController@create');

    Route::get('avisos/form', 'AvisosController@form');
    Route::resource('avisos', 'AvisosController');
    Route::get('avisosview', 'AvisosController@view');
    Route::get('avisosempresa', 'AvisosController@avisos');

    Route::resource('satisfaccion', 'SatisfaccionController');
    Route::get('satisfaccion/grafico', 'SatisfaccionController@grafico');

    Route::get('reportes/empleados','ReportesController@empleados');
    Route::get('reportes/solicitudes','ReportesController@solicitudes');
    Route::get('reportes/puestos','ReportesController@puestos');

    Route::get('session','LoginController@verifica_session');
});
Route::get('login',[ 'as' => 'login', 'uses' =>'LoginController@login']);
Route::post('loginpost','LoginController@login_in');
Route::get('logout','LoginController@logout');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});


Route::get('/list-routes/{type?}', function($type=null) {
    if($type!=null)
        Artisan::call('route:list', ['--path' => $type]);
    else
        Artisan::call('route:list');
    $routes= Artisan::output();
    dd($routes);
});
